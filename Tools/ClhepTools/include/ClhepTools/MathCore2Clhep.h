/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <CLHEP/Geometry/Point3D.h>
#include <CLHEP/Geometry/Transform3D.h>
#include <CLHEP/Geometry/Vector3D.h>
#include <GaudiKernel/Point3DTypes.h>
#include <GaudiKernel/Transform3DTypes.h>
#include <GaudiKernel/Vector3DTypes.h>

/** @namespace math2clhep MathCore2Clhep.h ClhepTools/MathCore2Clhep.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2005-12-13
 */
namespace LHCb {

  namespace math2clhep {
    inline HepGeom::Transform3D transform3D( const Gaudi::Transform3D& tr ) {
      using namespace CLHEP;
      double xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz;

      tr.GetComponents( xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz );

      return HepGeom::Transform3D( HepRotation( HepRep3x3( xx, xy, xz, yx, yy, yz, zx, zy, zz ) ),
                                   Hep3Vector( dx, dy, dz ) );
    }

    template <class aVector>
    HepGeom::Vector3D<double> vector3D( const aVector& v ) {
      return HepGeom::Vector3D<double>( v.x(), v.y(), v.z() );
    }

    template <class aPoint>
    HepGeom::Point3D<double> point3D( const aPoint& p ) {
      return HepGeom::Point3D<double>( p.x(), p.y(), p.z() );
    }

  } // namespace math2clhep

} // namespace LHCb
