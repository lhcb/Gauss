/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichG4AgelExitTag.h,v 1.2 2004-02-10 10:14:09 jonesc Exp $
#ifndef MISC_RICHG4AGELEXITTAG_H
#define MISC_RICHG4AGELEXITTAG_H 1
#include "G4ThreeVector.hh"
#include "G4Track.hh"

extern void RichG4AgelExitTag( const G4Track* aPhotonTk, const G4ThreeVector& aAgelExitPos );

#endif // MISC_RICHG4AGELEXITTAG_H
