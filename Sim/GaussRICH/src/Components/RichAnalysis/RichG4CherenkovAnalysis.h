/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaussRICH/RichG4AnalysisConstGauss.h"
#include "GaussRICH/RichG4GaussPathNames.h"

// extern void RichG4CherenkovAnalysis1(const G4Step& aStep,
//                                      G4double CosThCkv, G4double SinThCkv,
//                                      G4double  PhotProdCkvKE,
//                                      const G4ThreeVector & PhotProdPosition,
//                                      G4double RefInd, G4double BetaInvChPart );

extern void RichG4CherenkovAnalysis1( const G4Step& aStep, G4double CosThCkv, G4double PhotProdCkvKE,
                                      const G4ThreeVector& PhotProdPosition, G4double RefInd, G4double BetaInvChPart );

extern void RichG4CherenkovAnalysis2( const G4Step& aStep );

extern void RichG4CherenkovProdFeaturesHisto( const G4Track& aChTrack );
