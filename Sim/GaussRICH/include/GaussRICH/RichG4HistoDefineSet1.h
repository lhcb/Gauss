/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussRICH_RichG4HistoDefineSet1_H
#define GaussRICH_RichG4HistoDefineSet1_H 1

// Include files
// #include "GaudiKernel/Algorithm.h"
// from STL
#include "GaudiKernel/ISvcLocator.h"
#include <cmath>
#include <string>
#include <vector>

// Forward declarations
class IHistogram1D;
class IHistogram2D;

// Author SE 21-8-2002
class RichG4HistoDefineSet1 {

public:
  RichG4HistoDefineSet1();
  virtual ~RichG4HistoDefineSet1();
  void setRichG4HistoSet1Type( int aType ) { m_RichG4HistoSet1Type = aType; }
  void BookRichG4HistogramsSet1();

private:
  // These data members are used in the execution of this algorithm
  // They are set in the initialisation phase by the job options service
  // for the RichG4RunAction.
  // bool        m_DefineRichG4HistogramSet1;  // flag for histo production
  /// Book histograms
  void bookRichG4HistogramsSet1Type1();
  void bookRichG4HistogramsSet1Type2();
  void bookRichG4HistogramsSet1Type3();

  // Histograms ( used if m_produceHistogramSet1 = 1 (true) )
  IHistogram1D* m_hNumTotHitRich1;
  IHistogram2D* m_hGlobalRich1PEOriginXY;
  IHistogram2D* m_hGlobalRich1PeOriginPosHitXY;
  IHistogram2D* m_hGlobalPEOriginAgelPosXY;
  IHistogram2D* m_hGlobalPEOriginC4F10PosXY;
  IHistogram2D* m_hGlobalPEOriginCF4PosXY;

  IHistogram2D* m_hGlobalHitXY;
  IHistogram2D* m_hRefIndC4F10Rich1;
  IHistogram2D* m_hRefIndAgelRich1;
  IHistogram2D* m_hRefIndCF4Rich2;
  IHistogram1D* m_hCkvAgelRich1;
  IHistogram1D* m_hCkvC4F10Rich1;
  IHistogram1D* m_hCkvCF4Rich2;
  IHistogram1D* m_hCkvZEmissionPtRich1;
  IHistogram1D* m_hWaveLenBeforeRich1Mirror1;
  IHistogram1D* m_hWaveLenAfterRich1Mirror1;
  IHistogram1D* m_hWaveLenBeforeRich1GasQW;
  IHistogram1D* m_hWaveLenAfterRich1GasQW;
  IHistogram1D* m_hWaveLenBeforeRich1QE;
  IHistogram1D* m_hWaveLenAfterRich1QE;
  IHistogram2D* m_hGlobalPEOriginAgelXY;
  IHistogram2D* m_hGlobalPEOriginC4F10XY;
  IHistogram2D* m_hGlobalPEOriginCF4XY;

  IHistogram2D* M_hHpdQwOrigXZR1;
  IHistogram2D* M_hHpdQwOrigXZR2;
  IHistogram2D* M_hHpdQwOrigYZR1;
  IHistogram2D* M_hHpdQwOrigYZR2;

  //  IHistogram2D*         m_hGlobalPEOriginAgelTopXY;
  // IHistogram2D*         m_hGlobalPEOriginC4F10TopXY;
  // IHistogram2D*         m_hGlobalPEOriginAgelBotXY;
  // IHistogram2D*         m_hGlobalPEOriginC4F10BotXY;

  //  IHistogram1D*  m_hEnergyLossInCF4;

  // IHistogram1D* m_hNumScintPhotProdInCF4;

  IHistogram1D* m_hStepNumScintPhotRich2;
  IHistogram1D* m_hStepNumScintPhotLBARich2;
  IHistogram1D* m_hStepNumScintPhotSBARich2;
  IHistogram1D* m_hStepNumScintPhotSBA2Rich2;
  IHistogram1D* m_hStepNumCkvPhotRich2;
  IHistogram1D* m_hStepNumCkvPhotLBARich2;
  IHistogram1D* m_hStepNumCkvPhotSBA2Rich2;

  IHistogram1D* m_hPhtotScintVertAngleRich2;
  IHistogram1D* m_hPhtotScintForwardAngleRich2;
  IHistogram1D* m_hPhtotScintBackwardAngleRich2;
  IHistogram1D* m_hPhtotScintHorizAngleRich2;
  IHistogram2D* m_hPhtotScintForwardVsHorizAngleRich2;
  IHistogram2D* m_hPhtotScintNegZVsHorizAngleRich2;

  IHistogram1D* m_hPhtotScintXRich2;
  IHistogram1D* m_hPhtotScintYRich2;
  IHistogram1D* m_hPhtotScintZRich2;
  IHistogram2D* m_hPhtotScintXvsZRich2;

  IHistogram1D* m_hPhtotScintWavelengthRich2;
  IHistogram1D* m_hPhtotScintPartMomRich2;

  IHistogram1D* m_hPhDetPlaneScintVertAngleRich2;
  IHistogram1D* m_hPhDetPlaneScintForwardAngleRich2;
  IHistogram1D* m_hPhDetPlaneScintBackwardAngleRich2;
  IHistogram1D* m_hPhDetPlaneScintHorizAngleRich2;
  IHistogram2D* m_hPhDetPlaneScintForwardVsHorizAngleRich2;
  IHistogram2D* m_hPhDetPlaneScintNegZVsHorizAngleRich2;

  IHistogram1D* m_hPhDetPlaneScintXRich2;
  IHistogram1D* m_hPhDetPlaneScintYRich2;
  IHistogram1D* m_hPhDetPlaneScintZRich2;
  IHistogram2D* m_hPhDetPlaneScintXvsZRich2;

  IHistogram1D* m_hPhDetPlaneScintWavelengthRich2;
  IHistogram1D* m_hPhDetPlaneScintPartMomRich2;

  int m_RichG4HistoSet1Type;

  std::string  m_RichG4HistoPathSet1; ///< Histo path
  ISvcLocator* m_svcLocSet1;
};

#endif // end of GaussRICH_RichG4HistoDefineSet1
