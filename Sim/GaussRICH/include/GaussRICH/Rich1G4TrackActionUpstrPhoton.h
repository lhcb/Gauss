/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Rich1G4TrackActionUpstrPhoton.h,v 1.4 2007-01-12 15:32:03 ranjard Exp $
#ifndef Rich1G4TrackActionUpstrPhoton_h
#  define Rich1G4TrackActionUpstrPhoton_h 1

// STL
#  include <string>
#  include <vector>

// GiGa
#  include "GiGa/GiGaTrackActionBase.h"

// forward declarations
class G4Track;
class G4particleDefinition;

class Rich1G4TrackActionUpstrPhoton : virtual public GiGaTrackActionBase {

public:
  /// useful typedefs
  typedef std::vector<std::string>                 TypeNames;
  typedef std::vector<const G4ParticleDefinition*> PartDefs;
  ///

  Rich1G4TrackActionUpstrPhoton( const std::string& type, const std::string& name, const IInterface* parent );

  /// destructor (virtual and protected)
  virtual ~Rich1G4TrackActionUpstrPhoton();

  /** perform action
   *  @see G4UserTrackingAction
   *  @param pointer to new track opbject
   */
  void PreUserTrackingAction( const G4Track* ) override;

  /** perform action
   *  @see G4UserTrackingAction
   *  @param pointer to new track opbject
   */
  void PostUserTrackingAction( const G4Track* ) override;

private:
  Rich1G4TrackActionUpstrPhoton();                                                  ///< no default constructor
  Rich1G4TrackActionUpstrPhoton( const Rich1G4TrackActionUpstrPhoton& );            ///< no copy
  Rich1G4TrackActionUpstrPhoton& operator=( const Rich1G4TrackActionUpstrPhoton& ); ///< no copy
};

#endif
// ============================================================================
