/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichInfo.h,v 1.2 2003-07-28 10:27:11 witoldp Exp $
#ifndef GaussRICH_RICHINFO_H
#define GaussRICH_RICHINFO_H 1

// Include files

/** @class RichInfo RichInfo.h Misc/RichInfo.h
 *
 *
 *  @author Sajan EASO
 *  @date   2003-04-16
 */
#include "GaussTools/DetTrackInfo.h"
#include "RichPEInfo.h"
#include "RichPhotInfo.h"

class RichInfo : public DetTrackInfo {
public:
  /// Standard constructor

  RichInfo();

  RichInfo( RichPhotInfo* );

  RichInfo( RichPEInfo* );

  virtual ~RichInfo(); ///< Destructor

  RichPhotInfo* RichPhotInformation() { return m_RichPhotInformation; }
  void setRichPhotInformation( RichPhotInfo* aRichPhotInformation ) { m_RichPhotInformation = aRichPhotInformation; }

  RichPEInfo* RichPEInformation() { return m_RichPEInformation; }
  void        setRichPEInformation( RichPEInfo* aRichPEInformation ) { m_RichPEInformation = aRichPEInformation; }
  bool        HasUserPhotInfo() { return m_HasUserPhotInfo; }

  void setUserPhotInfo() { m_HasUserPhotInfo = true; }
  void unsetUserPhotInfo() { m_HasUserPhotInfo = false; }

  bool HasUserPEInfo() { return m_HasUserPEInfo; }
  void setUserPEInfo() { m_HasUserPEInfo = true; }
  void unsetUserPEInfo() { m_HasUserPEInfo = false; }

protected:
private:
  RichPhotInfo* m_RichPhotInformation;

  RichPEInfo* m_RichPEInformation;

  bool m_HasUserPhotInfo;
  bool m_HasUserPEInfo;
};
#endif // GaussRICH_RICHINFO_H
