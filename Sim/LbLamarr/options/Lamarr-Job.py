###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import LbLamarr
from Gaudi.Configuration import *
from LbLamarr import GeneratorConfigWizard

# eventType = '11102013'
eventType = "15874000"

GeneratorConfigWizard.configureGenerator("ParticleGun", eventType)

LbLamarr().EventType = eventType
LbLamarr().EvtMax = 50
LbLamarr().DataType = "2016"
LbLamarr().Polarity = "MagUp"
LbLamarr().OutputLevel = VERBOSE
