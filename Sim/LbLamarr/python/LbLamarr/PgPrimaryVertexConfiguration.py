###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import mm

"""
See https://its.cern.ch/jira/browse/LHCBGAUSS-1287
"""

PgPrimaryVertexConfiguration = {
    "2015": {
        "MagUp": {
            "X": {
                "mu": -3.29e-5 * mm,
                "f1": 0.71440,
                "f2": 0.07935,
                "sigma1": 0.01109 * mm,
                "sigma2": 0.04393 * mm,
                "sigma3": 0.02087 * mm,
            },
            "Y": {
                "mu": 1.33e-5 * mm,
                "f1": 0.69546,
                "f2": 0.07852,
                "sigma1": 0.01063 * mm,
                "sigma2": 0.04060 * mm,
                "sigma3": 0.19191 * mm,
            },
            "Z": {
                "mu": -1.9e-4 * mm,
                "f1": 0.01556,
                "f2": 0.22276,
                "sigma1": 0.37696 * mm,
                "sigma2": 0.13423 * mm,
                "sigma3": 0.06089 * mm,
            },
        },
        "MagDown": {
            "X": {
                "mu": 4.05e-5 * mm,
                "f1": 0.70373,
                "f2": 0.08552,
                "sigma1": 0.01082 * mm,
                "sigma2": 0.04187 * mm,
                "sigma3": 0.02012 * mm,
            },
            "Y": {
                "mu": -3.32e-5 * mm,
                "f1": 0.70820,
                "f2": 0.07844,
                "sigma1": 0.01041 * mm,
                "sigma2": 0.04072 * mm,
                "sigma3": 0.01930 * mm,
            },
            "Z": {
                "mu": -1.8e-4 * mm,
                "f1": 0.01368,
                "f2": 0.21009,
                "sigma1": 0.38345 * mm,
                "sigma2": 0.13362 * mm,
                "sigma3": 0.06049 * mm,
            },
        },
    },
    "2016": {
        "MagUp": {
            "X": {
                "mu": 1.46e-5 * mm,
                "f1": 0.39575,
                "f2": 0.13945,
                "sigma1": 0.02246 * mm,
                "sigma2": 0.04501 * mm,
                "sigma3": 0.01176 * mm,
            },
            "Y": {
                "mu": -5.39e-5 * mm,
                "f1": 0.39621,
                "f2": 0.13293,
                "sigma1": 0.02129 * mm,
                "sigma2": 0.04236 * mm,
                "sigma3": 0.01130 * mm,
            },
            "Z": {
                "mu": 2.3e-4 * mm,
                "f1": 0.03295,
                "f2": 0.73535,
                "sigma1": 0.42375 * mm,
                "sigma2": 0.07201 * mm,
                "sigma3": 0.17024 * mm,
            },
        },
        "MagDown": {
            "X": {
                "mu": 1.46e-5 * mm,
                "f1": 0.39575,
                "f2": 0.13945,
                "sigma1": 0.02246 * mm,
                "sigma2": 0.04501 * mm,
                "sigma3": 0.01176 * mm,
            },
            "Y": {
                "mu": 1.76e-5 * mm,
                "f1": 0.56669,
                "f2": 0.16150,
                "sigma1": 0.01160 * mm,
                "sigma2": 0.04330 * mm,
                "sigma3": 0.02243 * mm,
            },
            "Z": {
                "mu": 8.9e-4 * mm,
                "f1": 0.27633,
                "f2": 0.04973,
                "sigma1": 0.16142 * mm,
                "sigma2": 0.40333 * mm,
                "sigma3": 0.06978 * mm,
            },
        },
    },
    "2017": {
        "MagUp": {
            "X": {
                "mu": -3.343e-5 * mm,
                "f1": 0.50859,
                "f2": 0.18137,
                "sigma1": 0.01238 * mm,
                "sigma2": 0.04589 * mm,
                "sigma3": 0.02336 * mm,
            },
            "Y": {
                "mu": 4.15e-5 * mm,
                "f1": 0.58180,
                "f2": 0.15396,
                "sigma1": 0.01223 * mm,
                "sigma2": 0.04545 * mm,
                "sigma3": 0.02425 * mm,
            },
            "Z": {
                "mu": 2.0e-4 * mm,
                "f1": 0.03839,
                "f2": 0.69368,
                "sigma1": 0.42427 * mm,
                "sigma2": 0.07200 * mm,
                "sigma3": 0.16817 * mm,
            },
        },
        "MagDown": {
            "X": {
                "mu": -7.42e-5 * mm,
                "f1": 0.53837,
                "f2": 0.16596,
                "sigma1": 0.01232 * mm,
                "sigma2": 0.04692 * mm,
                "sigma3": 0.02418 * mm,
            },
            "Y": {
                "mu": 0.93e-5 * mm,
                "f1": 0.54446,
                "f2": 0.16130,
                "sigma1": 0.01180 * mm,
                "sigma2": 0.04433 * mm,
                "sigma3": 0.02295 * mm,
            },
            "Z": {
                "mu": -2.6e-4,
                "f1": 0.03819,
                "f2": 0.68620,
                "sigma1": 0.58298 * mm,
                "sigma2": 0.06970 * mm,
                "sigma3": 0.16299 * mm,
            },
        },
    },
    "2018": {
        "MagUp": {
            "X": {
                "mu": -7.97e-5 * mm,
                "f1": 0.54123,
                "f2": 0.16862,
                "sigma1": 0.01230 * mm,
                "sigma2": 0.04682 * mm,
                "sigma3": 0.02420 * mm,
            },
            "Y": {
                "mu": -3.34e-5 * mm,
                "f1": 0.54574,
                "f2": 0.16732,
                "sigma1": 0.01187 * mm,
                "sigma2": 0.04410 * mm,
                "sigma3": 0.02308 * mm,
            },
            "Z": {
                "mu": -1.6e-4 * mm,
                "f1": 0.03745,
                "f2": 0.70232,
                "sigma1": 0.42426 * mm,
                "sigma2": 0.07209 * mm,
                "sigma3": 0.17011 * mm,
            },
        },
        "MagDown": {
            "X": {
                "mu": -7.92e-5 * mm,
                "f1": 0.5408,
                "f2": 0.1771,
                "sigma1": 0.01249 * mm,
                "sigma2": 0.04630 * mm,
                "sigma3": 0.02393 * mm,
            },
            "Y": {
                "mu": -5.89e-5 * mm,
                "f1": 0.57869,
                "f2": 0.15315,
                "sigma1": 0.01223 * mm,
                "sigma2": 0.04540 * mm,
                "sigma3": 0.02381 * mm,
            },
            "Z": {
                "mu": 1.5e-4 * mm,
                "f1": 0.03805,
                "f2": 0.69137,
                "sigma1": 0.41754 * mm,
                "sigma2": 0.07009 * mm,
                "sigma3": 0.16409 * mm,
            },
        },
    },
}
