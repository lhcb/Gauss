# Sim Graph Plotter

Very simple tool for comparing the graph of HepMC generator-level quantities with the MCParticles imported in LHCb.

The workflow to exploit the tool is:
 * generate a simulated file with Lamarr (should work with standard Gauss as well, but was never tested)
 * run Bender with option file `BenderScript.py` producing a SQLite file `output.db`
 * run python script `make_graph.py` to load the `output.db` file produced by Bender and draw the graphs in svg format

### Dependencies
At the time of writing, Lamarr produces `.xsim` files that are compatible with Bender v35r6 made availble for platform `x86_64-centos7-gcc8-opt`

`make_graph.py` requires the following packages from PyPI:
 * [numpy](https://github.com/numpy/numpy)
 * [pandas](https://github.com/pandas-dev/pandas) to ease processing of the SQLite database
 * [tqdm](https://github.com/tqdm/tqdm) to show a progress bar of the conversion
 * [scikit-hep/particle](https://github.com/scikit-hep/particle) to convert the pdgid into a particle name
 * [graphviz](https://github.com/xflr6/graphviz) to draw the graph and store them in svg format

### Example workflow

Run Lamarr
```bash
 lb-run  \
   --nightly lhcb-gauss-lamarr/latest Gauss/HEAD \
   gaudirun.py \
   ../../tests/options/testLamarr-Job-LambdaB-Pythia.py
```

Run Bender producing `output.db`
```bash
lb-run\
  --platform=x86_64-centos7-gcc8-opt \
  Bender/v35r6 \
  python BenderScript.py \
  Lamarr_2016MagUp_15874000_1082.xsim
```

Read `output.db` and produce the SVG files.
```bash
python3 make_graph.py output.db
```

To visualize the output you can run a server on a port of your choice, for
example, 8901,
```bash
python3 -m http.server 8901
```
and access to that port through an ssh tunnel
```
ssh <your_machine> -L 8901:localhost:8901
```
which enables your local browser to connect to `localhost:8901` where the svg
files are shown.
