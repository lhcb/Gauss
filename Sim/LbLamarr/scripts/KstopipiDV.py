###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##################################################
# simple pi0 particle maker using Delphes as input
# Adam Davis, last update 23/4/18
##################################################

from CommonParticles.Utils import *
from Configurables import (
    CombineParticles,
    DataOnDemandSvc,
    DaVinci,
    DecayTreeTuple,
    FilterDesktop,
)

# setup environment
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from PhysSelPython.Wrappers import Selection, SelectionSequence

# get photons protoparticles, make them into particles.
# PhotonMaker takes directly from LHCb::ProtoParticleLocation::Neutrals
locations = {}
nppions = "Phys/StdAllNoPIDsPions/Particles"
# matchB02PiPi    = "(mcMatch('[KS0 ==> pi+ pi-]CC'))"
# matchB02PiPi    = "(M<6.0*GeV) & (M>5.0*GeV)"
matchB02PiPi = "ALL"


StdMCB02PiPi = CombineParticles("StdMCB02PiPi")
StdMCB02PiPi.Inputs = [nppions]
# StdMCB02PiPi.DecayDescriptor = "[B0 -> pi+ pi-]cc"
StdMCB02PiPi.DecayDescriptor = "[KS0 -> pi+ pi-]cc"
StdMCB02PiPi.DaughtersCuts = {"pi+": "ALL"}
StdMCB02PiPi.MotherCut = matchB02PiPi
# StdMCB02PiPi.CombinationCut = "(ADAMASS('rho(770)0') < 55.*MeV)"
StdMCB02PiPi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC",
]
locations.update(updateDoD(StdMCB02PiPi))

# input pi0s are set, make a decay tree tuple
myb0Sel = Selection("myb0Sel", Algorithm=StdMCB02PiPi, InputDataSetter=None)
myb0SelSeq = SelectionSequence("myb0SelSeq", TopSelection=myb0Sel)
# myb0Sel.OutputLevel = VERBOSE
tuple = DecayTreeTuple("B0tree")
# tuple.OutputLevel=DEBUG
tuple.Inputs = [myb0Sel.outputLocation()]
# tuple.Decay = '[(B0 -> ^pi+ ^pi-)]CC'
tuple.Decay = "[(KS0 -> ^pi+ ^pi-)]CC"
tuple.ToolList += [
    "TupleToolKinematic",
    "TupleToolGeometry",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
]
from Configurables import DaVinci

DaVinci().PrintFreq = 5000
# DaVinci().TupleFile = "B0_DV_Tuples_from_DelphesAlg.root"
DaVinci().TupleFile = "B0_DV_Tuples_from_DelphesAlg.root"

DaVinci().UserAlgorithms = [
    StdMCB02PiPi,
    # fltr,
    myb0SelSeq,
]
DaVinci().appendToMainSequence(
    [
        # fltr,
        myb0SelSeq.sequence(),
        tuple,
    ]
)
DaVinci().InputType = "DST"
DaVinci().Lumi = False
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().DataType = "2012"
# DaVinci().CondDBtag =
# DaVinci().DDDBtag =
from GaudiConf import IOHelper

# IOHelper().inputFiles(['/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00031024/0000/00031024_00000001_1.allstreams.dst'],clear=True)
IOHelper().inputFiles(["../options/Gauss-4000ev-20180628.gen"], clear=True)
