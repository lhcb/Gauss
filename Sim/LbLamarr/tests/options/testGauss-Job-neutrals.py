###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
####################################################
# simple test options for neutral events for Lamarr
#####################################################
from Configurables import LbLamarr
from Gaudi.Configuration import *
from LbLamarr import GeneratorConfigWizard

# Lamarr configuration
eventType = "11102421"  # B0 -> KK pi0, pi0-> gamma gamma

from LbLamarr import GeneratorConfigWizard

GeneratorConfigWizard.configureGenerator("ParticleGun", eventType)

from Configurables import MomentumRange, ParticleGun, SignalPlain
from GaudiKernel import SystemOfUnits

ParticleGun().addTool(MomentumRange)
ParticleGun().MomentumRange.MomentumMin = 1.0 * SystemOfUnits.GeV
ParticleGun().MomentumRange.MomentumMax = 100.0 * SystemOfUnits.GeV

from Configurables import MomentumRange

GeneratorConfigWizard.configureGenerator("ParticleGun", eventType)
ParticleGun().ParticleGunTool = "MomentumRange"
ParticleGun().NumberOfParticlesTool = "FlatNParticles"
pid_list = [511, -511]
ParticleGun().MomentumRange.PdgCodes = pid_list
ParticleGun().SignalPdgCode = abs(pid_list[0])
ParticleGun().DecayTool = "EvtGenDecay"

LbLamarr().EventType = eventType
LbLamarr().EvtMax = 1000
LbLamarr().DataType = "2016"
LbLamarr().Polarity = "MagUp"
# beam config
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2016-nu1.6.py")
# database tags
importOptions("$GAUSSOPTS/DBTags-2016.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
