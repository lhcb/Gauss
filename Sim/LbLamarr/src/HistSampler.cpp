/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HistSampler.h"
#include "TFile.h"
#include <TH2D.h>
#include <algorithm>
#include <iostream>
#include <numeric>

//============================================================================
//   Constructor
//============================================================================
HistSampler::HistSampler( const std::string& file_name, const std::string& hist_name, size_t nSamples ) {
  std::unique_ptr<TFile> file( TFile::Open( file_name.c_str() ) );
  if ( !file ) throw FileNotFoundException( file_name );

  TH2D* hist{ nullptr };
  file->GetObject( hist_name.c_str(), hist );
  // hist is owned by TFile, do not delete.
  if ( !hist ) throw HistNotFoundException( hist_name );

  m_xAxis.resize( hist->GetXaxis()->GetNbins() );
  size_t binCounter = 1;
  for ( auto& x : m_xAxis ) x = hist->GetXaxis()->GetBinCenter( binCounter++ );

  auto yAxis = std::vector<float>( hist->GetYaxis()->GetNbins() );
  binCounter = 1;
  for ( auto& y : yAxis ) y = hist->GetYaxis()->GetBinCenter( binCounter++ );

  m_yAxis = std::vector<std::vector<float>>( hist->GetXaxis()->GetNbins(), std::vector<float>( nSamples, 0.f ) );

  auto buffer     = std::vector<float>( yAxis.size() );
  int  rowCounter = 1;
  for ( auto& yRow : m_yAxis ) {
    binCounter = 1;
    for ( auto& c : buffer ) c = hist->GetBinContent( rowCounter, binCounter++ );
    std::partial_sum( buffer.begin(), buffer.end(), buffer.begin() );
    const float s = buffer.back();
    for ( auto& c : buffer ) c /= s;

    yRow.resize( nSamples );
    for ( size_t yBin = 0; yBin < nSamples; yBin++ ) {
      float  r = static_cast<float>( yBin ) / static_cast<float>( nSamples - 1 );
      size_t iBin;
      for ( iBin = 0; iBin < buffer.size() - 2; ++iBin )
        if ( r < buffer[iBin] ) break;

      const auto b1 = buffer[iBin];
      const auto b2 = buffer[iBin + 1];
      const auto y1 = yAxis[iBin];
      const auto y2 = yAxis[iBin + 1];

      yRow[yBin] = y1 + ( r - b1 ) / ( b2 - b1 + 1e-12 ) * ( y2 - y1 );
      if ( yRow[yBin] < yAxis[0] )
        yRow[yBin] = yAxis[0];
      else if ( yRow[yBin] > yAxis.back() )
        yRow[yBin] = yAxis.back();
    }
    //    std::cout << "yRow" << std::endl;
    //    for (auto& c: yRow) std::cout << c << " ";
    //    std::cout << std::endl;

    rowCounter++;
  }
}

//============================================================================
//   Sample
//============================================================================
float HistSampler::sample( const float xVar, const float rndmFlat ) const {
  const auto   upper    = std::upper_bound( m_xAxis.begin() + 1, m_xAxis.end(), xVar );
  const auto   id       = upper - m_xAxis.begin();
  const size_t nSamples = m_yAxis[id - 1].size();

  const auto x1 = m_xAxis[id - 1];
  const auto r1 = m_yAxis[id - 1][nSamples * rndmFlat];

  if ( static_cast<size_t>( id ) >= m_xAxis.size() ) return r1;

  const auto x2 = m_xAxis[id];

  const auto r2 = m_yAxis[id][nSamples * rndmFlat];

  return r1 + ( xVar - x1 ) / ( x2 - x1 ) * ( r2 - r1 );
}
