/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <iostream>
#include <vector>

// Local
#include "SimpleProfiler.h"

//==============================================================================
// start
//
void SimpleProfiler::start( const std::string& s ) {
  if ( !m_enabled ) return;
  m_active.push_back( std::make_tuple( std::string( s ), std::chrono::high_resolution_clock::now() ) );
}

//==============================================================================
// stop
//
void SimpleProfiler::stop() {
  if ( !m_enabled ) return;
  if ( m_active.size() == 0 ) throw std::domain_error( "Stopping a timer never started" );

  // Compose the name of the chrono
  std::string name = std::get<0>( m_active.front() );
  for ( auto level = m_active.begin() + 1; level != m_active.end(); level++ )
    name = name + "\\\\" + std::get<0>( *level );

  // Creates the slot if not exists
  if ( m_deltat.find( name ) == m_deltat.end() ) m_deltat[name] = std::vector<duration>();

  // Compute the time interval and fill the slot
  const auto now = std::chrono::high_resolution_clock::now();
  const auto t0  = std::get<1>( m_active.back() );
  m_deltat[name].push_back( now - t0 );
  m_active.pop_back();
}

//==============================================================================
// dump
//
std::string SimpleProfiler::json_dump() {
  if ( !m_enabled ) return "";

  std::stringstream s;

  s << "{" << std::endl;

  bool first_l = true;
  for ( auto l : m_deltat ) {
    if ( !first_l ) s << ", " << std::endl;
    s << "\"" << l.first << "\" : [";

    size_t counter = 0;
    for ( auto v : l.second ) {
      if ( counter > 0 ) s << ", ";
      if ( ++counter % 10 == 0 ) s << std::endl << "  ";
      s << double( v.count() );
    }
    s << "]";
    first_l = false;
  }

  s << std::endl << "}";

  return s.str();
}
