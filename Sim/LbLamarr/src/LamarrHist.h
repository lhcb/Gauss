/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LAMARRHIST_H
#define LAMARRHIST_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

/** @class LamarrHist LamarrHist.h
 *
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2015-10-20
 */
class LamarrHist : public GaudiHistoAlg {
public:
  /// Standard constructor
  LamarrHist( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_particles{ this, "MCFastParticleLocation", "/Event/MCFast/MCParticles",
                                            "MCFastParticleLocation" };
  Gaudi::Property<std::string> m_vertices{ this, "MCFastVerticesLocation", "/Event/MCFast/MCVertices",
                                           "MCFastVerticesLocation" };
  Gaudi::Property<std::string> m_generatedVertices{ this, "MCVerticesLocation", LHCb::MCVertexLocation::Default,
                                                    "MCVerticesLocation" };
  Gaudi::Property<std::string> m_generatedParticles{ this, "MCParticleLocation", LHCb::MCParticleLocation::Default,
                                                     "MCParticleLocation" };

  std::map<Long64_t, std::pair<LHCb::MCParticle*, LHCb::MCParticle*>> Pions;
  std::map<Long64_t, LHCb::MCParticle*>                               GenPions;
  std::map<Long64_t, LHCb::MCParticle*>                               RecPions;
  AIDA::IHistogram1D*                                                 m_RGen{ nullptr };
  AIDA::IHistogram1D*                                                 m_RRec{ nullptr };
  AIDA::IHistogram1D*                                                 m_ThetaGen{ nullptr };
  AIDA::IHistogram1D*                                                 m_ThetaRec{ nullptr };
  AIDA::IHistogram1D*                                                 m_PhiGen{ nullptr };
  AIDA::IHistogram1D*                                                 m_PhiRec{ nullptr };
  AIDA::IHistogram1D*                                                 m_EtaGen{ nullptr };
  AIDA::IHistogram1D*                                                 m_EtaRec{ nullptr };
  AIDA::IHistogram1D*                                                 m_ZGen{ nullptr };
  AIDA::IHistogram1D*                                                 m_ZRec{ nullptr };
  AIDA::IHistogram2D*                                                 m_XYGen{ nullptr };
  AIDA::IHistogram2D*                                                 m_XYRec{ nullptr };
  AIDA::IHistogram2D*                                                 m_EtaPhiGen{ nullptr };
  AIDA::IHistogram2D*                                                 m_EtaPhiRec{ nullptr };
  AIDA::IHistogram2D*                                                 m_P{ nullptr };
  AIDA::IHistogram2D*                                                 m_Pt{ nullptr };
  AIDA::IHistogram2D*                                                 m_PX{ nullptr };
  AIDA::IHistogram2D*                                                 m_PY{ nullptr };
  AIDA::IHistogram2D*                                                 m_PZ{ nullptr };
  AIDA::IHistogram2D*                                                 m_E{ nullptr };
  AIDA::IHistogram2D*                                                 m_M{ nullptr };
  AIDA::IHistogram2D*                                                 m_Vertex_X{ nullptr };
  AIDA::IHistogram2D*                                                 m_Vertex_Y{ nullptr };
  AIDA::IHistogram2D*                                                 m_Vertex_Z{ nullptr };
  AIDA::IHistogram2D*                                                 m_PID{ nullptr };
  AIDA::IHistogram2D*                                                 m_PhiXGen{ nullptr };
  AIDA::IHistogram2D*                                                 m_PhiXRec{ nullptr };
  AIDA::IHistogram2D*                                                 m_theta_XY_Gen{ nullptr };
  AIDA::IHistogram2D*                                                 m_theta_XY_Rec{ nullptr };
  AIDA::IHistogram2D*                                                 m_ThetaP_Gen{ nullptr };
  AIDA::IHistogram2D*                                                 m_ThetaP_Rec{ nullptr };
  AIDA::IHistogram3D*                                                 m_XYPhiGen{ nullptr };
  AIDA::IHistogram3D*                                                 m_XYPhiRec{ nullptr };
  AIDA::IHistogram3D*                                                 m_TxTyPGen{ nullptr };
  AIDA::IHistogram3D*                                                 m_TxTyPRec{ nullptr };
};
#endif // LAMARRHIST_H
