/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <chrono>
#include <deque>
#include <map>
#include <sstream>
#include <string>
#include <tuple>

class SimpleProfiler {
  using duration   = std::chrono::duration<double>;
  using time_point = std::chrono::time_point<std::chrono::high_resolution_clock>;

public:
  SimpleProfiler( bool enabled = true ) : m_enabled( enabled ), m_starts(), m_deltat(), m_active() {}

  void        start( const std::string& s );
  void        stop();
  void        enable( bool enabled = true ) { m_enabled = enabled; }
  std::string json_dump();

private:
  bool                                            m_enabled;
  std::map<std::string, std::vector<time_point>>  m_starts;
  std::map<std::string, std::vector<duration>>    m_deltat;
  std::deque<std::tuple<std::string, time_point>> m_active;
};
