/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include files
// from Gaudi
#include "Event/Particle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/RndmGenerators.h" //for MC integration
// Lamar
#include "LamarrPropagator.h"
// calo event
#include "CaloUtils/CaloAlgUtils.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloHypo.h"
#include "Event/CaloPosition.h"
// #include "CaloInterfaces/ICaloHypoEstimator.h"
#include "Detector/Calo/CaloCellCode.h" //for talking back and forth between conventions
#include "Event/CaloCluster.h"
// c++
#include <fstream>
#include <map>
#include <string>
#include <tuple>

// replace with inheritance/friend class
#include "Event/State.h"
#include "TFormula.h"
// define some classes for more useful info than std::tuples
/** @class LamarrEcalTower LamarrCaloProto.h
 *  primitive holder for ECAL cell
 *
 *  @author Adam Davis
 *  @date   2019-6-27
 *  @date   2021-1-18 - update for CaloCellCode::CaloArea enumerations
 */
class LamarrEcalTower {
public:
  LamarrEcalTower( double cellxsize, double cellysize, int region, int row, int col, float energy ) {
    m_cellXsize = cellxsize;
    m_cellYsize = cellysize;

    m_region = region;
    m_row    = row;
    m_col    = col;
    m_energy = energy;
  }

  int   Region() { return m_region; }
  int   Row() { return m_row; }
  int   Col() { return m_col; }
  float E() { return m_energy; }
  void  SetE( float e ) { m_energy = e; }
  void  SetTimeInfo( int t, int tmin, int tmax ) {
    m_time    = t;
    m_timemin = tmin;
    m_timemax = tmax;
  }
  void   SetCellXsize( double max_x_size ) { m_cellXsize = max_x_size; }
  void   SetCellYsize( double max_y_size ) { m_cellYsize = max_y_size; }
  double GetCellXsize() { return m_cellXsize; }
  double GetCellYsize() { return m_cellYsize; }

private:
  int   m_region{ -1 };
  uint  m_row{ 0 };
  uint  m_col{ 0 };
  float m_energy{ 0 };
  // size of cell for ease
  double m_cellXsize{ 0 };
  double m_cellYsize{ 0 };

  // for later
  int m_time{ 0 };
  int m_timemin{ 0 };
  int m_timemax{ 0 };
};

/** @class LamarrSimEcalCluster LamarrCaloProto.h
 *  group of towers with necessary MC info
 *
 *  @author Adam Davis
 *  @date   2019-6-27
 */
class LamarrSimEcalCluster {
public:
  // constructor
  LamarrSimEcalCluster( int seedreg, int seedrow, int seedcol ) {
    m_seedreg = seedreg;
    m_seedrow = seedrow;
    m_seedcol = seedcol;
  };

  int   SeedRegion() { return m_seedreg; }
  int   SeedRow() { return m_seedrow; }
  int   SeedCol() { return m_seedcol; }
  void  SetSeedEnergy( float energy ) { m_seedEnergy = energy; }
  float SeedEnergy() { return m_seedEnergy; }

  void AddTower( double cellxsize, double cellysize, int region, int row, int col, float energy ) {
    m_towers.push_back( LamarrEcalTower( cellxsize, cellysize, region, row, col, energy ) );
  }

  std::vector<LamarrEcalTower> Towers() { return m_towers; }
  void                         UpdateEnergy( int region, int row, int col, float newEnergy ) {
    // find tower matching region,row,col and update energy
    // good for spillover.
    // can do better than a loop here.
    for ( auto t : m_towers ) {
      if ( t.Region() == region && t.Row() == row && t.Col() == col ) t.SetE( newEnergy );
      break;
    }
  }
  float ClusterEnergy() {
    if ( m_energy == -1 ) { // recompute if uninitialized
      m_energy = 0;
      for ( auto t : m_towers ) { m_energy += t.E(); }
    }
    return m_energy;
  }
  void   SetMCKey( int key ) { m_MCKey = key; }
  int    MCKey() { return m_MCKey; }
  void   SetMCEnergy( float e ) { m_mc_energy = e; }
  float  MCEnergy() { return m_mc_energy; }
  double MaxXsize() { return m_max_x_size; }
  double MaxYsize() { return m_max_y_size; }
  void   SetMaxXsize( double x ) { m_max_x_size = x; }
  void   SetMaxYsize( double y ) { m_max_y_size = y; }
  int    NCellsY() { return m_n_cells_y; }
  int    NCellsX() { return m_n_cells_x; }
  void   SetNCellsX( int x ) { m_n_cells_x = x; }
  void   SetNCellsY( int y ) { m_n_cells_y = y; }

private:
  int    m_MCKey{ -2 };
  int    m_seedreg{ -1 };
  int    m_seedrow{ 0 };
  int    m_seedcol{ 0 };
  float  m_seedEnergy{ -1 };
  float  m_energy{ -1 };
  float  m_mc_energy{ -1 };
  double m_max_x_size{ -1 };
  double m_max_y_size{ -1 };
  int    m_n_cells_x{ -1 };
  int    m_n_cells_y{ -1 };

  std::vector<LamarrEcalTower> m_towers{};
};

typedef std::vector<LamarrSimEcalCluster> LamarrSimEcalClusters;

/** @class LamarrCaloProto LamarrCaloProto.h
 *
 *  @author Adam Davis
 *  @date   2019-6-27
 */
class LamarrCaloProto : public GaudiAlgorithm {
  // todo friend class LamarPropagator;

public:
  /// Standard constructor
  // LamarrCaloProto( const std::string& name, ISvcLocator* pSvcLocator );
  using GaudiAlgorithm::GaudiAlgorithm;

  // constructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  Rndm::Numbers m_flatDist;  ///< Flat random number generator
  Rndm::Numbers m_gaussDist; // Gaussian random number generator

  Gaudi::Property<std::string> m_particles{ this, "MCParticleLocation", LHCb::MCParticleLocation::Default,
                                            "Location of smeared MCParticles." }; ///< Location in TES of output smeared
                                                                                  ///< MCParticles.
  Gaudi::Property<std::string> m_neutralLocation{
      this, "MCFastNeutralProtoParticles", LHCb::ProtoParticleLocation::Neutrals,
      "Location to write neutral protoparticles" }; // location in TES of output neutrals
  Gaudi::Property<std::string> m_photonsLocation{ this, "MCFastPhotonsHypo", LHCb::CaloHypoLocation::Photons,
                                                  "Location to write photon hypothesis" };
  Gaudi::Property<std::string> m_caloDigitsLocation{ this, "MCFastCaloDigits", LHCb::CaloDigitLocation::Default,
                                                     "Location to wrtie calo psedudigits" };

  Gaudi::Property<std::string> m_caloClusterLocation{
      this, "MCFastCaloClusterLocation", LHCb::CaloClusterLocation::Ecal, "Location to write calo clusters" };

  /// stuff for delphes calo reading
  Gaudi::Property<double> m_zEcal{ this, "ZEcal", 12650.5, "nominal z position of ecal to consider (front face)" };
  Gaudi::Property<int>    m_CaloClusterLoopSize{ this, "CaloClusterSize", 3,
                                              "Number of cells for the calo clusterization to loop over, e.g 3=3x3" };
  Gaudi::Property<int>    m_num_int_pts{ this, "NumericalIntegrationPoints", 4000,
                                      "number of points for calorimeter clusterization numerical integration" };
  Gaudi::Property<int>    m_cellCenterX{ this, "CaloCenterX", 32, "central X cell of calo" };
  // central cell for CALO in X, assume 32 for LHCb Run I and II geo
  Gaudi::Property<int> m_cellCenterY{ this, "CaloCenterY", 32, "central Y cell of calo" };
  // central cell for CALO in Y, assume 32 for LHCb Run I and II geo
  Gaudi::Property<float> m_moliere_radius{ this, "MoliereRadius", 35., "Moliere Radius (in mm)" };

  std::map<int, std::map<std::string, double>> m_calo_boundary_info; // x,y,cellsize

  Gaudi::Property<std::vector<double>> m_calo_x_borders{
      this, "CalorimeterXborders", { 323.2, 969.6, 1939.2, 3878.4 }, "X borders for calorimeter regions" };

  Gaudi::Property<std::vector<double>> m_calo_y_borders{
      this, "CalorimeterYborders", { 323.2, 727.2, 1212.0, 3151.2 }, "Y borders for calorimeter regions" };
  Gaudi::Property<std::vector<double>> m_calo_x_bins{
      this, "CalorimeterCellSizeX", { 40.4, 60.6, 121.2 }, "calorimeter cell sizex" }; // to be filled at initialize
                                                                                       // from the above.
  Gaudi::Property<std::vector<double>> m_calo_y_bins{
      this, "CalorimeterCellSizeY", { 40.4, 60.6, 121.2 }, "calorimeter cell size y" }; // asssumes no crazy shapes,
                                                                                        // just a grid.

  // refactor cluster finding to improve readability

  LamarrSimEcalClusters find_ecal_clusters( LHCb::MCParticles* parts );

  LamarrSimEcalCluster Get_Ecal_MC_Cluster( double x, double y, double z );

  void CombineOverlappingCluster( LamarrSimEcalClusters& clusters );

  // intersection for moliere radius and calo cell
  std::vector<std::tuple<float, float>> PointsOfIntersection( float xhit, float yhit, float xmin, float xmax,
                                                              float ymin, float ymax, float rM );

  float EnergyFraction( float x1, float x2, float y1, float y2, float num_RM, int num_integration_points = 1000 );

  Gaudi::Property<std::vector<std::string>> m_RegionNames{
      this, "RegionNames", { "Inner", "Middle", "Outer" }, "Calorimeter region names" }; // pointer to names of regions.

  bool is_inside( float box_x1, float box_y1, float box_x2, float box_y2, float x, float y ) {
    // debug()<<"now texting box with (x1,y1)--(x2,y2)
    // ("<<box_x1<<","<<box_y1<<")--("<<box_x2<<","<<box_y2<<")"<<endmsg; debug()<<"point to test is
    // ("<<x<<","<<y<<")"<<endmsg;

    if ( x < box_x1 || x > box_x2 ) return 0;
    if ( y < box_y1 || y > box_y2 ) return 0;
    return 1;
  }

  // to be replaced by inheritance/friend class
  Gaudi::Property<std::string> m_magnetPolarity{ this, "MagnetPolarity", "up", "Magnet Polarity" };
  Gaudi::Property<double>      m_ptKick{ this, "PtKick", 1200, "Pt Kick [MeV/c]" };
  Gaudi::Property<std::string> m_formulaCenterMagnet{ this, "FormulaZCenterMagnet", "",
                                                      "Formula for the z center of magnet parameterization" };
  Gaudi::Property<double>      m_valueCenterMagnet{ this, "ValueZCenterMagnet", 5200,
                                               "Value of the z center of magnet parameterization" };
  double                       computeZMagnetCenter( double qOverP );

  Gaudi::Property<bool> m_smear_calo_intercept{ this, "SmearCaloIntercept", false,
                                                "Smear the intercept between the object and the calorimeter face" };

  std::unique_ptr<LHCb::State> MCPropagationToState( LHCb::MCParticle* mcPart, double zProp,
                                                     LHCb::State::Location location );
  // implementation of calorimeter position smearing and missing energy from Z. Xu.
  std::tuple<double, double, double> get_Cali_PositionAndEnergy( double x, double y, double E, int region );
  Gaudi::Property<bool>              m_SmearCalorimeterPositionAndEnergy{ this, "SmearCaloPositionAndEnergy", false,
                                                             "flag to use calorimeter position smearing" };
  // energy calibration parameters
  Gaudi::Property<double> m_inner_energy_cali{ this, "InnerEcalEnergyLoss", 0.05481,
                                               "missed energy for inner ecal region" };
  Gaudi::Property<double> m_middle_energy_cali{ this, "MiddleEcalEnergyLoss", 0.025,
                                                "missed energy for middle ecal region" };
  Gaudi::Property<double> m_outer_energy_cali{ this, "OuterEcalEnergyLoss", 0.00253,
                                               "missed energy for outer ecal region" };

  // position calibration parameters

  Gaudi::Property<double> m_inner_position_b{ this, "EcalInnerPositionSmear_b", 3.264040,
                                              "inner ecal smearing parameter b" };
  Gaudi::Property<double> m_inner_position_f{ this, "EcalinnerPositionSmear_f", 1.369868,
                                              "inner ecal smearing parameter f" };
  Gaudi::Property<double> m_middle_position_b{ this, "EcalMiddlePositionSmear_b", 3.264040,
                                               "middle ecal smearing parameter b" };
  Gaudi::Property<double> m_middle_position_f{ this, "EcalMiddlePositionSmear_f", 1.369868,
                                               "middle ecal smearing parameter f" };
  Gaudi::Property<double> m_outer_position_b{ this, "EcalOuterPositionSmear_b", 3.264040,
                                              "outer ecal smearing parameter b" };
  Gaudi::Property<double> m_outer_position_f{ this, "EcalOuterPositionSmear_f", 1.369868,
                                              "outer ecal smearing parameter f" };
};
