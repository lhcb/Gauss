/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LAMARRTUPLE_H
#define LAMARRTUPLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Tuples.h"

/** @class LamarrTuple LamarrTuple.h
 *
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2015-10-20
 */
class LamarrTuple : public GaudiTupleAlg {
public:
  /// Standard constructor
  LamarrTuple( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_particles{ this, "MCFastParticleLocation", "/Event/MCFast/MCParticles",
                                            "Location in TES of output smeared MCParticles" };
  Gaudi::Property<std::string> m_vertices{ this, "MCFastVerticesLocation", "/Event/MCFast/MCVertices",
                                           "Location in TES of output smeared MCVertices" };
  Gaudi::Property<std::string> m_generatedParticles{ this, "MCParticleLocation", LHCb::MCParticleLocation::Default,
                                                     "Location in TES of output generated MCParticles" };
  Gaudi::Property<std::string> m_generatedVertices{ this, "MCVerticesLocation", LHCb::MCVertexLocation::Default,
                                                    "Location in TES of output generated MCVertices" };
  Gaudi::Property<std::string> m_neutral_proto_location{ this, "NeutralProtoLocation",
                                                         LHCb::ProtoParticleLocation::Neutrals,
                                                         "location of neutral protoparticles in tes" };

  ///< Location in TES of input HepMC events.

  // std::string m_particles;  ///< Location in TES of output smeared MCParticles.
  // std::string m_vertices;///< Location in TES of output smeared MCVertices.
  // std::string m_generatedParticles;//< Location in TES of output generated MCParticles.
  // std::string m_generatedVertices;//< Location in TES of output generated MCVertices.
  // std::string m_neutral_proto_location;//location of neutral protoparticles in tes.

  std::map<Long64_t, std::pair<const LHCb::MCParticle*, const LHCb::MCParticle*>>    m_Pions;
  std::map<Long64_t, const LHCb::MCParticle*>                                        m_GenPions;
  std::map<Long64_t, const LHCb::MCParticle*>                                        m_RecPions;
  std::map<Long64_t, const LHCb::MCParticle*>                                        m_GenPhoton;
  std::map<Long64_t, const LHCb::ProtoParticle*>                                     m_ClusteredPhoton;
  std::map<Long64_t, std::pair<const LHCb::MCParticle*, const LHCb::ProtoParticle*>> m_Photon;

  Tuple m_tupleGen{ nullptr };
  Tuple m_tupleRec{ nullptr };
  Tuple m_tupleRes{ nullptr };
  // Tuple tupleGenPhoton;
  // Tuple tupleRecPhoton;
  Tuple m_tupleResPhoton{ nullptr };
};
#endif // DELPHESHIST_H
