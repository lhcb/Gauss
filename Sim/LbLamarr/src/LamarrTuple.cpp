/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
// Event.
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "LamarrTuple.h"
#include "TLorentzVector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LamarrTuple
//
// 2015-10-20 : Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
// DECLARE_ALGORITHM_FACTORY( LamarrTuple )
DECLARE_COMPONENT( LamarrTuple )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LamarrTuple::LamarrTuple( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiTupleAlg( name, pSvcLocator ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode LamarrTuple::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  // must be executed first
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm
  m_tupleGen = nTuple( "Gen", "gen_Tuple" );
  info() << "created Gen tuple" << endmsg;
  m_tupleRec = nTuple( "Rec", "rec_Tuple" );
  info() << "created Rec tuple" << endmsg;
  m_tupleRes = nTuple( "Res", "res_Tuple" );
  info() << "created Res tuple" << endmsg;
  // needed?
  // m_tuplem_GenPhoton = nTuple("m_GenPhoton", "gen_Photon_Tuple");
  // info()<<"created m_GenPhoton tuple"<<endmsg;
  // m_tupleRecPhoton = nTuple("RecPhoton", "gen_Photon_Tuple");
  // info()<<"created RecPhoton tuple"<<endmsg;
  m_tupleResPhoton = nTuple( "ResPhoton", "res_Photon_Tuple" );
  info() << "created ResPhoton tuple" << endmsg;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LamarrTuple::execute() {

  StatusCode sc = StatusCode::SUCCESS;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  // Get the run and event number from the MC Header
  LHCb::MCHeader* evt = get<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default, IgnoreRootInTES );

  // Get TES container
  LHCb::MCParticles* m_genParticleContainer = get<LHCb::MCParticles>( m_generatedParticles );

  //  LHCb::MCVertices  *m_genVerticesContainer =
  //    get<LHCb::MCVertices>(m_generatedVertices);

  LHCb::MCParticles* m_particleContainer = get<LHCb::MCParticles>( m_particles );

  //  LHCb::MCVertices  *m_verticesContainer =
  //    get<LHCb::MCVertices>(m_vertices);

  LHCb::ProtoParticles* m_neutralProtos = get<LHCb::ProtoParticles>( m_neutral_proto_location );

  // containers for testing gen vs rec.
  // charged tracks.
  m_GenPions.clear();
  m_RecPions.clear();
  m_Pions.clear();
  // photons
  m_GenPhoton.clear();
  m_ClusteredPhoton.clear();
  m_Photon.clear();

  LHCb::MCParticles::const_iterator i;
  for ( const LHCb::MCParticle* P : *m_genParticleContainer ) {

    m_GenPions.insert( std::pair<Long64_t, const LHCb::MCParticle*>( P->key(), P ) );

    // LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex&   Vtx = P->originVertex();
    const LHCb::ParticleID& ID  = P->particleID();
    Double_t                x   = Vtx.position().X();
    Double_t                y   = Vtx.position().Y();
    Double_t                z   = Vtx.position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z();
    Double_t R     = TMath::Power( x, 2 ) + TMath::Power( y, 2 );
    R              = TMath::Sqrt( R );
    Double_t eta   = P->pseudoRapidity();
    Double_t phi   = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t px    = P->momentum().Px();
    Double_t py    = P->momentum().Py();
    Double_t pz    = P->momentum().Pz();
    Double_t tx    = px / pz;
    Double_t ty    = py / pz;
    Double_t p     = P->momentum().P();
    Int_t    Pid   = ID.pid();

    sc &= m_tupleGen->column( "runNumber", evt->runNumber() );
    sc &= m_tupleGen->column( "eventNumber", evt->evtNumber() );
    sc &= m_tupleGen->column( "p", p );
    sc &= m_tupleGen->column( "theta", theta );
    sc &= m_tupleGen->column( "tx", tx );
    sc &= m_tupleGen->column( "ty", ty );
    sc &= m_tupleGen->column( "x", x );
    sc &= m_tupleGen->column( "y", y );
    sc &= m_tupleGen->column( "z", z );
    sc &= m_tupleGen->column( "px", px );
    sc &= m_tupleGen->column( "py", py );
    sc &= m_tupleGen->column( "pz", pz );
    sc &= m_tupleGen->column( "phi", phi );
    sc &= m_tupleGen->column( "eta", eta );
    sc &= m_tupleGen->column( "pid", Pid );
    sc &= m_tupleGen->write();
    // separate photons as well.
    if ( abs( Pid ) == 22 ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "got a photon with Z = " << z << endmsg;
      m_GenPhoton.insert( std::pair<Long64_t, const LHCb::MCParticle*>( P->key(), P ) );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "inserted a photon with key" << P->key() << ", now adding the tuple info" << endmsg;
    }
  }

  // info()<<"GenSize: "<<m_GenPions.size()<<endmsg;
  for ( const LHCb::MCParticle* P : *m_particleContainer ) {

    // for( i=m_particleContainer->begin(); i != m_particleContainer->end(); i++ )
    // {
    m_RecPions.insert( std::pair<Long64_t, const LHCb::MCParticle*>( P->key(), P ) );
    // LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex*   Vtx = P->endVertices()[0];
    const LHCb::ParticleID& ID  = P->particleID();

    Double_t x = Vtx->position().X();
    Double_t y = Vtx->position().Y();
    Double_t z = Vtx->position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z();
    Double_t eta   = P->pseudoRapidity();
    Double_t phi   = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t R     = TMath::Power( x, 2 ) + TMath::Power( y, 2 );
    Double_t px    = P->momentum().Px();
    Double_t py    = P->momentum().Py();
    Double_t pz    = P->momentum().Pz();
    Double_t tx    = px / pz;
    Double_t ty    = py / pz;
    Double_t p     = P->momentum().P();
    Int_t    Pid   = ID.pid();

    R = TMath::Sqrt( R );
    sc &= m_tupleRec->column( "runNumber", evt->runNumber() );
    sc &= m_tupleRec->column( "eventNumber", evt->evtNumber() );
    sc &= m_tupleRec->column( "p", p );
    sc &= m_tupleRec->column( "theta", theta );
    sc &= m_tupleRec->column( "tx", tx );
    sc &= m_tupleRec->column( "ty", ty );
    sc &= m_tupleRec->column( "x", x );
    sc &= m_tupleRec->column( "y", y );
    sc &= m_tupleRec->column( "z", z );
    sc &= m_tupleRec->column( "px", px );
    sc &= m_tupleRec->column( "py", py );
    sc &= m_tupleRec->column( "pz", pz );
    sc &= m_tupleRec->column( "phi", phi );
    sc &= m_tupleRec->column( "eta", eta );
    sc &= m_tupleRec->column( "pid", Pid );
    sc &= m_tupleRec->write();
  }
  // info()<<"RecSize: "<<m_RecPions.size()<<endmsg;
  std::map<Long64_t, LHCb::MCParticle*>::iterator it;
  for ( const auto& [key, part] : m_GenPions ) {

    if ( m_RecPions.find( key ) != m_RecPions.end() )
      m_Pions.insert( std::make_pair( key, std::make_pair( part, m_RecPions[key] ) ) );
  }
  // info()<<"PionsSize: "<<Pions.size()<<endmsg;

  LHCb::MCParticle* P1;
  LHCb::MCParticle* P2;
  //  LHCb::ParticleID *ID1;
  //  LHCb::ParticleID *ID2;
  Double_t Px1, Py1, Pz1, Pt1, x1, y1, z1, Pid1, E1, M1;
  Double_t Px2, Py2, Pz2, Pt2, x2, y2, z2, Pid2, E2, M2;

  for ( const auto& [key, particles] : m_Pions ) {
    P1                           = (LHCb::MCParticle*)particles.first;
    P2                           = (LHCb::MCParticle*)particles.second;
    const LHCb::ParticleID& ID1  = P1->particleID();
    const LHCb::ParticleID& ID2  = P2->particleID();
    const LHCb::MCVertex&   Vtx1 = P1->originVertex();
    const LHCb::MCVertex*   Vtx2 = P2->endVertices()[0];

    x1   = Vtx1.position().X();
    y1   = Vtx1.position().Y();
    z1   = Vtx1.position().Z();
    Pid1 = ID1.pid();
    Px1  = P1->momentum().Px();
    Py1  = P1->momentum().Py();
    Pz1  = P1->momentum().Pz();
    Pt1  = P1->momentum().Pt();
    E1   = P1->momentum().E();
    M1   = P1->momentum().M();

    Double_t eta1   = P1->pseudoRapidity();
    Double_t phi1   = P1->momentum().Phi();
    Double_t theta1 = P1->momentum().Theta();

    x2          = Vtx2->position().X();
    y2          = Vtx2->position().Y();
    z2          = Vtx2->position().Z();
    Pid2        = ID2.pid();
    Px2         = P2->momentum().Px();
    Py2         = P2->momentum().Py();
    Pz2         = P2->momentum().Pz();
    Pt2         = P2->momentum().Pt();
    E2          = P2->momentum().E();
    M2          = P2->momentum().M();
    Double_t p1 = P1->momentum().P();
    Double_t p2 = P2->momentum().P();

    Double_t tx1 = Px1 / Pz1;
    Double_t ty1 = Py1 / Pz1;
    Double_t tx2 = Px2 / Pz2;
    Double_t ty2 = Py2 / Pz2;

    Double_t eta2   = P2->pseudoRapidity();
    Double_t phi2   = P2->momentum().Phi();
    Double_t theta2 = P2->momentum().Theta();

    sc &= m_tupleRes->column( "runNumber", evt->runNumber() );
    sc &= m_tupleRes->column( "eventNumber", evt->evtNumber() );
    sc &= m_tupleRes->column( "pDiff", p2 - p1 );
    sc &= m_tupleRes->column( "thetaDiff", theta2 - theta1 );
    sc &= m_tupleRes->column( "txDiff", tx2 - tx1 );
    sc &= m_tupleRes->column( "tyDiff", ty2 - ty1 );
    sc &= m_tupleRes->column( "xDiff", x2 - x1 );
    sc &= m_tupleRes->column( "yDiff", y2 - y1 );
    sc &= m_tupleRes->column( "zDiff", z2 - z1 );
    sc &= m_tupleRes->column( "pxDiff", Px2 - Px1 );
    sc &= m_tupleRes->column( "pyDiff", Py2 - Py1 );
    sc &= m_tupleRes->column( "pzDiff", Pz2 - Pz1 );
    sc &= m_tupleRes->column( "phiDiff", phi2 - phi1 );
    sc &= m_tupleRes->column( "etaDiff", eta2 - eta1 );
    sc &= m_tupleRes->column( "pidDiff", Pid2 - Pid1 );
    sc &= m_tupleRes->column( "pt1", Pt1 );
    sc &= m_tupleRes->column( "pt2", Pt2 );
    sc &= m_tupleRes->column( "e1", E1 );
    sc &= m_tupleRes->column( "m2", M2 );
    sc &= m_tupleRes->column( "m1", M1 );
    sc &= m_tupleRes->column( "e2", E2 );

    sc &= m_tupleRes->column( "pGen", p1 );
    sc &= m_tupleRes->column( "thetaGen", theta1 );
    sc &= m_tupleRes->column( "txGen", tx1 );
    sc &= m_tupleRes->column( "tyGen", ty1 );
    sc &= m_tupleRes->column( "xGen", x1 );
    sc &= m_tupleRes->column( "yGen", y1 );
    sc &= m_tupleRes->column( "zGen", z1 );
    sc &= m_tupleRes->column( "pxGen", Px1 );
    sc &= m_tupleRes->column( "pyGen", Py1 );
    sc &= m_tupleRes->column( "pzGen", Pz1 );
    sc &= m_tupleRes->column( "phiGen", phi1 );
    sc &= m_tupleRes->column( "etaGen", eta1 );
    sc &= m_tupleRes->column( "pidGen", Pid1 );

    sc &= m_tupleRes->column( "pRec", p2 );
    sc &= m_tupleRes->column( "thetaRec", theta2 );
    sc &= m_tupleRes->column( "txRec", tx2 );
    sc &= m_tupleRes->column( "tyRec", ty2 );
    sc &= m_tupleRes->column( "xRec", x2 );
    sc &= m_tupleRes->column( "yRec", y2 );
    sc &= m_tupleRes->column( "zRec", z2 );
    sc &= m_tupleRes->column( "pxRec", Px2 );
    sc &= m_tupleRes->column( "pyRec", Py2 );
    sc &= m_tupleRes->column( "pzRec", Pz2 );
    sc &= m_tupleRes->column( "phiRec", phi2 );
    sc &= m_tupleRes->column( "etaRec", eta2 );
    sc &= m_tupleRes->column( "pidRec", Pid2 );

    sc &= m_tupleRes->write();
  }

  LHCb::ProtoParticles::const_iterator thingy;
  for ( thingy = m_neutralProtos->begin(); thingy != m_neutralProtos->end(); thingy++ ) {
    m_ClusteredPhoton.insert( std::make_pair( ( *thingy )->key(), ( *thingy ) ) );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "adding clustered photon with key " << ( *thingy )->key() << endmsg;
    // m_tupleRecPhoton->column("energy",(*thingy)->calo().at(0)->e());
    // m_tupleRecPhoton->column("x",(*thingy)->calo().at(0)->position()->x());
    // m_tupleRecPhoton->column("y",(*thingy)->calo().at(0)->position()->y());
    // m_tupleRecPhoton->column("e",(*thingy)->calo().at(0)->e());
    // m_tupleRecPhoton->column("cov_xx"(*thingy)->calo().at(0)->covariance()
    // //m_tupleRecPhoton->column("recoKey",(*thingy)->key());
    // m_tupleRecPhoton->write();
  }
  for ( const auto& [key, itPhoton] : m_GenPhoton ) {

    auto itClusPhoton = std::find_if(
        m_ClusteredPhoton.begin(), m_ClusteredPhoton.end(),
        [&key, &itPhoton]( std::pair<Long64_t, const LHCb::ProtoParticle*> element ) { return element.first == key; } );
    if ( itClusPhoton != m_ClusteredPhoton.end() ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "found matching gen Photon" << key << " <--> " << ( *itClusPhoton ).first << endmsg;
      m_Photon.insert( std::make_pair( key, std::make_pair( itPhoton, itClusPhoton->second ) ) );
    }
  }

  // //gen vs clustered photons matched. Write tuple.
  for ( const auto& [key, photons] : m_Photon ) {

    sc &= m_tupleResPhoton->column( "runNumber", evt->runNumber() );
    sc &= m_tupleResPhoton->column( "eventNumber", evt->evtNumber() );
    sc &= m_tupleResPhoton->column( "photon_key", key ); // probably don't need this, but whatever
    // gen vs rec info
    sc &= m_tupleResPhoton->column( "eGen", photons.first->momentum().P() );
    sc &= m_tupleResPhoton->column( "pxGen", photons.first->momentum().Px() );
    sc &= m_tupleResPhoton->column( "pyGen", photons.first->momentum().Py() );
    sc &= m_tupleResPhoton->column( "pzGen", photons.first->momentum().Pz() );
    // take xyz at production, then propagate to the ecal face.
    double x0    = photons.first->originVertex()->position().x();
    double y0    = photons.first->originVertex()->position().y();
    double z0    = photons.first->originVertex()->position().z();
    double genTx = photons.first->momentum().Px() / photons.first->momentum().Pz();
    double genTy = photons.first->momentum().Py() / photons.first->momentum().Pz();
    sc &= m_tupleResPhoton->column( "txGen", genTx );
    sc &= m_tupleResPhoton->column( "tyGen", genTy );
    sc &= m_tupleResPhoton->column( "x0Gen", x0 );
    sc &= m_tupleResPhoton->column( "y0Gen", y0 );
    sc &= m_tupleResPhoton->column( "z0Gen", z0 );
    // propagate to the final place
    // need x,y,z,tx,ty for
    auto   clus_pos = photons.second->calo().at( 0 )->position();
    double x        = clus_pos->x();
    double y        = clus_pos->y();
    double z        = clus_pos->z();
    double xGenCalo = x0 + genTx * ( z - z0 );
    double yGenCalo = y0 + genTy * ( z - z0 );
    sc &= m_tupleResPhoton->column( "xGenCalo", xGenCalo );
    sc &= m_tupleResPhoton->column( "yGenCalo", yGenCalo );
    sc &= m_tupleResPhoton->column( "zGenCalo", z );

    // auto ev = (*itResPhoton).second.first->endVertices()[0];
    // info()<<"**************** checking end vertex in tuple *****************************"<<endmsg;
    // ev->fillStream(std::cout);
    // std::cout<<std::endl;
    // info()<<"**************************************************************************"<<endmsg;
    double ebase = photons.second->calo().at( 0 )->e() / sqrt( x * x + y * y + z * z );
    // taken from Melody Ravonel's Low Multiplicity reco. used in px,py,pz
    sc &= m_tupleResPhoton->column( "eRec", photons.second->calo().at( 0 )->e() );
    sc &= m_tupleResPhoton->column( "pxRec", ebase * x );
    sc &= m_tupleResPhoton->column( "pyRec", ebase * y );
    sc &= m_tupleResPhoton->column( "pzRec", ebase * z );
    sc &= m_tupleResPhoton->column( "txRec", x / z );
    sc &= m_tupleResPhoton->column( "tyRec", y / z );
    sc &= m_tupleResPhoton->column( "xRec", x );
    sc &= m_tupleResPhoton->column( "yRec", y );
    sc &= m_tupleResPhoton->column( "zRec", z );

    sc &= m_tupleResPhoton->column( "RecCovXX", clus_pos->covariance()( 0, 0 ) );
    sc &= m_tupleResPhoton->column( "RecCovYY", clus_pos->covariance()( 1, 1 ) );
    sc &= m_tupleResPhoton->column( "RecCovEE", clus_pos->covariance()( 2, 2 ) );
    sc &= m_tupleResPhoton->column( "RecCovXY", clus_pos->covariance()( 0, 1 ) );
    sc &= m_tupleResPhoton->column( "RecCovXE", clus_pos->covariance()( 0, 2 ) );
    sc &= m_tupleResPhoton->column( "RecCovYE", clus_pos->covariance()( 1, 2 ) );
    auto seed = photons.second->calo().at( 0 )->clusters().at( 0 )->seed();
    sc &= m_tupleResPhoton->column( "RecSeedRegion", seed.area() );
    sc &= m_tupleResPhoton->column( "RecSeedRow", seed.row() );
    sc &= m_tupleResPhoton->column( "RecSeedCol", seed.col() );

    sc &= m_tupleResPhoton->write();
  }

  return StatusCode::SUCCESS;
}
