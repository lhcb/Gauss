/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// GaudiKernel
#include "GaudiKernel/IInterface.h"

class G4VPhysicalVolume;

// ============================================================================
// Minimal geometry source interface to GaussGeo
//
// 2015-11-11 : Dmitry Popov
// ============================================================================

class IGaussGeo : virtual public IInterface {
public:
  // Retrieve unique interface ID
  static const InterfaceID& interfaceID();

  // Retrieve the pointer to the G4 geometry tree root
  virtual G4VPhysicalVolume* world() = 0;
};
