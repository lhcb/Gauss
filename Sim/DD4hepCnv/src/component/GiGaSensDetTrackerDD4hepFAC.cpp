/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "GiGaMTDetFactories/GiGaMTG4SensDetFactory.h"

// Geant4 physics lists
#include "GaussTracker/GiGaSensDetTracker.h"

#include "DDG4/Geant4Mapping.h"
#include "DDG4/Geant4TouchableHandler.h"

class GiGaSensDetTrackerDD4hep : public GiGaSensDetTracker {
public:
  using GiGaSensDetTracker::GiGaSensDetTracker;

protected:
  virtual int GetDetectorID( G4Step*, G4TouchableHistory* ) const override final;
};

int GiGaSensDetTrackerDD4hep::GetDetectorID( G4Step* step, G4TouchableHistory* ) const {
  auto&                               g4map = dd4hep::sim::Geant4Mapping::instance();
  dd4hep::sim::Geant4TouchableHandler touchable_handler{ step };
  auto                                id = g4map.volumeManager().volumeID( touchable_handler.touchable );
  return id;
}

DECLARE_COMPONENT_WITH_ID( GiGaSensDetTrackerFAC<GiGaSensDetTrackerDD4hep>, "GiGaSensDetTrackerDD4hep" )
