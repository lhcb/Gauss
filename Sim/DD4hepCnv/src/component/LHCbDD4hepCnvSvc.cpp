/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GiGaMTDD4hep/DD4hepCnvSvc.h"
#include "LbDD4hep/IDD4hepSvc.h"

// ============================================================================
// Conversion service for convertion DD4hep geometry from LHCb to Geant4
//
// 2020-6-15 : Dominik Muller
// ============================================================================

class LHCbDD4hepCnvSvc : public DD4hepCnvSvc {
private:
  using DD4hepCnvSvc::DD4hepCnvSvc;
  ServiceHandle<LHCb::Det::LbDD4hep::IDD4hepSvc> m_dd4Svc{ this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc" };

protected:
  virtual const dd4hep::Detector& getDetector() const override;

public:
  virtual StatusCode initialize() override;
};

DECLARE_COMPONENT( LHCbDD4hepCnvSvc )

StatusCode LHCbDD4hepCnvSvc::initialize() {
  auto sc = m_dd4Svc.retrieve();
  sc &= DD4hepCnvSvc::initialize();
  return sc;
}

const dd4hep::Detector& LHCbDD4hepCnvSvc::getDetector() const { return dd4hep::Detector::getInstance(); }
