/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @class TrackerHit TrackerHit.h component/TrackerHit.h
 *
 *
 *  @author Witold POKORSKI
 *  @author Gloria CORTI
 *  @author Dominik MULLER
 *  @date   16/05/2002
 *  @date   26/07/2006
 *  @date   18/12/2018 (last modified)
 */

#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "GiGaMTCoreDet/GaussHitBase.h"

class TrackerHit : public Gaussino::HitBase {
public:
  TrackerHit()          = default;
  virtual ~TrackerHit() = default;
  TrackerHit( const TrackerHit& right ) : Gaussino::HitBase( right ) {
    m_edep         = right.m_edep;
    m_entryPos     = right.m_entryPos;
    m_exitPos      = right.m_exitPos;
    m_timeOfFlight = right.m_timeOfFlight;
    m_momentum     = right.m_momentum;
  }
  inline const TrackerHit& operator=( const TrackerHit& right );
  int                      operator==( const TrackerHit& ) const { return 0; };

  inline void* operator new( size_t );
  inline void operator delete( void* aHit );

  void Draw() override;
  void Print() override;

private:
  G4ThreeVector m_entryPos;
  G4ThreeVector m_exitPos;
  G4double      m_timeOfFlight;
  G4double      m_edep;
  G4ThreeVector m_momentum;

public:
  inline void     SetEdep( G4double de ) { m_edep = de; }
  inline G4double GetEdep() { return m_edep; }

  inline void          SetEntryPos( G4ThreeVector xyz ) { m_entryPos = xyz; }
  inline G4ThreeVector GetEntryPos() { return m_entryPos; }

  inline void          SetExitPos( G4ThreeVector xyz ) { m_exitPos = xyz; }
  inline G4ThreeVector GetExitPos() { return m_exitPos; }

  inline void     SetTimeOfFlight( G4double tof ) { m_timeOfFlight = tof; }
  inline G4double GetTimeOfFlight() { return m_timeOfFlight; }

  inline void          SetMomentum( G4ThreeVector p ) { m_momentum = p; }
  inline G4ThreeVector GetMomentum() { return m_momentum; }
};

typedef G4THitsCollection<TrackerHit> TrackerHitsCollection;

extern G4ThreadLocal G4Allocator<TrackerHit>* aTrackerHitAllocator;

inline void* TrackerHit::operator new( size_t ) {
  if ( !aTrackerHitAllocator ) { aTrackerHitAllocator = new G4Allocator<TrackerHit>; }
  return (void*)aTrackerHitAllocator->MallocSingle();
}

inline void TrackerHit::operator delete( void* aHit ) { aTrackerHitAllocator->FreeSingle( (TrackerHit*)aHit ); }
