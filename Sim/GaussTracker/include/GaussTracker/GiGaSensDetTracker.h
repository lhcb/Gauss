/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "G4VSensitiveDetector.hh"
#include "GaussTracker/TrackerHit.h"
#include "GiGaMTCoreMessage/IGiGaMessage.h"
#include "GiGaMTCoreRun/GaussinoTrackInformation.h"

// Forward declarations
class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

/** @class GiGaSensDetTracker GiGaSensDetTracker.h
 *
 *  Fill information in TrackerHits to be later stored
 *  in MCHits (entry point and displacement from exit point in default
 *  setup, displacement with momentum directions in alternative setup).
 *
 *  @author Witek POKORSKI
 *  @author Gloria CORTI
 *  @author Dominik Muller
 *  @date   2018-12-18 (last revision)
 */
class GiGaSensDetTracker : public G4VSensitiveDetector, public virtual GiGaMessage {
public:
  GiGaSensDetTracker( const std::string& name );
  virtual ~GiGaSensDetTracker() = default;

  /** Initialize method (Geant4).
   *  Called at the beginning of each event
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void Initialize( G4HCofThisEvent* HCE ) override;

  /** Process a hit (Geant4).
   *  The method is invoked by G4 for each step in the
   *  sensitive detector.
   *  This implementation stores information in TrackerHit
   *  to be later saved in MCHit for a tracker detector:
   *  the hits are saved for charged particles only and if
   *  the step and dE/dx in the step are non-zero.
   *  The default implementation stores entry point,
   *  displacement (as exit-entry), dE/Dx in the step,
   *  |P| of the particle and Time at entry point.
   *  @see   G4VSensitiveDetector
   *  @param step     pointer to current Geant4 step
   *  @param history  pointer to touchable history
   */
  bool ProcessHits( G4Step* step, G4TouchableHistory* history ) override final;

  // Flag to control storing of hits if track deposited energy or not
  void SetRequireEDep( bool val = true ) { m_requireEDep = val; }

protected:
  virtual int GetDetectorID( G4Step*, G4TouchableHistory* ) const { return -1; }

private:
  GiGaSensDetTracker();                                       ///< no default constructor
  GiGaSensDetTracker( const GiGaSensDetTracker& );            ///< no copy constructor
  GiGaSensDetTracker& operator=( const GiGaSensDetTracker& ); ///< no =

  /// Pointer to G4 collection for this sensitive detector
  TrackerHitsCollection* m_trackerCol;

  /// Flag to store hits if dE/dx occured, this is to enable hits when
  /// switching it off for special studies (like with geantinos)
  bool m_requireEDep = true;
};

#include "GiGaMTDetFactories/GiGaMTG4SensDetFactory.h"

template <typename T>
using TrackerBaseFAC = GiGaMTG4SensDetFactory<T>;

template <typename T>
class GiGaSensDetTrackerFAC : public TrackerBaseFAC<T> {
  static_assert( std::is_base_of<GiGaSensDetTracker, T>::value );

  Gaudi::Property<bool> m_requireEDep{ this, "RequireEDep", true, "Hits must have non-zero Energy deposition" };

public:
  using base_class = TrackerBaseFAC<T>;
  using base_class::base_class;
  T* construct() const override {
    auto tmp = base_class::construct();
    tmp->SetRequireEDep( m_requireEDep );
    return tmp;
  }
};
