/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//=============================================================================
// Constructor
//=============================================================================
TrackerHit::TrackerHit( const TrackerHit& right ) : Gaussino::HitBase( right ) {
  m_edep         = right.m_edep;
  m_entryPos     = right.m_entryPos;
  m_exitPos      = right.m_exitPos;
  m_timeOfFlight = right.m_timeOfFlight;
  m_momentum     = right.m_momentum;
}

//=============================================================================
// Accessors for energy deposition
//=============================================================================
void TrackerHit::SetEdep( G4double de ) { m_edep = de; }

G4double TrackerHit::GetEdep() { return m_edep; }

//=============================================================================
// Accessors for entry point
//=============================================================================
void TrackerHit::SetEntryPos( G4ThreeVector xyz ) { m_entryPos = xyz; }

G4ThreeVector TrackerHit::GetEntryPos() { return m_entryPos; }

//=============================================================================
// Accessors for exit point
//=============================================================================
void TrackerHit::SetExitPos( G4ThreeVector xyz ) { m_exitPos = xyz; }

G4ThreeVector TrackerHit::GetExitPos() { return m_exitPos; }

//=============================================================================
// Accessors for time of flight at entry point
//=============================================================================
void TrackerHit::SetTimeOfFlight( G4double tof ) { m_timeOfFlight = tof; }

G4double TrackerHit::GetTimeOfFlight() { return m_timeOfFlight; }

//=============================================================================
// Accessors for momentum at entry point
//=============================================================================
void TrackerHit::SetMomentum( G4ThreeVector p ) { m_momentum = p; }

G4ThreeVector TrackerHit::GetMomentum() { return m_momentum; }
