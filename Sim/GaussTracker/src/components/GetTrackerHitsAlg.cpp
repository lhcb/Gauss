/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaussTracker/TrackerHit.h"

#include "Defaults/Locations.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "DetDesc/DetectorElement.h"
#include "Event/MCExtendedHit.h"
#include "Event/MCHit.h"

#ifdef USE_DD4HEP
#  include "LbDD4hep/ConditionAccessorHolder.h"
#endif

#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiAlg/Transformer.h"
#include "GiGaMTCoreRun/G4EventProxy.h"
#include "GiGaMTCoreRun/MCTruthConverter.h"
#include "MCTruthToEDM/LinkedParticleMCParticleLink.h"

#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"

#include <vector>

namespace {

  /**
   *  @author Gloria CORTI
   *  @date   2005-10-02
   *  @author Dominik Muller
   *  @date   2018-04-05
   *
   * Base class for the different GetTrackerHitsAlg algorithms
   */
  template <bool HasDetector>
  class GetTrackerHitsAlgBase : public FixTESPath<Gaudi::Algorithm> {
  public:
    using FixTESPath::FixTESPath;
    Gaudi::Property<bool>        m_extendedInfo{ this, "ExtendedInfo", false,
                                          "Flag to control filling of MCExtendedHits instead of MCHits (def = false)" };
    Gaudi::Property<std::string> m_colName{ this, "CollectionName", "",
                                            "Name of Geant4 collection where to retrieve hits" };

  protected:
    StatusCode init();
    template <typename DE>
    LHCb::MCHits main( const G4EventProxies&, const LinkedParticleMCParticleLinks&, DE const* = nullptr ) const;

  private:
    template <typename DE>
    void fillHit( TrackerHit* g4Hit, LHCb::MCHit* mcHit, const Gaussino::MCTruth* mctruth,
                  const LinkedParticleMCParticleLinks& mcplinks, DE const* detector ) const;
  };

  template <bool HasDetector>
  StatusCode GetTrackerHitsAlgBase<HasDetector>::init() {
    if ( "" == m_colName ) {
      this->fatal() << "Property CollectionName needs to be set! " << endmsg;
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }

  template <bool HasDetector>
  template <typename DE>
  LHCb::MCHits GetTrackerHitsAlgBase<HasDetector>::main( G4EventProxies const&                eventproxies,
                                                         LinkedParticleMCParticleLinks const& mcp_links,
                                                         DE const*                            detector ) const {
    LHCb::MCHits hits;
    for ( auto& ep : eventproxies ) {
      auto hitCollection = ep->GetHitCollection<TrackerHitsCollection>( m_colName );
      if ( !hitCollection ) {
        this->warning() << "The hit collection='" + m_colName + "' is not found!" << endmsg;
        continue;
      }
      // reserve elements on output container
      int numOfHits = hitCollection->entries();
      // tranform G4Hit into MCHit and insert it in container
      for ( int iG4Hit = 0; iG4Hit < numOfHits; ++iG4Hit ) {
        // create hit or extended hit depending on choice
        if ( m_extendedInfo ) {
          LHCb::MCExtendedHit* newHit = new LHCb::MCExtendedHit();
          fillHit( ( *hitCollection )[iG4Hit], newHit, ep->truth(), mcp_links, detector );
          Gaudi::XYZVector mom( ( *hitCollection )[iG4Hit]->GetMomentum() );
          newHit->setMomentum( mom );
          hits.add( newHit );
        } else {
          LHCb::MCHit* newHit = new LHCb::MCHit();
          fillHit( ( *hitCollection )[iG4Hit], newHit, ep->truth(), mcp_links, detector );
          hits.add( newHit );
        }
      }
    }
    return hits;
  }

  template <bool HasDetector>
  template <typename DE>
  void GetTrackerHitsAlgBase<HasDetector>::fillHit( TrackerHit* g4Hit, LHCb::MCHit* mcHit,
                                                    const Gaussino::MCTruth*             mctruth,
                                                    const LinkedParticleMCParticleLinks& mcplinks,
                                                    DE const*                            detector ) const {
    // fill data members
    Gaudi::XYZPoint entry( g4Hit->GetEntryPos() );
    Gaudi::XYZPoint exit( g4Hit->GetExitPos() );
    mcHit->setEntry( entry );
    mcHit->setDisplacement( exit - entry );
    mcHit->setEnergy( g4Hit->GetEdep() );
    mcHit->setTime( g4Hit->GetTimeOfFlight() );
    mcHit->setP( g4Hit->GetMomentum().mag() );

    // get sensitive detector from the TrackerHit
    int detID = g4Hit->GetDetectorID();
    // If DetDesc detectors were specified, search the list of detectors
    if constexpr ( HasDetector ) {
      if ( detector && detector->isInside( mcHit->midPoint() ) ) {
        try {
          detID = detector->sensitiveVolumeID( mcHit->midPoint() );
        } catch ( GaudiException& e ) {
          if ( e.tag() == "DeMuonDetector" && e.message() == "No Gap found" ) {
            this->error() << "[DeMuonDetector] Gap not found!" << endmsg;
          } else {
            throw;
          }
        }
      }
    }
    mcHit->setSensDetID( detID );

    // fill reference to MCParticle using the Geant4->MCParticle table
    int trackID = g4Hit->GetTrackID();
    if ( auto lp = mctruth->GetParticleFromTrackID( trackID ); lp ) {
      if ( auto it = mcplinks.find( lp ); it != std::end( mcplinks ) ) {
        mcHit->setMCParticle( it->second );
      } else {
        this->warning() << "No pointer to MCParticle for MCHit associated to G4 trackID: " << trackID << endmsg;
      }
    } else {
      this->warning() << "No LinkedParticle found. Something went seriously wrong. trackID: " << trackID << endmsg;
    }
  }

} // namespace

namespace LHCb {

  /**
   * GetTrackerHitsAlg algorithm, version with no detector element associated
   */
  struct GetTrackerHitsAlg
      : Gaudi::Functional::Transformer<LHCb::MCHits( G4EventProxies const&, LinkedParticleMCParticleLinks const& ),
                                       Gaudi::Functional::Traits::BaseClass_t<GetTrackerHitsAlgBase<false>>> {
    GetTrackerHitsAlg( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer(
              name, pSvcLocator,
              { { "Input", Gaussino::G4EventsLocation::Default },
                { "LinkedParticleMCParticleLinks", Gaussino::LinkedParticleMCParticleLinksLocation::Default } },
              { "MCHitsLocation", "" } ) {}

    virtual LHCb::MCHits operator()( G4EventProxies const& p, LinkedParticleMCParticleLinks const& l ) const override {
      return main<void>( p, l );
    }

    virtual StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] { return init(); } );
    }
  };

  DECLARE_COMPONENT_WITH_ID( GetTrackerHitsAlg, "GetTrackerHitsAlg" )

  using DetDescBase = DetDesc::usesBaseAndConditions<GetTrackerHitsAlgBase<true>, DetectorElementPlus>;

#ifdef USE_DD4HEP
  using DD4hepBase = Det::LbDD4hep::usesBaseAndConditions<GetTrackerHitsAlgBase<true>, LHCb::Detector::DeIOV>;
#endif

  template <typename TBase, typename DE>
  using Transformer = Gaudi::Functional::Transformer<
      LHCb::MCHits( G4EventProxies const&, LinkedParticleMCParticleLinks const&, DE const& ), TBase>;

  /**
   *  @author Gloria CORTI
   *  @date   2005-10-02
   *  @author Dominik Muller
   *  @date   2018-04-05
   *
   * GetTrackerHitsAlg algorithm, version with a detector element associated
   */
  template <typename TBase, typename DE>
  class GetTrackerHitsAlgWithDet : public Transformer<TBase, DE> {
  public:
    /// Standard constructor
    GetTrackerHitsAlgWithDet( std::string const& name, ISvcLocator* pSvcLocator )
        : Transformer<TBase, DE>(
              name, pSvcLocator,
              { { "Input", Gaussino::G4EventsLocation::Default },
                { "LinkedParticleMCParticleLinks", Gaussino::LinkedParticleMCParticleLinksLocation::Default },
                { "Detector", "InvalidDetectorPath" } },
              { "MCHitsLocation", "" } ) {}

    virtual LHCb::MCHits operator()( const G4EventProxies& p, const LinkedParticleMCParticleLinks& l,
                                     DE const& d ) const override {
      return this->main( p, l, &d );
    }

    virtual StatusCode initialize() override {
      return Transformer<TBase, DE>::initialize().andThen( [&] { return this->init(); } );
    }
  };

  // Declaration of the Algorithm Factory
  using GetTrackerHitsAlgDetDesc = GetTrackerHitsAlgWithDet<DetDescBase, DetectorElementPlus>;
  DECLARE_COMPONENT_WITH_ID( GetTrackerHitsAlgDetDesc, "GetTrackerHitsAlgDetDesc" )

#ifdef USE_DD4HEP
  using GetTrackerHitsAlgDD4hep = GetTrackerHitsAlgWithDet<DD4hepBase, LHCb::Detector::DeIOV>;
  DECLARE_COMPONENT_WITH_ID( GetTrackerHitsAlgDD4hep, "GetTrackerHitsAlgDD4hep" )
#endif

} // namespace LHCb
