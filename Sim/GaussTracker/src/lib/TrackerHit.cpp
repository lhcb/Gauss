/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : TrackerHit
//
// 16/05/2002 : Witold POKORSKI
// 25/07/2006 : Gloria CORTI
// 18/12/2018 : Dominik Muller
//-----------------------------------------------------------------------------

// Include files
// from G4
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

// local
#include "GaussTracker/TrackerHit.h"

G4ThreadLocal G4Allocator<TrackerHit>* aTrackerHitAllocator = nullptr;

//=============================================================================
// Draw
//=============================================================================
void TrackerHit::Draw() {

  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if ( pVVisManager ) {
    G4Circle circle( ( m_exitPos + m_entryPos ) * 0.5 );
    circle.SetScreenSize( 5. );
    circle.SetFillStyle( G4Circle::filled );
    G4Colour        colour( 1., 0., 0.5 );
    G4VisAttributes attribs( colour );
    circle.SetVisAttributes( attribs );

    pVVisManager->Draw( circle );
  }
}

//=============================================================================
// Print
//=============================================================================
void TrackerHit::Print() {
  std::cout << std::endl;
  std::cout << "Printing TrackerHit:"
            << " entryPos=(" << this->m_entryPos.x() << "," << this->m_entryPos.y() << "," << this->m_entryPos.z()
            << ")"
            << " displacement=(" << this->m_exitPos.x() << "," << this->m_exitPos.y() << "," << this->m_exitPos.z()
            << ")"
            << " tof=" << this->m_timeOfFlight << " edep=" << this->m_edep << " momentum=(" << this->m_momentum.x()
            << "," << this->m_momentum.y() << "," << this->m_momentum.z() << ")" << std::endl;
}
