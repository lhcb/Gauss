/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaSensDetTracker.cpp,v 1.14 2009-06-10 16:57:25 gcorti Exp $
// Include files

// from CLHEP
#include "CLHEP/Geometry/Point3D.h"

// from Gaudi
#include "GaudiKernel/MsgStream.h"

// from Geant4
#include "G4HCofThisEvent.hh"
#include "G4LogicalVolume.hh"
#include "G4SDManager.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4Track.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ios.hh"

// local
#include "GaussTracker/GiGaSensDetTracker.h"
#include "GaussTracker/TrackerHit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GiGaSensDetTracker
//
// 2006-07-14 : Gloria CORTI (clean up)
//-----------------------------------------------------------------------------

GiGaSensDetTracker::GiGaSensDetTracker( const std::string& name ) : G4VSensitiveDetector( name ) {
  collectionName.insert( "Hits" );
}

void GiGaSensDetTracker::Initialize( G4HCofThisEvent* HCE ) {
  m_trackerCol = new TrackerHitsCollection( SensitiveDetectorName, collectionName[0] );

  std::string hit_location = SensitiveDetectorName + "/" + collectionName[0];

  debug( "Registering location at " + hit_location );
  int HCID = G4SDManager::GetSDMpointer()->GetCollectionID( hit_location );

  HCE->AddHitsCollection( HCID, m_trackerCol );
}

bool GiGaSensDetTracker::ProcessHits( G4Step* step, G4TouchableHistory* history ) {
  if ( 0 == step ) { return false; }

  G4Track* track  = step->GetTrack();
  G4double charge = track->GetDefinition()->GetPDGCharge();

  if ( charge == 0.0 ) { return false; } // fill hits only for charged tracks

  // If required to have energy deposition check
  double edep = step->GetTotalEnergyDeposit();
  if ( m_requireEDep && ( edep <= 0.0 ) ) { return false; }

  if ( step->GetStepLength() != 0.0 ) { // step must be finite

    debug( "Filling a hit" );

    G4ThreeVector prepos = step->GetPreStepPoint()->GetPosition();
    double        timeof = step->GetPreStepPoint()->GetGlobalTime();
    G4ThreeVector premom = step->GetPreStepPoint()->GetMomentum();

    TrackerHit* newHit = new TrackerHit();
    newHit->SetEdep( edep );
    newHit->SetEntryPos( prepos );
    newHit->SetTimeOfFlight( timeof );
    newHit->SetMomentum( premom );

    // Store exit point
    G4ThreeVector postpos = step->GetPostStepPoint()->GetPosition();
    newHit->SetExitPos( postpos );

    // Set id to track
    int trid = track->GetTrackID();
    newHit->SetTrackID( trid );

    // Get the hit ID. Default implementation just sets it to -1
    // but derived classes can overwrite this behaviour
    newHit->SetDetectorID( GetDetectorID( step, history ) );

    auto gi = GaussinoTrackInformation::Get( track );
    gi->setCreatedHit( true );
    gi->setToStoreTruth( true );
    gi->addHit( newHit );

    // add hit to collection
    m_trackerCol->insert( newHit );
    return true;
  }

  return false;
}
