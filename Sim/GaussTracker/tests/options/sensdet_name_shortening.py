###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This is a very specific configuration in order to trigger a debug message
# with a shortened name of the sensitive detector. In order to do so, in
# Gauss-on-Gaussino it will only be triggered by running construct() on the
# GiGaSensDetTracker. This happens usually in Geo services or directly in the
# detector construction factory in Gaussino, so the configuration had to be
# extended.

from Configurables import ExternalDetectorEmbedder, GaussGeometry
from ExternalDetector.Materials import OUTER_SPACE
from Gaudi.Configuration import DEBUG, importOptions

# All the rest provides a minimum, the fastest working example
# for this test
from GaudiKernel import SystemOfUnits as units

importOptions("$GAUSSOPTS/General/Events-1.py")
importOptions("$GAUSSOPTS/General/2018.py")
importOptions("$GAUSSOPTS/Geometry/NoDetectors.py")
importOptions("$GAUSSOPTS/Generation/ParticleGun-FixedMomentum-Photon1GeV.py")

# select a simple Geo service from Gaussino
# not relevant for the test, but needed for detector FAC
GaussGeometry().ExternalDetectorEmbedder = "ExternalGeoSvc"
geo = ExternalDetectorEmbedder("ExternalGeoSvc")

geo.Shapes = {
    "Tracker": {
        "xSize": 1 * units.m,
        "ySize": 1 * units.m,
        "zSize": 1 * units.m,
        "Type": "Cuboid",
    },
}

geo.Sensitive = {
    "Tracker": {
        "Type": "GiGaSensDetTracker",  # here the tracker is chosen
        "SensDetName": "Hello",
        "OutputLevel": DEBUG,
    },
}

geo.World = {
    "WorldMaterial": "Vacuum",
    "Type": "ExternalWorldCreator",
}

geo.Materials = {
    "Vacuum": OUTER_SPACE,
}

from GaudiPython import AppMgr

AppMgr().initialize()
