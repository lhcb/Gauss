/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaTrackActionSequence.cpp,v 1.6 2007-01-12 15:36:57 ranjard Exp $
// Include files

/// STD & STL
#include <algorithm>
#include <functional>

// from Gaudi

// GiGa
#include "GiGa/GiGaUtil.h"
#include "GiGa/IGiGaSvc.h"

// local
#include "GiGaTrackActionSequence.h"

// ============================================================================
/** @file
 *
 *  Implementation file for class : GiGaTrackActionSequence
 *  @date 24/07/2001
 *  @author Vanya  Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( GiGaTrackActionSequence )

// ============================================================================
/** standard constructor
 *  @see GiGaTrackActionBase
 *  @see GiGaBase
 *  @see AlgTool
 *  @param type type of the object (?)
 *  @param name name of the object
 *  @param parent  pointer to parent object
 */
// ============================================================================
GiGaTrackActionSequence::GiGaTrackActionSequence( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : GiGaTrackActionBase( type, name, parent )
    , m_members() ///< default empty vertor!
    , m_actions()
    , m_setMgr( false ) {
  declareProperty( "Members", m_members );
}

// ============================================================================
/// destructor
// ============================================================================
GiGaTrackActionSequence::~GiGaTrackActionSequence() {}

// ============================================================================
/** initialise the action object
 *  @return status code
 */
// ============================================================================
StatusCode GiGaTrackActionSequence::initialize() {
  StatusCode sc = GiGaTrackActionBase::initialize();
  if ( sc.isFailure() ) { return Error( "Could not initialize the base class!", sc ); }
  if ( m_members.empty() ) { Print( "The sequence is empty." ).ignore(); }
  // instantiate members
  m_actions.clear();
  std::string Type, Name;
  for ( MEMBERS::const_iterator member = m_members.begin(); m_members.end() != member; ++member ) {
    IGiGaTrackAction* action = tool<IGiGaTrackAction>( *member, this );
    if ( 0 == action ) { return Error( "Could not create IGiGaTrackAction '" + *member + "'" ); }
    m_actions.push_back( action );
  }
  //
  return Print( "Iinitialized successfully", StatusCode::SUCCESS, MSG::VERBOSE );
}

// ============================================================================
/** perform the pre-action
 *  @param track pointer to Geant4 track object
 */
// ============================================================================
void GiGaTrackActionSequence::PreUserTrackingAction( const G4Track* track ) {
  // set the tracking manager for all members
  if ( !m_setMgr ) {
    for ( ACTIONS::iterator iaction = m_actions.begin(); m_actions.end() != iaction; ++iaction ) {
      G4TrackingManager* mgr    = trackMgr();
      IGiGaTrackAction*  action = *iaction;
      if ( 0 != action ) { action->SetTrackingManagerPointer( mgr ); }
    }
    m_setMgr = true;
  }
  // tracking actions of all members
  for ( ACTIONS::iterator iaction = m_actions.begin(); m_actions.end() != iaction; ++iaction ) {
    IGiGaTrackAction* action = *iaction;
    if ( 0 != action ) { action->PreUserTrackingAction( track ); }
  }
}

// ============================================================================
/** perform the post-action
 *  @param track pointer to Geant4 track object
 */
// ============================================================================
void GiGaTrackActionSequence::PostUserTrackingAction( const G4Track* track ) {
  // set the tracking manager for all members
  if ( !m_setMgr ) {
    for ( ACTIONS::iterator iaction = m_actions.begin(); m_actions.end() != iaction; ++iaction ) {
      G4TrackingManager* mgr    = trackMgr();
      IGiGaTrackAction*  action = *iaction;
      if ( 0 != action ) { action->SetTrackingManagerPointer( mgr ); }
    }
    m_setMgr = true;
  }
  // tracking actions of all members
  for ( ACTIONS::iterator iaction = m_actions.begin(); m_actions.end() != iaction; ++iaction ) {
    IGiGaTrackAction* action = *iaction;
    if ( 0 != action ) { action->PostUserTrackingAction( track ); }
  }
}

// ============================================================================
// The END
// ============================================================================
