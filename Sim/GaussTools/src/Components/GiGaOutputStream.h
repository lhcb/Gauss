/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GIGA_GIGAOutputSTREAM_H
#  define GIGA_GIGAOutputSTREAM_H 1

// include
// local (GiGa)
#  include "GiGaStream.h"
// forward decclaartion
// template <class ALGORITHM> class AlgFactory ;

/** @class GiGaOutputStream GiGaOutputStream.h component/GiGaOutputStream.h
 *
 *  Output stream for GiGa
 *
 *  @author Ivan Belyaev
 *  @date   15/01/2002
 */

class GiGaOutputStream : virtual public GiGaStream {
public:
  StatusCode execute() override; ///< Algorithm execution

  /** Standard constructor
   *  @param name stream(algorithm) name
   *  @param pSvcLoc pointer to the Service Locator
   */
  GiGaOutputStream( const std::string& name, ISvcLocator* pSvcLoc );

  /** destructor
   */
  virtual ~GiGaOutputStream();
};

// ============================================================================
// The End
// ============================================================================
#endif // GIGA_GIGAOutputSTREAM_H
// ============================================================================
