/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GaussTrackActionByProcess.cpp,v 1.3 2007-01-12 15:36:43 ranjard Exp $
// Include files

// from Gaudi

// CLHEP
#include "CLHEP/Units/SystemOfUnits.h"

// Geant4
#include "G4ProcessType.hh"
#include "G4TrackingManager.hh"
#include "G4VProcess.hh"

// GaussTools
#include "GaussTools/GaussTrackInformation.h"
#include "GaussTools/GaussTrajectory.h"

// local
#include "GaussTrackActionByProcess.h"

/** @file
 *  Implementation file for class GaussTrackActionByProcess
 *
 *  @date 2004-02-19
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 */

// Declaration of the Tool Factory
DECLARE_COMPONENT( GaussTrackActionByProcess )

// ============================================================================
/** standard constructor
 *  @see GiGaTrackActionBase
 *  @see GiGaBase
 *  @see AlgTool
 *  @param type type of the object (?)
 *  @param name name of the object
 *  @param parent  pointer to parent object
 */
// ============================================================================
GaussTrackActionByProcess::GaussTrackActionByProcess( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : GaussTrackActionZ( type, name, parent )
    //
    , m_ownProcs()
    , m_childProcs()
    , m_ownPTypes( 1, (int)fDecay )
    , m_childPTypes() {
  declareProperty( "OwnProcesses", m_ownProcs );
  declareProperty( "ChildProcesses", m_childProcs );
  declareProperty( "OwnProcessTypes", m_ownPTypes );
  declareProperty( "ChildProcessTypes", m_childPTypes );
}

// ============================================================================
/// Destructor
// ============================================================================
GaussTrackActionByProcess::~GaussTrackActionByProcess() {}

// ============================================================================
/// perform initialization
// ============================================================================
StatusCode GaussTrackActionByProcess::initialize() {
  StatusCode sc = GaussTrackActionZ::initialize();
  if ( sc.isFailure() ) { return sc; }

  std::sort( m_ownProcs.begin(), m_ownProcs.end() );
  std::sort( m_childProcs.begin(), m_childProcs.end() );
  std::sort( m_ownPTypes.begin(), m_ownPTypes.end() );
  std::sort( m_childPTypes.begin(), m_childPTypes.end() );

  return StatusCode::SUCCESS;
}

// ============================================================================
/** perform action
 *  @see G4VUserTrackingAction
 *  @param pointer to new track opbject
 */
// ============================================================================
void GaussTrackActionByProcess::PreUserTrackingAction( const G4Track* track ) {
  // no action
  if ( m_ownProcs.empty() && m_ownPTypes.empty() ) { return; } // RETURN

  if ( 0 == trackMgr() ) {
    Error( "Pre..: G4TrackingManager* points to NULL!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  } // RETURN

  if ( 0 == track ) {
    Error( "Pre..: G4Track*           points to NULL!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  } // RETURN

  if ( track->GetVertexPosition().z() < zMin() ) {
    return;
  } // RETURN
  else if ( track->GetVertexPosition().z() > zMax() ) {
    return;
  } // RETURN

  // get the trajectory
  GaussTrajectory* tr = trajectory();

  // check the validity
  if ( 0 == tr ) {
    Error( "Pre...: GaussTrajectory*       points to NULL" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }

  // check the track information
  GaussTrackInformation* info = trackInfo();
  if ( 0 == info ) {
    Error( "Pre...: GaussTrackInformation* points to NULL" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }

  if ( info->toBeStored() ) { return; }

  if ( storeByOwnProcess() ) { mark( info ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
}

// ============================================================================
/** perform action
 *  @see G4VUserTrackingAction
 *  @param pointer to new track opbject
 */
// ============================================================================
void GaussTrackActionByProcess::PostUserTrackingAction( const G4Track* track ) {
  // no action
  if ( m_childProcs.empty() && m_childPTypes.empty() ) { return; } // RETURN

  if ( 0 == trackMgr() ) {
    Error( "Pos..: G4TrackingManager* points to NULL!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  } // RETURN

  if ( 0 == track ) {
    Error( "Pos..: G4Track*           points to NULL!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  } // RETURN

  if ( track->GetVertexPosition().z() < zMin() ) {
    return;
  } // RETURN
  else if ( track->GetVertexPosition().z() > zMax() ) {
    return;
  } // RETURN

  // get the trajectory
  GaussTrajectory* tr = trajectory();

  // check the validity
  if ( 0 == tr ) {
    Error( "Pos..: GaussTrajectory*       points to NULL" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }

  // check the track information
  GaussTrackInformation* info = trackInfo();
  if ( 0 == info ) {
    Error( "Pos..: GaussTrackInformation* points to NULL" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return;
  }

  if ( info->toBeStored() ) { return; }

  if ( storeByChildProcess() ) { mark( info ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
}

// ============================================================================
// The END
// ============================================================================
