/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Detector
#include "Core/Keys.h"
#include "Detector/Magnet/DeMagnet.h"
#include "LbDD4hep/IDD4hepSvc.h"

// Gaussino
#include "GiGaMTMagnetFactories/MagneticField.h"

namespace Gauss::Magnet::DD4hep {
  using FIELD   = Gaussino::MagneticField::FieldFromCallback;
  using FACTORY = Gaussino::MagneticField::FieldFactory<FIELD>;

  class FieldFactory : public FACTORY {
  private:
    ServiceHandle<LHCb::Det::LbDD4hep::IDD4hepSvc> m_dd4Svc{ this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc" };
    Gaudi::Property<int> m_runNumber{ this, "RunNumber", -1, "Run number to fill the ODIN value." };

  public:
    using FACTORY::FACTORY;
    virtual StatusCode initialize() override;
    virtual FIELD*     construct() const override;
  };
} // namespace Gauss::Magnet::DD4hep

DECLARE_COMPONENT_WITH_ID( Gauss::Magnet::DD4hep::FieldFactory, "DD4hepMagneticField" )

StatusCode Gauss::Magnet::DD4hep::FieldFactory::initialize() {
  return FACTORY::initialize().andThen( [&]() -> StatusCode {
    if ( m_runNumber < 0 ) {
      error() << "Run number must be explicitly set and be >= 0." << endmsg;
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  } );
}

Gauss::Magnet::DD4hep::FIELD* Gauss::Magnet::DD4hep::FieldFactory::construct() const {
  auto                     field    = FACTORY::construct();
  auto                     slice    = m_dd4Svc->get_slice( m_runNumber );
  auto                     magnet   = dd4hep::Detector::getInstance().detector( "Magnet" );
  LHCb::Detector::DeMagnet demagnet = slice->get( magnet, LHCb::Detector::Keys::deKey );
  field->setCallback(
      [=]( const Gaudi::XYZPoint& point ) -> Gaudi::XYZVector { return demagnet.fieldVector( point ); } );
  return field;
}
