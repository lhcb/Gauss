/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// LHCb
#include "Kernel/ILHCbMagnetSvc.h"

// Gaussino
#include "GiGaMTMagnetFactories/MagneticField.h"

namespace Gauss::Magnet::DetDesc {
  using FIELD   = Gaussino::MagneticField::FieldFromCallback;
  using FACTORY = Gaussino::MagneticField::FieldFactory<FIELD>;

  class FieldFactory : public FACTORY {
  private:
    ServiceHandle<ILHCbMagnetSvc> m_magfieldsvc{ this, "MagneticFieldService", "MagneticFieldSvc" };

  public:
    using FACTORY::FACTORY;
    virtual FIELD* construct() const override {
      auto field = FACTORY::construct();
      field->setCallback(
          [&]( const Gaudi::XYZPoint& point ) -> Gaudi::XYZVector { return m_magfieldsvc->fieldVector( point ); } );
      return field;
    };
  };
} // namespace Gauss::Magnet::DetDesc

DECLARE_COMPONENT_WITH_ID( Gauss::Magnet::DetDesc::FieldFactory, "DetDescMagneticField" )
