/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: G4AntiLambdabZero.h,v 1.1 2006-01-09 20:52:07 robbep Exp $

#ifndef G4AntiLambdabZero_h
#define G4AntiLambdabZero_h 1

#include "G4ParticleDefinition.hh"
#include "G4ios.hh"
#include "globals.hh"

// ######################################################################
// ###                         AntiLambdabZero                        ###
// ######################################################################

class G4AntiLambdabZero : public G4ParticleDefinition {
private:
  static G4AntiLambdabZero* theInstance;
  G4AntiLambdabZero() {}
  ~G4AntiLambdabZero() {}

public:
  static G4AntiLambdabZero* Definition();
  static G4AntiLambdabZero* AntiLambdabZeroDefinition();
  static G4AntiLambdabZero* AntiLambdabZero();
};

#endif
