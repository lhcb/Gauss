/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <algorithm>

// GaudiKernel
#include "GaudiKernel/PhysicalConstants.h"

// LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
// Geant4
#include "G4ParticleTable.hh"
#include "G4VPhysicsConstructor.hh"
// Local
#include "G4Higgses.h"
#include "GiGaMTCoreMessage/IGiGaMessage.h"
#include "GiGaMTPhysFactories/GiGaMTG4PhysicsConstrFAC.h"

class GiGaMTHiggsParticles : public G4VPhysicsConstructor, public GiGaMessage {
public:
  GiGaMTHiggsParticles() = delete;
  GiGaMTHiggsParticles( const LHCb::IParticlePropertySvc* ppsvc, std::vector<std::string> higgses )
      : m_higgses( higgses ), m_ppsvc( ppsvc ) {}
  void ConstructParticle() override;  // construct the particles
  void ConstructProcess() override{}; // construct the processed
protected:
  const LHCb::ParticleProperty* pp( const std::string& n, const LHCb::IParticlePropertySvc* s ) const {
    if ( !s ) { throw std::runtime_error( "Invalid ParticleProperty Service" ); }
    const LHCb::ParticleProperty* p = s->find( n );
    if ( !p ) { throw std::runtime_error( "No information is availale for '" + n + "'" ); }
    return p;
  }

private:
  std::vector<std::string>          m_higgses{};
  const LHCb::IParticlePropertySvc* m_ppsvc{ nullptr };
};

void GiGaMTHiggsParticles::ConstructParticle() // construct the particles
{

  auto tmp = m_higgses;
  {
    auto it = std::find( std::begin( tmp ), std::end( tmp ), "H_10" );
    if ( std::end( tmp ) != it ) {
      const LHCb::ParticleProperty* h10  = pp( "H_10", m_ppsvc );
      G4H_10*                       h_10 = G4H_10::Create( h10->mass(), h10->lifetime() * Gaudi::Units::c_light );
      if ( !h_10 ) { throw std::runtime_error( "Unable to create H_10" ); }
      if ( printInfo() ) { h_10->DumpTable(); }
      tmp.erase( it );
    }
    // ========================================================================
    it = std::find( std::begin( tmp ), std::end( tmp ), "H_20" );
    if ( std::end( tmp ) != it ) {
      const LHCb::ParticleProperty* h20  = pp( "H_20", m_ppsvc );
      G4H_20*                       h_20 = G4H_20::Create( h20->mass(), h20->lifetime() * Gaudi::Units::c_light );
      if ( !h_20 ) { throw std::runtime_error( "Unable to create H_20" ); }
      if ( printInfo() ) { h_20->DumpTable(); }
      tmp.erase( it );
    }
    // ========================================================================
    it = std::find( std::begin( tmp ), std::end( tmp ), "H_30" );
    if ( std::end( tmp ) != it ) {
      const LHCb::ParticleProperty* h30  = pp( "H_30", m_ppsvc );
      G4H_30*                       h_30 = G4H_30::Create( h30->mass(), h30->lifetime() * Gaudi::Units::c_light );
      if ( !h_30 ) { throw std::runtime_error( "Unable to create H_30" ); }
      if ( printInfo() ) { h_30->DumpTable(); }
      tmp.erase( it );
    }
    if ( !tmp.empty() ) { throw std::runtime_error( "Unknown Higgses in the list!" ); }
  }
}

class GiGaMTHiggsParticlesFAC : public extends<GiGaMTPhysConstr, GiGaFactoryBase<G4VPhysicsConstructor>> {
public:
  Gaudi::Property<std::vector<std::string>> m_higgses{ this, "Higgses", {}, "The List of Higgsed to be instantiated" };
  ServiceHandle<LHCb::IParticlePropertySvc> m_ppSvc{ this, "PropertyService", "LHCb::ParticlePropertySvc" };
  using extends::extends;
  GiGaMTHiggsParticles* construct() const override {
    auto tmp = new GiGaMTHiggsParticles{ m_ppSvc.get(), m_higgses };
    tmp->SetMessageInterface( message_interface() );
    tmp->SetVerboseLevel( verbosity() );
    tmp->SetPhysicsName( name() );
    return tmp;
  }
};

DECLARE_COMPONENT_WITH_ID( GiGaMTHiggsParticlesFAC, "GiGaMTHiggsParticles" )
