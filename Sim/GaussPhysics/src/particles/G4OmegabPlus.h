/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef G4OmegabPlus_h
#define G4OmegabPlus_h 1

#include "G4ParticleDefinition.hh"
#include "G4ios.hh"
#include "globals.hh"

// ######################################################################
// ###                         Omegab plus                        ###
// ######################################################################

class G4OmegabPlus : public G4ParticleDefinition {
private:
  static G4OmegabPlus* theInstance;
  G4OmegabPlus() {}
  ~G4OmegabPlus() {}

public:
  static G4OmegabPlus* Definition();
  static G4OmegabPlus* OmegabPlusDefinition();
  static G4OmegabPlus* OmegabPlus();
};

#endif
