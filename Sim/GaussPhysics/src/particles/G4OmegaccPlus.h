/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef G4OmegaccPlus_h
#define G4OmegaccPlus_h 1

#include "G4ParticleDefinition.hh"
#include "G4ios.hh"
#include "globals.hh"

// ######################################################################
// ###                         OmegaccPlus                        ###
// ######################################################################

class G4OmegaccPlus : public G4ParticleDefinition {
private:
  static G4OmegaccPlus* theInstance;
  G4OmegaccPlus() {}
  ~G4OmegaccPlus() {}

public:
  static G4OmegaccPlus* Definition();
  static G4OmegaccPlus* OmegaccPlusDefinition();
  static G4OmegaccPlus* OmegaccPlus();
};

#endif
