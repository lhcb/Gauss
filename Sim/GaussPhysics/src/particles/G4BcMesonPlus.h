/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: G4BcMesonPlus.h,v 1.2 2006-01-09 20:52:22 robbep Exp $

#ifndef G4BcMesonPlus_h
#define G4BcMesonPlus_h 1

#include "G4ParticleDefinition.hh"
#include "G4ios.hh"
#include "globals.hh"

// ######################################################################
// ###                         BcMesonPlus                        ###
// ######################################################################

class G4BcMesonPlus : public G4ParticleDefinition {
private:
  static G4BcMesonPlus* theInstance;
  G4BcMesonPlus() {}
  ~G4BcMesonPlus() {}

public:
  static G4BcMesonPlus* Definition();
  static G4BcMesonPlus* BcMesonPlusDefinition();
  static G4BcMesonPlus* BcMesonPlus();
};

#endif
