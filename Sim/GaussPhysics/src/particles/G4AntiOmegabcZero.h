/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef G4AntiOmegabcZero_h
#define G4AntiOmegabcZero_h 1

#include "G4ParticleDefinition.hh"
#include "G4ios.hh"
#include "globals.hh"

// ######################################################################
// ###                         AntiOmegabcZero                        ###
// ######################################################################

class G4AntiOmegabcZero : public G4ParticleDefinition {
private:
  static G4AntiOmegabcZero* theInstance;
  G4AntiOmegabcZero() {}
  ~G4AntiOmegabcZero() {}

public:
  static G4AntiOmegabcZero* Definition();
  static G4AntiOmegabcZero* AntiOmegabcZeroDefinition();
  static G4AntiOmegabcZero* AntiOmegabcZero();
};

#endif
