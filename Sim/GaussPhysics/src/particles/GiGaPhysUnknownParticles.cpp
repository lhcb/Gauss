/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"

// G4
#include "G4ParticleTable.hh"
#include "G4ProcessManager.hh"
#include "G4UnknownParticle.hh"

// local
#include "G4AntiLambdabZero.h"
#include "G4AntiOmegabcZero.h"
#include "G4AntiOmegaccMinus.h"
#include "G4AntiXibZero.h"
#include "G4AntiXibcMinus.h"
#include "G4AntiXibcZero.h"
#include "G4AntiXiccMinus.h"
#include "G4AntiXiccMinusMinus.h"
#include "G4AntiXiccStarMinus.h"
#include "G4BcMesonMinus.h"
#include "G4BcMesonPlus.h"
#include "G4CharginoMinus.h"
#include "G4CharginoPlus.h"
#include "G4LambdabZero.h"
#include "G4Neutralino.h"
#include "G4Neutralino2.h"
#include "G4OmegabMinus.h"
#include "G4OmegabPlus.h"
#include "G4OmegabcZero.h"
#include "G4OmegaccPlus.h"
#include "G4XibZero.h"
#include "G4XibcPlus.h"
#include "G4XibcZero.h"
#include "G4XiccPlus.h"
#include "G4XiccPlusPlus.h"
#include "G4XiccStarPlus.h"

#include "G4Decay.hh"
#include "G4UnknownDecay.hh"
#include "G4VPhysicsConstructor.hh"
#include "GiGaMTCoreMessage/IGiGaMessage.h"

class GiGaMTPhysUnknownParticles : public GiGaMessage, public G4VPhysicsConstructor {

public:
  void ConstructParticle() override;
  void ConstructProcess() override;

private:
  G4Decay        m_decayProcess;
  G4UnknownDecay m_unknownDecay;
};

void GiGaMTPhysUnknownParticles::ConstructParticle() {
  G4BcMesonMinus::BcMesonMinusDefinition();
  G4BcMesonPlus::BcMesonPlusDefinition();
  G4OmegabMinus::OmegabMinusDefinition();
  G4OmegabPlus::OmegabPlusDefinition();
  G4AntiLambdabZero::AntiLambdabZeroDefinition();
  G4LambdabZero::LambdabZeroDefinition();
  G4AntiXibZero::AntiXibZeroDefinition();
  G4XibZero::XibZeroDefinition();
  G4AntiXiccStarMinus::AntiXiccStarMinusDefinition();
  G4XiccStarPlus::XiccStarPlusDefinition();
  G4AntiXiccMinus::AntiXiccMinusDefinition();
  G4XiccPlus::XiccPlusDefinition();
  G4AntiXibcMinus::AntiXibcMinusDefinition();
  G4XibcPlus::XibcPlusDefinition();
  G4AntiXibcZero::AntiXibcZeroDefinition();
  G4XibcZero::XibcZeroDefinition();
  G4AntiXiccMinusMinus::AntiXiccMinusMinusDefinition();
  G4XiccPlusPlus::XiccPlusPlusDefinition();
  G4Neutralino::NeutralinoDefinition();
  G4Neutralino2::Neutralino2Definition();
  G4CharginoPlus::CharginoPlusDefinition();
  G4CharginoMinus::CharginoMinusDefinition();
  G4OmegaccPlus::OmegaccPlusDefinition();
  G4AntiOmegaccMinus::AntiOmegaccMinusDefinition();
  G4OmegabcZero::OmegabcZeroDefinition();
  G4AntiOmegabcZero::AntiOmegabcZeroDefinition();

  G4UnknownParticle::UnknownParticleDefinition();
}

void GiGaMTPhysUnknownParticles::ConstructProcess() {
  auto theParticleIterator = GetParticleIterator();
  theParticleIterator->reset();
  while ( ( *theParticleIterator )() ) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager*     pmanager = particle->GetProcessManager();
    if ( particle->GetParticleName() == "unknown" ) {
      pmanager->AddProcess( &m_unknownDecay );
      pmanager->SetProcessOrdering( &m_unknownDecay, idxPostStep );
      if ( printDebug() ) {
        std::stringstream message;
        message << "### Unknown Decays for " << particle->GetParticleName();
        debug( message.str() );
      }
      if ( printDebug() ) pmanager->DumpInfo();
    }
  }
}

#include "GiGaMTPhysFactories/GiGaMTG4PhysicsConstrFAC.h"
typedef GiGaMTG4PhysicsConstrFAC<GiGaMTPhysUnknownParticles> GiGaMT_GiGaMTPhysUnknownParticles;
DECLARE_COMPONENT_WITH_ID( GiGaMT_GiGaMTPhysUnknownParticles, "GiGaPhysUnknownParticles" )
