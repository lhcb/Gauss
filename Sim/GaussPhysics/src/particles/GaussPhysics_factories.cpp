/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GiGaMTPhysFactories/GiGaMTG4PhysicsConstrFAC.h"

// LHCb Physics Lists
#include "G4EmStandardPhysics_LHCbTest.hh"
#include "G4EmStandardPhysics_option1LHCb.hh"
#include "G4EmStandardPhysics_option1NoApplyCuts.hh"

typedef GiGaMTG4PhysicsConstrFAC<G4EmStandardPhysics_option1NoApplyCuts> GiGaMT_G4EmStandardPhysics_option1NoApplyCuts;
DECLARE_COMPONENT_WITH_ID( GiGaMT_G4EmStandardPhysics_option1NoApplyCuts,
                           "GiGaMT_G4EmStandardPhysics_option1NoApplyCuts" )

template <typename PhysConstr>
class GiGaMTG4PhysicsConstrFAC<
    PhysConstr, typename std::enable_if<std::is_same<PhysConstr, G4EmStandardPhysics_option1LHCb>::value ||
                                            std::is_same<PhysConstr, G4EmStandardPhysics_LHCbTest>::value,
                                        PhysConstr>::type>
    : public extends<GiGaMTPhysConstr, GiGaFactoryBase<G4VPhysicsConstructor>> {
  Gaudi::Property<bool> m_applyCuts{ this, "ApplyCuts", true,
                                     "Apply production cuts to all EM processes for the LHCb EM constructor" };
  Gaudi::Property<bool> m_newForE{ this, "NewModelForE", true, "Use new MS models for electrons and positrons" };

public:
  using extends::extends;
  PhysConstr* construct() const override {
    auto tmp = new PhysConstr{ verbosity(), m_applyCuts, m_newForE };
    tmp->SetVerboseLevel( verbosity() );
    tmp->SetPhysicsName( name() );
    return tmp;
  }
};

typedef GiGaMTG4PhysicsConstrFAC<G4EmStandardPhysics_option1LHCb> GiGaMT_G4EmStandardPhysics_option1LHCb;
DECLARE_COMPONENT_WITH_ID( GiGaMT_G4EmStandardPhysics_option1LHCb, "GiGaMT_G4EmStandardPhysics_option1LHCb" )

typedef GiGaMTG4PhysicsConstrFAC<G4EmStandardPhysics_LHCbTest> GiGaMT_G4EmStandardPhysics_LHCbTest;
DECLARE_COMPONENT_WITH_ID( GiGaMT_G4EmStandardPhysics_LHCbTest, "GiGaMT_G4EmStandardPhysics_LHCbTest" )
