/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CLHEP/Units/SystemOfUnits.h"
#include "G4VPhysicsConstructor.hh"
#include "GiGaMTCoreMessage/IGiGaMessage.h"
#include <vector>

/** @class TrCutsRunAction TrCutsRunAction.h
 *
 *  Class to pass Tracking Cuts to Geant4 as processes.
 *  Modified to
 *
 *
 *  @author Witek POKORSKI
 *  @date   2003-04-10
 *  @author Gloria CORTI
 *  @date   2009-03-31
 */

class TrCutsRunActionFAC;

class TrCutsRunAction : public G4VPhysicsConstructor, public GiGaMessage {
  friend TrCutsRunActionFAC;

public:
  void ConstructParticle() override{};
  void ConstructProcess() override;

  virtual ~TrCutsRunAction() = default;

private:
  double m_ecut{ 1.0 * CLHEP::MeV };     ///< kinetic energy cut for e+/-
  double m_phcut{ 1.0 * CLHEP::MeV };    ///< kinetic energy cut for gamma
  double m_pKpicut{ 10.0 * CLHEP::MeV }; ///< kinetic energy cut for proton, K+/- and pi+/-
  double m_ocut{ 0.0 * CLHEP::MeV };     ///< kinetic energy cut for other particles
  double m_ncut{ 10.0 * CLHEP::MeV };    ///< kinetic energy cut for neutrons
  double m_nucut{ 0.0 * CLHEP::MeV };    ///< kinetic energy cut for neutrinos (when tracked)
  double m_mcut{ 10.0 * CLHEP::MeV };    ///< kinetic energy cut for muons
  bool   m_killloops{ true };            ///< kill looping e- and gamma
  int    m_maxsteps{ 100 };              ///< maximum number of step looping
  double m_minstep{ 0.001 };             ///< minimum lenght for looping cut
  double m_minx{ -10000 * CLHEP::mm }, m_miny{ -10000 * CLHEP::mm }, m_minz{ -5000 * CLHEP::mm },
      m_maxx{ 10000 * CLHEP::mm }, m_maxy{ 10000 * CLHEP::mm }, m_maxz{ 25000 * CLHEP::mm }; ///< world cut limits
  bool             m_killPingPong{ true };                          ///< kill particles ping ponging
  int              m_nMaxForPingPong{ 1000000 };                    ///< maximum number of steps to check pingpong
  int              m_nMaxOfPingPong{ 20 };                          ///< maximum number of ping pong
  double           m_stepLenghtPingPong{ 1.0e-3 };                  ///< step lenght to consider in pingpong
  std::vector<int> m_killAtOrigin{ { 12, 14, 16, -12, -14, -16 } }; ///< particle to kill at origin (def=neutrino)
  bool             m_killZeroSteps{ true };                         ///< kill particle with 'zero' step
  int              m_nStepsForZero{ 1000000 }; ///< number of steps after which to check for 'zero' steps
  double           m_zeroMaxLenght{ 1.0e-9 };  ///< max lenght to consider a step of 'zero' lenght
  int              m_maxNZeroSteps{ 20 };      ///< number of steps of zero length to trigger cut
  std::string      m_world{ "Universe" };      ///< Name of world volume
};
