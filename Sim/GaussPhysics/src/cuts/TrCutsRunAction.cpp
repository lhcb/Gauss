/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// G4
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4ProcessManager.hh"

// local
#include "KillAtOriginCut.h"
#include "LoopCuts.h"
#include "MinEkineCuts.h"
#include "PingPongCut.h"
#include "TrCutsRunAction.h"
#include "WorldCuts.h"
#include "ZeroStepsCut.h"

void TrCutsRunAction::ConstructProcess() {

  // Loop on particles that have been defined and attach process
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  if ( !particleTable ) {
    error( "G4ParticleTable points to NULL!; return! " );
    return;
  }

  int ii, ptbSiz = particleTable->size();

  for ( ii = 0; ii < ptbSiz; ii++ ) {
    G4ParticleDefinition* particle = particleTable->GetParticle( ii );

    if ( !particle ) {
      warning( "G4ParticleDefinition* points to NULL, skip it" );
      continue;
    }

    std::string pname = particle->GetParticleName();

    // Do not attach cuts processes to optical photons nor short lived
    // particle as will disappear anyway for other reasons!!!
    if ( ( pname != "opticalphoton" ) && ( !particle->IsShortLived() ) ) {
      int particleCode = particle->GetPDGEncoding();

      double acut;

      if ( abs( particleCode ) == 11 ) {
        acut = m_ecut;
      } else if ( particleCode == 22 ) {
        acut = m_phcut;
      } else if ( abs( particleCode ) == 13 ) {
        acut = m_mcut;
      } else if ( abs( particleCode ) == 2112 ) {
        acut = m_ncut;
      } else if ( abs( particleCode ) == 12 || abs( particleCode ) == 14 || abs( particleCode ) == 16 ) {
        acut = m_nucut;
      } else if ( ( abs( particleCode ) == 2212 ) || ( abs( particleCode ) == 321 ) ||
                  ( abs( particleCode ) == 211 ) ) {
        acut = m_pKpicut;
      } else {
        acut = m_ocut;
      }

      if ( ( pname != "opticalphoton" ) && ( !particle->IsShortLived() ) ) {
        G4ProcessManager* procMgr = particle->GetProcessManager();
        if ( 0 == procMgr ) {
          error( "G4ProcessManager* points to NULL!" );
          return;
        }

        for ( std::vector<int>::iterator ik = m_killAtOrigin.begin(); m_killAtOrigin.end() != ik; ik++ ) {
          if ( particleCode == *ik ) { procMgr->AddDiscreteProcess( new GiGa::KillAtOriginCut( "KillAtOriginCut" ) ); }
        } // closes loop on particles to kill

        procMgr->AddDiscreteProcess( new GiGa::MinEkineCuts( "MinEkineCut", acut ) );
        procMgr->AddDiscreteProcess(
            new GiGa::WorldCuts( "WorldCut", m_minx, m_miny, m_minz, m_maxx, m_maxy, m_maxz ) );
        if ( ( pname == "e-" || pname == "gamma" ) && m_killloops ) {
          procMgr->AddDiscreteProcess( new GiGa::LoopCuts( "LoopCuts", m_maxsteps, m_minstep ) );
        }

        if ( m_killPingPong ) {
          procMgr->AddDiscreteProcess(
              new GiGa::PingPongCut( "PingPongCut", m_nMaxForPingPong, m_stepLenghtPingPong, m_nMaxOfPingPong ) );
        }
        if ( m_killZeroSteps ) {
          procMgr->AddDiscreteProcess(
              new GiGa::ZeroStepsCut( "ZeroStepsCut", m_nStepsForZero, m_zeroMaxLenght, m_maxNZeroSteps, m_world ) );
        }
      }
    }
  }
}

#include "GiGaMTFactories/GiGaFactoryBase.h"
#include "GiGaMTFactories/GiGaTool.h"

class TrCutsRunActionFAC : public extends<GiGaTool, GiGaFactoryBase<G4VPhysicsConstructor>> {
public:
  using extends::extends;
  G4VPhysicsConstructor* construct() const override {
    auto ret = new TrCutsRunAction{};
    ret->SetMessageInterface( message_interface() );
    ret->m_ecut               = m_ecut.value();
    ret->m_phcut              = m_phcut.value();
    ret->m_pKpicut            = m_pKpicut.value();
    ret->m_ocut               = m_ocut.value();
    ret->m_ncut               = m_ncut.value();
    ret->m_nucut              = m_nucut.value();
    ret->m_mcut               = m_mcut.value();
    ret->m_killloops          = m_killloops.value();
    ret->m_maxsteps           = m_maxsteps.value();
    ret->m_minstep            = m_minstep.value();
    ret->m_minx               = m_minx.value();
    ret->m_miny               = m_miny.value();
    ret->m_minz               = m_minz.value();
    ret->m_maxx               = m_maxx.value();
    ret->m_maxy               = m_maxy.value();
    ret->m_maxz               = m_maxz.value();
    ret->m_killPingPong       = m_killPingPong.value();
    ret->m_nMaxForPingPong    = m_nMaxForPingPong.value();
    ret->m_nMaxOfPingPong     = m_nMaxOfPingPong.value();
    ret->m_stepLenghtPingPong = m_stepLenghtPingPong.value();
    ret->m_killAtOrigin       = m_killAtOrigin.value();
    ret->m_killZeroSteps      = m_killZeroSteps.value();
    ret->m_nStepsForZero      = m_nStepsForZero.value();
    ret->m_zeroMaxLenght      = m_zeroMaxLenght.value();
    ret->m_maxNZeroSteps      = m_maxNZeroSteps.value();
    ret->m_world              = m_world.value();
    return ret;
  }
  Gaudi::Property<double>           m_ecut{ this, "ElectronTrCut", 1.0 * CLHEP::MeV };
  Gaudi::Property<double>           m_phcut{ this, "GammaTrCut", 1.0 * CLHEP::MeV };
  Gaudi::Property<double>           m_pKpicut{ this, "pKpiCut", 10.0 * CLHEP::MeV };
  Gaudi::Property<double>           m_ncut{ this, "NeutronTrCut", 10.0 * CLHEP::MeV };
  Gaudi::Property<double>           m_nucut{ this, "NeutrinoTrCut", 0.0 * CLHEP::MeV };
  Gaudi::Property<double>           m_mcut{ this, "MuonTrCut", 10.0 * CLHEP::MeV };
  Gaudi::Property<double>           m_ocut{ this, "OtherTrCut", 0.0 * CLHEP::MeV };
  Gaudi::Property<bool>             m_killloops{ this, "KillLoops", true };
  Gaudi::Property<int>              m_maxsteps{ this, "MaxNumberSteps", 100 };
  Gaudi::Property<double>           m_minstep{ this, "MininumStep", 0.001 };
  Gaudi::Property<double>           m_minx{ this, "MinX", -10000 * CLHEP::mm };
  Gaudi::Property<double>           m_miny{ this, "MinY", -10000 * CLHEP::mm };
  Gaudi::Property<double>           m_minz{ this, "MinZ", -5000 * CLHEP::mm };
  Gaudi::Property<double>           m_maxx{ this, "MaxX", 10000 * CLHEP::mm };
  Gaudi::Property<double>           m_maxy{ this, "MaxY", 10000 * CLHEP::mm };
  Gaudi::Property<double>           m_maxz{ this, "MaxZ", 25000 * CLHEP::mm };
  Gaudi::Property<bool>             m_killPingPong{ this, "KillPingPong", true };
  Gaudi::Property<int>              m_nMaxForPingPong{ this, "MaxNumStepsForPingPong", 1000000 };
  Gaudi::Property<int>              m_nMaxOfPingPong{ this, "MaxNumOfPingPong", 20 };
  Gaudi::Property<double>           m_stepLenghtPingPong{ this, "MaxStepLenForPingPong", 1.0e-3 };
  Gaudi::Property<std::vector<int>> m_killAtOrigin{
      this, "DoNotTrackParticles", { 12, 14, 16, -12, -14, -16 }, "List of particles to discard at their origin" };
  Gaudi::Property<bool>        m_killZeroSteps{ this, "KillZeroSteps", true,
                                         "Flag to activate killing particles that make step of zero "
                                                "lenght in a volume" };
  Gaudi::Property<int>         m_nStepsForZero{ this, "NmaxForZeroSteps", 1000000,
                                        "Number of steps after which to check for zero lenght steps" };
  Gaudi::Property<double>      m_zeroMaxLenght{ this, "StepLenghAsZero", 1.0e-9,
                                           "Lenght of steps below which a steps has to be considered as "
                                                "of zero lenght" };
  Gaudi::Property<int>         m_maxNZeroSteps{ this, "NZeroSteps", 20,
                                        "Number of consecutive steps of zero lenght after which the "
                                                "particle is killed" };
  Gaudi::Property<std::string> m_world{ this, "WorldName", "Universe", "Name of the world volume" };
};

DECLARE_COMPONENT_WITH_ID( TrCutsRunActionFAC, "TrCutsRunAction" )
