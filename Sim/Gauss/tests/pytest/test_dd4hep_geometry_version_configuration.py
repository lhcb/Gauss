###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

import pytest
from Gaudi.Configuration import importOptions
from Gauss.pytest.options import events_1, photon
from Gaussino.pytest.helpers import applyConfigurableUsers, reset_configurables


@reset_configurables
@events_1
@photon
@pytest.mark.skipif(
    "detdesc" in os.getenv("BINARY_TAG"),
    reason="DetDesc incompatible",
)
@pytest.mark.parametrize(
    "datatype, version",
    zip(
        ["2022", "2023"] * 3,
        ["", "run3/trunk", "incorrect"] * 2,
    ),
)
def test_dd4hep_geometry_version_configuration(datatype, version):
    from Configurables import GaussGeometry, LHCb__Det__LbDD4hep__DD4hepSvc

    importOptions(f"$GAUSSOPTS/General/{datatype}.py")
    if version:
        GaussGeometry().GeometryVersion = version
    if "incorrect" not in version:
        applyConfigurableUsers()
        svc_version = LHCb__Det__LbDD4hep__DD4hepSvc().getProp("GeometryVersion")
        assert svc_version.startswith("tmp")
    else:
        with pytest.raises(NotADirectoryError, match=r".*Invalid geometry version.*"):
            applyConfigurableUsers()
