###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

import pytest
from DDDB.Configuration import GIT_CONDDBS
from Gaudi.Configuration import importOptions
from Gauss.Defaults import CONDITIONS_VERSIONS_DD4HEP
from Gauss.pytest.options import events_1, photon
from Gaussino.pytest.helpers import applyConfigurableUsers, reset_configurables


@reset_configurables
@events_1
@photon
@pytest.mark.skipif(
    "detdesc" in os.getenv("BINARY_TAG"),
    reason="DetDesc incompatible",
)
@pytest.mark.parametrize(
    "datatype, location",
    zip(
        ["2022", "2023"] * 5,
        [
            "",
            GIT_CONDDBS["lhcb-conditions-database"],
            "/".join(GIT_CONDDBS["lhcb-conditions-database"].split("/")[:-1]),
            "/incorrect_path" "/incorrect_path.git",
        ]
        * 2,
    ),
)
def test_dd4hep_conditions_configuration(datatype, location):
    from Configurables import GaussGeometry, LHCb__Det__LbDD4hep__DD4hepSvc

    importOptions(f"$GAUSSOPTS/General/{datatype}.py")
    if location:
        GaussGeometry().ConditionsLocation = location
    if "incorrect" not in location:
        applyConfigurableUsers()
        cond_version = LHCb__Det__LbDD4hep__DD4hepSvc().getProp("ConditionsVersion")
        assert CONDITIONS_VERSIONS_DD4HEP[datatype] == cond_version
        cond_location = LHCb__Det__LbDD4hep__DD4hepSvc().getProp("ConditionsLocation")
        test_location = location
        if not test_location:
            test_location = GIT_CONDDBS["lhcb-conditions-database"]
        if ".git" in test_location:
            test_location = f"git:{test_location}"
        else:
            test_location = f"file:{test_location}/"
        assert test_location == cond_location
    else:
        with pytest.raises(NotADirectoryError):
            applyConfigurableUsers()
