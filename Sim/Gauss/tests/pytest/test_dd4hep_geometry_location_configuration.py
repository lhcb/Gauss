###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

import pytest
from Gaudi.Configuration import importOptions
from Gauss.pytest.options import events_1, photon
from Gaussino.pytest.helpers import applyConfigurableUsers, reset_configurables


@reset_configurables
@events_1
@photon
@pytest.mark.skipif(
    "detdesc" in os.getenv("BINARY_TAG"),
    reason="DetDesc incompatible",
)
@pytest.mark.parametrize(
    "datatype, location",
    zip(
        ["2022", "2023"] * 3,
        [
            None,  # use default
            os.path.join(os.environ["DETECTOR_PROJECT_ROOT"], "compact"),
            "/incorrect_path",
        ]
        * 2,
    ),
)
def test_dd4hep_geometry_location_configuration(datatype, location):
    from Configurables import GaussGeometry

    importOptions(f"$GAUSSOPTS/General/{datatype}.py")
    if location:
        GaussGeometry().GeometryLocation = location
    if location != "/incorrect_path":
        applyConfigurableUsers()
    else:
        with pytest.raises(
            NotADirectoryError, match=r".*Invalid directory with compact XML.*"
        ):
            applyConfigurableUsers()
