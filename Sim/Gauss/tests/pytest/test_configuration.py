###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

import pytest
from Gauss.pytest.options import *
from Gaussino.pytest.helpers import *


@reset_configurables
@events_1
@photon
@empty
@data_2022
@pytest.mark.skipif(
    "detdesc" in os.getenv("BINARY_TAG"),
    reason="DetDesc incompatible",
)
def test_minimum_working_example_dd4hep():
    applyConfigurableUsers()


@reset_configurables
@events_1
@photon
@empty
@data_2022
@detdesc
def test_minimum_working_example_detdesc():
    applyConfigurableUsers()


@reset_configurables
@events_1
@photon
@empty
@data_2018
@pytest.mark.parametrize(
    "datatype, correct",
    [
        ("", False),
        ("2018", True),
        ("WrongDataType", False),
    ],
)
def test_data_types(datatype, correct):
    from Configurables import Gauss

    Gauss().DataType = datatype
    if correct:
        applyConfigurableUsers()
    else:
        msg = r"Unknown DataType.*"
        if not datatype:
            msg = r"Must have a DataType.*"
        with pytest.raises(ValueError, match=msg):
            applyConfigurableUsers()


@reset_configurables
@events_1
@photon
@empty
@data_2022
def test_direct_use_of_gaussino():
    from Configurables import Gaussino

    Gaussino().EvtMax = 1
    with pytest.raises(RuntimeError, match=r".*Gaussino.*"):
        applyConfigurableUsers()


@reset_configurables
@events_1
@photon
@pytest.mark.parametrize(
    "datatype, legacy, correct",
    [
        # ("2010", True, True),
        # ("2010", False, False),
        # ("2011", True, True),
        # ("2011", False, False),
        # ("2013", True, True),
        # ("2013", False, False),
        ("2015", True, True),
        ("2015", False, False),
        ("2016", True, True),
        ("2016", False, False),
        ("2017", True, True),
        ("2017", False, False),
        ("2018", True, True),
        ("2018", False, False),
        # in 2022/23 we can use
        # DetDesc or DD4hep
        ("2022", True, True),
        ("2022", False, True),
        ("2023", True, True),
        ("2023", False, True),
    ],
)
def test_geo_service(datatype, legacy, correct):
    from Gaudi.Configuration import importOptions

    importOptions(f"$GAUSSOPTS/General/{datatype}.py")
    if legacy:
        importOptions("$GAUSSOPTS/Geometry/DetDesc.py")
    else:
        importOptions("$GAUSSOPTS/Geometry/DD4hep.py")
    if correct:
        try:
            applyConfigurableUsers()
        except ImportError as err:
            if "detdesc" in os.getenv("BINARY_TAG") and "LHCbDD4hepCnvSvc" in str(err):
                return True
            else:
                raise ImportError(err)
        if legacy:
            assert "GaussGeo" in Configurable.allConfigurables
        else:
            assert "LHCbDD4hepCnvSvc" in Configurable.allConfigurables
    else:
        with pytest.raises(NotImplementedError, match=r".*Legacy.*"):
            applyConfigurableUsers()


@reset_configurables
@events_1
@photon
@data_2022
@pytest.mark.parametrize(
    "incompatible",
    [
        ["Velo", "VP"],
        ["PuVeto", "VP"],
        ["TT", "UT"],
        ["Muon", "Torch"],
        ["FT", "IT"],
        ["FT", "OT"],
        ["Rich1", "Rich1Pmt"],
        ["Rich2", "Rich2Pmt"],
    ],
)
def test_incompatible_subdetectors(incompatible):
    from Configurables import GaussGeometry

    GaussGeometry(
        DetectorGeo={"Detectors": incompatible},
        DetectorSim={},
        DetectorMoni={},
    )
    with pytest.raises(ValueError, match=r".*Incompatible detectors.*"):
        applyConfigurableUsers()


@reset_configurables
@events_1
@photon
@data_2018
@pytest.mark.parametrize(
    "redecay",
    [
        True,
        False,
    ],
)
def test_correct_genrndinit_setup(redecay):
    from Configurables import Gauss

    Gauss().ReDecay = redecay
    rnd_no = 0
    if not redecay:
        from Configurables import GenRndInit

        rnd_no = GenRndInit().getProp("RunNumber")
    else:
        from Configurables import GenReDecayInit

        rnd_no = GenReDecayInit().getProp("RunNumber")
    Gauss().RunNumber = rnd_no + 1
    applyConfigurableUsers()
    if not redecay:
        assert GenRndInit().getProp("RunNumber") == rnd_no + 1
    else:
        assert GenReDecayInit().getProp("RunNumber") == rnd_no + 1
