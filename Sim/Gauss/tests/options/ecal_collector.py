###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    ExternalDetectorEmbedder,
    FlatNParticles,
    FlatPtRapidity,
    Gauss,
    GaussGeo,
    GenPhase,
    ParallelGeometry,
    ParticleGun,
    SimPhase,
)
from Gaudi.Configuration import DEBUG
from GaudiKernel import SystemOfUnits
from Gauss.Geometry import LHCbGeo
from Gauss.Geometry.BeamPipe import BeamPipe

Gauss().EvtMax = 1

# overwrite LHCbGeo, only ECAL in LHCb
LHCbGeo().DetectorGeo = {"Detectors": ["Ecal"]}
LHCbGeo().DetectorSim = {"Detectors": ["Ecal"]}
LHCbGeo().DetectorMoni = {"Detectors": ["Ecal"]}

# ensure there are no interactions before ECAL
GaussGeo().WorldMaterial = "/dd/Materials/Vacuum"

BeamPipe().State = "BeamPipeOff"

# Particle Gun On
GenPhase().ParticleGun = True
GenPhase().ParticleGunUseDefault = False
pgun = ParticleGun("ParticleGun")
pgun.ParticleGunTool = "FlatPtRapidity"
pgun.addTool(FlatPtRapidity, name="FlatPtRapidity")
pgun.FlatPtRapidity.PtMin = 10 * SystemOfUnits.MeV
pgun.FlatPtRapidity.PtMax = 10 * SystemOfUnits.MeV
pgun.FlatPtRapidity.RapidityMin = 2.5  # ensure in ECAL
pgun.FlatPtRapidity.RapidityMax = 4.8  # ensure in ECAL
pgun.FlatPtRapidity.PdgCodes = [22]
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool(FlatNParticles, name="FlatNParticles")
pgun.FlatNParticles.MinNParticles = 100
pgun.FlatNParticles.MaxNParticles = 100

# adding external detectors
external = ExternalDetectorEmbedder("EcalCollectorEmbedder")

# generating ecal collector plane
shapes = {
    "EcalCollector": {
        "Type": "Cuboid",
        "xPos": 0.0 * SystemOfUnits.m,
        "yPos": 0.0 * SystemOfUnits.m,
        "zPos": (11948.0 - 0.005) * SystemOfUnits.mm,  # zMax - thickness/2!
        "xSize": 20.0 * SystemOfUnits.m,
        "ySize": 20.0 * SystemOfUnits.m,
        "zSize": 0.01 * SystemOfUnits.mm,
        "xAngle": -0.207 * SystemOfUnits.degree,
        "OutputLevel": DEBUG,
    },
}

sensitive = {
    "EcalCollector": {
        "Type": "MCCollectorSensDet",
        "RequireEDep": False,
        "OnlyForward": True,
        "OnlyAtBoundary": True,
        "PrintStats": True,
        "OutputLevel": DEBUG,
    },
}

hit = {
    "EcalCollector": {
        "Type": "GetMCCollectorHitsAlg",
        "OutputLevel": DEBUG,
    },
}

external.Shapes = shapes
external.Sensitive = sensitive
external.Hit = hit

# embedding of the geometry in GaussGeo
LHCbGeo().ExternalDetectorEmbedder = "EcalCollectorEmbedder"

# generating ecal scoring plane in a parallel world
# (will have the same hits)
SimPhase().ParallelGeometry = True

# adding external detectors
parallel = ExternalDetectorEmbedder("EcalScoringEmbedder")

# generating ecal scoring plane
par_shapes = {
    "EcalScorer": {
        "Type": "Cuboid",
        "xPos": 0.0 * SystemOfUnits.m,
        "yPos": 0.0 * SystemOfUnits.m,
        "zPos": 11948.0 * SystemOfUnits.mm
        + 0.5 * SystemOfUnits.m,  # should enclose ECAL
        "xSize": 10.0 * SystemOfUnits.m,
        "ySize": 10.0 * SystemOfUnits.m,
        "zSize": 1.0 * SystemOfUnits.m,
        "xAngle": -0.207 * SystemOfUnits.degree,
        "OutputLevel": DEBUG,
    },
}

par_sensitive = {
    "EcalScorer": {
        "Type": "MCCollectorSensDet",
        "RequireEDep": True,
        "OnlyForward": False,
        "OnlyAtBoundary": False,
        "PrintStats": True,
        "OutputLevel": DEBUG,
    },
}

par_hit = {
    "EcalScorer": {
        "Type": "GetMCCollectorHitsAlg",
        "OutputLevel": DEBUG,
    },
}

parallel.Shapes = par_shapes
parallel.Sensitive = par_sensitive
parallel.Hit = par_hit

ParallelGeometry().ParallelWorlds = {
    "ParallelWorld1": {
        "ExternalDetectorEmbedder": "EcalScoringEmbedder",
    },
}
ParallelGeometry().ParallelPhysics = {
    "ParallelWorld1": {
        "LayeredMass": False,  # see ECAL material
        "ParticlePIDs": [],  # track all particles
    },
}
