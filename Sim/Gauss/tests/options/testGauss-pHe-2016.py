###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions

importOptions("$APPCONFIGOPTS/Gauss/pHe-Beam6500GeV-0GeV-md100-2016-fix1.py")
importOptions("$GAUSSOPTS/BeamGas.py")
importOptions("$GAUSSOPTS/DBTags-2016.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")

from Configurables import Gauss
from GaudiKernel import SystemOfUnits

Gauss().FixedTargetParticle = "He"
Gauss().FixedTargetLuminosity = 5 * (10**25) / (SystemOfUnits.cm2 * SystemOfUnits.s)
Gauss().FixedTargetXSection = 120.0 * SystemOfUnits.millibarn
