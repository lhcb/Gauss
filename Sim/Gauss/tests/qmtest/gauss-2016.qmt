<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Check that Gauss on Gaussino can run with a DD4hep geometry and save output.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="timeout"><integer>3600</integer></argument>
<argument name="args"><set>
  <text>$GAUSSOPTS/General/Threads-8.py</text>
  <text>$GAUSSOPTS/General/Events-10.py</text>
  <text>$GAUSSOPTS/General/2016.py</text>
  <text>$DECFILESROOT/options/30000000.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

import re

# check we processed the expected number of events
findReferenceBlock("GenRndInit           INFO 10 events processed", id="processed")

def get_hits_no(stdout, det, reg, identifier):
    matches = re.findall(reg, stdout)
    if not matches:
        causes.append(f"Something went wrong when setting up '{det}'.")
    subhits_str = re.findall(rf"{identifier}.*", matches[0])
    hits_no = int(subhits_str[0].split('|')[2].strip())
    if not hits_no:
        causes.append(f"Hits were not registered for '{det}'.")

calo_reg = r"GaussGeo\.{}.+\n.+\n.+\n.+\n.+\n.+?(?=\"#tslots).*"
for calo in ["Spd", "Prs", "Ecal", "Hcal"]:
    get_hits_no(stdout, calo, calo_reg.format(calo), "#tslots")

tracker_reg = r"{}HitMonitor.+\n.+\n.+\n.+\n.+?(?=\"numberHits).*"
for tracker in ["IT", "OT", "TT"]:
    get_hits_no(stdout, tracker, tracker_reg.format(tracker), "numberHits")

# check if some MCHits were produced (different message otherwise)
velo_reg = r"VeloGaussMoni\W+INFO\W+Number\W+of\W+MCHits\/Event:\W+[+-]?[0-9]+"
matches = re.findall(velo_reg, stdout)
if not matches:
    causes.append("Something went wrong when setting up 'Velo'.")
hits_no = float(matches[0].split()[-1])
if not hits_no:
    causes.append("Hits were not registered for 'Velo'.")

muon_reg = r"MuonHitChecker\W+INFO.+allR"
matches = re.findall(muon_reg, stdout)
if not matches:
    causes.append("Something went wrong when setting up 'Muon'.")
hits_no = [float(val) for val in matches[0].split()[2:7]]
if not any(hits_no):
    causes.append("Hits were not registered for 'Muon'.")

# check we persisted the expected number of events
findReferenceBlock("GaussTape            INFO Events output: 10", id="output")

</text></argument>
</extension>
