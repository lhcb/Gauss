###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import Configurable, importOptions


# FIXME: ported from Gaussino
#        as it is still needed for CALO...
#        drop it!!!!
def run_once(func):
    def decorated(*args, **kwargs):
        if not hasattr(func, "store"):
            func.store = {}
            if "name" in kwargs:
                key = kwargs["name"]
            else:
                key = Configurable.DefaultName
            if key not in func.store:
                func.store[key] = func(*args, **kwargs)
            return func.store[key]

    return decorated


@run_once
def _importBaseOptions():
    importOptions("$STDOPTS/PreloadUnits.opts")


def GaussImportOptions(options):
    _importBaseOptions()
    importOptions(options)


class HelperBase:
    def slotName(self, slot):
        name = slot
        if slot == "":
            name = "Main"
        return name

    def isGaussMP(self):
        import argparse

        parser = argparse.ArgumentParser(description="Hello")

        parser.add_argument("--ncpus", action="store", type=int, default=0)
        args = parser.parse_known_args()
        return args[0].ncpus != 0

    ##
    # Helper functions for spill-over
    def slot_(self, slot):
        if slot != "":
            return slot + "/"
        return slot

    ##
    def setTrackersHitsProperties(self, alg, det, slot, dd):
        alg.MCHitsLocation = "/Event/" + self.slot_(slot) + "MC/" + det + "/Hits"
        if det == "PuVeto":
            det = "VeloPu"
        alg.CollectionName = det + "SDet/Hits"
        alg.Detector = "/dd/Structure/LHCb/" + dd

    ##
    def evtMax(self):
        from Configurables import LHCbApp

        return LHCbApp().evtMax()

    ##
    def eventType(self):
        from Configurables import Generation

        evtType = ""
        if Generation("Generation").isPropertySet("EventType"):
            evtType = str(Generation("Generation").EventType)
        return evtType

    def setLHCbAppDetectors(self):
        from Configurables import LHCbApp

        # If detectors set in LHCbApp then use those
        if hasattr(LHCbApp(), "Detectors"):
            if not LHCbApp().Detectors:
                LHCbApp().Detectors = self.getProp("DetectorGeo")["Detectors"]
            else:
                log.warning(
                    "Value of 'LHCbApp().Detectors' already set, using that value: %s"
                    % (LHCbApp().Detectors)
                )
        return
