###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class OT(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("t")
        region = "AfterMagnetRegion"
        if "T" not in detPieces[region]:
            detPieces[region] += ["T/OT"]
        if "T/PipeInT" not in detPieces[region]:
            detPieces[region] += ["T/PipeInT"]

    def SetupExtractionImpl(self, slot=""):
        from Configurables import GetTrackerHitsAlgDetDesc

        self.simconf_name = "OT"
        region = "AfterMagnetRegion/T"
        det = "OT"
        alg = GetTrackerHitsAlgDetDesc(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
            Detector="/dd/Structure/LHCb/" + region + "/" + det,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        myZStations = [
            7938.0 * SystemOfUnits.mm,
            8625.0 * SystemOfUnits.mm,
            9315.0 * SystemOfUnits.mm,
        ]
        myZStationXMax = 350.0 * SystemOfUnits.cm
        myZStationYMax = 350.0 * SystemOfUnits.cm

        moni = MCHitMonitor(
            "OTHitMonitor" + slot,
            mcPathString="MC/OT/Hits",
            zStations=myZStations,
            xMax=myZStationXMax,
            yMax=myZStationYMax,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
