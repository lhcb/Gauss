###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class TT(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("tt")
        if "TT" not in detPieces["BeforeMagnetRegion"]:
            detPieces["BeforeMagnetRegion"] += ["TT"]

    def SetupExtractionImpl(self, slot=""):
        from Configurables import GetTrackerHitsAlgDetDesc

        self.simconf_name = "TT"
        region = "BeforeMagnetRegion"
        det = "TT"
        alg = GetTrackerHitsAlgDetDesc(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
            Detector="/dd/Structure/LHCb/" + region + "/" + det,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        myZStations = [2350.0 * SystemOfUnits.mm, 2620.0 * SystemOfUnits.mm]
        myZStationXMax = 100.0 * SystemOfUnits.cm
        myZStationYMax = 100.0 * SystemOfUnits.cm

        moni = MCHitMonitor(
            "TTHitMonitor" + slot,
            mcPathString="MC/TT/Hits",
            zStations=myZStations,
            xMax=myZStationXMax,
            yMax=myZStationYMax,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
