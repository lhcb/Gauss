###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class BLS(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the DD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named VP
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["Bls"] = "GiGaSensDetTrackerDD4hep/BlsSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the VP. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"Bls/{compVersion}/Bls.xml")

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        from Gauss.Geometry import GaussGeometry

        # Add the non-standard pieces of the BeforeMagnet region.
        region = "BeforeMagnetRegion"
        if region in detPieces:
            pieces = ["PipeJunctionBeforeVelo", "BeforeVelo"]
            for piece in pieces:
                if piece in detPieces[region]:
                    continue
                detPieces[region] += [piece]
        # Add the non-standard pieces of the Upstream region,
        # unless the Upstream region has been added as a whole.
        region = "UpstreamRegion"
        path = "/dd/Structure/LHCb/" + region
        if region in detPieces and path not in GaussGeometry._listOfGeoObjects_:
            pieces = ["BlockWallUpstr"]
            for piece in pieces:
                if piece in detPieces[region]:
                    continue
                detPieces[region] += [piece]

    def SetupExtractionImpl(self, slot=""):
        from Configurables import GetTrackerHitsAlgDetDesc

        self.simconf_name = "Bls"
        region = "BeforeMagnetRegion"
        det = "Bls"
        alg = GetTrackerHitsAlgDetDesc(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )
        # Only add the detectors if we are not using DD4hep so that the ID is
        # looked up using DetDesc
        if not self.getProp("UseDD4hep"):
            alg.Detector = "/dd/Structure/LHCb/" + region + "/" + det
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import BlsHitChecker
        from GaudiKernel import SystemOfUnits

        moni = BlsHitChecker("BlsHitCheckerAll" + slot)
        from Configurables import ApplicationMgr

        moni.HistoDir = "BlsHitChecker/BlsHitCheckerAll"
        moni.BlsAOn = True
        moni.BlsCOn = True
        moni.HistogramTitlePrefix = "BlsA+C: "
        moni.EntryXMin = -150.0 * SystemOfUnits.mm
        moni.EntryXMax = +150.0 * SystemOfUnits.mm
        moni.EntryXNbins = 300
        moni.EntryYMin = -150.0 * SystemOfUnits.mm
        moni.EntryYMax = +150.0 * SystemOfUnits.mm
        moni.EntryYNbins = 300
        moni.EntryZMin = -2200.0 * SystemOfUnits.mm
        moni.EntryZMax = -1900.0 * SystemOfUnits.mm
        moni.EntryZNbins = 300
        moni.EntryTimeOffset = +0.0 * SystemOfUnits.ns
        moni.EntryTimeMin = -50.0 * SystemOfUnits.ns
        moni.EntryTimeMax = +50.0 * SystemOfUnits.ns
        moni.EntryTimeNbins = 100
        moni.EventNumMin = 0.0
        moni.EventNumMax = 1000.0
        moni.EventNumNbins = 1000
        moni.EventNumTracksMin = 0.0
        moni.EventNumTracksMax = 50.0
        moni.EventNumTracksNbins = 50
        moni.TrackEnDepMin = 0.0
        moni.TrackEnDepMax = 50.0
        moni.TrackEnDepNbins = 50
        moni.TrackLengthMin = 0.0
        moni.TrackLengthMax = 7.0
        moni.TrackLengthNbins = 70
        ApplicationMgr().TopAlg += [moni]

        moni = BlsHitChecker("BlsHitCheckerBlsA" + slot)
        moni.HistoDir = "BlsHitChecker/BlsHitCheckerBlsA"
        moni.BlsAOn = True
        moni.HistogramTitlePrefix = "BlsA: "
        ApplicationMgr().TopAlg += [moni]

        moni = BlsHitChecker("BlsHitCheckerBlsC" + slot)
        moni.HistoDir = "BlsHitChecker/BlsHitCheckerBlsC"
        moni.BlsCOn = True
        moni.HistogramTitlePrefix = "BlsC: "
        ApplicationMgr().TopAlg += [moni]
