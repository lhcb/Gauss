###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getDetConditionPath, subdetector


@subdetector
class VP(det_base):
    __slots__ = {}
    region = "BeforeMagnetRegion"

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named VP
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["VP"] = "GiGaSensDetTrackerDD4hep/VPSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the VP. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"VP/{compVersion}/VP.xml")

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("velo")
        if self.region in detPieces:
            detPieces[self.region] += ["VP"]

    def SetupExtractionImpl(self, slot=""):
        if self.getProp("UseDD4hep"):
            from Configurables import GetTrackerHitsAlg
        else:
            from Configurables import GetTrackerHitsAlgDetDesc as GetTrackerHitsAlg

        self.simconf_name = "VP"
        det = "VP"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )
        if not self.getProp("UseDD4hep"):
            alg.Detector = getDetConditionPath(
                self.region, det, self.getProp("UseDD4hep")
            )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import VPGaussMoni

        moni = VPGaussMoni("VPGaussMoni" + slot)
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
