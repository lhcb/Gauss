###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class SL(det_base):
    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("t")
        region = "AfterMagnetRegion"
        if "T" not in detPieces[region]:
            detPieces[region] += ["T/SL"]
        # PSZ - line below might need to go depending on SL definition
        if "T/PipeInT" not in detPieces[region]:
            detPieces[region] += ["T/PipeInT"]

    def SetupMonitor(self, slot=""):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        myZStations = [
            7780.0 * SystemOfUnits.mm,
            # 8460.0*SystemOfUnits.mm,
            9115.0 * SystemOfUnits.mm,
        ]
        myZStationXMax = 150.0 * SystemOfUnits.cm
        myZStationYMax = 150.0 * SystemOfUnits.cm

        moni = MCHitMonitor(
            "SLHitMonitor" + slot,
            mcPathString="MC/SL/Hits",
            zStations=myZStations,
            xMax=myZStationXMax,
            yMax=myZStationYMax,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
