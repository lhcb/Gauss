###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getDetConditionPath, subdetector


@subdetector
class TV(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named TV
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["TV"] = "GiGaSensDetTrackerDD4hep/TVSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the TV. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"TV/{compVersion}/TV.xml")

    def SetupExtractionImpl(self, slot=""):
        if not self.getProp("UseDD4hep"):
            raise RuntimeError("ERROR: TV detector not supported in DetDesc")

        from Configurables import GetTrackerHitsAlg

        self.simconf_name = "TV"
        det = "TV"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )

        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import VPGaussMoni

        moni = VPGaussMoni(
            "TVGaussMoni" + slot, VeloMCHits="MC/TV/Hits", HistTopDirName="TV/"
        )  # able to reuse VPGaussMoni by just altering the hits location for now

        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
