###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# TODO: Michal M.: remove this, this does not have any sense
from Gauss.Detectors.BCM import *
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.BLS import *
from Gauss.Detectors.CALO import *
from Gauss.Detectors.FT import *
from Gauss.Detectors.HC import *
from Gauss.Detectors.IT import *
from Gauss.Detectors.Magnet import *
from Gauss.Detectors.MP import *
from Gauss.Detectors.Muon import *
from Gauss.Detectors.OT import *
from Gauss.Detectors.PuVeto import PuVeto
from Gauss.Detectors.RICH import *
from Gauss.Detectors.Shield import *
from Gauss.Detectors.SMOG2 import *
from Gauss.Detectors.TT import *
from Gauss.Detectors.TV import *
from Gauss.Detectors.UP import *
from Gauss.Detectors.UT import *
from Gauss.Detectors.Velo import *
from Gauss.Detectors.Velo import Velo
from Gauss.Detectors.VP import *

__all__ = ["BeamPipe"]
