###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getDetConditionPath, subdetector


@subdetector
class MP(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named MP
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["MP"] = "GiGaSensDetTrackerDD4hep/MPSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the MP. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        go = GaussGeometry._listOfXMLIncludes_.append(f"MP/{compVersion}/MP.xml")

    def SetupExtractionImpl(self, slot=""):
        from Configurables import GetTrackerHitsAlg

        self.simconf_name = "MP"
        det = "MP"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )

        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        myZStations = [
            7694.5 * SystemOfUnits.mm,
            8157.0 * SystemOfUnits.mm,
            8317.0 * SystemOfUnits.mm,
            8749.0 * SystemOfUnits.mm,
            8909.0 * SystemOfUnits.mm,
            9371.5 * SystemOfUnits.mm,
        ]

        moni = MCHitMonitor(
            "MPHitMonitor" + slot,
            mcPathString="MC/MP/Hits",
            zStations=myZStations,
            xMax=170.0 * SystemOfUnits.cm,
            yMax=50.0 * SystemOfUnits.cm,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
