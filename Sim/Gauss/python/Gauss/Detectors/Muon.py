###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getDetConditionPath, subdetector


@subdetector
class Muon(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the DD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named VP
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")

        #
        mappings["Muon"] = "GiGaSensDetTrackerDD4hep/MuonSDet"

        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the VP. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"Muon/{compVersion}/Muon.xml")

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        region = "DownstreamRegion"
        detPieces[region] += ["Muon"]

    def SetupExtractionImpl(self, slot=""):
        if self.getProp("UseDD4hep"):
            from Configurables import GetTrackerHitsAlg
        else:
            from Configurables import GetTrackerHitsAlgDetDesc as GetTrackerHitsAlg

        self.simconf_name = "Muon"
        det = "Muon"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )
        if not self.getProp("UseDD4hep"):
            alg.Detector = getDetConditionPath(
                "DownstreamRegion", det, self.getProp("UseDD4hep")
            )

        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        moni = None
        if self.getProp("UseDD4hep"):
            from Configurables import MuonHitCheckerDD4hep

            moni = MuonHitCheckerDD4hep
        else:
            from Configurables import MuonHitCheckerDetDesc

            moni = MuonHitCheckerDetDesc
        from Configurables import ApplicationMgr, GaussGeometry

        ApplicationMgr().TopAlg.append(
            moni(
                "MuonHitChecker" + slot,
                FullDetail=True,
                IsRun1Or2=GaussGeometry.run1or2,
            )
        )
