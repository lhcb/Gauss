###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getsubdetector, subdetector


@subdetector
class PuVeto(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        pass

    def SetupExtractionImpl(self, slot=""):
        self.simconf_name = "PuVeto"
        det = "PuVeto"
        region = "BeforeMagnetRegion"
        from Configurables import GetTrackerHitsAlgDetDesc

        if not getsubdetector("VP").Active:
            hits = GetTrackerHitsAlgDetDesc(
                "Get" + det + "Hits" + slot,
                MCHitsLocation="MC/" + det + "/Hits",
                CollectionName="VeloPuSDet/Hits",
                Detector="/dd/Structure/LHCb/" + region + "/Velo",
            )
            from Configurables import ApplicationMgr

            ApplicationMgr().TopAlg += [hits]
