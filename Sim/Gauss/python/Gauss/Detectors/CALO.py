###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel import SystemOfUnits
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector
from Gauss.Utilities import run_once


@run_once
def add_histos():
    from Configurables import HistogramDataSvc

    HistogramDataSvc().Input += [
        "GaussCalo DATAFILE='$PARAMFILESROOT/data/gausscalo.root' TYP='ROOT'"
    ]


@subdetector
class SPD(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, _, detPieces):
        # options ported from the old file: "GaussGeo-Spd.opts"
        from Configurables import GaussGeo, SpdPrsSensDet

        geo = GaussGeo()
        geo.addTool(SpdPrsSensDet, name="Spd")
        geo.Spd.StartVolumes = [
            "/dd/Geometry/DownstreamRegion/Spd/Modules/InnCell",
            "/dd/Geometry/DownstreamRegion/Spd/Modules/MidCell",
            "/dd/Geometry/DownstreamRegion/Spd/Modules/OutCell",
        ]
        geo.Spd.EndVolume = "/dd/Geometry/DownstreamRegion/Spd/Installation/Spd"
        geo.Spd.zMin = 12300.0 * SystemOfUnits.mm
        geo.Spd.zMax = 15000.0 * SystemOfUnits.mm
        geo.Spd.Detector = "/dd/Structure/LHCb/DownstreamRegion/Spd"
        geo.Spd.IntegrationDelays = [
            18.5 * SystemOfUnits.ns,
            17.5 * SystemOfUnits.ns,
            16.5 * SystemOfUnits.ns,
        ]
        geo.Spd.Histograms = [
            "GaussCalo/SPDTIME/h1",
            "GaussCalo/SPDTIME/h2",
            "GaussCalo/SPDTIME/h3",
        ]
        # populate detector pieces
        BeamPipe.removeBeamPipeElements("calo")
        region = "DownstreamRegion"
        detPieces[region] += ["Spd"]
        detPieces[region] += ["Converter"]

    def SetupExtractionImpl(self, slot=""):
        add_histos()
        from Configurables import GetCaloHitsAlg

        self.simconf_name = "Spd"
        alg = GetCaloHitsAlg(
            f"GetSpdHits{slot}",
            MCHitsLocation="MC/Spd/Hits",
            CollectionName="Spd/Hits",
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        if slot != "":
            # Histograms are currently not split by SpillOver slot so skip
            # for now
            return
        from Configurables import SpdMonitorDetDesc

        moni = SpdMonitorDetDesc(
            f"SpdMonitor{slot}",
            OutputLevel=4,
            Regions=True,
            MaximumEnergy=10.0 * SystemOfUnits.MeV,
            Threshold=1.5 * SystemOfUnits.MeV,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]


@subdetector
class PRS(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, _, detPieces):
        # options ported from the old file: "GaussGeo-Prs.opts"
        from Configurables import GaussGeo, SpdPrsSensDet

        geo = GaussGeo()
        geo.addTool(SpdPrsSensDet, name="Prs")
        geo.Prs.StartVolumes = [
            "/dd/Geometry/DownstreamRegion/Prs/Modules/InnCell",
            "/dd/Geometry/DownstreamRegion/Prs/Modules/MidCell",
            "/dd/Geometry/DownstreamRegion/Prs/Modules/OutCell",
        ]
        geo.Prs.EndVolume = "/dd/Geometry/DownstreamRegion/Prs/Installation/Prs"
        geo.Prs.zMin = 12300.0 * SystemOfUnits.mm
        geo.Prs.zMax = 15000.0 * SystemOfUnits.mm
        geo.Prs.Detector = "/dd/Structure/LHCb/DownstreamRegion/Prs"
        geo.Prs.IntegrationDelays = [
            18.5 * SystemOfUnits.ns,
            17.5 * SystemOfUnits.ns,
            16.5 * SystemOfUnits.ns,
        ]
        geo.Prs.Histograms = [
            "GaussCalo/PRSTIME/h1",
            "GaussCalo/PRSTIME/h2",
            "GaussCalo/PRSTIME/h3",
        ]
        # populate detector pieces
        region = "DownstreamRegion"
        detPieces[region] += ["Prs"]

    def SetupExtractionImpl(self, slot=""):
        add_histos()
        from Configurables import GetCaloHitsAlg

        self.simconf_name = "Prs"
        alg = GetCaloHitsAlg(
            f"GetPrsHits{slot}",
            MCHitsLocation="MC/Prs/Hits",
            CollectionName="Prs/Hits",
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        if slot != "":
            # Histograms are currently not split by SpillOver slot so skip
            # for now
            return
        from Configurables import PrsMonitorDetDesc

        moni = PrsMonitorDetDesc(
            f"PrsMonitor{slot}",
            OutputLevel=4,
            Regions=True,
            MaximumEnergy=10.0 * SystemOfUnits.MeV,
            Threshold=1.5 * SystemOfUnits.MeV,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]


@subdetector
class ECAL(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, _, detPieces):
        # options ported from the old file: "GaussGeo-Ecal.opts"
        from Configurables import EcalSensDet, GaussGeo

        geo = GaussGeo()
        geo.addTool(EcalSensDet, name="Ecal")
        geo.Ecal.StartVolumes = [
            "/dd/Geometry/DownstreamRegion/Ecal/Modules/InnCell",
            "/dd/Geometry/DownstreamRegion/Ecal/Modules/MidCell",
            "/dd/Geometry/DownstreamRegion/Ecal/Modules/OutCell",
        ]
        geo.Ecal.EndVolume = "/dd/Geometry/DownstreamRegion/Ecal/Installation/Ecal"
        geo.Ecal.zMin = 12300.0 * SystemOfUnits.mm
        geo.Ecal.zMax = 15000.0 * SystemOfUnits.mm
        geo.Ecal.Histograms = ["GaussCalo/ECALTIME/h1", "GaussCalo/ECALTIME/h2"]
        # populate detector pieces
        region = "DownstreamRegion"
        detPieces[region] += ["Ecal"]

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Configurables import EcalSensDetFromDD4hep, Gauss, LHCbDD4hepCnvSvc

        svc = LHCbDD4hepCnvSvc()
        mappings = svc.getProp("SensDetMappings")
        mappings["Ecal"] = "EcalSensDetFromDD4hep/Ecal"
        sens_det = EcalSensDetFromDD4hep("LHCbDD4hepCnvSvc.Ecal")
        # needed to get the IOV slice
        sens_det.RunNumber = Gauss().getProp("RunNumber")
        sens_det.Histograms = ["GaussCalo/ECALTIME/h1", "GaussCalo/ECALTIME/h2"]
        # populate xml files
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"ECAL/{compVersion}/ECAL.xml")

    def SetupExtractionImpl(self, slot=""):
        add_histos()
        from Configurables import GetCaloHitsAlg

        self.simconf_name = "Ecal"
        alg = GetCaloHitsAlg(
            f"GetEcalHits{slot}",
            MCHitsLocation="MC/Ecal/Hits",
            CollectionName="Ecal/Hits",
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        if slot != "":
            # Histograms are currently not split by SpillOver slot so skip
            # for now
            return
        moni_conf = None
        if self.getProp("UseDD4hep"):
            from Configurables import EcalMonitorDD4hep

            moni_conf = EcalMonitorDD4hep
        else:
            from Configurables import EcalMonitorDetDesc

            moni_conf = EcalMonitorDetDesc
        moni = moni_conf(
            f"EcalMonitor{slot}",
            OutputLevel=4,
            Regions=True,
            MaximumEnergy=1000.0 * SystemOfUnits.MeV,
            Threshold=10.0 * SystemOfUnits.MeV,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]


@subdetector
class HCAL(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, _, detPieces):
        # options ported from the old file: "GaussGeo-Hcal.opts"
        from Configurables import GaussGeo, HcalSensDet

        geo = GaussGeo()
        geo.addTool(HcalSensDet, name="Hcal")
        geo.Hcal.StartVolumes = [
            "/dd/Geometry/DownstreamRegion/Hcal/Cells/lvHcalInnCellUpScTile",
            "/dd/Geometry/DownstreamRegion/Hcal/Cells/lvHcalInnCellLowScTile",
            "/dd/Geometry/DownstreamRegion/Hcal/Cells/lvHcalOutCellScTile",
        ]
        geo.Hcal.EndVolume = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcal"
        geo.Hcal.zMin = 12300.0 * SystemOfUnits.mm
        geo.Hcal.zMax = 15000.0 * SystemOfUnits.mm
        geo.Hcal.Histograms = ["GaussCalo/HCALTIME/h1", "GaussCalo/HCALTIME/h2"]
        # populate detector pieces
        region = "DownstreamRegion"
        detPieces[region] += ["Hcal"]

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Configurables import Gauss, HcalSensDetFromDD4hep, LHCbDD4hepCnvSvc

        svc = LHCbDD4hepCnvSvc()
        mappings = svc.getProp("SensDetMappings")
        mappings["Hcal"] = "HcalSensDetFromDD4hep/Hcal"
        sens_det = HcalSensDetFromDD4hep("LHCbDD4hepCnvSvc.Hcal")
        # needed to get the IOV slice
        sens_det.RunNumber = Gauss().getProp("RunNumber")
        sens_det.Histograms = ["GaussCalo/HCALTIME/h1", "GaussCalo/HCALTIME/h2"]

        # populate xml files
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"HCAL/{compVersion}/HCAL.xml")

    def SetupExtractionImpl(self, slot=""):
        add_histos()
        from Configurables import GetCaloHitsAlg

        self.simconf_name = "Hcal"
        alg = GetCaloHitsAlg(
            f"GetHcalHits{slot}",
            MCHitsLocation="MC/Hcal/Hits",
            CollectionName="Hcal/Hits",
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        if slot != "":
            # Histograms are currently not split by SpillOver slot so skip
            # for now
            return
        moni_conf = None
        if self.getProp("UseDD4hep"):
            from Configurables import HcalMonitorDD4hep

            moni_conf = HcalMonitorDD4hep
        else:
            from Configurables import HcalMonitorDetDesc

            moni_conf = HcalMonitorDetDesc
        moni = moni_conf(
            f"HcalMonitor{slot}",
            OutputLevel=4,
            Regions=True,
            MaximumEnergy=1000.0 * SystemOfUnits.MeV,
            Threshold=5.0 * SystemOfUnits.MeV,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
