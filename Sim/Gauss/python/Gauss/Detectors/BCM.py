###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class BCM(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"Bcm/{compVersion}/Bcm.xml")

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        from Gauss.Geometry import GaussGeometry

        # Add the non-standard pieces of the BeforeMagnet region.
        region = "BeforeMagnetRegion"
        if region in detPieces:
            pieces = ["PipeJunctionBeforeVelo", "BeforeVelo"]
            for piece in pieces:
                if piece in detPieces[region]:
                    continue
                detPieces[region] += [piece]
        # Add the entire Upstream region.
        region = "UpstreamRegion"
        if region in detPieces:
            detPieces[region] = []
        GaussGeometry._listOfGeoObjects_.append("/dd/Structure/LHCb/" + region)
        # Add the AfterMuon part of the Downstream region.
        region = "DownstreamRegion"
        if region in detPieces:
            pieces = ["AfterMuon"]
            for piece in pieces:
                if piece in detPieces[region]:
                    continue
                detPieces[region] += [piece]
