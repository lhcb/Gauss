###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import log
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class SMOG2(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Gauss.Geometry import GaussGeometry

        dets = GaussGeometry().getProp("DetectorGeo")["Detectors"]

        smog2Parent = "VP"
        if smog2Parent in dets:
            GaussGeometry._listOfXMLIncludes_.append(f"SMOG2/{compVersion}/SMOG2.xml")

    def ApplyDetectorDetDesc(self, *_):
        log.warning("SMOG2 is not yet implemented for DetDesc")
