###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector
from Gauss.Geometry import GaussGeometry


@subdetector
class BeamPipe(det_base):
    __slots__ = {
        "State": "BeamPipeOn",
    }

    _beamPipeStates = ["beampipeon", "beampipeoff", "beampipeindet"]
    _beamPipeElements = {
        "velo": [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/" "DownStreamWakeFieldCone",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamWakeFieldCone",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownstreamPipeSections",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank",
        ],
        "rich1": [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1BeforeSubM",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1SubMaster",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1AfterSubM",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1BeamPipe",
        ],
        "tt": ["/dd/Structure/LHCb/BeforeMagnetRegion/TT/PipeInTT"],
        "ut": ["/dd/Structure/LHCb/BeforeMagnetRegion/UT/PipeInUT"],
        "magnet": [
            "/dd/Structure/LHCb/MagnetRegion/PipeInMagnet",
            "/dd/Structure/LHCb/MagnetRegion/PipeSupportsInMagnet",
        ],
        "AfterMagnetRegion": [
            "/dd/Structure/LHCb/AfterMagnetRegion/PipeAfterT",
            "/dd/Structure/LHCb/AfterMagnetRegion/PipeSupportsAfterMagnet",
        ],
        "t": ["/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT"],
        "rich2": ["/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe"],
        "downstreamregion": [
            "/dd/Structure/LHCb/DownstreamRegion/PipeDownstream",
            "/dd/Structure/LHCb/DownstreamRegion/PipeSupportsDownstream",
            "/dd/Structure/LHCb/DownstreamRegion/PipeBakeoutDownstream",
        ],
    }

    @staticmethod
    def removeBeamPipeElements(det):
        det = det.lower()
        # Remove beampipe elements in <det> - will be included automatically
        if det in BeamPipe._beamPipeElements.keys():
            for element in BeamPipe._beamPipeElements[det]:
                # remove all instances of the element
                while element in GaussGeometry._listOfGeoObjects_:
                    GaussGeometry._listOfGeoObjects_.remove(element)

    @staticmethod
    def removeAllBeamPipeElements():
        # Remove all beampipe elements
        for det in BeamPipe._beamPipeElements.keys():
            for element in BeamPipe._beamPipeElements[det]:
                # remove all instances of the element
                while element in GaussGeometry._listOfGeoObjects_:
                    GaussGeometry._listOfGeoObjects_.remove(element)

    def validateBeamPipeSwitch(self):
        bpString = self.getProp("State")
        bpLower = bpString.lower()
        if bpLower not in self._beamPipeStates:
            raise RuntimeError(
                "ERROR: BeamPipe configuration '%s' not recognised!" % bpString
            )

    def ApplyDetectorDD4hep(self, compVersion, *_):
        if "BeamPipeOn" != self.getProp("State"):
            return
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_ += [
            # Upstream region
            f"Pipe/{compVersion}/PipeUpstream.xml",
            # Before magnet region
            f"Pipe/{compVersion}/PipeBeforeVelo.xml",
            f"Pipe/{compVersion}/PipeBeforeMagnet.xml",
            f"Pipe/{compVersion}/PipeMagnet.xml",
            # After magnet region
            f"Pipe/{compVersion}/AfterMagnet-Params.xml",
            f"Pipe/{compVersion}/PipeAfterMagnet.xml",
            f"Pipe/{compVersion}/PipeT.xml",
            f"Pipe/{compVersion}/PipeAfterT.xml",
            # Downstream region
            # FIXME: PingPong cuts
            # "Pipe/PipeDownstream.xml",
            # "Pipe/PipeAfterMuon.xml",
        ]
        dets = GaussGeometry().getProp("DetectorGeo")["Detectors"]
        dets_with_beampipe = ["VP", "TV", "UT", "UP"]
        for det in dets_with_beampipe:
            if det in dets:
                GaussGeometry._listOfXMLIncludes_.append(
                    f"Pipe/{compVersion}/Pipe{det}.xml"
                )

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        # This function is only executed if BeamPipe was set to Active
        self.validateBeamPipeSwitch()

        if not "BeamPipeOn" == self.getProp("State"):
            return
        all_dets = GaussGeometry().getProp("DetectorGeo")["Detectors"]
        # Here commences a hack to deal with daft DDDB structure
        ignoreList = ["ut", "tt"]
        # decide if TT or UT in dets to simulate
        ttDetectorList = [det for det in ["UT", "TT"] if det in all_dets]
        # lower strings
        if ttDetectorList:
            # lower everything in ttDetectorList
            ttDetectorList = [det.lower() for det in ttDetectorList]
            # get the elements to ignore
            ignoreList = [det for det in ignoreList if det not in ttDetectorList]

        for region in self._beamPipeElements.keys():
            if region in ignoreList:
                continue
            for element in self._beamPipeElements[region]:
                GaussGeometry._listOfGeoObjects_.append(element)

        # Finally add in the TT or UT beampipe if we're not defining the
        # detectors but want the BP anyway depending on DataType
        # Nasty and unclean - change the DDDB s.t. it makes sense please!
        if "UT" not in all_dets and "TT" not in all_dets:
            from Configurables import Gauss

            datatype = Gauss().getProp("DataType")
            if datatype in Gauss.Run2DataTypes + Gauss.Run1DataTypes:
                for element in self._beamPipeElements["tt"]:
                    GaussGeometry._listOfGeoObjects_.append(element)
            elif datatype in Gauss.Run3DataTypes + Gauss.Run4DataTypes:
                for element in self._beamPipeElements["ut"]:
                    GaussGeometry._listOfGeoObjects_.append(element)
            else:
                msg = "UP is not implemented yet" "Understand what to do for UP"
                log.error(msg)
                raise NotImplementedError(msg)
