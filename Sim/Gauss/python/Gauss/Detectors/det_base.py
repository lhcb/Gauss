###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import Configurable, log
from Gaussino.Utilities import GaussinoConfigurable


# FIXME: stop using configurables for every sub-detector
#        it's too much, a class is enough
class det_base(GaussinoConfigurable):

    """Base class for all subdetector configurables. These configurables should
    not instantiate everything during the apply configuration call to allow
    for a more fine-grained activation. Maybe ..."""

    __required_configurables__ = [
        "GaussGeometry",
    ]

    __slots__ = {
        "active": False,
        "simulate": False,
        "monitor": False,
        "UseDD4hep": True,
        "simconf_name": "NONE",
    }

    @property
    def Active(self):
        return self.getProp("active")

    @property
    def Simulate(self):
        return self.getProp("simulate")

    @property
    def Monitor(self):
        return self.getProp("monitor")

    @Active.setter
    def Active(self, value):
        self.active = value

    @Simulate.setter
    def Simulate(self, value):
        if not self.Active:
            log.warning("Setting {} to be simulated but not active".format(self.name()))
        self.simulate = value

    @Monitor.setter
    def Monitor(self, value):
        if not self.Active or not self.Simulate:
            log.warning(
                "Setting {} to be monitored but not active"
                "or simulated".format(self.name())
            )
        self.monitor = value

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        raise NotImplementedError()

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        raise NotImplementedError()

    def ApplyStream(self):
        pass

    def SetupExtraction(self, slot=""):
        from Configurables import SimConf

        self.SetupExtractionImpl(slot)
        detlist = SimConf().getProp("Detectors")
        n = self.getProp("simconf_name")
        if n != "NONE" and n not in detlist:
            detlist += [n]

    def SetupExtractionImpl(self, slot=""):
        pass

    def SetupMonitor(self, slot=""):
        pass
