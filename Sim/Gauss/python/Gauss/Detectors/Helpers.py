###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__subdetector_configurables = {}


# TODO: Michal M.: remove this, this does not have any sense
def subdetector(obj):
    lname = obj.__name__.lower()
    if lname in __subdetector_configurables:
        raise RuntimeError("Class definition conflict found")
    __subdetector_configurables[lname] = obj
    obj.lname = lname
    return obj


# TODO: Michal M.: remove this, this does not have any sense
def getsubdetector(name):
    lname = name.lower()
    if lname not in __subdetector_configurables:
        raise RuntimeError("Class definition not found: {}".format(name))
    return __subdetector_configurables[lname]()


def getDetConditionPath(region, detector, isDD4Hep):
    """Returns the path of the alignment condition for a given subdetector"""
    if not isDD4Hep:
        return f"/dd/Structure/LHCb/{region}/{detector}"
    else:
        return f"/world/{region}/{detector}:DetElement-Info-IOV"
