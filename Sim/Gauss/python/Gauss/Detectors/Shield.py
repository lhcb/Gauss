###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class Shield(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(
            f"NeutronShielding/{compVersion}/NeutronShielding.xml"
        )

    def ApplyDetectorDetDesc(self, _, detPieces):
        region = "DownstreamRegion"
        if "NeutronShielding" not in detPieces[region]:
            detPieces[region] += ["NeutronShielding"]
