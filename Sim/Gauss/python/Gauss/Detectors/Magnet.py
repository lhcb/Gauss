###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getsubdetector, subdetector


@subdetector
class Magnet(det_base):
    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"Magnet/{compVersion}/Magnet.xml")

    def ApplyDetectorDetDesc(self, _, detPieces):
        detPieces["MagnetRegion"] = ["Magnet", "BcmDown"]

    def ApplyMagneticField(self):
        from Configurables import Gauss, GaussGeometry, MagneticFieldManager

        if self.Simulate:
            geosvc = magfield = None
            if GaussGeometry().getProp("Legacy"):
                from Configurables import DetDescMagneticField, GaussGeo

                geosvc = GaussGeo()
                magfield = DetDescMagneticField("LHCbField")
            else:
                from Configurables import DD4hepMagneticField, LHCbDD4hepCnvSvc

                geosvc = LHCbDD4hepCnvSvc()
                magfield = DD4hepMagneticField(
                    "LHCbField",
                    RunNumber=Gauss().getProp("RunNumber"),
                )

            geosvc.FieldManager = "MagneticFieldManager/FieldMgr"
            geosvc.addTool(MagneticFieldManager("FieldMgr"), name="FieldMgr")
            geosvc.FieldMgr.StepperFactory = "G4ClassicalRK4"
            geosvc.FieldMgr.FieldFactory = magfield.getFullName()
            geosvc.FieldMgr.addTool(magfield, name="LHCbField")

        if getsubdetector("HC").Simulate:
            # FIXME: there was a piece of code here that did not work
            # it has to be revisited before we can put it here again
            raise NotImplementedError()
