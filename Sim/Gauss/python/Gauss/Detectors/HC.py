###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import log
from GaudiKernel import SystemOfUnits
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class HC(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        from Configurables import LHCbApp
        from Gauss.Geometry import GaussGeometry

        year = LHCbApp().getProp("DataType")
        if year not in self.Run2DataTypes:
            log.warning(
                "Check your options: you have asked"
                "to simulate Herschel but not in %s." % year
            )
            log.warning("Simulating only the LHC tunnel.")
        # Add the non-standard pieces of the BeforeMagnet region.
        region = "BeforeMagnetRegion"
        if region in detPieces:
            pieces = ["PipeJunctionBeforeVelo", "BeforeVelo"]
            for piece in pieces:
                if piece in detPieces[region]:
                    continue
                detPieces[region] += [piece]
        # Add the AfterMuon part of the Downstream region.
        region = "DownstreamRegion"
        if region in detPieces:
            pieces = ["AfterMuon"]
            for piece in pieces:
                if piece in detPieces[region]:
                    continue
                detPieces[region] += [piece]
        # Add the entire Upstream, BeforeUpstream, and AfterDownstream regions.
        regions = ["UpstreamRegion", "BeforeUpstreamRegion", "AfterDownstreamRegion"]
        for region in regions:
            if region in detPieces:
                detPieces[region] = []
            GaussGeometry._listOfGeoObjects_.append("/dd/Structure/LHCb/" + region)

        # Extend the world volume.
        from Configurables import GaussGeo

        GaussGeo().ZsizeOfWorldVolume = 300.0 * SystemOfUnits.m
