###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import Configurable, log
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class Velo(det_base):
    __slots__ = {
        "State": "BeamPipeOn",
    }

    @staticmethod
    def checkVeloDDDB():
        """
        Check if the Velo geometry is compatible with the chosen tags
        """
        from Configurables import GaussGeometry
        from DetCond.Configuration import CondDB

        # set validity limits for  Velo geometry
        # first postMC09 Velo geometry
        GTagLimit1 = "head-20091120"
        GTagLimit1 = GTagLimit1.split("-")[1].strip()
        VeloLTagLimit1 = "velo-20091116"
        VeloLTagLimit1 = VeloLTagLimit1.split("-")[1].strip()
        # Thomas L. Velo geometry
        GTagLimit2 = "head-20100119"
        GTagLimit2 = GTagLimit2.split("-")[1].strip()
        VeloLTagLimit2 = "velo-20100114"
        VeloLTagLimit2 = VeloLTagLimit2.split("-")[1].strip()

        # DDDB global tag used
        DDDBDate = GaussGeometry().GeometryVersion
        DDDBDate = DDDBDate.split("-")

        # Check if DDDB tag has a regular format
        # (instead of e.g. a user git branch)
        if len(DDDBDate) > 1:
            DDDBDate = DDDBDate[1].strip()
            if DDDBDate.isdigit():
                # check if/which local tag is used for Velo
                cdb = CondDB()
                cdbVeloDate = 0
                for p in cdb.LocalTags:
                    if p == "DDDB":
                        taglist = list(cdb.LocalTags[p])
                        for ltag in taglist:
                            if ltag.find("velo") != -1:
                                cdbVeloDate = ltag.split("-")[1].strip()

                # Put this here rather than as an argument
                VeloPostMC09 = 0
                # check if the selected tags require
                # one of the postMC09 Velo geometries
                if (DDDBDate >= GTagLimit1) or (cdbVeloDate >= VeloLTagLimit1):
                    VeloPostMC09 = 1
                if (DDDBDate >= GTagLimit2) or (cdbVeloDate >= VeloLTagLimit2):
                    VeloPostMC09 = 2

                return VeloPostMC09

        log.warning(
            "DDDB tag not parsable as date. "
            "Using post-MC09 velo geometry : %s" % (DDDBDate)
        )
        return 2

    def veloMisAlignGeometry(self, VeloPostMC09):
        """
        File containing the list of detector element to explicitely set
        to have misalignement in the VELO.
        """
        # remove Automatically included detector elements
        from Gauss.Detectors.BeamPipe import BeamPipe

        BeamPipe.removeBeamPipeElements("velo")

        from Gauss.Geometry import GaussGeometry

        go = GaussGeometry._listOfGeoObjects_

        if "/dd/Structure/LHCb/BeforeMagnetRegion/Velo" in go:
            go.remove("/dd/Structure/LHCb/BeforeMagnetRegion/Velo")

        go += ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU00"]
        go += ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU02"]
        go += [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU01"
        ]  # NOQA
        go += [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU03"
        ]  # NOQA

        txt = "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModuleXX"
        import math

        for i in range(42):
            nr = str(i)
            if len(nr) == 1:
                nr = "0" + str(i)
            temp1 = txt.replace("XX", nr)
            if math.modf(float(nr) / 2.0)[0] > 0.1:
                temp1 = temp1.replace("Left", "Right")
            go += [temp1]

        go += [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamWakeFieldCone"
        ]  # NOQA
        go += [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamWakeFieldCone"
        ]  # NOQA
        if VeloPostMC09 == 1:
            # description postMC09 of Velo (head-20091120),
            # problem with Velo Tank simulation
            go += ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank"]
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownstreamPipeSections"
            ]  # NOQA
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections"
            ]  # NOQA
        elif VeloPostMC09 == 2:
            # Thomas L. newer description postMC09 of Velo
            # --- Velo Right
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFBoxRight"
            ]  # NOQA
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/DetSupportRight"
            ]  # NOQA
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ConstSysRight"
            ]  # NOQA
            # --- Velo Left
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFBoxLeft"
            ]  # NOQA
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/DetSupportLeft"
            ]  # NOQA
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ConstSysLeft"
            ]  # NOQA
            # --- Velo
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownstreamPipeSections"
            ]  # NOQA
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections"
            ]  # NOQA
            go += ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank"]
        else:
            go += ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamVacTank"]
            go += [
                "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamVacTank"
            ]  # NOQA

        go += [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFFoilRight"
        ]  # NOQA
        go += ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFFoilLeft"]

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        VeloP = Velo.checkVeloDDDB()
        if VeloP == 1 or VeloP == 2:
            basePieces["BeforeMagnetRegion"] = []

        # Also sort out mis-alignment
        VeloP = Velo.checkVeloDDDB()
        # No need to misalign if only PuVeto exits - check me PSZ.
        self.veloMisAlignGeometry(VeloP)  # To misalign VELO

    def SetupExtractionImpl(self, slot=""):
        from Configurables import GetTrackerHitsAlgDetDesc

        self.simconf_name = "Velo"
        region = "BeforeMagnetRegion"
        det = "Velo"
        alg = GetTrackerHitsAlgDetDesc(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
            Detector="/dd/Structure/LHCb/" + region + "/" + det,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import VeloGaussMoni

        moni = VeloGaussMoni("VeloGaussMoni" + slot)
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
