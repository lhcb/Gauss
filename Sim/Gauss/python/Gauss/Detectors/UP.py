###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class UP(det_base):
    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named UP
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["UP"] = "GiGaSensDetTrackerDD4hep/UPSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the UP. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"UP/{compVersion}/UP.xml")

    def SetupExtractionImpl(self, slot=""):
        from Configurables import GetTrackerHitsAlg

        self.simconf_name = "UP"
        det = "UP"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        myZStations = [
            2317.5 * SystemOfUnits.mm,
            2372.5 * SystemOfUnits.mm,
            2597.5 * SystemOfUnits.mm,
            2652.5 * SystemOfUnits.mm,
        ]

        moni = MCHitMonitor(
            "UPHitMonitor" + slot,
            mcPathString="MC/UP/Hits",
            zStations=myZStations,
            xMax=85.0 * SystemOfUnits.cm,
            yMax=85.0 * SystemOfUnits.cm,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
