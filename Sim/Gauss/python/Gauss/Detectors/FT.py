###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getDetConditionPath, subdetector


@subdetector
class FT(det_base):
    __slots__ = {}

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named FT
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["FT"] = "GiGaSensDetTrackerDD4hep/FTSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the FT. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"FT/{compVersion}/FT.xml")

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("t")
        region = "AfterMagnetRegion"
        if "T" not in detPieces[region]:
            detPieces[region] += ["T/FT"]
        if "T/PipeInT" not in detPieces[region]:
            detPieces[region] += ["T/PipeInT"]
        region = "DownstreamRegion"
        if "NeutronShielding" not in detPieces[region]:
            detPieces[region] += ["NeutronShielding"]

    def SetupExtractionImpl(self, slot=""):
        if self.getProp("UseDD4hep"):
            from Configurables import GetTrackerHitsAlg
        else:
            from Configurables import GetTrackerHitsAlgDetDesc as GetTrackerHitsAlg

        self.simconf_name = "FT"
        det = "FT"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )
        if not self.getProp("UseDD4hep"):
            alg.Detector = getDetConditionPath(
                "AfterMagnetRegion/T", det, self.getProp("UseDD4hep")
            )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot="", dataType="Run3"):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        if dataType == "Run5":
            myZStations = [
                7941.0 * SystemOfUnits.mm,
                8533.0 * SystemOfUnits.mm,
                9125.0 * SystemOfUnits.mm,
            ]
        else:
            myZStations = [
                7938.0 * SystemOfUnits.mm,
                8625.0 * SystemOfUnits.mm,
                9315.0 * SystemOfUnits.mm,
            ]

        myZStationXMax = 350.0 * SystemOfUnits.cm
        myZStationYMax = 350.0 * SystemOfUnits.cm

        moni = MCHitMonitor(
            "FTHitMonitor" + slot,
            mcPathString="MC/FT/Hits",
            zStations=myZStations,
            xMax=myZStationXMax,
            yMax=myZStationYMax,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
