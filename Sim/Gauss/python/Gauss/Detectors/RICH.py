###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector


@subdetector
class RICH1(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("rich1")
        detPieces["BeforeMagnetRegion"] += ["Rich1"]

    def ApplyStream(self):
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfGeoObjects_.append(
            "/dd/Geometry/BeforeMagnetRegion/Rich1/Rich1Surfaces"
        )
        GaussGeometry._listOfGeoObjects_.append(
            "/dd/Geometry/BeforeMagnetRegion/Rich1/RichHPDSurfaces"
        )


@subdetector
class RICH2(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("rich2")
        detPieces["AfterMagnetRegion"] += ["Rich2"]

    def ApplyStream(self):
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfGeoObjects_.append(
            "/dd/Geometry/AfterMagnetRegion/Rich2/Rich2Surfaces"
        )


@subdetector
class RICH1PMT(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("rich1")
        detPieces["BeforeMagnetRegion"] += ["Rich1"]

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Gauss.Geometry import GaussGeometry

        # FIXME: do not import pipe elements here
        GaussGeometry._listOfXMLIncludes_.append(f"Rich/{compVersion}/Rich1/Rich1.xml")

    def ApplyStream(self):
        from Configurables import GaussGeo
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfGeoObjects_.append(
            "/dd/Geometry/BeforeMagnetRegion/Rich1/Rich1Surfaces"
        )
        GaussGeometry._listOfGeoObjects_.append(
            "/dd/Geometry/BeforeMagnetRegion/Rich1/RichPMTSurfaces"
        )
        GaussGeo().UseAlignment = False
        GaussGeo().AlignAllDetectors = False


@subdetector
class RICH2PMT(det_base):
    __slots__ = {}

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("rich2")
        detPieces["AfterMagnetRegion"] += ["Rich2"]

    def ApplyDetectorDD4hep(self, compVersion, *_):
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"Rich/{compVersion}/Rich2/Rich2.xml")

    def ApplyStream(self):
        from Configurables import GaussGeo
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfGeoObjects_.append(
            "/dd/Geometry/AfterMagnetRegion/Rich2/Rich2Surfaces"
        )
        GaussGeo().UseAlignment = False
        GaussGeo().AlignAllDetectors = False
