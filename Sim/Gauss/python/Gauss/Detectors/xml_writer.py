###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import tempfile
import xml.etree.ElementTree as ET
from collections import OrderedDict
from xml.dom import minidom

from Gaudi.Configuration import log


# Combine the includes into a valid LHCb DD4hep xml and return name of temporary
# file
def create_xml(
    includesfiles: list,
    directory: str,
    beampipeversion: str,
    materialsversion: str,
    regionsversion=str,
    beampipeoff: bool = False,
    tmp_file_name: str = "LHCb.xml",
) -> tuple[str, str]:
    unique_includes = list(OrderedDict.fromkeys(includesfiles))

    def addroot(path: str, folder: str = "components"):
        new_path = path
        if not path.startswith(".") and not path.startswith("/"):
            new_path = os.path.join(directory, folder, path)
        if not os.path.isfile(new_path):
            raise FileNotFoundError(f"XML path '{new_path}' does not exist.")
        return new_path

    root = ET.Element("lccdd")
    debug = ET.SubElement(root, "debug")
    ET.SubElement(debug, "type", name="includes", value="0")
    ET.SubElement(debug, "type", name="incguard", value="1")
    includes = ET.SubElement(root, "includes")
    ET.SubElement(
        includes, "file", ref=addroot(f"Materials/{materialsversion}/common.xml")
    )
    if not beampipeoff:
        ET.SubElement(
            includes, "file", ref=addroot(f"Pipe/{beampipeversion}/PipeCommon.xml")
        )
    ET.SubElement(root, "include", ref=addroot(f"Regions/{regionsversion}/Regions.xml"))
    for inc in unique_includes:
        ET.SubElement(root, "include", ref=addroot(inc))
    tmp_file_dir = tempfile.mkdtemp()
    tmp_file_path = f"{tmp_file_dir}/{tmp_file_name}"
    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ")
    with open(tmp_file_path, "w") as f:
        f.write(xmlstr)
    log.debug("Wrote xml file to {}".format(tmp_file_path))
    return tempfile.tempdir, os.path.basename(tmp_file_dir)
