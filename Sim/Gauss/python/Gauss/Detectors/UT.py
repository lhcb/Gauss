###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Detectors.BeamPipe import BeamPipe
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import getDetConditionPath, subdetector


@subdetector
class UT(det_base):
    region = "BeforeMagnetRegion"

    def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
        # Configuring the DD4hep detector conversion.
        # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
        # the G4 volume with the correct sensdet factory if marked with a
        # dd4hep sensitive detector named UT
        from Configurables import LHCbDD4hepCnvSvc

        mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
        mappings["UT"] = "GiGaSensDetTrackerDD4hep/UTSDet"
        LHCbDD4hepCnvSvc().SensDetMappings = mappings

        # Add the necessary dd4hep includes for the UT. Also added all the
        # dependencies for material definitions that were identified using
        # trial and error. As the list is made unique before being added to
        # the xml this should be fine if they appear in multiple places
        from Gauss.Geometry import GaussGeometry

        GaussGeometry._listOfXMLIncludes_.append(f"UT/{compVersion}/UT.xml")

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        BeamPipe.removeBeamPipeElements("ut")
        if "UT" not in detPieces[self.region]:
            detPieces[self.region] += ["UT"]

    def SetupExtractionImpl(self, slot=""):
        if self.getProp("UseDD4hep"):
            from Configurables import GetTrackerHitsAlg
        else:
            from Configurables import GetTrackerHitsAlgDetDesc as GetTrackerHitsAlg

        self.simconf_name = "UT"
        det = "UT"
        alg = GetTrackerHitsAlg(
            "Get" + det + "Hits" + slot,
            MCHitsLocation="MC/" + det + "/Hits",
            CollectionName=det + "SDet/Hits",
        )
        if not self.getProp("UseDD4hep"):
            alg.Detector = getDetConditionPath(
                self.region, det, self.getProp("UseDD4hep")
            )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [alg]

    def SetupMonitor(self, slot=""):
        from Configurables import MCHitMonitor
        from GaudiKernel import SystemOfUnits

        myZStations = [2350.0 * SystemOfUnits.mm, 2620.0 * SystemOfUnits.mm]
        myZStationXMax = 100.0 * SystemOfUnits.cm
        myZStationYMax = 100.0 * SystemOfUnits.cm

        moni = MCHitMonitor(
            "UTHitMonitor" + slot,
            mcPathString="MC/UT/Hits",
            zStations=myZStations,
            xMax=myZStationXMax,
            yMax=myZStationYMax,
        )
        from Configurables import ApplicationMgr

        ApplicationMgr().TopAlg += [moni]
