###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Default list of sub-detectors for Run 1 (with DetDesc)
RUN1_DETECTORS = [
    "PuVeto",
    "Velo",
    "TT",
    "IT",
    "OT",
    "Rich1",
    "Rich2",
    "Spd",
    "Prs",
    "Ecal",
    "Hcal",
    "Muon",
    "Magnet",
]

# Default list of sub-detectors for Run 2 (with DetDesc)
RUN2_DETECTORS = RUN1_DETECTORS

# Default list of sub-detectors for Run 3 (with DetDesc or DD4hep)
RUN3_DETECTORS = [
    # "BLS",
    # "BCM",
    "VP",
    "SMOG2",
    "UT",
    "FT",
    "Shield",
    "Rich1Pmt",
    "Rich2Pmt",
    "Ecal",
    "Hcal",
    "Muon",
    "Magnet",
]

RUN3_DETECTORS_NO_UT = [det for det in RUN3_DETECTORS if det != "UT"]

# Default list of sub-detectors for Run 3 (with DetDesc)
RUN3_DETECTORS_DETDESC = RUN3_DETECTORS

RUN3_DETECTORS_DETDESC_NO_UT = [det for det in RUN3_DETECTORS_DETDESC if det != "UT"]

RUN4_DETECTORS = [
    "VP",
    "SMOG2",
    "UT",
    "FT",
    "Shield",
    "Rich1Pmt",
    "Rich2Pmt",
    "Ecal",  # TODO: Change to new Run4 Ecal when available
    "Hcal",
    "Muon",
    "Magnet",
]

RUN5_DETECTORS = [
    "FT",
    "MP",
    "Shield",
    "Rich1Pmt",
    "Rich2Pmt",
    "Hcal",
    "Muon",
    "Magnet",
    "TV",
    "UP",
]

DEFAULT_DETECTORS = {
    # Run 1
    "2010": RUN1_DETECTORS,
    "2011": RUN1_DETECTORS,
    "2012": RUN1_DETECTORS,
    "2013": RUN1_DETECTORS,
    # Run 2
    "2015": RUN2_DETECTORS,
    "2016": RUN2_DETECTORS,
    "2017": RUN2_DETECTORS,
    "2018": RUN2_DETECTORS,
    # Run 3
    "2022": RUN3_DETECTORS_NO_UT,
    "2023": RUN3_DETECTORS,
    "2024": RUN3_DETECTORS,
    "Run3": RUN3_DETECTORS,
    # Future Upgrades
    "Run4": RUN4_DETECTORS,
    "Run5": RUN5_DETECTORS,
}

CONDITIONS_VERSIONS_DD4HEP = {
    # TODO: use 'sim11/2022' when ready
    "2022": "sim10/run3-ideal",
    # TODO: use 'sim11/2023' when ready
    "2023": "sim10/run3-ideal",
    "2024": "sim10/run3-ideal",
    # Ideal Detectors
    "Run3": "sim10/run3-ideal",
    "Run4": "sim10/run4-ideal",
    "Run5": "sim11/run5-ideal",
}

GEOMETRY_VERSIONS_DD4HEP = {
    # TODO: use 2022 geometry version when ready
    "2022": "run3/trunk",
    # TODO: use 2023 geometry version when ready
    "2023": "run3/trunk",
    "2024": "run3/trunk",
    # Ideal Detectors,
    "Run3": "run3/trunk",
    "Run4": "run4/trunk",
    "Run5": "run5/trunk",
}

DEFAULT_DD4HEP_BEAMPIPE_VERSION = "trunk"

INCOMPATIBLE_DETECTORS = [
    # Upgrade 2 Developers, probably add your detector here
    # [det1, det2, ...]
    ["Velo", "VP", "TV"],
    ["PuVeto", "VP", "TV"],
    ["TT", "UT", "UP"],
    # ["Muon", "MuonNoM1"], not supported
    ["Muon", "Torch"],
    ["MP", "IT"],
    ["MP", "OT"],
    ["FT", "IT"],
    ["FT", "OT"],
    ["Rich1", "Rich1Pmt"],
    ["Rich2", "Rich2Pmt"],
]

NON_SIMULATABLE_DETECTORS = [
    "Rich1",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Rich2",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Rich1Pmt",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Rich2Pmt",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
]

NON_MONITORABLE_DETECTORS = [
    "Rich1",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Rich2",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Rich1Pmt",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Rich2Pmt",  # FIXME: waiting for https://gitlab.cern.ch/lhcb/Gauss/-/merge_requests/820
    "Magnet",
]
