###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaussino.pytest.options import add_option

debug = add_option("$GAUSSOPTS/General/Debug.py")
events_1 = add_option("$GAUSSOPTS/General/Events-1.py")
photon = add_option("$GAUSSOPTS/Generation/ParticleGun-FixedMomentum-Photon1GeV.py")
cube = add_option("$GAUSSOPTS/Geometry/ExternalDetector-SimpleCube.py")
empty = add_option("$GAUSSOPTS/Geometry/NoDetectors.py")
data_2022 = add_option("$GAUSSOPTS/General/2022.py")
data_2018 = add_option("$GAUSSOPTS/General/2018.py")
detdesc = add_option("$GAUSSOPTS/Geometry/DetDesc.py")
