###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = "Gloria Corti, Dominik Muller, and Michal Mazurek"
__email__ = "lhcb-simulation@cern.ch"

from GaudiKernel import SystemOfUnits as units
from Gaussino.Generation import GaussinoGeneration

# Configurables (do NOT use 'from Configurables' here)
from Gaussino.Utilities import GaussinoConfigurable


class GaussGeneration(GaussinoConfigurable):
    __required_configurables__ = [
        "Gauss",
    ]

    __slots__ = {
        "BeamMomentum": 3.5 * units.TeV,
        "BeamHCrossingAngle": -0.520 * units.mrad,
        "BeamVCrossingAngle": 0.0,
        "BeamEmittance": 0.0037 * units.mm,
        "BeamBetaStar": 3.1 * units.m,
        "BeamBetaStarX": -1.0 * units.m,
        "BeamBetaStarY": -1.0 * units.m,
        "BeamLineAngles": [-0.075 * units.mrad, 0.035 * units.mrad],
        "InteractionPosition": [0.459 * units.mm, -0.015 * units.mm, 0.5 * units.mm],
        "BunchRMS": 82.03 * units.mm,
        "Luminosity": 0.247 * (10**30) / (units.cm2 * units.s),
        "TotalCrossSection": 91.1 * units.millibarn,
        # FIXME: beam particles are now fixed in LHCbDefaults.cmd
        #        to be made generic! (in Gaussino)
        # "B2Momentum": 3.5 * units.TeV,
        # "B1Particle": 'p',
        # "B2Particle": 'p',
        "GenMonitor": True,
        "ParticleGun": False,
        "SampleGenerationTool": "SignalPlain",
        "SampleGenerationToolOpts": {},
        "PileUpTool": "FixedLuminosityWithSvc",
        "VertexSmearingTool": "BeamSpotSmearVertexWithSvc",
        "ProductionTool": "Pythia8ProductionMT",
        "ProductionToolOpts": {},
        "DecayTool": "",
        "CutTool": "",
        "CutToolOpts": {},
        "FullGenEventCutTool": "",
        "FullGenEventCutToolOpts": {},
    }

    GAUSSINO_GENERATION_OPTIONS = [
        "BeamMomentum",
        "BeamHCrossingAngle",
        "BeamVCrossingAngle",
        "BeamEmittance",
        "BeamBetaStar",
        "BeamBetaStarX",
        "BeamBetaStarY",
        "BeamLineAngles",
        "InteractionPosition",
        "BunchRMS",
        "Luminosity",
        "TotalCrossSection",
        "GenMonitor",
        "ParticleGun",
        "SampleGenerationTool",
        "SampleGenerationToolOpts",
        "PileUpTool",
        "VertexSmearingTool",
        "ProductionTool",
        "ProductionToolOpts",
        "DecayTool",
        "CutTool",
        "CutToolOpts",
        "FullGenEventCutTool",
        "FullGenEventCutToolOpts",
    ]

    def __apply_configuration__(self):
        self.propagateProperties(
            self.GAUSSINO_GENERATION_OPTIONS,
            GaussinoGeneration(),
        )
