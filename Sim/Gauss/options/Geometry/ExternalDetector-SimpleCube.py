###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import ExternalDetectorEmbedder, GaussGeometry
from ExternalDetector.Materials import OUTER_SPACE
from GaudiKernel import SystemOfUnits as units

emb_id = len(ExternalDetectorEmbedder.configurables)
emb_name = f"ExternalDetectorEmbedder_{emb_id}"
cube_name = f"{emb_name}_Cube"

GaussGeometry().ExternalDetectorEmbedder = emb_name
external = ExternalDetectorEmbedder(emb_name)
external.Shapes = {
    cube_name: {
        "Type": "Cuboid",
        "xSize": 1.0 * units.m,
        "ySize": 1.0 * units.m,
        "zSize": 1.0 * units.m,
    },
}

external.Sensitive = {
    cube_name: {
        "Type": "MCCollectorSensDet",
    },
}

# plain/testing geometry service
external.World = {
    "WorldMaterial": "OuterSpace",
    "Type": "ExternalWorldCreator",
}

# material needed for the external world
external.Materials = {
    "OuterSpace": OUTER_SPACE,
}
