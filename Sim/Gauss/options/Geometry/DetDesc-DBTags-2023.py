###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# File with LATEST database tags for Sim11
# YEAR = 2023 for pp-collisions
from Configurables import GaussGeometry

# The latest tags are the following
GaussGeometry().GeometryVersion = "dddb-20220323"
GaussGeometry().ConditionsVersion = "sim-20220509-vc-md100"
