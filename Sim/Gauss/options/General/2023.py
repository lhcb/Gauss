###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Gauss, GaussGeometry
from Gaudi.Configuration import importOptions

# FIXME: update the beam parameters for 2023
## # Here are beam settings as for various nu (i.e. mu and Lumi per bunch with
## # 25 ns bunch spacing are given
## This is the Run3 default luminosity
##   nu=7.6 (i.e. mu=5.31, Lumi=2.0*(10**33) with2400 colliding bunches)
importOptions("$APPCONFIGOPTS/Gauss/Beam6800GeV-md100-expected-2023-nu1.4.py")
#
## Nominal Lumi to begin with - and for faster tests
##   nu=3.8 (i.e. mu=2.66, Lumi=1.0*(10**33) )
# importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu3.8-HorExtAngle.py")
#
## For robustness studies
##   nu=11.4 (i.e. mu=4, Lumi=1.77*(10**33) )
# importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu11.4-HorExtAngle.py")

## # The spill-over is off for quick tests
## to enable spill-over use the followign options
# importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")

Gauss().DataType = "2023"

## # The latest supported global tag for 2023
if GaussGeometry().isPropertySet("Legacy") and GaussGeometry().Legacy:
    importOptions("$GAUSSOPTS/Geometry/DetDesc-DBTags-2023.py")
else:
    importOptions("$GAUSSOPTS/Geometry/DD4hep-DBTags-2023.py")
