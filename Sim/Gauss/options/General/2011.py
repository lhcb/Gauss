###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Gauss with Sim10 configuration and beam conditions as in
# production for 2011 data (3.5 TeV beams, nu=2, no spill-over)
#
# Syntax is:
#   gaudirun.py Gauss-2011.py <someInputJobConfiguration>.py
##############################################################################

# --Pick beam conditions as set in AppConfig
from Gaudi.Configuration import importOptions

importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam3500GeV-md100-2011-nu2.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2011.py")
importOptions("$APPCONFIGOPTS/Gauss/RICHRandomHits.py")
importOptions("$APPCONFIGOPTS/Gauss/NoPacking.py")


# --Set database tags
importOptions("$GAUSSOPTS/Geometry/DetDesc-DBTags-2011.py")
