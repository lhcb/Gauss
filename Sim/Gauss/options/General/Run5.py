###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions

# For quick test, need nominal conditions and dedicated studies for Run5
importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py")

# To be done: add nominal conditions for dedicated studies for Run5

from Configurables import Gauss

# In principle we don't need to propagate to LHCbApp()/DDDConf().DataType = "Run5" as that is not used in DD4Hep, but we need to understand better which DDDBConf we are using when running with DD4hep. If from Run2Support we may need to do some changes
Gauss().DataType = "Run5"

importOptions("$GAUSSOPTS/Geometry/DD4hep-DBTags-Run5.py")
