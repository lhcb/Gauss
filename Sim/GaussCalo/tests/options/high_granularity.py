###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# implement high granularity in all calos
from GaussCalo.HighGranularity import HighGranularCalo

hcg = HighGranularCalo()
hcg.Force = True
# apply the same level of granularity to all regions
nrows = 1
ncols = 1

hcg.Ecal["Inner"]["NRows"] = nrows
hcg.Ecal["Inner"]["NCols"] = ncols
hcg.Ecal["Middle"]["NRows"] = nrows
hcg.Ecal["Middle"]["NCols"] = ncols
hcg.Ecal["Outer"]["NRows"] = nrows
hcg.Ecal["Outer"]["NCols"] = ncols

hcg.Spd["Inner"]["NRows"] = nrows
hcg.Spd["Inner"]["NCols"] = ncols
hcg.Spd["Middle"]["NRows"] = nrows
hcg.Spd["Middle"]["NCols"] = ncols
hcg.Spd["Outer"]["NRows"] = nrows
hcg.Spd["Outer"]["NCols"] = ncols

hcg.Prs["Inner"]["NRows"] = nrows
hcg.Prs["Inner"]["NCols"] = ncols
hcg.Prs["Middle"]["NRows"] = nrows
hcg.Prs["Middle"]["NCols"] = ncols
hcg.Prs["Outer"]["NRows"] = nrows
hcg.Prs["Outer"]["NCols"] = ncols

hcg.Hcal["Inner"]["NRows"] = nrows
hcg.Hcal["Inner"]["NCols"] = ncols
hcg.Hcal["Outer"]["NRows"] = nrows
hcg.Hcal["Outer"]["NCols"] = ncols

from Configurables import NTupleSvc

NTupleSvc().Output = ["FILE1 DATAFILE='CaloCollectorHG.root' TYP='ROOT' OPT='NEW'"]
