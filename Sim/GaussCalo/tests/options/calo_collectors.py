###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    ExternalDetectorEmbedder,
    FlatNParticles,
    FlatPtRapidity,
    GaussGeneration,
    GaussGeo,
    GaussGeometry,
    NTupleSvc,
    ParallelGeometry,
    ParticleGun,
)
from Gaudi.Configuration import DEBUG
from GaudiKernel import SystemOfUnits as units
from Gauss.Detectors.BeamPipe import BeamPipe

GaussGeometry().DetectorGeo = {"Detectors": ["Spd", "Prs", "Ecal", "Hcal"]}
GaussGeometry().DetectorSim = {"Detectors": ["Spd", "Prs", "Ecal", "Hcal"]}
GaussGeometry().DetectorMoni = {"Detectors": ["Spd", "Prs", "Ecal", "Hcal"]}
BeamPipe().State = "BeamPipeOff"

GaussGeo().WorldMaterial = "/dd/Materials/Vacuum"
# Particle Gun On

GaussGeneration().ParticleGun = True
pgun = ParticleGun("ParticleGun")
pgun.ParticleGunTool = "FlatPtRapidity"
pgun.addTool(FlatPtRapidity, name="FlatPtRapidity")
pgun.FlatPtRapidity.PtMin = 0.5 * units.GeV
pgun.FlatPtRapidity.PtMax = 0.5 * units.GeV
pgun.FlatPtRapidity.RapidityMin = 2.5  # ensure in ECAL
pgun.FlatPtRapidity.RapidityMax = 4.8  # ensure in ECAL
pgun.FlatPtRapidity.PdgCodes = [22, 2112]
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool(FlatNParticles, name="FlatNParticles")
pgun.FlatNParticles.MinNParticles = 16
pgun.FlatNParticles.MaxNParticles = 16

# Run2 zmax options
zMax = 12280.0 * units.mm
collector_z_size = 0.001 * units.mm
collector_z_pos = zMax - collector_z_size / 2.0
collector_angle = 0.207 * units.degree

collector_shape = {
    "Type": "Cuboid",
    "zPos": collector_z_pos,
    "xSize": 10.0 * units.m,
    "ySize": 10.0 * units.m,
    "zSize": collector_z_size,
    "xAngle": -collector_angle,
    "OutputLevel": DEBUG,
}

collector_sensitive = {
    "Type": "MCCollectorSensDet",
    "RequireEDep": False,
    "OnlyForward": True,
    "OnlyAtBoundary": True,
    "OutputLevel": DEBUG,
}

collector_hit = {
    "Type": "GetMCCollectorHitsAlg",
    "OutputLevel": DEBUG,
}

collector_moni = {
    "CheckParent": True,
    "HitsPropertyName": "CollectorHits",
    "CollectorZ": collector_z_pos,
    "CollectorXAngle": collector_angle,
    "OutputLevel": DEBUG,
}

# here each different type of collector will placed
# in a separate parallel world

collector_types = ["", "Double", "Triple", "Quadrupole"]
geo_type = "DetDesc"
for index, (collector_type, calo) in enumerate(
    zip(collector_types, GaussGeometry().DetectorSim["Detectors"])
):
    full_type = f"{collector_type}CaloCollector{geo_type}"
    collector_moni["CaloHits" + str(index + 1)] = "MC/" + calo + "/Hits"
    collector_moni["CaloLocation" + str(index + 1)] = (
        "/dd/Structure/LHCb/DownstreamRegion/" + calo
    )
    external_name = collector_type + "CollectorEmbedder"
    external = ExternalDetectorEmbedder(external_name)
    external.Shapes[full_type] = dict(collector_shape)
    external.Sensitive[full_type] = dict(collector_sensitive)
    external.Hit[full_type] = dict(collector_hit)
    external.Moni[full_type] = {"Type": full_type, **dict(collector_moni)}
    pg = ParallelGeometry()
    pg_name = collector_type + "CollectorParallelWorld"
    pg.ParallelWorlds[pg_name] = {
        "ExternalDetectorEmbedder": external_name,
        "OutputLevel": DEBUG,
    }
    pg.ParallelPhysics[pg_name] = {
        "LayeredMass": True,
        "OutputLevel": DEBUG,
    }

NTupleSvc().Output = ["FILE1 DATAFILE='CaloCollector.root' TYP='ROOT' OPT='NEW'"]
