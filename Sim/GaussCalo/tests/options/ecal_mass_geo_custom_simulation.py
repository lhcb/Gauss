###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    CustomSimulation,
    EcalSensDet,
    FlatNParticles,
    Gauss,
    GaussGeneration,
    GaussGeo,
    GaussGeometry,
    GaussSimulation,
    MaterialEval,
    ParticleGun,
)
from Gaudi.Configuration import DEBUG
from GaudiKernel import SystemOfUnits as units
from Gauss.Detectors.BeamPipe import BeamPipe

# The idea is to scan ECAL with particles from a particle gun.
# It will produce x_steps * y_steps particles, so that when they pass through
# z = z_plane they will create a grid of deposits in ECAL (evenly spaced).

# number of steps in x
x_steps = 64
# number of steps in y
y_steps = 52
# EcalModXYSize is the size of the largest cell (outer)
EcalModXYSize = 121.7 * units.mm

Gauss(EvtMax=x_steps * y_steps)

GaussGeometry(
    DetectorGeo={"Detectors": ["Ecal"]},
    DetectorSim={"Detectors": ["Ecal"]},
    DetectorMoni={"Detectors": ["Ecal"]},
)
BeamPipe().State = "BeamPipeOff"

GaussGeneration(
    ParticleGun=True,
)

pgun = ParticleGun("ParticleGun")
# pgun.OutputLevel = DEBUG
pgun.ParticleGunTool = "MaterialEval"
pgun.addTool(MaterialEval, name="MaterialEval")
mateval = pgun.MaterialEval
mateval.UseGrid = True
# this is the beginning of the ECAL
mateval.ZPlane = 12520.0 * units.mm
# set ecal = grid x-y boundaries
mateval.Xmin = -32 * EcalModXYSize
mateval.Xmax = 32 * EcalModXYSize
mateval.Ymin = -26 * EcalModXYSize
mateval.Ymax = 26 * EcalModXYSize
# produce only gammas
mateval.PdgCode = 22
# set the granularity of the grid
mateval.NStepsInX = x_steps
mateval.NStepsInY = y_steps
# low energy so that the shower is not too big
mateval.ModP = 10.0 * units.MeV

pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool(FlatNParticles, name="FlatNParticles")
flat_p = pgun.FlatNParticles
flat_p.MinNParticles = 1
flat_p.MaxNParticles = 1

# general setup for an ImmediateDeposit model for fast simulation
GaussSimulation(CustomSimulation="ImmediateDepositCreator")

CustomSimulation(
    "ImmediateDepositCreator",
    Model={
        "EcalImmediateDeposit": {
            "Type": "ImmediateDepositModel",
            "OutputLevel": DEBUG,
        }
    },
    Region={
        "EcalImmediateDeposit": {
            "SensitiveDetectorName": "Ecal",
            "OutputLevel": DEBUG,
        }
    },
    Physics={
        "ParticlePIDs": [22],
        "OutputLevel": DEBUG,
    },
)

# Bypass postprecessing in ECAL sensitive detector when creating
# a fast-generated MCCaloHit
EcalSensDet(
    "GaussGeo.Ecal",
    # bypass Birk's & Non-Uniformity
    BypassCaloCorrections=True,
    # bypass time binning
    # (will create only one hit /cell/particle/25ns)
    BypassCaloTiming=True,
    # having both true means basically G4FastHit = MCCaloHit
)

# make sure there's no other volume on the way
# each photon must be able to reach the target cell
# note: this is just needed for having a reproducible test in DetDesc
visible_volumes = [
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/InnCell",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/InnCellSteel",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/InnCellPlastic",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/InnCellPb",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/FiberPb",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/MidCell",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/MidCellSteel",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/MidCellPlastic",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/MidCellPb",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/OutCell",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/OutCellSteel",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/OutCellPlastic",
    "/dd/Geometry/DownstreamRegion/Ecal/Modules/OutCellPb",
]

GaussGeo(
    VisibleVolumes=visible_volumes,
    WorldMaterial="/dd/Materials/Vacuum",
)
