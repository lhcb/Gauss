###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.pytest.options import data_2022, detdesc
from Gaussino.pytest.helpers import run_gaudi

results = [
    '| "#energy"                                       |      3328 |      33.04 |',
    '| "#hits"                                         |      3328 |       3304 |',
    '| "#subhits"                                      |      3328 |       3304 |',
    '| "#tslots"                                       |      3328 |       3304 |',
]


@data_2022
@detdesc
def test_ecal_mass_geo_custom_simulation():
    ex = run_gaudi(
        # additional options
        "$GAUSSCALOROOT/tests/options/ecal_mass_geo_custom_simulation.py",
    )
    assert ex.returncode == 0
    for result in results:
        assert result in ex.stdout
