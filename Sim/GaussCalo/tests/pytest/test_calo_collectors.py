###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re

from Gauss.pytest.options import data_2018, events_1
from Gaussino.pytest.helpers import run_gaudi


@events_1
@data_2018
def test_calo_collectors():
    ex = run_gaudi(
        # additional options
        "$GAUSSCALOROOT/tests/options/calo_collectors.py",
    )
    assert ex.returncode == 0

    det_calo_hits = {}
    calo_reg = r"GaussGeo\.{}.+\n.+\n.+\n.+\n.+\n.+?(?=\"#tslots).*"
    for calo in ["Spd", "Prs", "Ecal", "Hcal"]:
        matches = re.findall(calo_reg.format(calo), ex.stdout)
        if not matches:
            raise AssertionError(f"No hit counter info about {calo}")
        subhits_str = re.findall(r"#tslots.*", matches[0])
        det_calo_hits[calo] = int(subhits_str[0].split("|")[2].strip())

    calo_hits = {}
    coll_hits = {}
    coll_reg = r'^{}CaloC.+Number\W+of\W+counters[\s\S]+?(?="CollectorHitsCounter").*'
    for coll in ["", "Double", "Triple", "Quadrupole"]:
        coll_matches = re.findall(coll_reg.format(coll), ex.stdout, flags=re.MULTILINE)
        if not coll_matches:
            raise AssertionError(f"No hit counter info about {coll}")
        calo_hits_str = re.findall(r"CaloHitsCounter.*", coll_matches[0])
        calo_hits[coll] = int(calo_hits_str[0].split("|")[1].strip())
        coll_hits_str = re.findall(r"CollectorHitsCounter.*", coll_matches[0])
        coll_hits[coll] = int(coll_hits_str[0].split("|")[1].strip())

    coll_hits_uq = set(coll_hits.values())
    if len(coll_hits_uq) != 1 or sum(coll_hits_uq) != 16:
        raise AssertionError("MCCollector hit info was not written correctly")

    if calo_hits[""] != det_calo_hits["Spd"]:
        raise AssertionError("Single collector hit info was not written correctly")

    if calo_hits["Double"] != det_calo_hits["Spd"] + det_calo_hits["Prs"]:
        raise AssertionError("Double collector hit info was not written correctly")

    if (
        calo_hits["Triple"]
        != det_calo_hits["Spd"] + det_calo_hits["Prs"] + det_calo_hits["Ecal"]
    ):
        raise AssertionError("Triple collector hit info was not written correctly")

    if calo_hits["Quadrupole"] != sum(det_calo_hits.values()):
        raise AssertionError("Quadrupole collector hit info was not written correctly")
