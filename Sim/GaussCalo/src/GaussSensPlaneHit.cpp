/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Geant4
#include "G4Allocator.hh"
// local
#include "GaussSensPlaneHit.h"
// ============================================================================

using CLHEP::HepLorentzVector;

// ============================================================================
/** @file
 *
 *  Implementation file for class : GaussSensPlaneHit
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2003-07-07
 */
// ============================================================================

G4ThreadLocal G4Allocator<GaussSensPlaneHit>* aGaussSensPlaneHitAllocator{ nullptr };

// ============================================================================
/** Standard constructor
 *  @param track   trackID of th eparticle (or its parent particle!)
 *  @param position position(3D+time) of the hit
 *  @param momentum 4-momentum of the particle
 */
// ============================================================================
GaussSensPlaneHit::GaussSensPlaneHit( const TrackID& track, const LHCb::ParticleID& pid,
                                      const HepLorentzVector& position, const HepLorentzVector& momentum )
    : Gaussino::HitBase(), m_pid( pid ), m_position( position ), m_momentum( momentum ) {
  setTrackID( track );
}

// ============================================================================
/** copy constructor
 *  @param hit hit to be copied
 */
// ============================================================================
GaussSensPlaneHit::GaussSensPlaneHit( const GaussSensPlaneHit& hit )
    : Gaussino::HitBase( hit ), m_pid( hit.pid() ), m_position( hit.position() ), m_momentum( hit.momentum() ) {
  // ==========================================================================
#ifdef GIGA_DEBUG
  GaussSensPlaneHitLocal::s_Counter.increment();
#endif
  // ==========================================================================
}
