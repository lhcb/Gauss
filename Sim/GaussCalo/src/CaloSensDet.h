/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/HashMap.h"
#include "GaudiKernel/Point3DTypes.h"

// Gaussino
#include "GiGaMTCoreMessage/IGiGaMessage.h"

// AIDA
#include "AIDA/IHistogram1D.h"

// Gauss
#include "GaussCalo/CaloHit.h"
#include "GaussCalo/CaloSim.h"
#include "GaussCalo/CaloSimHash.h"
// #include "GaussCalo/SmallCaloCell.h"

// Geant4
#ifdef CUSTOMSIM
#  include "G4FastHit.hh"
#  include "G4VFastSimSensitiveDetector.hh"
#else
class G4VFastSimSensitiveDetector {};
#endif
#include "G4VSensitiveDetector.hh"

#include "G4EnergyLossTables.hh"
#include "G4Material.hh"
#include "G4MaterialCutsCouple.hh"

#include "CLHEP/Geometry/Transform3D.h"

/** @class CaloSensDet CaloSensDet.h CaloSensDet.h
 *  Base class for all calo sensitive detectors.
 *  Templated in the CELLGETTER that provides information
 *  from the geometry description to avoid vtable calls.
 *  This base class must provide
 *  inline double cellX(const LHCb::Detector::Calo::CellID& id) const
 *  inline double cellY(const LHCb::Detector::Calo::CellID& id) const
 *  inline double cellSize(const LHCb::Detector::Calo::CellID& id) const
 *  inline LHCb::Detector::Calo::CellID cell(const G4Step* point) const
 *  inline void GeoBaseInitialize()
 *
 *
 *  @author  Vanya Belyaev
 *  @author  Dominik Muller
 *  @author  Michał Mazurek
 *  @date    23/01/2001
 */

template <typename CELLGETTER>
class CaloSensDet : public G4VSensitiveDetector,
                    public G4VFastSimSensitiveDetector,
                    public virtual CELLGETTER,
                    public virtual GiGaMessage {
public:
  /// translator from Path to CellID
  typedef GaudiUtils::HashMap<LHCb::Detector::Calo::CellID, CaloHit*> HitMap;
  /// type for all histograms
  typedef std::vector<AIDA::IHistogram1D*> Histos;
  /// the typedef for vector of fractions
  typedef std::vector<double> Fractions;

public:
  CaloSensDet( const std::string& name );
  virtual ~CaloSensDet() = default;

  /** process the hit.
   *  The method is invoked by G4 for each step in the
   *  sensitive detector. This implementation performs the
   *  generic (sub-detector) independent action:
   *    - decode G4Step, G4Track information
   *    - determine the LHCb::Detector::Calo::CellID of the active cell
   *    - determine is the hit to be associated with
   *      the track or its parent track
   *    - find/create CaloHit object for the given cell
   *    - find/create CaloSubHit object for given track
   *    - invoke fillHitInfo method for filling the hit with
   *      the actual (subdetector-specific) informaton
   *      (Birk's corrections, local/global non-uniformity
   *      corrections, timing, etc)
   *  @param step     pointer to current Geant4 step
   *  @param history  pointert to touchable history
   *  @attention One should not redefine this method for specific sub-detectors.
   */
  bool ProcessHits( G4Step*, G4TouchableHistory* ) override;

#ifdef CUSTOMSIM
  bool ProcessHits( const G4FastHit* fastHit, const G4FastTrack* fastTrack, G4TouchableHistory* history ) override;
#endif

  CaloSubHit* RetrieveAndSetupHit( const G4Track*, G4TouchableHistory* );

public:
  /** method from G4
   *  (Called at the begin of each event)
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void Initialize( G4HCofThisEvent* HCE ) override;

  /** method from G4
   *  (Called at the end of each event)
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void EndOfEvent( G4HCofThisEvent* HCE ) override;

public:
  // no default constructor
  CaloSensDet() = delete;
  // no copy constructor
  CaloSensDet( const CaloSensDet& ) = delete;
  // no assignement
  CaloSensDet& operator=( const CaloSensDet& ) = delete;

protected:
  /// access to hit collection
  inline CaloHitsCollection* hits() const { return m_collection; }

  /// access to hit map
  inline HitMap& hitmap() { return m_hitmap; }

  /// access to minimal z
  inline double zMin() const { return m_zmin; }

  /// access to maximal z
  inline double zMax() const { return m_zmax; }

  /// the additive correction to 't0' evaluation
  inline double dT0() const { return m_dT0; }

  /** the evaluation of t0 for given calorimetr cell
   *  @param cellID the calorimeter cell identifier
   *  @return "time"-parameter for the given cell
   *           it is evaluated per cell basis in DeCalorimeter
   */
  inline double t0( const LHCb::Detector::Calo::CellID& cellID ) const;

  /// the first coefficient of Birks's law
  inline double birk_c1() const { return m_birk_c1; }
  /// the second coefficient of Birks's law
  inline double birk_c2() const { return m_birk_c2; }
  /// the correction to the first coefficient of Birks's law
  inline double birk_c1cor() const { return m_birk_c1correction; }

  /// the accessor to all time-histograms      (const version)
  inline const Histos& histos() const { return m_histos; }
  /// the accessor to all time-histograms  (non-const version)
  inline Histos& histos() { return m_histos; }

protected:
  /** fill the hit with the concrete information about the energy and the time.
   *  The function is to be called from ProcessHits method.
   *
   *  @param hit        hit  to be filled with the correct information,
   *                    the cellID information is accessible from the hit
   *  @param prePoint   PreStepPoint  for the given step
   *  @param globalTime the global time of prestep point
   *  @param deposit    raw energy deposition for the step
   *  @param track      the actual G4Track pbject (decoded from G4Step)
   *  @param pdef       the actual particle type (decoded from G4Step/G4Track)
   *  @param material   the actual material       (decoded from G4Step)
   *
   *  @attention all subdetector specific action
   *    (Birk's,timing, non-uniformities, etc ) need to be applied here!
   *
   */
  virtual bool fillHitInfo( CaloSubHit* hit, const HepGeom::Point3D<double>& prePoint, const double globalTime,
                            const double deposit, const G4Track* track, const G4ParticleDefinition* pdef,
                            const G4MaterialCutsCouple* material ) const = 0;

  /** The fractions of energy deposited in consequitive time-bins
   *  for the given calorimeter cell
   *  @param time global time of energy deposition
   *  @param cell cellID of the cell
   *  @param bin (out) the first time bin
   *  @param fracs the vector of fractions for subsequent time-bins;
   *  @return StatuscCode
   */
  virtual bool timing( const double time, const LHCb::Detector::Calo::CellID& cell, CaloSubHit::Time& slot,
                       Fractions& fractions ) const = 0;

protected:
  /** Correction factor from Birk's Law
   *  @param step current G4step
   *  @return the correction factor
   */
  inline double birkCorrection( const G4Step* step ) const;

  /** Birk's correction for given particle with given kinetic energy
   *  for the given material
   *  @param  particle pointer to particle definition
   *  @param  Ekine    particle kinetic energy
   *  @param  maerial  pointer ot teh material
   */
  inline double birkCorrection( const G4ParticleDefinition* particle, const double eKine,
                                const G4MaterialCutsCouple* material ) const;

  /** evaluate the correction for Birk's law
   *  @param charge   the charge of the particle
   *  @param dEdX     the nominal dEdX in the material
   *  @param material the pointer ot teh material
   *  @return the correction coefficient
   */
  inline double birkCorrection( const double charge, const double dEdX, const G4Material* material ) const;

  /** evaluate the correction for Birk's law
   *  @param charge   the charge of the particle
   *  @param dEdX     the nominal dEdX in the material
   *  @param density  the density ot the material
   *  @return the correction coefficient
   */
  inline double birkCorrection( const double charge, const double dEdX, const double density ) const;

private:
  HitMap m_hitmap;

  CaloHitsCollection* m_collection;

public:
  /// all histograms
  Histos m_histos;

  // the first coefficient of Birk's law                    (c1)
  double m_birk_c1{ 0.013 * CLHEP::g / CLHEP::MeV / CLHEP::cm2 };
  // the second coefficient of Birk's law                   (c2)
  double m_birk_c2{ 9.6E-6 * CLHEP::g * CLHEP::g / CLHEP::MeV / CLHEP::MeV / CLHEP::cm2 / CLHEP::cm2 };
  // the correction to the first coefficient of Birk's law  (c1')
  double m_birk_c1correction{ 0.57142857 };

  double m_zmin{ -1 * CLHEP::km };
  double m_zmax{ 1 * CLHEP::km };

  // the additive correction for the evaluation of t0
  double                                       m_dT0{ 0.5 * CLHEP::ns };
  Gaudi::Accumulators::SummingCounter<size_t>* m_nhits{ nullptr };
  Gaudi::Accumulators::SummingCounter<size_t>* m_nshits{ nullptr };
  Gaudi::Accumulators::SummingCounter<size_t>* m_nslots{ nullptr };
  Gaudi::Accumulators::SummingCounter<double>* m_energy{ nullptr };

  // Fast Simulation options
  // reuse methods used in calo sensitive detectors or not

  bool m_bypassCaloCorrections = true;
  bool m_bypassCaloTiming      = false;
};

#include "CaloSensDet.icpp"
