/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// GaudiKernel
// Ntuple Svc
#include "GaudiKernel/INTuple.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"

#include "G4VSensitiveDetector.hh"
// local
#include "GaussCalo/CaloHit.h"
#include "GaussSensPlaneHit.h"
#include "GiGaMTCoreMessage/IGiGaMessage.h"

#include "GaussSensPlaneHit.h"

/** @class GaussSensPlaneDet GaussSensPlaneDet.h GaussSensPlaneDet.h
 *
 *  @author  Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date    23/01/2001
 */

class GaussSensPlaneDet : public G4VSensitiveDetector, public virtual GiGaMessage {
  /// friend factory
  //  friend class GiGaFactory<GaussSensPlaneDet>;

public:
  /** process the hit.
   *  The method is invoked by G4 for each step in the
   *  sensitive detector. This implementation performs the
   *  generic (sub-detector) independent action:
   *    - decode G4Step, G4Track information
   *    - determine the Calo::CellID of the active cell
   *    - determine is the hit to be associated with
   *      the track or its parent track
   *    - find/create CaloHit object for the given cell
   *    - find/create CaloSubHit object for given track
   *    - invoke fillHitInfo method for filling the hit with
   *      the actual (subdetector-specific) informaton
   *      (Birk's corrections, local/global non-uniformity
   *      corrections, timing, etc)
   *  @param step     pointer to current Geant4 step
   *  @param history  pointert to touchable history
   *  @attention One should not redefine this method for specific sub-detectors.
   */
  bool ProcessHits( G4Step* step, G4TouchableHistory* history ) override;

  /** method from G4
   *  (Called at the begin of each event)
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void Initialize( G4HCofThisEvent* HCE ) override;

  /** method from G4
   *  (Called at the end of each event)
   *  @see G4VSensitiveDetector
   *  @param HCE pointer to hit collection of current event
   */
  void EndOfEvent( G4HCofThisEvent* HCE ) override;

  GaussSensPlaneDet( const std::string& name );

  /// destructor (virtual and protected)
  virtual ~GaussSensPlaneDet(){};

protected:
  /// keep all original links to G4Tracks/MCParticles
  inline bool keepLinks() const { return m_keepLinks; }
  /// only one entry ?
  inline bool oneEntry() const { return m_oneEntry; }

  /// cut for photons to create hit
  inline double cutForPhoton() const { return m_cutForPhoton; }

  /// cut for e-   to create hit
  inline double cutForElectron() const { return m_cutForElectron; }

  /// cut for e+   to create hit
  inline double cutForPositron() const { return m_cutForPositron; }

  /// cut for muon to create hit
  inline double cutForMuon() const { return m_cutForMuon; }

  /// cut for other charged particle to create hit
  inline double cutForCharged() const { return m_cutForCharged; }

  /// cut for othe rneutral particle to create hit
  inline double cutForNeutral() const { return m_cutForNeutral; }

private:
  // no default constructor
  GaussSensPlaneDet();
  // no copy constructor
  GaussSensPlaneDet( const GaussSensPlaneDet& );
  // no assignement
  GaussSensPlaneDet& operator=( const GaussSensPlaneDet& );

protected:
  GaussSensPlaneHitsCollection* m_collection{ nullptr };

public:
  bool m_keepLinks{ false };
  bool m_oneEntry{ true };

  double m_cutForPhoton{ 50 * CLHEP::MeV };
  double m_cutForElectron{ 10 * CLHEP::MeV };
  double m_cutForPositron{ 10 * CLHEP::MeV };
  double m_cutForMuon{ -1 * CLHEP::MeV };
  double m_cutForCharged{ 10 * CLHEP::MeV };
  double m_cutForNeutral{ 10 * CLHEP::MeV };

private:
  // final statistics
  bool   m_stat{ true };
  long   m_events{ 0 };
  double m_hits{ 0 };
  double m_hits2{ 0 };
  double m_hitsMin{ 1.e+10 };
  double m_hitsMax{ -1.e+10 };
};
