/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gauss
#include "CellFromDetDesc.h"
#include "EcalSensDetFactory.h"

namespace Gauss::Calo::Ecal::DetDesc {
  using BASE = Ecal::SensDetFactory<CellFromDetDesc>;

  class SensDetFactory : public BASE {

    Gaudi::Property<std::string>              m_caloName{ this, "Detector", DeCalorimeterLocation::Ecal };
    Gaudi::Property<std::string>              m_endVolumeName{ this, "EndVolume", "" };
    Gaudi::Property<std::vector<std::string>> m_startVolumeNames{ this, "StartVolumes", {} };

  public:
    using BASE::SensDetFactory;
    virtual ECAL<CellFromDetDesc>* construct() const override {
      auto tmp    = BASE::construct();
      tmp->m_calo = this->template getDet<DeCalorimeter>( m_caloName );
      if ( !tmp->m_calo ) { throw std::runtime_error( "Could not locate CALO " + m_caloName ); }
      tmp->m_caloID           = LHCb::Detector::Calo::CellCode::CaloNumFromName( m_caloName );
      tmp->m_endVolumeName    = m_endVolumeName;
      tmp->m_startVolumeNames = m_startVolumeNames;

      if ( 0 > tmp->m_caloID ) { throw std::runtime_error( "Invalid detector name/number!" ); }
      return tmp;
    }
  };

  DECLARE_COMPONENT_WITH_ID( SensDetFactory, "EcalSensDet" )
} // namespace Gauss::Calo::Ecal::DetDesc
