/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifdef USE_DD4HEP
// Gauss
#  include "CellFromDD4hep.h"
#  include "HcalSensDetFactory.h"

// Detector
#  include "Core/Keys.h"
#  include "LbDD4hep/IDD4hepSvc.h"

namespace Gauss::Calo::Hcal::DD4hep {
  using BASE = Hcal::SensDetFactory<CellFromDD4hep>;

  class SensDetFactory : public BASE {

    Gaudi::Property<std::string> m_caloName{ this, "Detector", LHCb::Detector::Calo::DeCalorimeterLocation::Hcal };
    Gaudi::Property<int>         m_runNumber{ this, "RunNumber", -1, "Run number to fill the ODIN value." };

    ServiceHandle<LHCb::Det::LbDD4hep::IDD4hepSvc> m_dd4Svc{ this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc" };

  public:
    using BASE::SensDetFactory;

    StatusCode initialize() override {
      return BASE::initialize().andThen( [&]() -> StatusCode {
        if ( m_runNumber < 0 ) {
          error() << "Run number must be explicitly set and be >= 0." << endmsg;
          return StatusCode::FAILURE;
        }
        return StatusCode::SUCCESS;
      } );
    }

    virtual HCAL<CellFromDD4hep>* construct() const override {
      auto fac    = BASE::construct();
      auto slice  = m_dd4Svc->get_slice( m_runNumber );
      auto hcal   = dd4hep::Detector::getInstance().detector( "Hcal" );
      fac->m_calo = slice->get( hcal, LHCb::Detector::Keys::deKey );
      return fac;
    }
  };
  DECLARE_COMPONENT_WITH_ID( SensDetFactory, "HcalSensDetFromDD4hep" )
} // namespace Gauss::Calo::Hcal::DD4hep
#endif
