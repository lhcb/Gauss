/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// LHCb
#include "LHCbMath/LHCbMath.h"

// Gauss
#include "CaloSensDet.h"

/** @class EcalSensDet EcalSensDet.h
 *
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @author  Patrick Robbe robbe@lal.in2p3.fr
 *
 *  @date    23/01/2001
 */

template <typename CELLGETTER>
class EcalSensDet : public CaloSensDet<CELLGETTER> {
public:
  using base_class = CaloSensDet<CELLGETTER>;
  using base_class::CaloSensDet;

protected:
  /** fill the hit with the concrete information about the energy and the time.
   *  The function is to be called from ProcessHits method.
   *
   *  @param hit        hit  to be filled with the correct information,
   *                    the cellID information is accessible from the hit
   *  @param prePoint   PreStepPoint  for the given step
   *  @param globalTime the global time of prestep point
   *  @param deposit    raw energy deposition for the step
   *  @param track      the actual G4Track pbject (decoded from G4Step)
   *  @param pdef       the actual particle type  (decoded from G4Step/G4Track)
   *  @param material   the actual material       (decoded from G4Step)
   *
   */
  bool fillHitInfo( CaloSubHit* hit, const HepGeom::Point3D<double>& prePoint, const double globalTime,
                    const double deposit, const G4Track* track, const G4ParticleDefinition* pdef,
                    const G4MaterialCutsCouple* material ) const override;

  /** The fractions of energy deposited in consequitive time-bins
   *  in the given Ecal/Hcal cell
   *  @see CaloSensDet
   *  @param time global time of energy deposition
   *  @param cell cellID of the cell
   *  @param slot (out) the first time bin
   *  @param fracs (out) the vector of fractions for subsequent time-slots;
   *  @return StatuscCode
   */
  bool timing( const double time, const LHCb::Detector::Calo::CellID& cell, CaloSubHit::Time& slot,
               typename CaloSensDet<CELLGETTER>::Fractions& fractions ) const override;

protected:
  /** Correction due to the local non uniformity due to the light
   *  collection efficiency in cell cell
   */
  inline double localNonUniformity( const HepGeom::Point3D<double>&     prePoint,
                                    const LHCb::Detector::Calo::CellID& cell ) const;

public:
  // Amplitudes of the local non uniformity correction
  // depending on the region of the Ecal
  double m_a_local_inner_ecal{ 0. };
  double m_a_local_middle_ecal{ 0. };
  double m_a_local_outer_ecal{ 0. };
  // Amplitudes of the global non uniformity correction
  // depending on the region of the Ecal
  double m_a_global_inner_ecal{ 0.0004 };
  double m_a_global_middle_ecal{ 0.002 };
  double m_a_global_outer_ecal{ 0.03 };
  // Correction for light reflection at the edges
  double m_a_reflection_height{ 0.09 };
  double m_a_reflection_width{ 6. * CLHEP::mm };

  /// width of time-slot (25ns)
  double m_slotWidth{ 25 * CLHEP::ns };
};

// ============================================================================
// Local Non Uniformity
// ============================================================================
/** Correction due to the local non uniformity due to the light
 *  collection efficiency in cell cell
 */
template <typename CELLGETTER>
inline double EcalSensDet<CELLGETTER>::localNonUniformity( const HepGeom::Point3D<double>&     prePoint,
                                                           const LHCb::Detector::Calo::CellID& cell ) const {
  // Only for ECal for the moment
  double correction = 1.;

  // Find the position of the step
  double x = prePoint.x();
  double y = prePoint.y();
  // Center of the cell
  double x0 = this->cellX( cell );
  double y0 = this->cellY( cell );

  // Distance between fibers
  // and correction amplitude
  double d        = 10.1 * CLHEP::mm;
  double A_local  = m_a_local_inner_ecal; // in inner Ecal
  double A_global = m_a_global_inner_ecal;

  // Cell size
  double cellSize = this->cellSize( cell );

  // Assign amplitude of non uniformity as a function of the
  // Ecal region

  if ( cell.area() == 0 ) { // outer Ecal
    A_local  = m_a_local_outer_ecal;
    A_global = m_a_global_outer_ecal;
    d        = 15.25 * CLHEP::mm;
  } else if ( cell.area() == 1 ) { // middle Ecal
    A_local  = m_a_local_middle_ecal;
    A_global = m_a_global_middle_ecal;
  }

  // Local uniformity is product of x and y sine-like functions
  // The Amplitude of the sin-like function is a function of x and
  // y
  if ( A_local > LHCb::Math::lowTolerance )
    correction += A_local / 2. * ( 1. - cos( 2. * CLHEP::pi * ( x - x0 ) / d ) ) *
                  ( 1. - cos( 2. * CLHEP::pi * ( y - y0 ) / d ) );

  double rX( 0. ), rY( 0. ), hCell( 0. );

  // Global non uniformity
  if ( A_global > LHCb::Math::lowTolerance ) {
    rX    = x - x0;
    rY    = y - y0;
    hCell = cellSize / 2.;
    correction += A_global * ( hCell - rX ) * ( rX + hCell ) / ( hCell * hCell ) * ( hCell - rY ) * ( rY + hCell ) /
                  ( hCell * hCell );
  }

  // Light Reflexion on the edges
  if ( m_a_reflection_height > LHCb::Math::lowTolerance ) {
    rX    = rX / m_a_reflection_width;
    rY    = rY / m_a_reflection_width;
    hCell = hCell / m_a_reflection_width;
    correction += m_a_reflection_height * ( exp( -fabs( rX + hCell ) ) + exp( -fabs( rX - hCell ) ) +
                                            exp( -fabs( rY + hCell ) ) + exp( -fabs( rY - hCell ) ) );
  }

  return correction;
}

// ============================================================================
/** fill the hit with the concrete information about the energy and the time.
 *  The function is to be called from ProcessHits method.
 *
 *  @param hit        hit  to be filled with the correct information,
 *                    the cellID information is accessible from the hit
 *  @param prePoint   PreStepPoint  for the given step
 *  @param globalTime the global time of prestep point
 *  @param deposit    raw energy deposition for the step
 *  @param track      the actual G4Track pbject (decoded from G4Step)
 *  @param pdef       the actual particle type  (decoded from G4Step/G4Track)
 *  @param material   the actual material       (decoded from G4Step)
 *  @param step       the step itself, the most important
 *                    information from the step
 *                    is already decoded into prePoint,globalTime,track,
 *                    particle definition,material etc for efficiency reasons.
 *
 */
// ============================================================================
template <typename CELLGETTER>
bool EcalSensDet<CELLGETTER>::fillHitInfo( CaloSubHit* hit, const HepGeom::Point3D<double>& prePoint, const double time,
                                           const double deposit, const G4Track* track,
                                           const G4ParticleDefinition* particle,
                                           const G4MaterialCutsCouple* material ) const {

  if ( 0 == hit ) { return false; }

  // get the cell
  const LHCb::Detector::Calo::CellID& cellID = hit->cellID();

  // Birk's Law Correction
  double ecorrected = deposit * this->birkCorrection( particle, track->GetKineticEnergy(), material );

  // Local NonUniformity
  // if the cell is not valid do not apply the correction
  // (Anyway it will not be used)
  // if ( calo() -> valid ( cellID ) ) {
  ecorrected *= localNonUniformity( prePoint, cellID );
  //}

  // Timing
  // Uses method in EHCalSensDet
  CaloSubHit::Time               slot = 0;
  typename base_class::Fractions fractions;
  bool                           sc = timing( time, cellID, slot, fractions );

  if ( !sc ) {
    this->error( "Error in timing()" );
    return false;
  }

  if ( fractions.empty() ) { this->warning( "The vector of fractions of energy is empty." ); }

  for ( double fr : fractions ) {
    if ( fr > 1.e-5 ) hit->add( slot, ecorrected * fr ).ignore();
    slot++;
  }

  return true;
}

//=============================================================================
// The fractions of energy deposited in consequitive time-slots
// in the given Ecal/Hcal cell
//=============================================================================
template <typename CELLGETTER>
bool EcalSensDet<CELLGETTER>::timing( const double time, const LHCb::Detector::Calo::CellID& cell,
                                      CaloSubHit::Time& slot, typename base_class::Fractions& fractions ) const {
  // clear input data
  slot = 0;
  fractions.clear();

  // evaluate the real delta time
  const double deltaT = time - this->t0( cell );

  // find the absolute time slot
  slot = (CaloSubHit::Time)floor( deltaT / m_slotWidth );

  // the time into the slot
  const double dt = deltaT - slot * m_slotWidth;

  for ( auto histo : this->histos() ) {
    const int    bin  = histo->coordToIndex( dt / CLHEP::ns );
    const double frac = histo->binHeight( bin );
    fractions.push_back( frac );
  };

  if ( fractions.empty() ) { this->warning( "timing()::no time information is available!" ); }

  return true;
}
