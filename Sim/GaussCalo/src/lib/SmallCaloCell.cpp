/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaussCalo/SmallCaloCell.h"

// LHCb
#include "CaloDet/DeCalorimeter.h"
#include "CaloDet/DeSubCalorimeter.h"
#include "CaloDet/DeSubSubCalorimeter.h"

// Geant4
#include "G4StepPoint.hh"

SmallCaloCell::SmallCaloCell( SmallCaloCell::NDims ncols, SmallCaloCell::NDims nrows, const G4StepPoint* point,
                              const DeCalorimeter* calo, const LHCb::Detector::Calo::CellID& cellID )
    : m_cellID( cellID ) {
  // find the small cell coordinates; this piece of code is almost identical to
  // that found in LHCb::DeCalorimeter::Cell_ to make sure the positions are consistent
  //
  //    the only difference is that we take the position directly from the step point:
  //
  auto                  pos = point->GetPosition();
  const Gaudi::XYZPoint globalPoint( pos.x(), pos.y(), pos.z() );
  //
  //    instead of taking the position from the g4 touchable like here:
  //
  //    G4TouchableHistory*   tHist = (G4TouchableHistory*)point->GetTouchable();
  //    HepGeom::Transform3D  mtrx( *( tHist->GetRotation() ), tHist->GetTranslation() );
  //    auto                  heppoint = mtrx * HepGeom::Point3D<double>{};
  //    const Gaudi::XYZPoint globalPoint( heppoint.x(), heppoint.y(), heppoint.z() );
  //
  for ( auto const& sc : calo->subCalos() ) {
    int side = ( sc->side() == DeSubCalorimeter::Side::C ? -1 : +1 );
    for ( auto const& ssc : sc->subSubCalosSorted() ) {
      const IGeometryInfoPlus* subSubGeo = ssc->geometryPlus();
      Gaudi::XYZPoint          subSubLocalPoint( subSubGeo->toLocal( globalPoint ) );
      // we are extra careful here, there should be not point outside the cells as
      // it has already been checked by LHCb::DeCalorimeter::Cell_
      if ( !subSubGeo->isInside( globalPoint ) || fabs( subSubLocalPoint.x() ) > ssc->xSize() / 2. ||
           fabs( subSubLocalPoint.y() ) > ssc->ySize() / 2. )
        continue;

      double xOffset  = -side * ssc->xSize() / 2.;
      double cellSize = ssc->cellSize();
      auto   px       = subSubLocalPoint.x();
      auto   py       = subSubLocalPoint.y();

      // compute the remainder of the division operation on cell position
      // and then find the SMALL cell position;
      // we do not really care here about the standard cell index because
      // this is computer earlier in the code of DeCalorimeter;
      //
      auto col_r = std::fmod( px - xOffset, cellSize );
      // invert the negative value (otherwise it is a reflection)
      if ( col_r < 0. ) col_r += cellSize;
      m_ncols    = ncols[(int)( ssc->area() )];
      m_col      = (int)( col_r / cellSize * m_ncols );
      auto row_r = std::fmod( py, cellSize );
      // invert the negative value (otherwise it is a reflection)
      if ( row_r < 0. ) row_r += cellSize;
      m_nrows = nrows[(int)( ssc->area() )];
      m_row   = (int)( row_r / cellSize * m_nrows );
      break;
    }
  }
}
