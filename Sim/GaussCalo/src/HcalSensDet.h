/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gauss
#include "CaloSensDet.h"

/** @class HcalSensDet HcalSensDet.h
 *
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @author  Patrick Robbe robbe@lal.in2p3.fr
 *
 *  @date    23/01/2001
 */

template <typename CELLGETTER>
class HcalSensDet : public CaloSensDet<CELLGETTER> {
public:
  using base_class = CaloSensDet<CELLGETTER>;
  using base_class::CaloSensDet;

protected:
  /** fill the hit with the concrete information about the energy and the time.
   *  The function is to be called from ProcessHits method.
   *
   *  @param hit        hit  to be filled with the correct information,
   *                    the cellID information is accessible from the hit
   *  @param prePoint   PreStepPoint  for the given step
   *  @param globalTime the global time of prestep point
   *  @param deposit    raw energy deposition for the step
   *  @param track      the actual G4Track pbject (decoded from G4Step)
   *  @param pdef       the actual particle type  (decoded from G4Step/G4Track)
   *  @param material   the actual material       (decoded from G4Step)
   *
   */
  bool fillHitInfo( CaloSubHit* hit, const HepGeom::Point3D<double>& prePoint, const double globalTime,
                    const double deposit, const G4Track* track, const G4ParticleDefinition* pdef,
                    const G4MaterialCutsCouple* material ) const override;

  /** The fractions of energy deposited in consequitive time-bins
   *  in the given Ecal/Hcal cell
   *  @see CaloSensDet
   *  @param time global time of energy deposition
   *  @param cell cellID of the cell
   *  @param slot (out) the first time bin
   *  @param fracs (out) the vector of fractions for subsequent time-slots;
   *  @return StatuscCode
   */
  bool timing( const double time, const LHCb::Detector::Calo::CellID& cell, CaloSubHit::Time& slot,
               typename CaloSensDet<CELLGETTER>::Fractions& fractions ) const override;

public:
  /// width of time-slot (25ns)
  double m_slotWidth{ 25 * CLHEP::ns };
};

// ============================================================================
/** fill the hit with the concrete information about the energy and the time.
 *  The function is to be called from ProcessHits method.
 *
 *  @param hit        hit  to be filled with the correct information,
 *                    the cellID information is accessible from the hit
 *  @param prePoint   PreStepPoint  for the given step
 *  @param time       the global time of prestep point
 *  @param deposit    raw energy deposition for the step
 *  @param track      the actual G4Track pbject (decoded from G4Step)
 *  @param particle   the actual particle type  (decoded from G4Step/G4Track)
 *  @param material   the actual material       (decoded from G4Step)
 *
 */
// ============================================================================
template <typename CELLGETTER>
bool HcalSensDet<CELLGETTER>::fillHitInfo( CaloSubHit*  hit, const HepGeom::Point3D<double>& /* prePoint */,
                                           const double time, const double deposit, const G4Track* track,
                                           const G4ParticleDefinition* particle,
                                           const G4MaterialCutsCouple* material ) const {

  if ( 0 == hit ) { return false; }

  // Birk's Law Correction
  const double energy = deposit * this->birkCorrection( particle, track->GetKineticEnergy(), material );

  // get the cell
  const LHCb::Detector::Calo::CellID& cellID = hit->cellID();

  // add current energy deposition to the sub-hit
  CaloSubHit::Time slot = 0;

  typename base_class::Fractions fractions;
  bool                           sc = timing( time, cellID, slot, fractions );

  if ( !sc ) {
    this->error( "Error from timing()" );
    return false;
  }
  if ( fractions.empty() ) { this->warning( "The empty vector of fractions()" ); }

  for ( auto fr : fractions ) {
    if ( fr > 1.e-5 ) { hit->add( slot, energy * fr ).ignore(); }
    ++slot;
  }

  return true;
}

//=============================================================================
// The fractions of energy deposited in consequitive time-slots
// in the given Ecal/Hcal cell
//=============================================================================
template <typename CELLGETTER>
bool HcalSensDet<CELLGETTER>::timing( const double time, const LHCb::Detector::Calo::CellID& cell,
                                      CaloSubHit::Time& slot, typename base_class::Fractions& fractions ) const {
  // clear input data
  slot = 0;
  fractions.clear();

  // evaluate the real delta time
  const double deltaT = time - this->t0( cell );

  // find the absolute time slot
  slot = (CaloSubHit::Time)floor( deltaT / m_slotWidth );

  // the time into the slot
  const double dt = deltaT - slot * m_slotWidth;

  for ( auto histo : this->histos() ) {
    const int    bin  = histo->coordToIndex( dt / CLHEP::ns );
    const double frac = histo->binHeight( bin );
    fractions.push_back( frac );
  };

  if ( fractions.empty() ) { this->warning( "timing()::no time information is available!" ); }

  return true;
}
