/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gauss
#include "CaloSensDet.h"

// AIDA
#include "AIDA/IAxis.h"

/** @class SpdPrsSensDet SpdPrsSensDet.h
 *
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @author  Grigori Rybkine Grigori.Rybkine@cern.ch
 *
 *  @date    23/01/2001
 */

template <typename CELLGETTER>
class SpdPrsSensDet : public CaloSensDet<CELLGETTER> {
public:
  using base_class = CaloSensDet<CELLGETTER>;
  using base_class::CaloSensDet;

protected:
  /** fill the hit with the concrete information about the energy and the time.
   *  The function is to be called from ProcessHits method.
   *
   *  @param hit        hit  to be filled with the correct information,
   *                    the cellID information is accessible from the hit
   *  @param prePoint   PreStepPoint  for the given step
   *  @param globalTime the global time of prestep point
   *  @param deposit    raw energy deposition for the step
   *  @param track      the actual G4Track pbject (decoded from G4Step)
   *  @param pdef       the actual particle type  (decoded from G4Step/G4Track)
   *  @param material   the actual material       (decoded from G4Step)
   *
   */
  bool fillHitInfo( CaloSubHit* hit, const HepGeom::Point3D<double>& prePoint, const double globalTime,
                    const double deposit, const G4Track* track, const G4ParticleDefinition* pdef,
                    const G4MaterialCutsCouple* material ) const override;

  /** The fractions of energy deposited in consequitive time-bins
   *  for the given calorimeter cell
   *  @param time global time of energy deposition
   *  @param cell cellID of the cell
   *  @param slot (out) the first time slot
   *  @param fracs the vector of fractions for subsequent time-slots;
   *  @return bool
   */
  bool timing( const double time, const LHCb::Detector::Calo::CellID& cell, CaloSubHit::Time& slot,
               typename CaloSensDet<CELLGETTER>::Fractions& fractions ) const override;

public:
  /** standard constructor
   *  @see CaloSensDet
   *  @see GiGaSensDetBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  SpdPrsSensDet( const std::string& type, const std::string& name, const IInterface* parent );

  /// destructor (virtual and protected)
  virtual ~SpdPrsSensDet(){};

public:
  double              m_BX{ 25. * CLHEP::ns };
  unsigned int        m_numBXs{ 6 };
  std::vector<double> m_sDelays{ { 0, 0, 0 } };
  double              m_fracMin{ 1.e-5 };

  // flag controlling the correction C1'= 7.2/12.6 * C1
  // for multiply charged particles: == true  correction is applied
  //                                 == false  correction is not applied
  bool m_multiChargedBirks{ true };
};

// ============================================================================
/** fill the hit with the concrete information about the energy and the time.
 *  The function is to be called from ProcessHits method.
 *
 *  @param hit        hit  to be filled with the correct information,
 *                    the cellID information is accessible from the hit
 *  @param prePoint   PreStepPoint  for the given step
 *  @param globalTime the global time of prestep point
 *  @param deposit    raw energy deposition for the step
 *  @param track      the actual G4Track pbject (decoded from G4Step)
 *  @param pdef       the actual particle type  (decoded from G4Step/G4Track)
 *  @param material   the actual material       (decoded from G4Step)
 *
 */
// ============================================================================
template <typename CELLGETTER>
bool SpdPrsSensDet<CELLGETTER>::fillHitInfo( CaloSubHit*  hit, const HepGeom::Point3D<double>& /* prePoint   */,
                                             const double globalTime, const double deposit, const G4Track* track,
                                             const G4ParticleDefinition* particle,
                                             const G4MaterialCutsCouple* material ) const {
  if ( 0 == hit ) { return false; }

  // Birks' Law Correction
  double edep = deposit;
  edep *= this->birkCorrection( particle, track->GetKineticEnergy(), material );

  // add the current energy deposition to the sub-hit
  // smearing the energy deposition over a number of bunch crossings (timing)
  CaloSubHit::Time                            slot;
  typename CaloSensDet<CELLGETTER>::Fractions frac;
  frac.reserve( m_numBXs );

  const LHCb::Detector::Calo::CellID cellID = hit->cellID();

  bool sc = timing( globalTime, cellID, slot, frac );
  if ( !sc ) { this->error( "Could not smear Edep!" ); }

  for ( unsigned int i = 0; i < frac.size(); i++, slot += 1 ) {
    if ( frac[i] > m_fracMin ) { hit->add( slot, edep * frac[i] ).ignore(); }
  }
  return true;
}
// ============================================================================
/** The fractions of energy deposited in consequitive time-slots
 *  for the given calorimeter cell
 *  @param time global time of energy deposition
 *  @param cell cellID of the cell
 *  @param slot (out) the first time slot
 *  @param fracs the vector of fractions for subsequent time-slots;
 *  @return bool
 */
// ============================================================================
template <typename CELLGETTER>
bool SpdPrsSensDet<CELLGETTER>::timing( const double time, const LHCb::Detector::Calo::CellID& cell,
                                        CaloSubHit::Time&                            slot,
                                        typename CaloSensDet<CELLGETTER>::Fractions& fractions ) const {
  const double locTime = time - this->t0( cell );

  // number of the current 25 ns bx w.r.t. local time
  slot = static_cast<CaloSubHit::Time>( floor( locTime / m_BX ) );

  const double refTime = locTime - slot * m_BX;

  // which area the cell is in: 0-Outer, 1-Middle, 2-Inner
  const unsigned int area = cell.area();

  const IAxis& axis      = this->histos()[area]->axis();
  const double lowerEdge = axis.lowerEdge();
  const double upperEdge = axis.upperEdge();

  unsigned int i;
  double       t;
  for ( i = 0, t = -m_BX + m_sDelays[area] - refTime; i < m_numBXs; i++, t += m_BX )
    if ( lowerEdge < t && t < upperEdge ) {
      fractions.push_back( this->histos()[area]->binHeight( axis.coordToIndex( t ) ) );
    } else
      fractions.push_back( 0. );

  slot -= 1;

  return true;
}
