/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Geant4
#include "G4LogicalVolumeStore.hh"
#include "G4TouchableHistory.hh"

// Gaudi
#include "GaudiKernel/HashMap.h"

// Run2Support
#include "CaloDet/DeCalorimeter.h"

// Gaussino
#include "GiGaMTCoreMessage/IGiGaMessage.h"

// Gauss
#include "GaussCalo/CaloSim.h"
#include "GaussCalo/CaloSimHash.h"

class CellFromDetDesc : public virtual GiGaMessage {
protected:
  inline double cellTime( const LHCb::Detector::Calo::CellID& cellID ) const { return m_calo->cellTime( cellID ); };

  inline double cellX( const LHCb::Detector::Calo::CellID& id ) const { return m_calo->cellX( id ); };
  inline double cellY( const LHCb::Detector::Calo::CellID& id ) const { return m_calo->cellY( id ); };
  inline double cellSize( const LHCb::Detector::Calo::CellID& id ) const { return m_calo->cellSize( id ); }
  inline LHCb::Detector::Calo::CellID cell( G4TouchableHistory* tHist ) const {

    const int     nLevel = tHist->GetHistoryDepth();
    CaloSim::Path path;
    path.reserve( nLevel );

    for ( int level = 0; level < nLevel; ++level ) {
      const G4VPhysicalVolume* pv = tHist->GetVolume( level );
      if ( !pv ) continue;

      const G4LogicalVolume* lv = pv->GetLogicalVolume();
      if ( !lv ) continue;

      // start volume ??
      if ( m_end == lv ) { break; } // BREAK
      // useful volume ?
      if ( !path.empty() ) {
        path.push_back( pv );
      } else if ( m_start.end() != std::find( m_start.begin(), m_start.end(), lv ) ) {
        path.push_back( pv );
      }
    }

    if ( path.empty() ) {
      error( "Volume path is invalid(empty) " );
      return LHCb::Detector::Calo::CellID();
    }

    // find the appropriate ID
    Table::iterator ifound = m_table.find( path );
    // already in the table ?
    if ( m_table.end() != ifound ) { return ifound->second; } // RETURN

    // if not: add it into the table!
    // CLHEP -> ROOT
    HepGeom::Transform3D  mtrx( *( tHist->GetRotation() ), tHist->GetTranslation() );
    auto                  heppoint = mtrx * HepGeom::Point3D<double>{};
    const Gaudi::XYZPoint p( heppoint.x(), heppoint.y(), heppoint.z() );

    // get the right cell id
    LHCb::Detector::Calo::CellID id;
    const CellParam*             par = m_calo->Cell_( p );
    if ( !par || !par->valid() ) {
      id = LHCb::Detector::Calo::CellID(); // RETURN
    }
    id = par->cellID();
    m_table.insert( Table::value_type( path, id ) );

    return id;
  }

  // ============================================================================
  /** helpful method to locate start and end volumes
   *  @return status code
   */
  // ============================================================================
  inline void GeoBaseInitialize() {
    if ( m_initialized ) { return; }
    // locate start volumes
    for ( auto& vol : m_startVolumeNames ) {
      // look through converted volumes
      const G4LogicalVolume* lv = G4LogicalVolumeStore::GetInstance()->GetVolume( vol );
      if ( 0 == lv ) {
        throw std::runtime_error( std::string( __PRETTY_FUNCTION__ ) + "G4LogicalVolume* points to 0 for " + ( vol ) );
      }
      m_start.push_back( lv );
    }
    if ( m_start.empty() ) {
      throw std::runtime_error( std::string( __PRETTY_FUNCTION__ ) + "Size of 'StartVolumes' is 0 " );
    }
    // locate end volume : look through converted volumes
    m_end = G4LogicalVolumeStore::GetInstance()->GetVolume( m_endVolumeName );
    if ( 0 == m_end ) {
      throw std::runtime_error( std::string( __PRETTY_FUNCTION__ ) + "G4LogicalVolume* points to 0 for '" +
                                m_endVolumeName + "'" );
    }
    m_initialized = true;
  }

public:
  // Need to be set by the corresponding factory that has access to
  // the Gaudi based functionality
  const DeCalorimeter* m_calo{ nullptr };
  int                  m_caloID{ 0 };

  /// useful type for list of names
  typedef std::vector<std::string> Names;
  std::string                      m_endVolumeName{};
  Names                            m_startVolumeNames{};

private:
  /// useful type for list of logical volumes
  typedef std::vector<const G4LogicalVolume*>                              Volumes;
  typedef GaudiUtils::HashMap<CaloSim::Path, LHCb::Detector::Calo::CellID> Table;
  const G4LogicalVolume*                                                   m_end{ nullptr };
  // definition logical volumes where will be analysed signal
  Volumes m_start;
  // translation table
  mutable Table m_table;

private:
  bool m_initialized{ false };
};
