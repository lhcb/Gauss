/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Geant4
#include "G4TouchableHistory.hh"

// DDG4
#include "DDG4/Geant4Mapping.h"
#include "DDG4/Geant4TouchableHandler.h"
#include "DDG4/Geant4VolumeManager.h"
// Detector
#include "Detector/Calo/DeCalorimeter.h"

// Gaussino
#include "GiGaMTCoreMessage/IGiGaMessage.h"

#include <map>
#include <utility>
#include <vector>
struct CellFromDD4hep : public virtual GiGaMessage {

  LHCb::Detector::Calo::DeCalorimeter m_calo;

  inline double cellTime( const LHCb::Detector::Calo::CellID& id ) const { return m_calo.cellTime( id ); };
  inline double cellX( const LHCb::Detector::Calo::CellID& id ) const { return m_calo.cellX( id ); };
  inline double cellY( const LHCb::Detector::Calo::CellID& id ) const { return m_calo.cellY( id ); };
  inline double cellSize( const LHCb::Detector::Calo::CellID& id ) const { return m_calo.cellSize( id ); };
  inline void   GeoBaseInitialize() {}
  inline LHCb::Detector::Calo::CellID cell( G4TouchableHistory* history ) const {
    auto&                               g4map = dd4hep::sim::Geant4Mapping::instance();
    dd4hep::sim::Geant4TouchableHandler touchable_handler{ history };
    dd4hep::DDSegmentation::VolumeID    sens_det_id = g4map.volumeManager().volumeID( touchable_handler.touchable );
    const LHCb::Detector::Calo::CellID  cell_id     = m_calo.cellIDfromVolumeID( sens_det_id );
    return cell_id;
  }
};
