/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gauss
#include "CaloSensDetFactory.h"
#include "EcalSensDet.h"

namespace Gauss::Calo::Ecal {

  template <class CELLGETTER>
  using ECAL = EcalSensDet<CELLGETTER>;

  template <class CELLGETTER>
  using BASE = Calo::SensDetFactory<ECAL<CELLGETTER>>;

  template <class CELLGETTER>
  class SensDetFactory : public BASE<CELLGETTER> {
    Gaudi::Property<double> m_a_local_inner_ecal{ this, "a_local_inner_ecal", 0. };
    Gaudi::Property<double> m_a_local_middle_ecal{ this, "a_local_middle_ecal", 0. };
    Gaudi::Property<double> m_a_local_outer_ecal{ this, "a_local_outer_ecal", 0. };
    Gaudi::Property<double> m_a_global_inner_ecal{ this, "a_global_inner_ecal", 0.0004 };
    Gaudi::Property<double> m_a_global_middle_ecal{ this, "a_global_middle_ecal", 0.002 };
    Gaudi::Property<double> m_a_global_outer_ecal{ this, "a_global_outer_ecal", 0.03 };
    Gaudi::Property<double> m_a_reflection_height{ this, "a_reflection_height", 0.09 };
    Gaudi::Property<double> m_a_reflection_width{ this, "a_reflection_width", 6. * CLHEP::mm };
    Gaudi::Property<double> m_slotWidth{ this, "SlotWidth", 25 * CLHEP::ns };

  public:
    using BASE<CELLGETTER>::SensDetFactory;
    virtual ECAL<CELLGETTER>* construct() const override {
      auto tmp                    = BASE<CELLGETTER>::construct();
      tmp->m_a_local_inner_ecal   = m_a_local_inner_ecal;
      tmp->m_a_local_middle_ecal  = m_a_local_middle_ecal;
      tmp->m_a_local_outer_ecal   = m_a_local_outer_ecal;
      tmp->m_a_global_inner_ecal  = m_a_global_inner_ecal;
      tmp->m_a_global_middle_ecal = m_a_global_middle_ecal;
      tmp->m_a_global_outer_ecal  = m_a_global_outer_ecal;
      tmp->m_a_reflection_height  = m_a_reflection_height;
      tmp->m_a_reflection_width   = m_a_reflection_width;
      tmp->m_slotWidth            = m_slotWidth;
      return tmp;
    }
  };
} // namespace Gauss::Calo::Ecal
