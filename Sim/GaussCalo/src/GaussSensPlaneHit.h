/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// ============================================================================
// Include files
// ============================================================================
// CLHEP
#include "CLHEP/Vector/LorentzVector.h"
// Event
#include "Kernel/ParticleID.h"
// GaussTools
#include "GiGaMTCoreDet/GaussHitBase.h"
// Geant4
#include "G4THitsCollection.hh"
// ============================================================================

/** @class GaussSensPlaneHit GaussSensPlaneHit.h
 *
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2003-07-07
 */
class GaussSensPlaneHit : public Gaussino::HitBase {
public:
  /// the actual type of TrackID
  typedef int TrackID;

public:
  /// overloaded 'new'    operator
  void* operator new( size_t );

  /// overloaded 'delete' operator
  void operator delete( void* hit );

  /** Standard constructor
   *  @param track   trackID of th eparticle (or its parent particle!)
   *  @param pid     the actual pid of the particle (can be different!)
   *  @param position position(3D+time) of the hit
   *  @param momentum 4-momentum of the particle
   */
  GaussSensPlaneHit( const TrackID& track = 0, const LHCb::ParticleID& pid = LHCb::ParticleID(),
                     const CLHEP::HepLorentzVector& position = CLHEP::HepLorentzVector(),
                     const CLHEP::HepLorentzVector& momentum = CLHEP::HepLorentzVector() );

  /** copy constructor
   *  @param hit hit to be copied
   */
  GaussSensPlaneHit( const GaussSensPlaneHit& hit );

  /// access to actual particle ID
  inline const LHCb::ParticleID& pid() const { return m_pid; }
  // set new value for particle ID
  inline void setPID( const LHCb::ParticleID& val ) { m_pid = val; }

  /// accessor to the hit position         (const version)
  inline const CLHEP::HepLorentzVector& position() const { return m_position; }
  /// accessor to the hit position     (non-const version)
  inline CLHEP::HepLorentzVector& position() { return m_position; }
  /// set the new value for hist position
  inline void setPosition( const CLHEP::HepLorentzVector& val ) { m_position = val; }

  /// accessor to the particle momentum     (const version)
  inline const CLHEP::HepLorentzVector& momentum() const { return m_momentum; }
  /// accessor to the particle momentum (non-const version)
  inline CLHEP::HepLorentzVector& momentum() { return m_momentum; }
  /// set the new value for particle momentum
  inline void setMomentum( const CLHEP::HepLorentzVector& val ) { m_momentum = val; }

private:
  // the actual particle tye (can be different from track PID)
  LHCb::ParticleID m_pid;
  // the actual hit position (3D + global time)
  CLHEP::HepLorentzVector m_position;
  // the particle 4-momentum at hit position
  CLHEP::HepLorentzVector m_momentum;
};

/// type for the hit collection
typedef G4THitsCollection<GaussSensPlaneHit> GaussSensPlaneHitsCollection;

extern G4ThreadLocal G4Allocator<GaussSensPlaneHit>* aGaussSensPlaneHitAllocator;

inline void* GaussSensPlaneHit::operator new( size_t ) {
  if ( !aGaussSensPlaneHitAllocator ) { aGaussSensPlaneHitAllocator = new G4Allocator<GaussSensPlaneHit>; }
  return (void*)aGaussSensPlaneHitAllocator->MallocSingle();
}

inline void GaussSensPlaneHit::operator delete( void* aHit ) {
  aGaussSensPlaneHitAllocator->FreeSingle( (GaussSensPlaneHit*)aHit );
}
