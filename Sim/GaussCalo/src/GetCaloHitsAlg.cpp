/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Event
#include "Event/MCCaloHit.h"
// #include "Event/MCSmallCaloHit.h"

// Gauss
#include "GaussCalo/CaloHit.h"
#include "GaussCalo/CaloSubHit.h"

// Gaussino
#include "Defaults/Locations.h"
#include "GiGaMTCoreRun/G4EventProxy.h"
#include "GiGaMTCoreRun/MCTruthConverter.h"
#include "MCTruthToEDM/LinkedParticleMCParticleLink.h"

// Gaudi
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiAlg/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GetCaloHitsAlg
//
// 2005-11-14 : Patrick Robbe
//-----------------------------------------------------------------------------

/** @class GetCaloHitsAlg GetCaloHitsAlg.h
 *  Conversion from G4 hits to MC CaloHits
 *
 *  @author Patrick Robbe
 *  @date   2005-11-14
 */
class GetCaloHitsAlg : public Gaudi::Functional::Transformer<LHCb::MCCaloHits( const G4EventProxies&,
                                                                               const LinkedParticleMCParticleLinks& ),
                                                             Gaudi::Functional::Traits::useLegacyGaudiAlgorithm> {
public:
  /// Standard constructor
  GetCaloHitsAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer(
            name, pSvcLocator,
            { KeyValue{ "Input", Gaussino::G4EventsLocation::Default },
              KeyValue{ "LinkedParticleMCParticleLinks", Gaussino::LinkedParticleMCParticleLinksLocation::Default } },
            KeyValue{ "MCHitsLocation", "" } ) {}

  virtual LHCb::MCCaloHits operator()( const G4EventProxies&, const LinkedParticleMCParticleLinks& ) const override;

protected:
private:
  Gaudi::Property<std::string> m_colName{ this, "CollectionName", "" };
};

DECLARE_COMPONENT( GetCaloHitsAlg )

LHCb::MCCaloHits GetCaloHitsAlg::operator()( const G4EventProxies&                eventproxies,
                                             const LinkedParticleMCParticleLinks& mcplinks ) const {
  debug() << "==> Execute" << endmsg;

  // Register output container to contain MCCaloHits
  LHCb::MCCaloHits hits{};
  size_t           total_numHits{ 0 };

  for ( auto& ep : eventproxies ) {
    auto hitCollection = ep->GetHitCollection<CaloHitsCollection>( m_colName );
    if ( hitCollection ) { total_numHits += hitCollection->entries(); }
  }
  if ( total_numHits > 0 ) { hits.reserve( total_numHits ); }

  for ( auto& ep : eventproxies ) {
    auto hitCollection = ep->GetHitCollection<CaloHitsCollection>( m_colName );
    if ( !hitCollection ) {
      warning() << "The hit collection='" + m_colName + "' is not found!" << endmsg;
      continue;
    }
    // reserve elements on output container
    size_t numOfHits = hitCollection->entries();
    // transform G4Hit in MCHits:
    // Loop over all hits in collection
    for ( size_t iHit = 0; iHit < numOfHits; ++iHit ) {
      // The calo hit in the calorimeter
      const CaloHit* hit = ( *hitCollection )[iHit];
      if ( !hit ) continue;

      // Loop over all sub hits of this calo hit
      for ( CaloHit::iterator iter = hit->begin(); iter != hit->end(); ++iter ) {
        // The sub hit
        const CaloSubHit* subhit = iter->second;
        if ( !subhit ) continue;

        // Loop over all energy/time deposits strored in the subhit
        for ( CaloSubHit::iterator entry = subhit->begin(); entry != subhit->end(); ++entry ) {
          // Create the new MCHit
          LHCb::MCCaloHit* mchit = new LHCb::MCCaloHit();
          // Fill it with:
          //   - Calorimeter CellID of the hit
          mchit->setCellID( hit->cellID() );
          //   - Time when the energy is deposited
          mchit->setTime( entry->first );
          //   - Active energy deposited
          mchit->setActiveE( entry->second );
          //   - Pointer to the LHCb::MCParticle giving the hit
          int trackID = subhit->GetTrackID();
          if ( auto lp = ep->truth()->GetParticleFromTrackID( trackID ); lp ) {
            if ( auto it = mcplinks.find( lp ); it != std::end( mcplinks ) ) {
              mchit->setParticle( it->second );
            } else {
              warning() << "No pointer to MCParticle for MCHit "
                           "associated to G4 "
                           "trackID: "
                        << trackID << endmsg;
            }
          } else {
            warning() << "No LinkedParticle found. Something went "
                         "seriously wrong. "
                         "trackID: "
                      << trackID << endmsg;
          }

          // Now insert in output container
          hits.add( mchit );
        }
      }
    }
  }

  return hits;
}
