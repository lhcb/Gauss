/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gauss
#include "CaloSensDetFactory.h"
#include "HcalSensDet.h"

namespace Gauss::Calo::Hcal {

  template <class CELLGETTER>
  using HCAL = HcalSensDet<CELLGETTER>;

  template <class CELLGETTER>
  using BASE = Calo::SensDetFactory<HCAL<CELLGETTER>>;

  template <class CELLGETTER>
  class SensDetFactory : public BASE<CELLGETTER> {
    Gaudi::Property<double> m_slotWidth{ this, "SlotWidth", 25 * Gaudi::Units::ns };

  public:
    using BASE<CELLGETTER>::SensDetFactory;
    virtual HCAL<CELLGETTER>* construct() const override {
      auto tmp         = BASE<CELLGETTER>::construct();
      tmp->m_slotWidth = m_slotWidth;
      return tmp;
    }
  };
} // namespace Gauss::Calo::Hcal
