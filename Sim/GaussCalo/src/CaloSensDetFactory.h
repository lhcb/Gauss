/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Gauss
#include "CaloSensDet.h"

// Gaussino
#include "GiGaMTDetFactories/GiGaMTG4SensDetFactory.h"

namespace Gauss::Calo {
  template <typename CALO>
  class SensDetFactory : public GiGaMTG4SensDetFactory<CALO> {
    double c1unit = Gaudi::Units::g / Gaudi::Units::MeV / Gaudi::Units::cm2;
    double c2unit = Gaudi::Units::g * Gaudi::Units::g / Gaudi::Units::MeV / Gaudi::Units::MeV / Gaudi::Units::cm2 /
                    Gaudi::Units::cm2;

    Gaudi::Property<double> m_birk_c1{ this, "BirkC1", 0.013 * c1unit, "the first coefficient of Birk's law" };
    Gaudi::Property<double> m_birk_c2{ this, "BirkC2", 9.6E-6 * c2unit, "the second coefficient of Birk's law" };
    Gaudi::Property<double> m_birk_c1correction{ this, "BirkC1cor", 0.57142857,
                                                 "the correction to the first coefficient of Birk's law" };
    Gaudi::Property<double> m_zmin{ this, "zMin", -1 * Gaudi::Units::km };
    Gaudi::Property<double> m_zmax{ this, "zMax", 1 * Gaudi::Units::km };
    Gaudi::Property<double> m_dT0{ this, "dT0", 0.5 * Gaudi::Units::ns,
                                   "the additive correction for the evaluation of t0" };
    // Fast Simulation options
    Gaudi::Property<bool> m_bypassCaloCorrections{ this, "BypassCaloCorrections", true };
    Gaudi::Property<bool> m_bypassCaloTiming{ this, "BypassCaloTiming", false };

    /// the vector of histogram names/addresses
    Gaudi::Property<std::vector<std::string>> m_histoNames{ this, "Histograms", {} };
    ServiceHandle<IHistogramSvc>              m_histoSvc{ this, "HistogramDataSvc", "HistogramDataSvc" };

    // counters
    mutable Gaudi::Accumulators::SummingCounter<size_t> m_nhits{ this, "#hits" };
    mutable Gaudi::Accumulators::SummingCounter<size_t> m_nshits{ this, "#subhits" };
    mutable Gaudi::Accumulators::SummingCounter<size_t> m_nslots{ this, "#tslots" };
    mutable Gaudi::Accumulators::SummingCounter<double> m_energy{ this, "#energy" };

    // histograms
    std::vector<AIDA::IHistogram1D*> m_histos;

  public:
    using gaussino_base_class = GiGaMTG4SensDetFactory<CALO>;
    using gaussino_base_class::GiGaMTG4SensDetFactory;

    virtual CALO* construct() const override {
      auto tmp                     = gaussino_base_class::construct();
      tmp->m_birk_c1               = m_birk_c1;
      tmp->m_birk_c2               = m_birk_c2;
      tmp->m_birk_c1correction     = m_birk_c1correction;
      tmp->m_zmin                  = m_zmin;
      tmp->m_zmax                  = m_zmax;
      tmp->m_dT0                   = m_dT0;
      tmp->m_histos                = m_histos;
      tmp->m_nhits                 = &m_nhits;
      tmp->m_nshits                = &m_nshits;
      tmp->m_nslots                = &m_nslots;
      tmp->m_energy                = &m_energy;
      tmp->m_bypassCaloCorrections = m_bypassCaloCorrections;
      tmp->m_bypassCaloTiming      = m_bypassCaloTiming;
      return tmp;
    }

    StatusCode initialize() override {
      return gaussino_base_class::initialize().andThen( [&]() -> StatusCode {
        m_histoSvc.retrieve().ignore();
        for ( auto& histo : m_histoNames ) {
          SmartDataPtr<IHistogram1D> pHist( m_histoSvc.get(), histo );
          IHistogram1D*              hist = pHist;
          if ( 0 == hist ) { return this->Error( "Cannot load histogram '" + ( histo ) + "'" ); }
          m_histos.push_back( hist );
        }
        if ( m_histos.empty() ) { this->warning() << "Empty vector of input time-histograms" << endmsg; }
        return StatusCode::SUCCESS;
      } );
    }
  };
} // namespace Gauss::Calo
