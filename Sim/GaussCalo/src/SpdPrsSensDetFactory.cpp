/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gauss
#include "CaloSensDetFactory.h"
#include "CellFromDetDesc.h"
#include "SpdPrsSensDet.h"

namespace Gauss::Calo::SpdPrs {

  using SPDPRS = SpdPrsSensDet<CellFromDetDesc>;
  using BASE   = Calo::SensDetFactory<SPDPRS>;

  class SensDetFactory : public BASE {
    Gaudi::Property<double>                   m_BX{ this, "BunchCrossing", 25. * Gaudi::Units::ns };
    Gaudi::Property<unsigned int>             m_numBXs{ this, "NumberBXs", 6 };
    Gaudi::Property<std::vector<double>>      m_sDelays{ this, "IntegrationDelays", { 0, 0, 0 } };
    Gaudi::Property<double>                   m_fracMin{ this, "FracMin", 1.e-5 };
    Gaudi::Property<std::string>              m_caloName{ this, "Detector", DeCalorimeterLocation::Prs };
    Gaudi::Property<std::string>              m_endVolumeName{ this, "EndVolume", "" };
    Gaudi::Property<std::vector<std::string>> m_startVolumeNames{ this, "StartVolumes", {} };

  public:
    using BASE::SensDetFactory;
    virtual SPDPRS* construct() const override {
      auto tmp       = BASE::construct();
      tmp->m_BX      = m_BX;
      tmp->m_numBXs  = m_numBXs;
      tmp->m_sDelays = m_sDelays;
      tmp->m_fracMin = m_fracMin;
      tmp->m_calo    = this->template getDet<DeCalorimeter>( m_caloName );
      if ( !tmp->m_calo ) { throw std::runtime_error( "Could not locate CALO " + m_caloName ); }
      tmp->m_caloID           = LHCb::Detector::Calo::CellCode::CaloNumFromName( m_caloName );
      tmp->m_endVolumeName    = m_endVolumeName;
      tmp->m_startVolumeNames = m_startVolumeNames;
      return tmp;
    }
  };

  DECLARE_COMPONENT_WITH_ID( SensDetFactory, "SpdPrsSensDet" )
} // namespace Gauss::Calo::SpdPrs
