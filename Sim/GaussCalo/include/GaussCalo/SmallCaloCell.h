/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiTool.h"

// LHCb
#include "Detector/Calo/CaloCellID.h"

class G4StepPoint;
class DeCalorimeter;

class SmallCaloCell {
  using Dim   = int;
  using NDims = std::vector<int>;

  Dim                          m_col   = 0; // col (along y-axis) to be computed
  Dim                          m_row   = 0; // row (along x-axis) to be computed
  Dim                          m_ncols = 1; // ncols (along y-axis)
  Dim                          m_nrows = 1; // nrows (along x-axis)
  LHCb::Detector::Calo::CellID m_cellID;

public:
  SmallCaloCell() = default;
  SmallCaloCell( NDims, NDims, const G4StepPoint*, const DeCalorimeter*, const LHCb::Detector::Calo::CellID& );
  size_t      index() const { return (size_t)m_cellID.index() << 32 | (size_t)m_row << 16 | m_col; }
  size_t      hash() const { return index(); }
  friend bool operator==( const SmallCaloCell& lhs, const SmallCaloCell& rhs ) { return lhs.index() == rhs.index(); }

  inline Dim col() { return m_col; };
  inline Dim ncols() { return m_ncols; };
  inline Dim row() { return m_row; };
  inline Dim nrows() { return m_nrows; };
};

inline size_t hash_value( const SmallCaloCell& smc ) { return smc.index(); }
