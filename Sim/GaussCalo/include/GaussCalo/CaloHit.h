/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include files
#include <unordered_map>
// GaudiKernel
#include "GaudiKernel/StatusCode.h"
// Kernel
#include "Detector/Calo/CaloCellID.h"
// G4
#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4VHit.hh"
// local
#include "GaussCalo/CaloSim.h"
#include "GaussCalo/CaloSimHash.h"
#include "GaussCalo/CaloSubHit.h"
// #include "GaussCalo/SmallCaloCell.h"

// forward decalration
template <class T>
class G4THitsCollection;

/** @class CaloHit CaloHit.h
 *
 *  Elementary "hit" for calorimeter devices.
 *  The class represents an list of energy depositions
 *  from all MC particles for given calorimeter cell
 *
 *  The new created hit (of type <tt>CaloHit</tt>
 *  will be attributed to the parent particle of the track
 *  with 2 exceptions
 *   - if the track is already forced to be stored
 *   - if the track is produces outside the "forbidden" zone
 *
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   2002-12-03
 */

class CaloHit : public G4VHit {
public:
  /// the actual data type for particle
  typedef int TrackID;
  /// collection of sub-hits
  // FIXME: Use thread-local allocator
  typedef std::unordered_map<TrackID, CaloSubHit*> TheMap;
  /// actual type of the iterator
  typedef TheMap::const_iterator iterator;

public:
  /** standard  constructor
   *  @param cell ID of calorimeter cell
   */
  inline CaloHit( const LHCb::Detector::Calo::CellID& cell = LHCb::Detector::Calo::CellID() )
      : m_cellID{ cell }, m_map{} {}

  /** copy constructor
   *  @param right object to be copied
   */
  inline CaloHit( const CaloHit& right ) : G4VHit( right ), m_cellID( right.cellID() ), m_map() {
    for ( auto& [id, hit] : *this )

    {
      m_map[id] = new CaloSubHit{ *hit };
    }
  }

  /// destructor / delete all subhits
  inline virtual ~CaloHit() {
    /// delete all sub hits
    for ( TheMap::iterator ihit = m_map.begin(); m_map.end() != ihit; ++ihit ) {
      CaloSubHit* hit = ihit->second;
      if ( 0 != hit ) {
        delete hit;
        ihit->second = 0;
      }
    }
    m_map.clear();
  }

  inline void* operator new( size_t );
  inline void operator delete( void* hit );

  /// get the cellID
  inline const LHCb::Detector::Calo::CellID& cellID() const { return m_cellID; }
  // set new cellID
  inline void setCellID( const LHCb::Detector::Calo::CellID& cell ) { m_cellID = cell; }

  /** get the hit for the given particle
   *
   *  @code
   *
   *  CaloHit*                hit   = ... ;
   *  const CaloHit::TrackID  track = ... ;
   *
   *  CaloSubHit*& subhit = hit->hit( track ) ;
   *  if ( 0 == subhit )
   *        { subhit = new CaloSubHit( hit->cellID() , track ) ; }
   *
   *  @endcode
   *
   *  @return the sub-hit for the given track id (or NULL)
   */
  inline CaloSubHit*& hit( const TrackID track ) { return m_map[track]; }

  /** access to 'begin' iterator of the sub-hit collections (const)
   *
   *  @code
   *
   *  CaloHit* hit = ... ;
   *
   *  for ( CaloHit::iterator entry = hit->begin() ;
   *        hit->end() != entry ; ++entry  )
   *       {
   *         const CaloHit::TrackID track  = entry->first  ;
   *         const CaloSubHit*      subhit = entry->second ;
   *       }
   *
   *  @endcode
   *
   *  @return 'begin' iterator for sequence of subhits
   */
  iterator begin() const { return m_map.begin(); }
  /** access to 'end'   iterator of the sub-hit collections (const)
   *
   *  @code
   *
   *  CaloHit* hit = ... ;
   *
   *  for ( CaloHit::iterator entry = hit->begin() ;
   *        hit->end() != entry ; ++entry  )
   *       {
   *         const CaloHit::TrackID track  = entry->first  ;
   *         const CaloSubHit*      subhit = entry->second ;
   *       }
   *
   *  @endcode
   *
   *  @return 'end' iterator for sequence of subhits
   */
  iterator end() const { return m_map.end(); }

  /// number of entries/map size
  inline size_t size() const { return m_map.size(); }
  inline size_t entries() const { return size(); }

  /// total number of (sub)entries
  inline size_t totalSize() const {
    size_t size = 0;
    for ( iterator hit = begin(); end() != hit; ++hit ) {
      if ( 0 != hit->second ) { size += hit->second->size(); }
    }
    return size;
  };
  /** the total energy (integrated over all time slots  and all particles)
   *
   *  @code
   *
   *  const CaloHit*           hit = ... ;
   *  const CaloSubHit::Energy e   = hit->energy() ;
   *
   *  @endcode
   *
   *  @return the total energy (integrated over all time
   *                             slots  and all particles)
   */
  inline CaloSubHit::Energy energy() const {
    CaloSubHit::Energy e = 0;
    for ( iterator entry = begin(); end() != entry; ++entry ) {
      const CaloSubHit* hit = entry->second;
      if ( 0 != hit ) { e += hit->energy(); }
    }
    return e;
  }

private:
  LHCb::Detector::Calo::CellID m_cellID;
  TheMap                       m_map;
};

/// type for the hit collection
typedef G4THitsCollection<CaloHit> CaloHitsCollection;

extern G4ThreadLocal G4Allocator<CaloHit>* aCaloHitAllocator;

inline void* CaloHit::operator new( size_t ) {
  if ( !aCaloHitAllocator ) { aCaloHitAllocator = new G4Allocator<CaloHit>; }
  return (void*)aCaloHitAllocator->MallocSingle();
}

inline void CaloHit::operator delete( void* aHit ) { aCaloHitAllocator->FreeSingle( (CaloHit*)aHit ); }
