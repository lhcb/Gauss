/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "GaussCherenkov/CkvGrandSensDet.h"
#include "GaussCherenkov/CkvSensDet.h"
#include "GaussCherenkov/CkvStdSensDet.h"

#include "GaussCherenkov/GetMCCkvHitsAlg.h"
#include "GaussCherenkov/GetMCCkvOpticalPhotonsAlg.h"
#include "GaussCherenkov/GetMCCkvSegmentsAlg.h"
#include "GaussCherenkov/GetMCCkvTracksAlg.h"

// Declaration of the Tool Factory

DECLARE_COMPONENT( CkvSensDet )
DECLARE_COMPONENT( CkvStdSensDet )
DECLARE_COMPONENT( CkvGrandSensDet )

DECLARE_COMPONENT( GetMCCkvHitsAlg )
DECLARE_COMPONENT( GetMCCkvOpticalPhotonsAlg )
DECLARE_COMPONENT( GetMCCkvSegmentsAlg )
DECLARE_COMPONENT( GetMCCkvTracksAlg )
