/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichG4ReconTransformHpd.cpp,v 1.11 2008-06-24 15:56:35 jonrob Exp $
// Include files
#include <boost/lexical_cast.hpp>

#include "GaudiKernel/IConversionSvc.h"
#include "GaudiKernel/IConverter.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/RegistryEntry.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "GaudiKernel/MsgStream.h"

#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/ILVolume.h"
#include "DetDesc/IPVolume.h"
#include "DetDesc/Material.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/TabulatedProperty.h"

// RichDet
#include "RichDet/DeRich.h"

#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// local
#include "GaussCherenkov/CkvG4SvcLocator.h"
#include "GaussCherenkov/CkvGeometrySetupUtil.h"
#include "GaussRICH/RichG4GaussPathNames.h"
#include "RichG4ReconTransformPmt.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichG4ReconTransformPmt
//
// 2003-09-09 : Sajan EASO
//-----------------------------------------------------------------------------
//

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichG4ReconTransformPmt::RichG4ReconTransformPmt() {}
RichG4ReconTransformPmt::~RichG4ReconTransformPmt() {}

void RichG4ReconTransformPmt::initialise() {

  IDataProviderSvc* detSvc = CkvG4SvcLocator::RichG4detSvc();

  m_Rich1SubMasterPvIndex = 0;
  m_PmtSMasterIndex       = 0;

  m_Rich1MagShPvName0   = "pvRich1MagShH0:0";
  m_Rich1MagShPvName1   = "pvRich1MagShH1:1";
  m_Rich1PhtDetSupName0 = "pvRich1PhDetSupFrameH0:0";
  m_Rich1PhtDetSupName1 = "pvRich1PhDetSupFrameH1:1";
  m_Rich2PmtPanelName0  = "pvRich2HPDPanel:0";
  m_Rich2PmtPanelName1  = "pvRich2HPDPanel:1";
  m_Rich2PmtN2EnclName0 = "pvRich2HPDN2Encl0";
  m_Rich2PmtN2EnclName1 = "pvRich2HPDN2Encl1";

  m_Rich1PmtStdModuleName        = "pvRich1PmtStdModule";
  m_Rich1PmtNoEC0TypeModuleName  = "pvRich1PmtNoEC0TypeModule";
  m_Rich1PmtNoEC3TypeModuleName  = "pvRich1PmtNoEC3TypeModule";
  m_Rich1PmtNoEC01TypeModuleName = "pvRich1PmtNoEC01TypeModule";
  m_Rich1PmtNoEC23TypeModuleName = "pvRich1PmtNoEC23TypeModule";
  m_Rich2PmtStdModuleName        = "pvRich2PmtStdModule";
  m_Rich2PmtGrandModuleName      = "pvRich2PmtGrandModule";

  m_Rich1PmtStdECName                   = { "pvRichPmtStdECR", "InModule" };
  m_Rich1PmtStdECInNoEC0TypeModuleName  = { "pvRichPmtStdECR", "InNoEC0TypeModule" };
  m_Rich1PmtStdECInNoEC3TypeModuleName  = { "pvRichPmtStdECR", "InNoEC3TypeModule" };
  m_Rich1PmtStdECInNoEC01TypeModuleName = { "pvRichPmtStdECR", "InNoEC01TypeModule" };
  m_Rich1PmtStdECInNoEC23TypeModuleName = { "pvRichPmtStdECR", "InNoEC23TypeModule" };
  m_Rich2PmtStdECName                   = { "pvRichPmtStdEC", "InModule" };
  m_Rich2PmtGrandECName                 = { "pvRichPmtGrandECH", "InModule" };

  SmartDataPtr<DetDesc::DetectorElementPlus> Rich1DE( detSvc, Rich1DeStructurePathName );
  if ( Rich1DE ) {
    m_Rich1PmtModuleMaxH0       = ( Rich1DE->param<int>( "Rich1TotNumModules" ) ) / 2;
    m_Rich2PmtModuleMaxH0       = ( Rich1DE->param<int>( "Rich2TotNumModules" ) ) / 2;
    m_RichNumPmtInModule        = Rich1DE->param<int>( "RichTotNumPmtInModule" );
    m_RichMaxNumEcInModule      = Rich1DE->param<int>( "RichMaxNumEcInModule" );
    m_RichMaxNumPmtInEc         = Rich1DE->param<int>( "RichMaxNumPmtInEc" );
    m_Rich1NumberOfModulesInRow = Rich1DE->param<int>( "Rich1NumberOfModulesInRow" );
    m_Rich1NumberOfModulesInCol = Rich1DE->param<int>( "Rich1NumberOfModulesInCol" );
  }
}

RichG4ReconTransformPmt::RichG4ReconTransformPmt( int aRichDetNum, int aPmtModuleNumber, int aPmtNumberInModule ) {

  initialise();
  IDataProviderSvc* detSvc = CkvG4SvcLocator::RichG4detSvc();
  IMessageSvc*      msgSvc = CkvG4SvcLocator::RichG4MsgSvc();
  MsgStream         RichG4ReconTransformPmtlog( msgSvc, "RichG4TransformPmt" );

  CkvGeometrySetupUtil* aCkvGeometrySetup = CkvGeometrySetupUtil::getCkvGeometrySetupUtilInstance();

  //

  // RichG4ReconTransformPmtlog<<MSG::INFO
  //                <<"Now creating Pmt transform "
  //                <<  aRichDetNum
  //                              << "  "<<aPmtNumber <<endmsg;

  if ( aRichDetNum == 0 ) {

    SmartDataPtr<DetDesc::DetectorElementPlus> Rich1DE( detSvc, DeRichLocations::Rich1 );

    if ( !Rich1DE ) {

      RichG4ReconTransformPmtlog << MSG::ERROR << "Can't retrieve " << DeRichLocations::Rich1
                                 << " for RichG4TransformPmt" << endmsg;

    } else {

      const ILVolume*           aRich1MasterLogVol   = Rich1DE->geometryPlus()->lvolume();
      const Gaudi::Transform3D& aRich1MasterTrans    = Rich1DE->geometryPlus()->toLocalMatrix();
      const Gaudi::Transform3D& aRich1MasterTransInv = Rich1DE->geometryPlus()->toGlobalMatrix();

      const IPVolume* apva = aRich1MasterLogVol->pvolume( m_Rich1SubMasterPvIndex );

      //   RichG4ReconTransformPmtlog<<MSG::INFO
      //  <<"Now creating Pmt transform for rich1 hpd "
      //                          <<aPmtNumber<<endmsg;

      if ( apva ) {
        //  RichG4ReconTransformPmtlog<<MSG::INFO
        //         << " Rich1SubMaster pvol lvol num sub vol"
        //                          <<apva->name() <<"   "
        //                          <<apva->lvolumeName()<<"  "
        //                          <<apva->lvolume()-> noPVolumes()
        //                          <<endmsg;

        const Gaudi::Transform3D& apvaTrans    = apva->matrix();
        const Gaudi::Transform3D& apvaTransInv = apva->matrixInv();
        // for test print the names of sub volumes
        // RichG4ReconTransformPmtlog<<MSG::INFO
        //                           <<"rich1 test of hpd number "
        //                           <<aPmtNumber<<endmsg;

        //  for (int it=0;it< (int) apva->lvolume()-> noPVolumes(); it++) {
        //  RichG4ReconTransformPmtlog<<MSG::INFO
        //                         <<"daughter pv index name "
        //                         <<it<<"  "
        //                         <<  apva->lvolume()->
        //                      pvolume(it)->name()
        //                         <<endmsg;

        // }

        //   RichG4ReconTransformPmtlog<<MSG::INFO
        //                     <<"IN rich1 current hpd num = "
        //                     <<aPmtNumber<<endmsg;

        const IPVolume* apvb = ( aPmtModuleNumber < m_Rich1PmtModuleMaxH0 )
                                   ? apva->lvolume()->pvolume( m_Rich1MagShPvName0 )
                                   : apva->lvolume()->pvolume( m_Rich1MagShPvName1 );

        if ( apvb ) {

          // RichG4ReconTransformPmtlog<<MSG::INFO
          //             << " Rich1Magsh pvol lvol hpdnum "
          //                        <<apvb->name() <<"   "
          //                        <<apvb->lvolumeName()
          //                        <<"  "<<aPmtNumber
          //                        <<endmsg;

          const Gaudi::Transform3D& apvbTrans    = apvb->matrix();
          const Gaudi::Transform3D& apvbTransInv = apvb->matrixInv();

          const IPVolume* apvc = ( aPmtModuleNumber < m_Rich1PmtModuleMaxH0 )
                                     ? apvb->lvolume()->pvolume( m_Rich1PhtDetSupName0 )
                                     : apvb->lvolume()->pvolume( m_Rich1PhtDetSupName1 );

          if ( apvc ) {

            //  RichG4ReconTransformPmtlog<<MSG::INFO
            //         << " Rich1PhotDetSup pvol lvol "
            //                       <<apvc->name() <<"   "
            //                      <<apvc->lvolumeName()
            //                      <<endmsg;
            // now account for the fact that the index is
            // restarted int he bottom ph det sup vol.

            const Gaudi::Transform3D& apvcTrans    = apvc->matrix();
            const Gaudi::Transform3D& apvcTransInv = apvc->matrixInv();

            //            m_PhDetSupGlobalToLocal = apvdTrans * apvcTrans * apvbTrans *
            //       apvaTrans * aRich1MasterTrans;
            // m_PhDetSupLocalToGlobal = aRich1MasterTransInv*apvaTransInv*
            //                          apvbTransInv* apvcTransInv*
            //                          apvdTransInv;

            // int aPmtModuleIndex =  aPmtModuleNumber;
            //  if( aPmtModuleNumber >= m_Rich1PmtModuleMaxH0 ){
            //   aPmtModuleIndex = aPmtModuleNumber-m_Rich1PmtModuleMaxH0;
            //  }

            //            const IPVolume* apvd = apvc->lvolume()
            //->pvolume( aPmtModuleIndex);

            // outer modules numbers (0, 5, 66, 71)
            const auto firstOuterModuleNumber    = 0;
            const int  aPmtOuterModuleNumbers[4] = {
                firstOuterModuleNumber, firstOuterModuleNumber + ( m_Rich1NumberOfModulesInRow - 1 ),
                m_Rich1PmtModuleMaxH0 + firstOuterModuleNumber,
                m_Rich1PmtModuleMaxH0 + firstOuterModuleNumber + ( m_Rich1NumberOfModulesInRow - 1 ) };

            const int         aPmtModuleNumberGlobal   = aPmtModuleNumber;
            const std::string aPmtModuleNumberAsString = boost::lexical_cast<std::string>( aPmtModuleNumberGlobal );
            std::string       aPmtModuleNumberAsStringFull;
            if ( aPmtModuleNumberGlobal < 10 )
              aPmtModuleNumberAsStringFull = "00" + aPmtModuleNumberAsString;
            else if ( aPmtModuleNumberGlobal < 100 )
              aPmtModuleNumberAsStringFull = "0" + aPmtModuleNumberAsString;
            else
              aPmtModuleNumberAsStringFull = aPmtModuleNumberAsString;

            std::string aCurrentModuleName = m_Rich1PmtStdModuleName;
            // for modules # 0,6,12, ..., 126 (calculate module# in it's row)
            if ( aPmtModuleNumber - ( aPmtModuleNumber / m_Rich1NumberOfModulesInRow ) * m_Rich1NumberOfModulesInRow ==
                 0 ) {

              if ( aPmtModuleNumber == aPmtOuterModuleNumbers[0] || aPmtModuleNumber == aPmtOuterModuleNumbers[2] )
                aCurrentModuleName = m_Rich1PmtNoEC01TypeModuleName;
              else
                aCurrentModuleName = m_Rich1PmtNoEC0TypeModuleName;
              // for modules # 5,11,17, ..., 131 (calculate module# in it's row - offset by 5)
            } else if ( ( aPmtModuleNumber - ( m_Rich1NumberOfModulesInRow - 1 ) ) -
                            ( ( aPmtModuleNumber - ( m_Rich1NumberOfModulesInRow - 1 ) ) /
                              m_Rich1NumberOfModulesInRow ) *
                                m_Rich1NumberOfModulesInRow ==
                        0 ) {

              if ( aPmtModuleNumber == aPmtOuterModuleNumbers[1] || aPmtModuleNumber == aPmtOuterModuleNumbers[3] )
                aCurrentModuleName = m_Rich1PmtNoEC23TypeModuleName;
              else
                aCurrentModuleName = m_Rich1PmtNoEC3TypeModuleName;
            }

            const std::string aPmtModuleName =
                aCurrentModuleName + aPmtModuleNumberAsStringFull + ":" + aPmtModuleNumberAsString;

            const IPVolume* apvd = apvc->lvolume()->pvolume( aPmtModuleName );
            if ( apvd ) {
              //      RichG4ReconTransformPmtlog<<MSG::INFO
              //         << " Rich1PmtModulemaster pvol lvol num index "
              //                    <<apvd->name() <<"   "
              //                    <<apvd->lvolumeName()
              //                              <<"  "<< aPmtModuleNumber<<endmsg;

              //                                  <<"  "<< aPmtModuleIndex
              //                <<endmsg;
              //   RichG4ReconTransformPmtlog<<MSG::INFO<<" Pmt num in Module  "
              //      <<aPmtModuleNumber<<"  "<<aPmtNumberInModule<<endmsg;

              const Gaudi::Transform3D& apvdTrans    = apvd->matrix();
              const Gaudi::Transform3D& apvdTransInv = apvd->matrixInv();

              // relation between local EC/PMT number for std modules
              const int         aPmtECNumberInModule         = aPmtNumberInModule / m_RichMaxNumPmtInEc;
              const std::string aPmtECNumberInModuleAsString = boost::lexical_cast<std::string>( aPmtECNumberInModule );
              const std::string aPmtECNumberGlobalAsString   = boost::lexical_cast<std::string>(
                  aPmtModuleNumberGlobal * m_RichMaxNumEcInModule + aPmtECNumberInModule );

              auto aCurrentECName = m_Rich1PmtStdECName;
              // for modules # 0,6,12, ..., 126 (calculate module# in it's row)
              if ( aPmtModuleNumber -
                       ( aPmtModuleNumber / m_Rich1NumberOfModulesInRow ) * m_Rich1NumberOfModulesInRow ==
                   0 ) {

                if ( aPmtModuleNumber == aPmtOuterModuleNumbers[0] || aPmtModuleNumber == aPmtOuterModuleNumbers[2] )
                  aCurrentECName = m_Rich1PmtStdECInNoEC01TypeModuleName;
                else
                  aCurrentECName = m_Rich1PmtStdECInNoEC0TypeModuleName;
                // for modules # 5, 11, 17, ..., 131 (calculate module# in it's row - offset by 5)
              } else if ( ( aPmtModuleNumber - ( m_Rich1NumberOfModulesInRow - 1 ) ) -
                              ( ( aPmtModuleNumber - ( m_Rich1NumberOfModulesInRow - 1 ) ) /
                                m_Rich1NumberOfModulesInRow ) *
                                  m_Rich1NumberOfModulesInRow ==
                          0 ) {

                if ( aPmtModuleNumber == aPmtOuterModuleNumbers[1] || aPmtModuleNumber == aPmtOuterModuleNumbers[3] )
                  aCurrentECName = m_Rich1PmtStdECInNoEC23TypeModuleName;
                else
                  aCurrentECName = m_Rich1PmtStdECInNoEC3TypeModuleName;
              }

              const std::string aPmtECName = aCurrentECName[0] + aPmtECNumberInModuleAsString + aCurrentECName[1] +
                                             aPmtModuleNumberAsStringFull + ":" + aPmtECNumberGlobalAsString;

              const IPVolume* apve = apvd->lvolume()->pvolume( aPmtECName );
              if ( apve ) {

                const Gaudi::Transform3D& apveTrans    = apve->matrix();
                const Gaudi::Transform3D& apveTransInv = apve->matrixInv();

                const IPVolume* apvf =
                    apve->lvolume()->pvolume( aPmtNumberInModule - aPmtECNumberInModule * m_RichMaxNumPmtInEc );
                if ( apvf ) {

                  const Gaudi::Transform3D& apvfTrans    = apvf->matrix();
                  const Gaudi::Transform3D& apvfTransInv = apvf->matrixInv();

                  // RichG4ReconTransformPmtlog<<MSG::INFO<<" Pmt num in Module Vol names "<<
                  //  aPmtNumberInModule << apvf->name()<<"   "<<apvf->lvolumeName()<<endmsg;

                  const IPVolume* apvg = apvf->lvolume()->pvolume( m_PmtSMasterIndex );
                  if ( apvg ) {
                    // RichG4ReconTransformPmtlog<<MSG::INFO
                    //      << " Rich1hpdsubmaster pvol lvol "
                    //                  <<apvf->name() <<"   "
                    //                  <<apvf->lvolumeName()
                    //                  <<endmsg;

                    const Gaudi::Transform3D& apvgTrans    = apvg->matrix();
                    const Gaudi::Transform3D& apvgTransInv = apvg->matrixInv();

                    m_PmtGlobalToLocal = apvgTrans * apvfTrans * apveTrans * apvdTrans * apvcTrans * apvbTrans *
                                         apvaTrans * aRich1MasterTrans;
                    m_PmtLocalToGlobal = aRich1MasterTransInv * apvaTransInv * apvbTransInv * apvcTransInv *
                                         apvdTransInv * apveTransInv * apvfTransInv * apvgTransInv;

                    m_PmtPhDetPanelToLocal    = apvgTrans * apvfTrans * apveTrans * apvdTrans;
                    m_PmtLocalToPmtPhDetPanel = apvdTransInv * apveTransInv * apvfTransInv * apvgTransInv;
                  }
                }
              }
            }
          }
        }
      }
    }
  } else if ( aRichDetNum == 1 ) {

    SmartDataPtr<DetDesc::DetectorElementPlus> Rich2DE( detSvc, DeRichLocations::Rich2 );

    if ( !Rich2DE ) {

      RichG4ReconTransformPmtlog << MSG::ERROR << "Can't retrieve " << DeRichLocations::Rich2
                                 << " for RichG4TransformPmt" << endmsg;

    } else {

      //        RichG4ReconTransformPmtlog<<MSG::INFO
      //                       <<"Now creating Pmt transform for rich2 "
      //                       <<endmsg;

      //      RichG4ReconTransformPmtlog<<MSG::INFO
      //        << " Rich2Master lvol num sub vol "
      //                         <<Rich2DE->geometryPlus()->lvolume()->name()
      //              <<"  "<<Rich2DE->geometryPlus()->lvolume()-> noPVolumes()
      //                         <<endmsg;
      //
      // for (int it2=0;it2< (int) Rich2DE->geometryPlus()->lvolume()
      //               -> noPVolumes(); it2++) {
      // RichG4ReconTransformPmtlog<<MSG::INFO
      //                         <<"rich2 daughter pv index name "
      //                         <<it2<<"  "
      //                         << Rich2DE->geometryPlus() ->lvolume()->
      //                      pvolume(it2)->name()
      //                         <<endmsg;
      //
      //
      // }

      const ILVolume*           aRich2MasterLogVol   = Rich2DE->geometryPlus()->lvolume();
      const Gaudi::Transform3D& aRich2MasterTrans    = Rich2DE->geometryPlus()->toLocalMatrix();
      const Gaudi::Transform3D& aRich2MasterTransInv = Rich2DE->geometryPlus()->toGlobalMatrix();

      //      Gaudi::XYZPoint testk(0.0,0.0,0.0);
      // RichG4ReconTransformPmtlog<<MSG::INFO<<"test K  "<<testk<<"   "<<aRich2MasterTrans*testk<<endmsg;

      const IPVolume* bpva = ( aPmtModuleNumber < m_Rich2PmtModuleMaxH0 )
                                 ? aRich2MasterLogVol->pvolume( m_Rich2PmtN2EnclName0 )
                                 : aRich2MasterLogVol->pvolume( m_Rich2PmtN2EnclName1 );

      if ( bpva ) {
        //       RichG4ReconTransformPmtlog<<MSG::INFO
        //                   <<" pmt num hpdpanel name "
        //                  <<aPmtModuleNumber<<"   "
        //                  << bpva->name()<<endmsg;

        const Gaudi::Transform3D& bpvaTrans    = bpva->matrix();
        const Gaudi::Transform3D& bpvaTransInv = bpva->matrixInv();

        const IPVolume* bpvb = ( aPmtModuleNumber < m_Rich2PmtModuleMaxH0 )
                                   ? bpva->lvolume()->pvolume( m_Rich2PmtPanelName0 )
                                   : bpva->lvolume()->pvolume( m_Rich2PmtPanelName1 );

        if ( bpvb ) {

          const Gaudi::Transform3D& bpvbTrans    = bpvb->matrix();
          const Gaudi::Transform3D& bpvbTransInv = bpvb->matrixInv();

          //  RichG4ReconTransformPmtlog<<MSG::INFO<<" Now get Rich2 bpvs pmtModule  pmtModuleinSide PmtNumInModule   "
          //      << aPmtModuleNumber <<"    "<< aPmtModuleIndexR2<<"   "<<aPmtNumberInModule<< endmsg;

          const int         aPmtModuleNumberGlobal       = aPmtModuleNumber + m_Rich1PmtModuleMaxH0 * 2;
          const std::string aPmtModuleNumberAsString     = boost::lexical_cast<std::string>( aPmtModuleNumberGlobal );
          std::string       aPmtModuleNumberAsStringFull = "";
          if ( aPmtModuleNumberGlobal < 10 )
            aPmtModuleNumberAsStringFull = "00" + aPmtModuleNumberAsString;
          else if ( aPmtModuleNumberGlobal < 100 )
            aPmtModuleNumberAsStringFull = "0" + aPmtModuleNumberAsString;
          else
            aPmtModuleNumberAsStringFull = aPmtModuleNumberAsString;

          // check if the module is grand one (using global module number)
          const bool aPmtModuleIsGrand = aCkvGeometrySetup->ModuleWithGrandPMT( aPmtModuleNumberGlobal );

          const std::string aCurrentModuleName =
              aPmtModuleIsGrand ? m_Rich2PmtGrandModuleName : m_Rich2PmtStdModuleName;

          const std::string aPmtModuleName =
              aCurrentModuleName + aPmtModuleNumberAsStringFull + ":" + aPmtModuleNumberAsString;

          const IPVolume* bpvc = bpvb->lvolume()->pvolume( aPmtModuleName );

          if ( bpvc ) {
            const Gaudi::Transform3D& bpvcTrans    = bpvc->matrix();
            const Gaudi::Transform3D& bpvcTransInv = bpvc->matrixInv();

            // relation between local EC/PMT number for grand/std modules (is different)
            const int aPmtECNumberInModule =
                aPmtModuleIsGrand ? aPmtNumberInModule : aPmtNumberInModule / m_RichMaxNumPmtInEc;
            const std::string aPmtECNumberInModuleAsString = boost::lexical_cast<std::string>( aPmtECNumberInModule );
            const std::string aPmtECNumberGlobalAsString   = boost::lexical_cast<std::string>(
                aPmtModuleNumberGlobal * m_RichMaxNumEcInModule + aPmtECNumberInModule );

            auto aCurrentECName = aPmtModuleIsGrand ? m_Rich2PmtGrandECName : m_Rich2PmtStdECName;

            const std::string aPmtECName = aCurrentECName[0] + aPmtECNumberInModuleAsString + aCurrentECName[1] +
                                           aPmtModuleNumberAsStringFull + ":" + aPmtECNumberGlobalAsString;

            // in grand modules the PMT copy numbers run as 0,1,2,3 - so the relation from std modules that EC# =
            // aPmtNumberInModule/4 is not valid
            const IPVolume* bpve = bpvc->lvolume()->pvolume( aPmtECName );

            if ( bpve ) {

              const Gaudi::Transform3D& bpveTrans    = bpve->matrix();
              const Gaudi::Transform3D& bpveTransInv = bpve->matrixInv();

              // in grand EC the PMT copy number is always 0
              const IPVolume* bpvf =
                  aPmtModuleIsGrand
                      ? bpve->lvolume()->pvolume( 0 )
                      : bpve->lvolume()->pvolume( aPmtNumberInModule - aPmtECNumberInModule * m_RichMaxNumPmtInEc );

              if ( bpvf ) {
                const Gaudi::Transform3D& bpvfTrans    = bpvf->matrix();
                const Gaudi::Transform3D& bpvfTransInv = bpvf->matrixInv();

                const IPVolume* bpvg = bpvf->lvolume()->pvolume( m_PmtSMasterIndex );
                if ( bpvg ) {
                  const Gaudi::Transform3D& bpvgTrans    = bpvg->matrix();
                  const Gaudi::Transform3D& bpvgTransInv = bpvg->matrixInv();

                  m_PmtGlobalToLocal =
                      bpvgTrans * bpvfTrans * bpveTrans * bpvcTrans * bpvbTrans * bpvaTrans * aRich2MasterTrans;
                  m_PmtLocalToGlobal = aRich2MasterTransInv * bpvaTransInv * bpvbTransInv * bpvcTransInv *
                                       bpveTransInv * bpvfTransInv * bpvgTransInv;

                  m_PmtPhDetPanelToLocal    = bpvgTrans * bpvfTrans * bpveTrans * bpvcTrans;
                  m_PmtLocalToPmtPhDetPanel = bpvcTransInv * bpveTransInv * bpvfTransInv * bpvgTransInv;

                  // Gaudi::XYZPoint testkall(0.0,0.0,0.0);
                  // RichG4ReconTransformPmtlog<<MSG::INFO<<"test Kall  "<<testkall<<"   "<<m_PmtGlobalToLocal
                  // *testkall<<endmsg;

                  //           Gaudi::XYZPoint testkallInv(0.0,0.0,0.0);
                  //  RichG4ReconTransformPmtlog<<MSG::INFO<<"test KallInv  "<<testkallInv<<"   "
                  //              <<  m_PmtLocalToGlobal  *testkallInv<<endmsg;
                }
              }
            }
          }
        }
      }
    }
  }
}

//=============================================================================
