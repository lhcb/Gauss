/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// Include files
#include "GaussRICH/RichInfo.h"
#include "GaussRICH/RichPhotInfo.h"
#include "GaussTools/GaussTrackInformation.h"
#include <bitset>

// local
#include "GaussCherenkov/CherenkovG4PmtReflTag.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CherenkovG4PmtReflTag
//
// 2011-04-19 : Sajan Easo
//-----------------------------------------------------------------------------

extern void RichG4PmtQWIncidentTag( const G4Track& aPhotonTk, const G4ThreeVector& aPmtQWPoint, int aQWLensFlag ) {

  G4VUserTrackInformation* aUserTrackInfo     = aPhotonTk.GetUserInformation();
  GaussTrackInformation*   aRichPhotTrackInfo = (GaussTrackInformation*)aUserTrackInfo;
  if ( aRichPhotTrackInfo ) {
    if ( aRichPhotTrackInfo->detInfo() ) {
      RichInfo* aRichTypeInfo = (RichInfo*)( aRichPhotTrackInfo->detInfo() );

      if ( aRichTypeInfo && aRichTypeInfo->HasUserPhotInfo() ) {
        RichPhotInfo* aRichPhotInfo = aRichTypeInfo->RichPhotInformation();
        if ( aRichPhotInfo ) {
          // now add the info
          if ( aRichPhotInfo->VerbosePhotTagFlag() ) {
            if ( aQWLensFlag == 0 ) {

              aRichPhotInfo->setHpdQWPhotIncidentPosition( aPmtQWPoint );
            } else {
              aRichPhotInfo->setPmtLensPhotIncidentPosition( aPmtQWPoint );
            }
          }
        }
      }
    }
  }
}
