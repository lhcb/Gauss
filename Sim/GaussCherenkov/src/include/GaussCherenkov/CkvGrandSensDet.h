/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GAUSSCHERENKOV_CKVGRANDSENSDET_H
#define GAUSSCHERENKOV_CKVGRANDSENSDET_H 1

// Include files
// from GiGa
#include "GiGa/GiGaSensDetBase.h"

// local
#include "CkvG4GeomProp.h"
#include "GaussCherenkov/CkvG4Hit.h"
#include "GaussCherenkov/CkvG4HitCollName.h"
#include <map>
// forward declarations
class G4HCofThisEvent;

/** @class CkvGrandSensDet CkvGrandSensDet.h GaussCherenkov/CkvGrandSensDet.h
 *
 *
 *  @author Sajan Easo
 *  @date   2014-08-18
 */
class CkvGrandSensDet : virtual public GiGaSensDetBase {

public:
  /// Standard constructor
  CkvGrandSensDet( const std::string& type, const std::string& name, const IInterface* parent );

  /// initialize
  StatusCode initialize() override;

  /// finalize
  StatusCode finalize() override;

  void Initialize( G4HCofThisEvent* HCE ) override;
  // virtual void EndOfEvent(G4HCofThisEvent* HCE);
  bool ProcessHits( G4Step* step, G4TouchableHistory* history ) override;
  void clear() override;
  void DrawAll() override;
  void PrintAll() override;

protected:
private:
  CkvGrandSensDet();                                    // no default constructor
  CkvGrandSensDet( const CkvGrandSensDet& );            ///< no copy constructor
  CkvGrandSensDet& operator=( const CkvGrandSensDet& ); ///< no =

  Gaudi::Property<bool> m_RichPmtAviodDuplicateHitsActivate{ this, "RichPmtAviodDuplicateHitsActivate", false,
                                                             "RichPmtAviodDuplicateHitsActivate" };
  Gaudi::Property<bool> m_RichPmtFlagDuplicateHitsActivate{ this, "RichPmtFlagDuplicateHitsactivate", false,
                                                            "RichPmtFlagDuplicateHitsactivate" };
  CkvG4HitCollName*     m_RichG4HCName{ nullptr };
  G4int                 m_NumberOfHCInRICH;
  std::vector<CkvG4HitsCollection*> m_RichHC;
  std::vector<G4int>                m_PhdHCID;
};
#endif // GAUSSCHERENKOV_CKVGRANDSENSDET_H
