/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussCherenkov_CkvG4SvcLocator_h
#define GaussCherenkov_CkvG4SvcLocator_h 1

/// STD & STL
#include <string>
/// GaudiKernel
class IDataProviderSvc;
class IMessageSvc;
class IHistogramSvc;
class INTupleSvc;

namespace CkvG4SvcLocator {

  static const std::string a_RichG4DataSvcName( "DetectorDataSvc" );
  /// name of Message  Service
  static const std::string a_RichG4MessageSvcName( "MessageSvc" );
  //
  static const std::string a_RichG4HistoSvcName( "HistogramDataSvc" );

  static const std::string a_RichG4NtupSvcName( "NTupleSvc" );

  IDataProviderSvc* RichG4detSvc();

  IMessageSvc* RichG4MsgSvc();

  IHistogramSvc* RichG4HistoSvc();

  INTupleSvc* RichG4NtupleSvc();
} // namespace CkvG4SvcLocator

#endif // GaussCherenkov_CkvG4SvcLocator
