/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GAUSSGEO_GAUSSGEOVOLUMEUTILS_H_
#define GAUSSGEO_GAUSSGEOVOLUMEUTILS_H_

// Standard
#include <string>

class G4LogicalVolume;
class G4Region;
class G4VPhysicalVolume;
class GaussGeoAssembly;

namespace GaussGeoVolumeUtils {
  G4LogicalVolume*   findLVolume( const std::string& name );
  G4VPhysicalVolume* findPVolume( const std::string& name );
  GaussGeoAssembly*  findLAssembly( const std::string& name );
  G4Region*          findRegion( const std::string& name );
} // namespace GaussGeoVolumeUtils

#endif // GAUSSGEO_GAUSSGEOVOLUMEUTILS_H_
