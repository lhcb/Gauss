/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GAUSSGEO_GAUSSGEOASSEMBLY_H_
#define GAUSSGEO_GAUSSGEOASSEMBLY_H_

// Standard
#include <string>
#include <vector>

// Gaudi
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/Transform3DTypes.h"

// Local
#include "GaussGeoVolumePair.h"

class GaussGeoAssembly {
public:
  typedef std::pair<G4LogicalVolume*, std::string>      NamedG4LVolume;
  typedef std::pair<NamedG4LVolume, Gaudi::Transform3D> Volume;
  typedef std::vector<Volume>                           Volumes;

  GaussGeoAssembly( const std::string& name = "" );
  GaussGeoAssembly( const GaussGeoAssembly& right );

  virtual ~GaussGeoAssembly();

  inline const Volumes& volumes() const { return m_volumes; }

  const std::string& name() const { return m_name; }

  inline void setName( const std::string& name = "" ) { m_name = name; }

  StatusCode addVolume( const GaussGeoVolumePair& volume_pair, const std::string& name = "" );

private:
  std::string m_name;
  Volumes     m_volumes;
};

#endif // GAUSSGEO_GAUSSGEOASSEMBLY_H_
