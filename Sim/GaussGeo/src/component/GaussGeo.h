/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Kernel includes
#include "Kernel/IPropertyConfigSvc.h"

// Gaudi includes
#include "G4VSensitiveDetector.hh"
#include "GaudiKernel/DataStoreItem.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IDataSelector.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GiGaMTFactories/GiGaFactoryBase.h"

#include "GiGaMTGeo/IGiGaMTGeoSvc.h"

#include "DetDesc/DetectorElement.h"

// Forward declaration
class GaussGeoVolume;
class GaussGeoAssembly;
class G4Material;
class G4VSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;

// Missing from the original
struct ISolid;
struct IPVolume;
class SolidBoolean;
class G4MaterialPropertiesTable;

// ============================================================================
// Interface file for class : GaussGeo
//
// Conversion service for convertion Gaudi detector and geometry description
// into GEANT4 format. Based on GiGaGeo convertion service.
//
// 2015-11-11 : Dmitry Popov
// 2019-5-16 : Dominik Muller
// ============================================================================

class GaussGeo : public Service, virtual public IGiGaMTGeoSvc, virtual public IIncidentListener {
public:
  GaussGeo( const std::string& service_name, ISvcLocator* service_locator );
  virtual ~GaussGeo() = default;

  // Service pure member functions
  StatusCode initialize() override;
  StatusCode finalize() override;

  StatusCode queryInterface( const InterfaceID& iid, void** ppi ) override;

  // IncidentListener interface
  void handle( const Incident& incident ) override;

  // Pointer to the root of G4 geometry tree
  G4VPhysicalVolume* constructWorld() override;
  void               constructSDandField() override;

private:
  GaussGeo();
  GaussGeo( const GaussGeo& );
  GaussGeo& operator=( const GaussGeo& );

  // Services variables
  IPropertyConfigSvc* m_property_config_svc;
  IIncidentSvc*       m_incident_svc;
  IToolSvc*           m_tool_svc;
  IDataProviderSvc*   m_detector_data_svc;

  // Geometry objects
  std::vector<std::string>   m_geo_items_names;
  std::vector<DataStoreItem> m_geo_items;
  IDataSelector              m_data_selector;

  // G4 geometry variables
  G4VPhysicalVolume* m_world_root;
  std::string        m_world_material;
  std::string        m_world_pv_name;
  std::string        m_world_lv_name;

  float m_world_volume_size_x;
  float m_world_volume_size_y;
  float m_world_volume_size_z;

  std::string m_mag_field_mgr;

  std::string m_budget;                // Special sensitive detector for estimation of material budget
  bool        m_performOverlapCheck;   // Given to G4VPlacements to control Overlap checking
  std::string m_assemblyNameSeparator; // Separator between assembly and placement name

  bool m_use_alignment; // Flag to switch on use of condDB info for children detector elements
  bool m_align_all;     // Flag to switch on for which detector to use condDB info for children detector elements
  std::vector<std::string> m_align_dets_names; // List of paths in TES to which to apply condDB info

  bool m_clear_stores;

  std::map<std::string, unsigned int> m_objects_counters; // Counters by object class ID
  std::map<std::string, unsigned int> m_figures_counters; // Counters by geometrical figure

  // Prefetch materials for external & parallel geometry
  Gaudi::Property<std::vector<std::string>> m_pre_materials{ this, "PrefetchMaterials", {} };

  // For verbose info printout beautification
  std::string m_str_prefix;

  // Services accessors
  inline ISvcLocator*                    svcLocator() const { return serviceLocator(); }
  inline IToolSvc*                       toolSvc() const { return m_tool_svc; }
  inline IDataProviderSvc*               detectorDataSvc() const { return m_detector_data_svc; }
  inline bool                            useAlignment() { return m_use_alignment; }
  inline bool                            alignAll() { return m_align_all; }
  inline const std::vector<std::string>& alignDetsNames() { return m_align_dets_names; }

  // Conversion of geo items
  StatusCode convertGeometry();
  StatusCode convertGeoObject( DataObject* object );
  StatusCode convertDetectorElementObject( DataObject* object );
  StatusCode convertLVolumeObject( DataObject* object );
  StatusCode convertLAssemblyObject( DataObject* object );
  StatusCode convertMixture( DataObject* object );
  StatusCode convertIsotope( DataObject* object );
  StatusCode convertSurface( DataObject* object );
  StatusCode convertElement( DataObject* object );
  StatusCode convertCatalog( DataObject* object );

  // Methods for returning G4 objects
  G4Material*      material( const std::string& material_name );
  GaussGeoVolume   volume( const std::string& volume_name );
  G4VSolid*        solid( const ISolid* isolid );
  G4VSolid*        solidBoolToG4Solid( const SolidBoolean* solid_bool );
  G4LogicalVolume* createG4LVolume( G4VSolid* g4_solid, G4Material* g4_material, const std::string& volume_name );

  // Misc utilities
  StatusCode  reportError( const std::string& message, const StatusCode& status = StatusCode::FAILURE ) const;
  void        printConfiguraion() const;
  inline void prefixIn() { m_str_prefix.resize( m_str_prefix.size() + 1, ' ' ); }
  inline void prefixOut() {
    if ( m_str_prefix.size() > 0 ) { m_str_prefix.resize( m_str_prefix.size() - 1 ); }
  }

  // Extract the object type name
  template <class T>
  inline const std::string objectTypeName( T object ) {
    return object ? std::string( System::typeinfoName( typeid( *object ) ) ) : std::string( "'UNKNOWN_type'" );
  }

  // Tabulated properties
  template <class T>
  StatusCode copyTableProperties( const T& src_table, G4MaterialPropertiesTable* dst_table );

  // Volumes installation
  StatusCode installVolume( const GaussGeoVolume& gg_volume, const std::string& name, const Gaudi::Transform3D& matrix,
                            G4LogicalVolume* mother_volume );
  StatusCode installVolume( const GaussGeoAssembly* gg_assembly, const std::string& name,
                            const Gaudi::Transform3D& matrix, G4LogicalVolume* mother_volume );
  StatusCode installVolume( G4LogicalVolume* g4_volume, const std::string& name, const Gaudi::Transform3D& matrix,
                            G4LogicalVolume* mother_volume );

  // Register that sensitive detector of given name is supposed to be
  // associated to given logical volume during worker thread initialisation
  std::map<GiGaFactoryBase<G4VSensitiveDetector>*, std::set<G4LogicalVolume*>> mmap_sensdet_to_lvols;
  std::map<std::string, GiGaFactoryBase<G4VSensitiveDetector>*>                mmap_name_to_sensdetfac;
  std::map<GiGaFactoryBase<G4FieldManager, bool>*, std::set<G4LogicalVolume*>> mmap_fieldmgr_to_lvols;
  std::map<std::string, GiGaFactoryBase<G4FieldManager, bool>*>                mmap_name_to_fieldmgrfac;
  StatusCode register_sensitive( const std::string& name, G4LogicalVolume* );
  // FIXME: FIELD STUFF
  StatusCode       register_mag_field( const std::string& name, G4LogicalVolume* );
  std::atomic_bool m_found_global_fieldmgr{ false };
  // StatusCode fieldMgr(const std::string& name, IGiGaFieldMgr*& fmanager);
  // StatusCode fieldMgr(const std::string& name, IGiGaFieldMgr*& fmanager);

  // Methods to simplify tools retrieval
  template <class T>
  T* getTool( const std::string& type, const std::string& name, T*& tool, const IInterface* parent = 0,
              bool create = true ) const;
  template <class T>
  T* getTool( const std::string& type, T*& tool, const IInterface* parent = 0, bool create = true ) const;

  // Methods to handle missalignment for the volumes conversion method
  StatusCode transformWithAlignment( const IPVolume* pvolume, Gaudi::Transform3D& result_matrix );
  int        detElementByLVNameWithAlignment( const DetDesc::IDetectorElementPlus*               det_element,
                                              const std::string&                                 lvolume_name,
                                              std::vector<const DetDesc::IDetectorElementPlus*>& det_elements );
  int findBestDetElemFromPVName( std::string pv_name, std::vector<const DetDesc::IDetectorElementPlus*> found_detelem,
                                 int& found_detelems_num );
  StatusCode detectorElementSupportPath( const DetDesc::IDetectorElementPlus* det_elem, std::string& path,
                                         const std::string& parent_path = "" );

  // FIXME: this is just a temporary change in GaussGeo to allow tests
  //        it should be implemented in Parallel Geometry
  using VisibleVolumes            = std::vector<std::string>;
  VisibleVolumes m_visibleVolumes = {};
};
