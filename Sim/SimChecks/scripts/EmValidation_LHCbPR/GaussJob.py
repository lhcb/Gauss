###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#################################################################################################
#
# Set options for gauss job. Options are set from config function.
#
#  Peter Griffith 21.11.14
#  email:peter.griffith@cern.ch
#
# Georgios Chatzikonstantinidis
# email.georgios.chatzikonstantinidis@cern.ch
#
#################################################################################################

import sys

sys.path.append("./")
from Configurables import CondDB, CondDBAccessSvc, DDDBConf, Gauss, LHCbApp
from configurations import config
from Gauss.Configuration import *

opts = config()
saveSim = opts["saveSim"]
nEvts = opts["nEvts"]
Gauss().Production = "PGUN"


GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 4585


LHCbApp().EvtMax = nEvts
Gauss().Production = "PGUN"
Gauss().OutputType = "NONE"


if saveSim:
    tape = OutputStream("GaussTape")
    tape.Output = "DATAFILE='PFN:myOutputFile.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"
    ApplicationMgr(OutStream=[tape])
histosName = "RootFileSimMonitor_{}_{}_{}_{}-histos.root".format(
    str(opts["emPL"]),
    str(float(opts["pgunID"])),
    str(opts["pgunE"]),
    str(opts["veloType"]),
)
HistogramPersistencySvc().OutputFile = histosName
ApplicationMgr().ExtSvc += ["NTupleSvc"]
NTupleSvc().Output = ["FILE1 DATAFILE='testout.root' TYP='ROOT' OPT='NEW'"]
importOptions("./Pgun.py")

from Configurables import EMGaussMoni

SimMonitor = GaudiSequencer("SimMonitor")
SimMonitor.Members += [EMGaussMoni("VeloGaussMoni")]
