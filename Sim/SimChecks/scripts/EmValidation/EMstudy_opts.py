###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#############################################
# 	 EM physics study and validation		#
#   with EMGaussMoni and BremVeloCheck		#
# 											#
# 	Detector and tool config configuration	#
# 	     Peter Griffith 21.11.14			#
#############################################

# imports and functions
####################################################################################

from __future__ import print_function

import sys

sys.path.append("./")
from Configurables import CondDB, CondDBAccessSvc, Gauss, LHCbApp
from EMstudy_config import config
from Gaudi.Configuration import *
from Gauss.Configuration import *

importOptions("./EMstudy_config.py")
# emPL = config()
# veloType = config()
# testType = config()
# dRays = config()
opts = config()
veloType = opts["veloType"]
testType = opts["testType"]
dRays = opts["dRays"]
emPL = opts["emPL"]


# Get physics list from temp. file as this depends on whether PL scan is activated or not
print("EM physics list set in config: " + str(emPL))


def switchOffRICHCuts():
    from Configurables import SimulationSvc

    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/SimulationRICHesOff.xml"


def addTool():
    from Configurables import BremVeloCheck, GiGa

    giga = GiGa()
    giga.TrackSeq.Members.append("BremVeloCheck")


####################################################################################

if veloType == "velo":
    LHCbApp().DDDBtag = "dddb-20130312-1"
    LHCbApp().CondDBtag = "sim-20130222-1-vc-md100"

    Gauss.DetectorGeo = {"Detectors": ["PuVeto", "Velo", "Rich1", "Rich2"]}
    Gauss.DetectorSim = {"Detectors": ["PuVeto", "Velo", "Rich1", "Rich2"]}
    Gauss.DetectorMoni = {"Detectors": ["PuVeto", "Velo", "Rich1", "Rich2"]}
    importOptions("$GAUSSOPTS/RICHesOff.py")

    appendPostConfigAction(switchOffRICHCuts)


Gauss.PhysicsList = {
    "Em": emPL,
    "Hadron": "QGSP_BERT",
    "GeneralPhys": True,
    "LHCbPhys": False,
}

from Configurables import SimulationSvc

Gauss.DeltaRays = dRays


if testType == "brem" or testType == "both":
    appendPostConfigAction(addTool)
