/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GIGA_GDMLWRITERUNACTION_H
#define GIGA_GDMLWRITERUNACTION_H 1

// GiGa
#include "GiGa/GiGaRunActionBase.h"

class GDMLRunAction : public virtual GiGaRunActionBase {

public:
  /// Constructor
  GDMLRunAction( const std::string& type, const std::string& name, const IInterface* parent );
  /// Destructor
  virtual ~GDMLRunAction() {}

  /// Action to be performed at the begin of each run
  void BeginOfRunAction( const G4Run* run ) override;

private:
  /// No default constructor
  GDMLRunAction();
  /// No copy constructor
  GDMLRunAction( const GDMLRunAction& );
  /// No assignement
  GDMLRunAction& operator=( const GDMLRunAction& );

  // parallel world name (empty for mass geometry)
  Gaudi::Property<std::string> m_parallelWorld{ this, "ParallelWorldName", "",
                                                "Name of the parallel world, empty for mass geometry" };

  bool m_done;
  /// Schema to be used for GDML
  std::string m_schema;
  /// Name of the file to write geometry
  std::string m_outfile;
  /// Add pointer values to make names unique
  bool m_refs;

  // export auxilliary information
  Gaudi::Property<bool> m_exportSD{ this, "ExportSD", false };
  Gaudi::Property<bool> m_exportEnergyCuts{ this, "ExportEnergyCuts", false };
  Gaudi::Property<bool> m_exportRegion{ this, "ExportRegion", false };
};
#endif
