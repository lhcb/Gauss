/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Event/Event
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "ConversionFilter.h"
#include "GiGaMTCoreRun/SimResults.h"
#include "GiGaMTCoreRun/SimResultsProxyAttribute.h"
#include "Kernel/IParticlePropertySvc.h"
#include "NewRnd/RndGlobal.h"

#include "Defaults/HepMCAttributes.h"
#include "HepMCUser/VertexAttribute.h"
#include "HepMCUtils/PrintDecayTree.h"
#include "MCTruthToEDM/VertexType.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ConversionFilter
//
// 2017-10-26 : Michel De Cian
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ConversionCutTool )

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode ConversionCutTool::initialize() {

  StatusCode sc = extends::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;       // error printed already by GaudiAlgorithm

  if ( m_mother == "" ) {
    m_motherID       = -1;
    m_maxSearchDepth = 1;
    return StatusCode::SUCCESS;
  }

  sc &= m_ppSvc.retrieve();
  if ( !m_ppSvc ) {
    error() << "Could not retrieve ParticlePropertySvc" << endmsg;
    m_motherID       = -1;
    m_maxSearchDepth = 1;
    return StatusCode::SUCCESS;
  }

  const LHCb::ParticleProperty* prop = m_ppSvc->find( m_mother );
  if ( !prop ) {
    error() << "Could not retrieve ParticleProperty" << endmsg;
    m_motherID       = -1;
    m_maxSearchDepth = 1;
    return StatusCode::SUCCESS;
  }

  // -- Get the ID of the mother (particle and antiparticle)
  m_motherID = prop->pdgID().abspid();

  info() << "Filtering on gamma conversion with " << m_mother << " in the decay chain" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
bool ConversionCutTool::studyFullEvent( const HepMC3::GenEventPtrs& theEvents, const LHCb::GenCollisions& ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Calling tool" << endmsg;

  // First loop over events and find the signal event
  HepMC3::GenVertexPtr signal_vertex{ nullptr };
  for ( auto& evt : theEvents ) {

    auto sig_attr = evt->attribute<HepMC3::VertexAttribute>( Gaussino::HepMC::Attributes::SignalProcessVertex );
    if ( sig_attr.get() && sig_attr->value().get() ) { signal_vertex = sig_attr->value(); }
  }
  if ( !signal_vertex ) {
    warning() << "No signal vertex found, returning FALSE" << endmsg;
    return false;
  }

  // Simulating the signal decay, first make sure that the signal vertex has only one outgoing particle
  if ( signal_vertex->particles_in().size() != 1 ) {
    warning() << "Signal vertex does not have exactly one child, returning FALSE" << endmsg;
    return false;
  }
  auto sig_part = *std::begin( signal_vertex->particles_in() );
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Simulating Decay:" << endmsg;
    debug() << PrintDecay( sig_part, 0, m_ppSvc.get() ) << endmsg;
  }
  Gaussino::GiGaSimReturn simresult = m_gigaSvc->simulateDecay( sig_part, ThreadLocalEngine::GetPtr() );
  // Now, we can search for the presence of the whatever we need in the simulated result.
  auto& [g4event, mctruth] = simresult;
  if ( msgLevel( MSG::DEBUG ) ) { mctruth->DumpToStream( debug() ); }

  const std::string filterString = "event with gamma conversion from " + m_mother;

  bool filter = false;
  int  nConv  = 0;
  // -- Loop over all MCParticles
  // -- First check that they are gammas
  // -- do pair production at their endvertex and have the right z-position.
  for ( auto linkedPart : mctruth->GetParticles() ) {
    if ( linkedPart->GetPDG() != 22 ) continue;
    bool goodPhoton = false;
    for ( auto& endVtx : linkedPart->GetEndVtxs() ) {
      if ( !endVtx ) continue;
      msgStream();
      auto vertex_type = Gaussino::GetLinkedVertexType( endVtx.get(), &msgStream() );
      if ( vertex_type == LHCb::MCVertex::PairProduction && std::abs( endVtx->GetPosition().z() ) < m_maxZ ) {
        goodPhoton = true;
        break;
      }
    }
    if ( !goodPhoton ) continue;
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Found a good photon with PairProduction endvertex" << endmsg; }
    nConv++;

    unsigned int searchDepth = 0;
    auto         getmother   = [&]( LinkedParticle* part ) -> LinkedParticle* {
      auto parents = part->GetParents();
      if ( parents.size() > 0 ) {
        return *std::begin( parents );
      } else {
        return nullptr;
      }
    };
    auto momPart = getmother( linkedPart );

    // -- Search up the decay tree to see if this gamma is coming from the mother we care for
    // -- If we don't care about the mother, we just check if it fullfills the p,pt and theta requirements
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Mother particle: " << momPart << ( momPart ? momPart->GetPDG() : -1 ) << endmsg;
    }
    while ( momPart && searchDepth < m_maxSearchDepth ) {
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Search Depth: " << searchDepth << " < " << m_maxSearchDepth << endmsg;
        debug() << "Mother particle: " << momPart << ( momPart ? momPart->GetPDG() : -1 ) << endmsg;
      }

      // -- If we only want to search for a given depth, and not for a maximum
      // -- loop until we have reach that depth.
      if ( m_matchSearchDepth && searchDepth < m_maxSearchDepth - 1 ) {
        searchDepth++;
        momPart = getmother( momPart );
        continue;
      }

      // FIXME: Does not support nested MCTruth structures yet but probably not needed

      if ( ( std::abs( momPart->GetPDG() ) == m_motherID ) || ( m_motherID == -1 ) ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Found gamma conversion from mother: " << m_motherID << endmsg;
        // -- we found the right mother
        // -- let's see if the daughters (=electrons) have enough momentum, pt and are within the acceptance.
        for ( auto& endVtx : linkedPart->GetEndVtxs() ) {
          if ( !endVtx ) continue;
          if ( endVtx->outgoing_particles.size() != 2 ) continue; // want pair production

          // -- We ask that all of the daughters have enough p and pt, and are within the LHCb acceptance. In case there
          // is more than 1 end vertex
          // -- we take the "or" of them.
          filter |= std::all_of( std::begin( endVtx->outgoing_particles ), std::end( endVtx->outgoing_particles ),
                                 [&]( LinkedParticle* dau ) {
                                   return ( dau->GetMomentum().p3mod() > m_minP ) &&
                                          ( dau->GetMomentum().pt() > m_minPT ) &&
                                          ( dau->GetMomentum().theta() > m_minTheta ) &&
                                          ( dau->GetMomentum().theta() < m_maxTheta );
                                 } );

          // --
          if ( msgLevel( MSG::DEBUG ) ) {
            for ( auto dau : endVtx->outgoing_particles ) {
              debug() << "particle: " << dau->GetPDG() << " p: " << dau->GetMomentum().p3mod()
                      << " pt: " << dau->GetMomentum().pt() << " theta: " << dau->GetMomentum().theta() << endmsg;
            }
            if ( filter ) {
              debug() << "accepted" << endmsg;
            } else {
              debug() << "rejected" << endmsg;
            }
          }
          // --
        }
        // -- We have found the right mother, no need to loop deeper
        break;
      }
      searchDepth++;
      momPart = getmother( momPart );
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Found " << nConv << " gamma conversions in total" << endmsg;

  // Lastly, if the filter has been successful, we attach the simulation result to the
  // particle via an hepmc attribute. If not, the results will automatically be
  // destroyed at the end of this function.
  if ( filter ) {
    counter( filterString )++;
    sig_part->add_attribute( Gaussino::HepMC::Attributes::SimResults,
                             std::make_shared<HepMC3::SimResultsAttribute>( simresult ) );
  }

  return filter;
}
