/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiTool.h"
#include "GenInterfaces/IFullGenEventCutTool.h"

#include "GiGaMT/IGiGaMTSvc.h"

class IHepMC3ToMCTruthConverter;
namespace LHCb {
  class IParticlePropertySvc;
}
/** @class ConversionFilter ConversionFilter.h
 *  Filter on gamma conversions. Applied as a cut tool in the generation, only the signal is simulated first and the
 * presence of gamma conversion is checked. If not present in the simulated output, the event is rejected.
 *
 *  Parameters:
 *  - Mother: Mother up the decay tree the gamma conversion has to originate from. Use DecayDescriptor symbols, like B+,
 * D*(2007)0, etc. An empty string accepts all mothers.
 *  - MaxSearchDepth: Maximum "distance" in decay tree between gamma and mother.
 *  - MatchSearchDepth: Does the search depth need to match, or is it only a maximum?
 *  - MaxZ: Maximum z value the gamma conversion has to occur.
 *  - MinP: Minimum momentum of the products of the gamma conversion.
 *  - MinPT: Minimum transverse momentum of the products of the gamma conversion.
 *  - MinTheta: Minimum value of angle theta for conversion products
 *  - MaxTheta: Maximum value of angle theta for conversion products
 *
 *  @author Michel De Cian, adapted to SimCutTool by Dominik Muller
 *  @date   2017-10-26
 */

class ConversionCutTool : public extends<GaudiTool, IFullGenEventCutTool> {

public:
  /// Standard constructor
  using extends::extends;
  bool studyFullEvent( const HepMC3::GenEventPtrs& theEvents, const LHCb::GenCollisions& theCollisions ) const override;
  StatusCode initialize() override;

private:
  Gaudi::Property<std::string>  m_mother{ this, "Mother", "" };
  Gaudi::Property<unsigned int> m_maxSearchDepth{ this, "MaxSearchDepth", 20 };
  Gaudi::Property<bool>         m_matchSearchDepth{ this, "MatchSearchDepth", false };
  Gaudi::Property<double>       m_maxZ{ this, "MaxZ", 500.0 };
  Gaudi::Property<double>       m_minP{ this, "MinP", 1500.0 };
  Gaudi::Property<double>       m_minPT{ this, "MinPT", 50.0 };
  Gaudi::Property<double>       m_minTheta{ this, "MinTheta", 0.010 };
  Gaudi::Property<double>       m_maxTheta{ this, "MaxTheta", 0.400 };

  int m_motherID = -1;

  ServiceHandle<IGiGaMTSvc>                 m_gigaSvc{ this, "GiGaMTSvc", "GiGaMT" };
  ServiceHandle<LHCb::IParticlePropertySvc> m_ppSvc{ this, "PropertyService", "LHCb::ParticlePropertySvc" };
};
