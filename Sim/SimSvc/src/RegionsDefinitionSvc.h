/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include files
#include "GaudiKernel/Service.h"

#include "GiGaMTRegions/IRegionsDefinitionSvc.h"

#include <map>

/** @class RegionsDefinitionSvc RegionsDefinitionSvc.h SimDesc/RegionsDefinitionSvc.h
 *
 *  this interface defines a RegionsDefinition service that is able to answer
 *  many question concerning the simulation of the detector. It
 *  is able to associate simulation attributes to logical volumes.
 *  These describe the way these volumes should be simulated
 *
 * @author Sebastien Ponce
 */
class RegionsDefinitionSvc : public Service, virtual public IRegionsDefinitionSvc {
public:
  /**
   * Standard Constructor
   * @param name   String with service name
   * @param svc    Pointer to service locator interface
   */
  RegionsDefinitionSvc( const std::string& name, ISvcLocator* svc );

  /**
   * default destructor
   */
  virtual ~RegionsDefinitionSvc();

  //  inline void operator delete (void* p) throw()
  // { operator delete( const_cast<void*> (p)) ; }

  /**
   * Initializes the service
   * @return status depending on the completion of the call
   */
  StatusCode initialize() override;

  /**
   * Queries interfaces of Interface.
   * @param riid ID of Interface to be retrieved
   * @param ppvInterface Pointer to Location for interface pointer
   * @return status depending on the completion of the call
   */
  StatusCode queryInterface( const InterfaceID& riid, void** ppvInterface ) override;

  ///////////////////////////////////////////////////////
  // implementation of the IRegionsDefinitionSvc interface //
  ///////////////////////////////////////////////////////

  /**
   * This method returns the simulation attribute associated to a given
   * logical volume
   * @param vol the logical volume
   * @return the simulation attribute that should be used to simulate
   * this logical volume
   */
  const PartAttr* simAttribute( const std::string volname ) const override;

  /**
   * This method tells whether a simulation attribute is associated to a given
   * logical volume or not
   * @param vol the logical volume
   */
  bool hasSimAttribute( const std::string volname ) const override;

  /**
   * This method returns the pointer to the vector of region definitions.
   * @param
   * @return std::vector<RegionCuts>
   */
  const std::vector<RegionCuts>* regionsDefs() const override;

  /**
   * this method erases the current set of attributes and loads a new set
   */
  void reload() override;

  /**
   * this method erases the current set of attributes
   */
  virtual void clear();

private:
  /**
   * This defines a set of attributes. Each one is associated to a name
   */
  typedef std::map<std::string, const SimAttribute*> AttributeSet;

  /**
   * This defines a dictionnary
   */

  typedef std::map<std::string, PartAttr*> Dict;

  /// a map of attributes to be used
  AttributeSet m_attributeSet;

  /// a dictionnary linking logical volumes and SimAttributes
  Dict m_logvol2Sim;

  /// vector of regions definitions
  VectOfRegCuts m_regionsDefs;

  /// definition of a struct used for temporary purposes
  struct Prcuts {
    double gammacut;
    double electroncut;
    double positroncut;
    double protoncut;
    double aprotoncut;
    double neutroncut;
    double aneutroncut;
  };

  /// The location of the Xml file containing all definitions
  std::string m_simDbLocation;
};
