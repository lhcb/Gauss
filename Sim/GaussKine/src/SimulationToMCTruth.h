/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SimulationToMCTruth.h,v 1.5 2009-03-26 21:42:04 robbep Exp $
#ifndef SIMULATIONTOMCTRUTH_H
#define SIMULATIONTOMCTRUTH_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// from Event
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

// from LHCb
#include "LHCbMath/LHCbMath.h"

// Forward declarations
// from GiGa
#include "GiGa/IGiGaSvc.h"
// from GiGaCnv
#include "GiGaCnv/GiGaKineRefTable.h"
#include "GiGaCnv/IGiGaCnvSvcLocation.h"
#include "GiGaCnv/IGiGaKineCnvSvc.h"

// class IGiGaKineCnvSvc ;

class IFlagSignalChain;
namespace HepMC {
  class GenParticle;
  class GenVertex;
} // namespace HepMC
namespace LHCb {
  class IParticlePropertySvc;
  class MCHeader;
} // namespace LHCb

/** @class SimulationToMCTruth SimulationToMCTruth.h
 *
 *  Algorithm to retrieve HepMC as filled during G4 tracking and fill
 *  corresponding LHCb MCParticles/MCVertices, rebuilding links left
 *  over from GenerationToSimulation
 *
 *  @author  Witold Pokorski, adapted by G.Corti and P. Robbe
 *  @date    06/10/2008
 */
class SimulationToMCTruth : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// Delete a complete tree from event record
  void deleteParticle( LHCb::MCParticle* P );

  /// Convert a HepMC particle from Geant4 to a MC Particle
  void convert( const HepMC::GenParticle* genpart, LHCb::MCVertex* prodvertex, bool isPrimary );

  /// Check if a vertex is at the Geant4 volume boundary
  bool isEndOfWorldVertex( HepMC::GenVertex* ev );

  /// Determines the MCVertex event type
  LHCb::MCVertex::MCVertexType vertexType( int id );

  /// Find the primary vertex
  LHCb::MCVertex* findPrimaryVertex( const Gaudi::XYZPoint& vertex ) const;

  // Data members
  Gaudi::Property<std::string> m_gigaSvcName{ this, "GiGaService", "GiGa",
                                              "Name of GiGa Service" }; ///< Name of GiGa Service
  Gaudi::Property<std::string> m_kineSvcName{ this, "KineCnvService", IGiGaCnvSvcLocation::Kine,
                                              "Name of GiGaCnv Service" }; ///< Name of GiGaCnv Service
  Gaudi::Property<bool>        m_checkUnknown{ this, "CheckForUnknownParticle", false,
                                        "Check for unknown PDG Ids" }; ///< Check for unknown PDG Ids
  IGiGaSvc*                    m_gigaSvc{ nullptr };                          ///< Pointer to GiGa Service
  IGiGaKineCnvSvc*             m_gigaKineCnvSvc{ nullptr };                   ///< Pointer to GiGaKine Service

  Gaudi::Property<std::string> m_particles{ this, "Particles", LHCb::MCParticleLocation::Default,
                                            "Name of MCParticles location" }; ///< Name of MCParticles location
  Gaudi::Property<std::string> m_vertices{ this, "Vertices", LHCb::MCVertexLocation::Default,
                                           "Name of MCVertices location" }; ///< Name of MCVertices location
  Gaudi::Property<std::string> m_mcHeaderLocation{ this, "MCHeader", LHCb::MCHeaderLocation::Default,
                                                   "Name of MCHeader Location" }; ///< Name of MCHeader Location

  LHCb::MCParticles* m_particleContainer{ nullptr }; ///< MCParticles container
  LHCb::MCVertices*  m_vertexContainer{ nullptr };   ///< MCVertices container
  LHCb::MCHeader*    m_mcHeader{ nullptr };          ///< MCHeader container

  /// Reference to the particle property service
  LHCb::IParticlePropertySvc* m_ppSvc{ nullptr };

  /// Reference to tool to propagate fromSignal flag
  IFlagSignalChain* m_setSignalFlagTool{ nullptr };

  /// Set to hold keys of treated MCParticles when rebuilding decay tree
  std::set<LHCb::MCParticle::key_type> m_treatedParticles;

  /// PDG Id of the intermediate particles
  int m_intermediatePDG{ 0 };
};

#endif // SIMULATIONTOMCTRUTH_H
