/***********************************************************************************\
* (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#ifndef GAUSSKINE_MAKEG4UNKNOWNION_H
#define GAUSSKINE_MAKEG4UNKNOWNION_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from Kernel
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"

// local
#include "IMakeG4UnknownTool.h"

/** @class MakeG4UnknownIon MakeG4UnknownIon.h
 *
 *  Tool to add an ion, unknown to Geant4,
 *  to the G4ParticleTable using the info
 *  retrieved from the G4IonTable
 *
 *  @author M. Veltri
 */
class MakeG4UnknownIon : public extends<GaudiTool, IMakeG4UnknownTool> {

public:
  // Standard Constructor
  using extends::extends;

  // MakeG4UnknownIon interface
  void AddIonToG4Table( int ) const override;
};
#endif // GAUSSKINE_MAKEG4UNKNOWNION_H
