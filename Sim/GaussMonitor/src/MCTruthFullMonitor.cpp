/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MCTruthFullMonitor.cpp,v 1.4 2008-05-07 09:54:20 gcorti Exp $
// Include files

// from Gaudi

// from Event/Event
#include "Event/MCParticle.h"

// local
#include "MCTruthFullMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCTruthFullMonitor
//
// 2004-01-27 : Gloria CORTI
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCTruthFullMonitor )

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode MCTruthFullMonitor::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm

  debug() << "==> Initialize: Booking Ntuple" << endmsg;

  std::string   ntname = "/NTUPLES/FILE1";
  NTupleFilePtr ntfile( ntupleSvc(), ntname );
  if ( ntfile ) {
    StatusCode status = StatusCode::SUCCESS;
    ntname += "/" + ( this->name() ) + "/100";
    NTuplePtr nt1( ntupleSvc(), ntname );
    if ( !nt1 ) {
      nt1 = ntupleSvc()->book( ntname, CLID_ColumnWiseTuple, "Truth" );
      if ( nt1 ) {
        if ( status.isSuccess() )
          nt1->addItem( "PType", m_pType ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "PXOrigin", m_pxOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "PYOrigin", m_pyOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "PZOrigin", m_pzOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "VTypeOrigin", m_typeOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "XOrigin", m_xOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "YOrigin", m_yOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "ZOrigin", m_zOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "TOrigin", m_tOvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "ParentType", m_parent ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "NDaughVtx", m_daughVtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( status.isSuccess() )
          nt1->addItem( "NDaughPart", m_daughPart ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( !status.isSuccess() ) {
          error() << "Failure booking ntuples" << endmsg;
          return StatusCode::FAILURE;
        }
        m_ntuple = nt1;
      } else {
        error() << "Ntuple already exist" << endmsg;
        return StatusCode::FAILURE;
      }
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MCTruthFullMonitor::execute() {

  debug() << "==> Execute" << endmsg;

  LHCb::MCParticles* parts = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );

  for ( LHCb::MCParticles::iterator aPart = parts->begin(); parts->end() != aPart; ++aPart ) {
    m_pType    = ( *aPart )->particleID().pid();
    m_pxOvtx   = ( *aPart )->momentum().x();
    m_pyOvtx   = ( *aPart )->momentum().y();
    m_pzOvtx   = ( *aPart )->momentum().z();
    m_typeOvtx = -1;
    m_xOvtx    = -9999.0;
    m_yOvtx    = -9999.0;
    m_zOvtx    = -9999.0;
    m_tOvtx    = -9999.0;
    if ( ( *aPart )->originVertex() ) {
      m_typeOvtx = ( *aPart )->originVertex()->type();
      m_xOvtx    = ( *aPart )->originVertex()->position().x();
      m_yOvtx    = ( *aPart )->originVertex()->position().y();
      m_zOvtx    = ( *aPart )->originVertex()->position().z();
      m_tOvtx    = ( *aPart )->originVertex()->time();
    }
    m_parent = -1000000;
    if ( ( *aPart )->mother() ) { m_parent = ( ( *aPart )->mother() )->particleID().pid(); }
    m_daughVtx  = ( ( *aPart )->endVertices() ).size();
    int nTotDau = 0;
    for ( SmartRefVector<LHCb::MCVertex>::const_iterator aVert = ( ( *aPart )->endVertices() ).begin();
          ( ( *aPart )->endVertices() ).end() != aVert; ++aVert ) {
      nTotDau += ( ( *aVert )->products() ).size();
    }
    m_daughPart = nTotDau;
    m_ntuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MCTruthFullMonitor::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize(); // must be called after all other actions
}

//=============================================================================
