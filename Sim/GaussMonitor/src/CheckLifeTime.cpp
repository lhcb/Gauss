/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CheckLifeTime.cpp,v 1.2 2008-12-08 10:11:22 ibelyaev Exp $
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
// ============================================================================
// Kernel/PartProp
// ============================================================================
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/ParticleProperties.h"
// ============================================================================
// local
// ============================================================================
#include "CheckLifeTime.h"
// ============================================================================
/** @file
 *  Implementation file for class GaussMonitor::CheckLifeTime
 *  @date 2008-06-23
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
// ============================================================================
/*  The standard constructor
 *  @param name the algorithm instance name
 *  @param pSvc service locator
 */
// ============================================================================
GaussMonitor::CheckLifeTime::CheckLifeTime( const std::string& name, // algorithm instance name
                                            ISvcLocator*       pSvc )      // service locator
    : GaudiHistoAlg( name, pSvc ) {
  setProperty( "HistoPrint", true ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}
// ============================================================================
// standard initialization of the algorithm
// ============================================================================
StatusCode GaussMonitor::CheckLifeTime::initialize() {
  // initialize the base class
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) { return sc; }
  // load LoKi service
  svc<IService>( "LoKiSvc", true );
  // check particle lists
  if ( m_particles.value().empty() ) { Warning( "No particles are specified" ).ignore(); }
  // create the slots
  for ( PIDs::const_iterator ipid = m_particles.value().begin(); m_particles.value().end() != ipid; ++ipid ) {
    const std::string title = cntName( *ipid );
    counter( title );
    //
    // get the particle property
    const LHCb::ParticleProperty* pp = LoKi::Particles::ppFromName( *ipid );
    if ( 0 == pp ) { return Error( "Invalid particle: " + ( *ipid ) ).ignore(); }
    book( title, 0, highEdge( *pp ), 100 );
  }
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// standard finalization   of  the algorithm
// ============================================================================
StatusCode GaussMonitor::CheckLifeTime::finalize() {
  check( true );
  // finalize the base
  return GaudiHistoAlg::finalize();
}
// ============================================================================
// check the lifetimes
// ============================================================================
void GaussMonitor::CheckLifeTime::check( const bool iErr ) const {
  //
  for ( PIDs::const_iterator ip = particles().begin(); particles().end() != ip; ++ip ) {
    // get the cunter
    const StatEntity& cnt = counter( cntName( *ip ) );
    // get the particle property
    const LHCb::ParticleProperty* pp = LoKi::Particles::ppFromName( *ip );
    if ( 0 == pp ) {
      Error( "Invalid particle: " + ( *ip ) ).ignore();
      continue;
    }
    if ( 0 == cnt.nEntries() ) {
      Warning( "No '" + ( *ip ) + "' Particles are found!" ).ignore();
      continue;
    }
    // get the mean lifetime (in mm)
    const double lTime = cnt.flagMean();
    // get the error in ctau  (in mm)
    const double ltErr = cnt.flagMeanErr();
    // get the nominal  ctau  (in mm)
    double nominal = pp->ctau() / Gaudi::Units::mm;
    if ( nominal < 1.e-5 * Gaudi::Units::micrometer ) { nominal = 0; }
    //
    const double diff = ( lTime - nominal );
    if ( std::fabs( diff ) > 3 * ltErr ) {
      fatal() << " >3   sigma deviation in lifetime for '" << ( *ip ) << "' \tActual is " << lTime << "+-" << ltErr
              << "(" << cnt.flagRMS() << ")"
              << "\tNominal is " << nominal << " [mm] " << endmsg;
      if ( iErr ) { Error( ">3 sigma deviation for '" + ( *ip ) + "'" ).ignore(); }
    } else if ( std::fabs( diff ) > 2 * ltErr ) {
      error() << " >2   sigma deviation in lifetime for '" << ( *ip ) << "' \tActual is " << lTime << "+-" << ltErr
              << "(" << cnt.flagRMS() << ")"
              << "\tNominal is " << nominal << " [mm] " << endmsg;
      if ( iErr ) { Error( ">2 sigma deviation for '" + ( *ip ) + "'" ).ignore(); }
    } else if ( std::fabs( diff ) > 1 * ltErr ) {
      warning() << " >1   sigma deviation in lifetime for '" << ( *ip ) << "' \tActual is " << lTime << "+-" << ltErr
                << "(" << cnt.flagRMS() << ")"
                << "\tNominal is " << nominal << " [mm] " << endmsg;
      if ( iErr ) { Warning( ">1 sigma deviation for '" + ( *ip ) + "'" ).ignore(); }
    } else if ( std::fabs( diff ) > 0.5 * ltErr ) {
      info() << " >0.5 sigma deviation in lifetime for '" << ( *ip ) << "' \tActual is " << lTime << "+-" << ltErr
             << "(" << cnt.flagRMS() << ")"
             << "\tNominal is " << nominal << " [mm] " << endmsg;
    } else {
      info() << " <0.5 sigma deviation in lifetime for '" << ( *ip ) << "' \tActual is " << lTime << "+-" << ltErr
             << "(" << cnt.flagRMS() << ")"
             << "\tNominal is " << nominal << " [mm] " << endmsg;
    }
  }
}
// ============================================================================
// The END
// ============================================================================
