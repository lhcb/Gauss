/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: EMGaussMoni.h,v 1.5 2009-03-26 22:02:12 robbep Exp $
#ifndef EMGAUSSMONI_H
#define EMGAUSSMONI_H 1

// Include files
// from Gaudi
#include "Event/MCHit.h"
#include "GaudiAlg/GaudiTupleAlg.h"

#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/SmartIF.h"
// velo
#include "VeloDet/DeVelo.h"

/** @class EMGaussMoni EMGaussMoni.h
 * 	modified VeloGaussMoni renamed to avoid conflicts
 *	Peter Griffith & Nigel Watson
 *
 *  @author Tomasz Szumlak Chris Parkes
 *  *  @date   2005-12-13
 *
 */

// class DeVelo;
class DeVeloRType;
class DeVeloPhiType;
class IRndmGen;
class IRndmGenSvc;

class EMGaussMoni : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  StatusCode checkTests();
  StatusCode getData();
  StatusCode veloMCHitMonitor();
  StatusCode veloPileUpMCHitMonitor();
  StatusCode basicMonitor();

private:
  Gaudi::Property<std::string> m_veloDetLocation{ this, "VeloDetLocation", DeVeloLocation::Default, "VeloDetLocation" };
  DeVelo*                      m_veloDet{ nullptr };
  LHCb::MCHits*                m_veloMCHits{ nullptr };
  LHCb::MCHits*                m_veloPileUpMCHits{ nullptr };
  Gaudi::Property<int>         m_print{ this, "Print", 0, "Print (unused)" };
  Gaudi::Property<bool>        m_printInfo{ this, "PrintInfo", false, "Print Info" };
  Gaudi::Property<bool>        m_detailedMonitor{ this, "DetailedMonitor", true, "Use Detailed Monitor" };
  bool                         m_bremmMonitor;
  Gaudi::Property<bool>        m_testMCHit{ this, "TestMCHit", true, "Test MCHit" };
  Gaudi::Property<bool>        m_testPileUpMCHit{ this, "TestPileUpMCHit", true, "Test PileUp MCHit" };
  double                       m_nMCH{ 0. };
  double                       m_nMCH2{ 0. };
  double                       m_nPUMCH{ 0. };
  double                       m_nPUMCH2{ 0. };
  int                          m_nEvent{ 0 };
  /// Location of Velo MCHits
  std::string m_veloMCHitsLocation;
  /// Location of PuVeto MCHits
  std::string m_puVetoMCHitsLocation;
  //
  SmartIF<IRndmGen> m_gaussGen;
};
#endif // EMGAUSSMONI_H
