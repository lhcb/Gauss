/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DecayAnalysis.cpp,v 1.8 2010-02-24 19:02:33 robbep Exp $
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// From HepMC
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"

// From LHCb
#include "Kernel/ParticleID.h"

// local
#include "DecayAnalysis.h"
#include "GaussGenUtil.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DecayAnalysis
//
// 2020-05-04 : M. Kreps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DecayAnalysis )

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode DecayAnalysis::initialize() {

  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;             // error printed already by GaudiHistoAlg

  debug() << "==> Initialize" << endmsg;

  if ( m_generatorName.value().empty() ) {
    info() << "Monitor will be applied to all events in container " << m_dataPath.value() << endmsg;
  } else {
    info() << "Monitor will be applied to events produced with generator " << m_generatorName.value()
           << " in container " << m_dataPath.value() << endmsg;
  }

  if ( produceHistos() ) { bookHistos( m_neutralHistos.value() ); }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DecayAnalysis::execute() {

  debug() << "==> Execute" << endmsg;

  // Initialize counters
  int nParticles( 0 ), nStable( 0 ), nProtoStable( 0 );
  int nChargedStable( 0 ), nNeutralStable( 0 );

  // Initalize particle variables
  double pmax_fr( 0. ), emax_fr( 0. ), ptmax_fr( 0. );
  double pmax_neut( 0. ), emax_neut( 0. ), ptmax_neut( 0. );

  // Retrieve data from selected path
  SmartDataPtr<LHCb::HepMCEvents> hepMCptr( eventSvc(), m_dataPath.value() );

  if ( 0 == hepMCptr ) {
    info() << "No HepMCEvents at location " << m_dataPath.value() << endmsg;
  } else {
    LHCb::HepMCEvents::iterator it;
    for ( it = hepMCptr->begin(); it != hepMCptr->end(); ++it ) {

      // Check if monitor has to be applied to this event
      if ( !m_generatorName.value().empty() ) {
        if ( m_generatorName.value() != ( *it )->generatorName() ) { continue; }
      }
      debug() << "Monitor for " << ( *it )->generatorName() << endmsg;

      for ( HepMC::GenEvent::particle_const_iterator itp = ( *it )->pGenEvt()->particles_begin();
            itp != ( *it )->pGenEvt()->particles_end(); itp++ ) {
        HepMC::GenParticle* hepMCpart = *itp;
        nParticles++;

        // Particle properties:
        double           pseudoRap = hepMCpart->momentum().pseudoRapidity();
        double           energy    = hepMCpart->momentum().e() / Gaudi::Units::GeV;
        double           pt        = hepMCpart->momentum().perp() / Gaudi::Units::GeV;
        double           p         = hepMCpart->momentum().rho() / Gaudi::Units::GeV;
        int              pID       = hepMCpart->pdg_id();
        LHCb::ParticleID LHCb_pID( hepMCpart->pdg_id() );

        if ( produceHistos() ) {
          m_hAllParticlesPRap->fill( pseudoRap );
          m_hAllParticlesEnergy->fill( energy );
          m_hAllParticlesP->fill( p );
          m_hAllParticlesPt->fill( pt );
          m_hAllParticlesPID->fill( pID );
          m_hPartP->fill( p );
          m_hPartPDG->fill( pID );
          // Why are these histo's here? Throwback from the original code...?
        }
        // Note that the following is really multiplicity of particle defined
        // as stable by Pythia just after hadronization: all particles known by
        // EvtGen are defined stable for Pythia (it will count rho and pi0
        // and gamma all togheter as stable ...
        if ( ( hepMCpart->status() != 2 ) && ( hepMCpart->status() != 3 ) ) {
          nProtoStable++;
          if ( produceHistos() ) {
            m_hProtoP->fill( p );
            m_hProtoPDG->fill( pID );
            m_hProtoLTime->fill( GaussGenUtil::lifetime( hepMCpart ) / Gaudi::Units::mm );
          }

          // A stable particle does not have an outgoing vertex
          if ( !hepMCpart->end_vertex() ) {
            // should be the same as the following
            //             if ( ( hepMCpart -> status() == 999 )
            nStable++;
            if ( 0 != int( LHCb_pID.threeCharge() ) ) {
              nChargedStable++;
              if ( produceHistos() ) {
                m_hAllChargedStableP->fill( p );
                m_hAllChargedStablePt->fill( pt );
                m_hAllChargedStableEnergy->fill( energy );
                m_hAllChargedStablePRap->fill( pseudoRap );
                m_hAllChargedStablePID->fill( pID );
              }
              // Set maximum values
              if ( pt > ptmax_fr ) ptmax_fr = pt;
              // Max energy limited to overlook initial energy transfer
              if ( ( energy > emax_fr ) && ( energy < 7000. ) ) emax_fr = energy;
              if ( p > pmax_fr ) pmax_fr = p;

            } else {
              nNeutralStable++;
              if ( pt > ptmax_neut ) ptmax_neut = pt;
              if ( p > pmax_neut ) pmax_neut = p;
              if ( energy > emax_neut ) emax_neut = energy;
              if ( produceHistos() && m_neutralHistos.value() ) {
                m_hNeutralStablePt->fill( pt );
                m_hNeutralStableP->fill( p );
                m_hNeutralStableEnergy->fill( energy );
                m_hNeutralStablePID->fill( pID );
                m_hNeutralStablePRap->fill( pseudoRap );
              }
            }
            if ( produceHistos() ) {
              m_hStableEta->fill( pseudoRap );
              m_hStablePt->fill( pt );
            }
          }
        }
      }
    }
  }
  if ( produceHistos() ) {
    // All Particles
    m_hAllParticlesMult->fill( nParticles );

    // All Charged Stable PArticles
    m_hAllChargedStablePMax->fill( pmax_fr );
    m_hAllChargedStablePtMax->fill( ptmax_fr );
    m_hAllChargedStableEnergyMax->fill( emax_fr );
    m_hAllChargedStableMult->fill( nChargedStable );

    // Neutral Stable within EtaMin and Max
    if ( m_neutralHistos.value() ) {
      m_hNeutralStablePMax->fill( pmax_neut );
      m_hNeutralStablePtMax->fill( ptmax_neut );
      m_hNeutralStableEnergyMax->fill( emax_neut );
      m_hNeutralStableMult->fill( nNeutralStable );
    }

    // WHat are these histograms? Gloria?
    m_hNPart->fill( nParticles );
    m_hNStable->fill( nStable );
    m_hNSCharg->fill( nChargedStable );
  }
  m_counter += nParticles;
  m_counterStable += nStable;
  m_counterCharged += nChargedStable;
  m_nEvents++;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode DecayAnalysis::finalize() {

  debug() << "==> Finalize" << endmsg;
  if ( produceHistos() ) { normHistos(); }

  info() << std::endl
         << "======================== Generators Statistics ====================" << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of particles generated: " << m_counter << " m_nAllParticles = " << m_nAllParticles << std::endl
         << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean multiplicity: " << m_counter / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of pseudo stable particles generated: " << m_counterStable << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean pseudo stable multiplicity: " << m_counterStable / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of charged stable particles generated: " << m_counterCharged << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean charged stable multiplicity: " << m_counterCharged / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "===================================================================" << endmsg;

  return GaudiHistoAlg::finalize();
}

//============================================================================
// Booking of histograms
//============================================================================
void DecayAnalysis::bookHistos( bool neutral ) {

  debug() << "==> Book histograms" << endmsg;

  int i( 0 );

  // General Histograms
  debug() << "Booking General Histograms." << endmsg;
  i++;
  m_hNPart = book( 105, "Multiplicity all particles", 0., 80., 80 );
  m_pHisto.push_back( m_hNPart );
  i++;
  m_hNStable = book( 106, "Multiplicity protostable particles", 0., 50., 50 );
  m_pHisto.push_back( m_hNStable );
  i++;
  m_hNSCharg = book( 107, "Multiplicity stable charged particles", 0., 30., 30 );
  m_pHisto.push_back( m_hNSCharg );
  i++;
  m_hPartP = book( 120, "Momentum of all particles (GeV)", 0., 5., 100 );
  m_pHisto.push_back( m_hPartP );
  i++;
  m_hPartPDG = book( 121, "PDGid of all particles", -5000., 5000., 10000 );
  m_pHisto.push_back( m_hPartPDG );
  i++;
  m_hProtoP = book( 130, "Momentum of protostable particles (GeV)", 0., 5., 100 );
  m_pHisto.push_back( m_hProtoP );
  i++;
  m_hProtoPDG = book( 131, "PDGid of protostable particles", -5000., 5000., 10000 );
  m_pHisto.push_back( m_hProtoPDG );
  i++;
  m_hProtoLTime = book( 132, "Lifetime protostable particles (mm)", -1., 20., 105 );
  m_pHisto.push_back( m_hProtoLTime );
  i++;
  m_hStableEta = book( 133, "Pseudorapidity stable charged particles", -8., 8., 150 );
  m_pHisto.push_back( m_hStableEta );
  i++;
  m_hStablePt = book( 140, "Pt stable charged particles (GeV)", 0., 3., 100 );
  m_pHisto.push_back( m_hStablePt );

  // All Charged Stable Particle Histo's
  debug() << "Booking Histograms for all Charged Stable particles" << endmsg;
  i++;
  m_hAllChargedStableMult = book( 201, "All Eta: Charged Stable Multiplicity", 0., 30., 30 );
  m_pHisto.push_back( m_hAllChargedStableMult );

  i++;
  m_hAllChargedStablePRap = book( 202, "All Eta: Charged Stable PseudoRapidity", -8., 8., 150 );
  m_pHisto.push_back( m_hAllChargedStablePRap );

  i++;
  m_hAllChargedStableEnergy = book( 203, "All Eta: Charged Stable Energy (GeV)", 0., 3., 150 );
  m_pHisto.push_back( m_hAllChargedStableEnergy );

  i++;
  m_hAllChargedStableEnergyMax = book( 204, "All Eta: Charged Stable Maximum Energy (GeV)", 0., 3., 50 );
  m_pHisto.push_back( m_hAllChargedStableEnergyMax );

  i++;
  m_hAllChargedStableP = book( 205, "All Eta: Charged Stable Momentum (GeV)", 0., 3., 150 );
  m_pHisto.push_back( m_hAllChargedStableP );

  i++;
  m_hAllChargedStablePMax = book( 206, "All Eta: Charged Stable Maximum Momentum (GeV)", 0., 3., 50 );
  m_pHisto.push_back( m_hAllChargedStablePMax );

  i++;
  m_hAllChargedStablePt = book( 207, "All Eta: Charged Stable Transverse Momentum (GeV)", 0., 3., 150 );
  m_pHisto.push_back( m_hAllChargedStablePt );

  i++;
  m_hAllChargedStablePtMax = book( 208, "All Eta: Charged Stable Maximum Transverse Momentum (GeV)", 0., 3., 50 );
  m_pHisto.push_back( m_hAllChargedStablePtMax );

  i++;
  m_hAllChargedStablePID = book( 209, "All Eta: Charged Stable particles PID", -5000., 5000., 10000 );
  m_pHisto.push_back( m_hAllChargedStablePID );

  // Histograms for all particles
  debug() << "Booking Histos for All Particles" << endmsg;
  i++;
  m_hAllParticlesMult = book( 401, "All Eta: Particle Multiplicity", 0., 80., 80 );
  m_pHisto.push_back( m_hAllParticlesMult );

  i++;
  m_hAllParticlesPRap = book( 402, "All Eta: Particle PseudoRapidity", -8., 8., 150 );
  m_pHisto.push_back( m_hAllParticlesPRap );

  i++;
  m_hAllParticlesEnergy = book( 403, "All Eta: Particle Energy (GeV)", 0., 5., 100 );
  m_pHisto.push_back( m_hAllParticlesEnergy );

  i++;
  m_hAllParticlesP = book( 404, "All Eta: Particle Momentum (GeV)", -0., 3., 150 );
  m_pHisto.push_back( m_hAllParticlesP );

  i++;
  m_hAllParticlesPt = book( 405, "All Eta: Particle Transverse Momentum (GeV)", -0., 3., 150 );
  m_pHisto.push_back( m_hAllParticlesPt );

  i++;
  m_hAllParticlesPID = book( 406, "All Eta: Particle particles PID", -5000., 5000., 10000 );
  m_pHisto.push_back( m_hAllParticlesPID );

  // Histograms for neutral particles
  if ( neutral ) {
    debug() << "Booking Histos for neutral particles" << endmsg;
    i++;
    m_hNeutralStableMult = book( 501, "Neutral Stable Multiplicity", 0., 50., 50 );
    m_pHisto.push_back( m_hNeutralStableMult );

    i++;
    m_hNeutralStablePRap = book( 502, "Neutral Stable PseudoRapidity", -8., 8., 150 );
    m_pHisto.push_back( m_hNeutralStablePRap );

    i++;
    m_hNeutralStableEnergy = book( 503, "Neutral Stable Energy (GeV)", 0., 5., 100 );
    m_pHisto.push_back( m_hNeutralStableEnergy );

    i++;
    m_hNeutralStableEnergyMax = book( 504, "Neutral Stable Maximum Energy (GeV)", 0., 5., 50 );
    m_pHisto.push_back( m_hNeutralStableEnergyMax );

    i++;
    m_hNeutralStableP = book( 505, "Neutral Stable Momentum (GeV)", 0., 5., 150 );
    m_pHisto.push_back( m_hNeutralStableP );

    i++;
    m_hNeutralStablePMax = book( 506, "Maximum Neutral Stable Momentum (GeV)", 0., 3., 150 );
    m_pHisto.push_back( m_hNeutralStablePMax );

    i++;
    m_hNeutralStablePt = book( 507, "Neutral Stable Transverse Momentum (GeV)", 0., 3., 150 );
    m_pHisto.push_back( m_hNeutralStablePt );

    i++;
    m_hNeutralStablePtMax = book( 508, "Neutral Stable Maximum Transverse Momentum (GeV)", 0., 3., 50 );
    m_pHisto.push_back( m_hNeutralStablePtMax );

    i++;
    m_hNeutralStablePID = book( 509, "Neutral Stable Particles PID", -5000., 5000., 10000 );
    m_pHisto.push_back( m_hNeutralStablePID );
  }

  debug() << "Booked " << i + 1 << " Histograms." << endmsg;

  return;
}

//============================================================================
//  Normalize Histograms
//============================================================================
void DecayAnalysis::normHistos() {
  debug() << "==> Normalize histograms" << endmsg;

  double binSize = 0;
  int    i       = -1;
  for ( std::vector<AIDA::IHistogram1D*>::iterator iHisto = m_pHisto.begin(); m_pHisto.end() != iHisto; ++iHisto ) {

    ++i;
    AIDA::IHistogram1D* pHisto = ( *iHisto );
    if ( ( pHisto )->axis().isFixedBinning() ) {
      binSize = ( pHisto->axis().upperEdge() - pHisto->axis().lowerEdge() ) / ( pHisto->axis().bins() );

      debug() << "Currently normalizing histo #: " << i << " : " << pHisto->title() << endmsg;
      debug() << "nBins = " << pHisto->axis().bins() << ", Bins Width = " << pHisto->axis().binWidth( 0 )
              << ", Top range = " << pHisto->axis().upperEdge() << ", Lower Range = " << pHisto->axis().lowerEdge()
              << endmsg;
      if ( m_nEvents != 0 && binSize != 0.0 ) {
        debug() << "Scale factor = " << 1. / ( (double)m_nEvents * binSize ) << endmsg;
        pHisto->scale( 1. / ( (double)m_nEvents * binSize ) );
      } else {
        warning() << "Scale factor = 0, unable to normalise histo!" << endmsg;
      }
    } else {
      warning() << "Histo: " << pHisto->title() << " not normalised (no fixed binSize)." << endmsg;
    }
  }

  return;
}
