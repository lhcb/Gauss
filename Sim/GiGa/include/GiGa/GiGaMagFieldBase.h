/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaMagFieldBase.h,v 1.9 2003-04-06 18:49:45 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.8  2002/05/07 12:21:29  ibelyaev
//  see $GIGAROOT/doc/release.notes  7 May 2002
//
// ============================================================================
#ifndef GIGA_GIGAMagFieldBase_H
#  define GIGA_GIGAMagFieldBase_H 1
// ============================================================================
#  include "GiGa/GiGaFieldMgrBase.h"
#  include "GiGa/IGiGaMagField.h"

class IMagneticFieldSvc;

/** @class GiGaMagFieldBase     GiGaMagFieldBase.h GiGa/GiGaMagFieldBase.h
 *
 *  A base class for implementation of Magnetic Field Objects.
 *
 *   @author Vanya Belyaev
 */

class GiGaMagFieldBase : public virtual IGiGaMagField, public GiGaFieldMgrBase {
protected:
  /** standard constructor
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  GiGaMagFieldBase( const std::string& type, const std::string& name, const IInterface* parent );

  /// virtual destructor
  virtual ~GiGaMagFieldBase();

public:
  /** accessor to magnetic field
   *  @see G4MagneticField
   *  @return pointer to the magnetic field object
   */
  G4MagneticField* field() const override;

  /** initialize the object
   *  @see GiGaBase
   *  @see  AlgTool
   *  @see IAlgTool
   *  @return status code
   */
  StatusCode initialize() override;

  /** finalize the object
   *  @see GiGaBase
   *  @see  AlgTool
   *  @see IAlgTool
   *  @return status code
   */
  StatusCode finalize() override;

protected:
  /** aceess to Magnetic Field Service
   *  @return pointer to Magnetic Field Service
   */
  inline IMagneticFieldSvc* mfSvc() const { return m_mfSvc; }

private:
  ///
  GiGaMagFieldBase();                                     ///< no default constructor!
  GiGaMagFieldBase( const GiGaMagFieldBase& );            ///< no copy constructor!
  GiGaMagFieldBase& operator=( const GiGaMagFieldBase& ); ///< no assignment!
  ///
private:
  ///
  std::string        m_nameMFSvc;
  IMagneticFieldSvc* m_mfSvc;
  //
  mutable G4MagneticField* m_self;
  ///
};

// ============================================================================
// The END
// ============================================================================
#endif /// GIGA_GIGAMagFieldBase_H
// ============================================================================
