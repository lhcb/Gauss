/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaStepActionBase.h,v 1.12 2007-03-18 18:25:05 gcorti Exp $
#ifndef GIGA_GiGaStepActionBase_H
#define GIGA_GiGaStepActionBase_H 1

// Include files
// STL
#include <vector>
// GiGa
#include "GiGa/GiGaBase.h"
#include "GiGa/IGiGaStepAction.h"

// Forward declarations
class G4Step;

/** @class GiGaStepActionBase GiGaStepActionBase.h
 *
 *  Base class for implementation of concrete Steping Action for GiGa
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date    23/01/2001
 */

class GiGaStepActionBase : public virtual IGiGaStepAction, public GiGaBase {

public:
  /** standard constructor
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  GiGaStepActionBase( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~GiGaStepActionBase(); ///< Destructor

  /** initialize the step action
   *  @see GiGaBase
   *  @see  AlgTool
   *  @see IAlgTool
   *  @return status code
   */
  StatusCode initialize() override;

  /** finalize the step action
   *  @see GiGaBase
   *  @see  AlgTool
   *  @see IAlgTool
   *  @return status code
   */
  StatusCode finalize() override;

protected:
  GiGaStepActionBase();                                       ///< no default constructor!
  GiGaStepActionBase( const GiGaStepActionBase& );            ///< no copy
  GiGaStepActionBase& operator=( const GiGaStepActionBase& ); ///< no =

private:
};

#endif // GIGA_GiGaStepActionBase_H
