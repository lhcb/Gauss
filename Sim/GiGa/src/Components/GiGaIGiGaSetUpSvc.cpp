/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaIGiGaSetUpSvc.cpp,v 1.1 2002-12-07 14:27:52 ibelyaev Exp $
// ============================================================================
/// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.9  2002/05/07 12:21:36  ibelyaev
//  see $GIGAROOT/doc/release.notes  7 May 2002
//
// ============================================================================
#define GIGA_GIGASVCIGIGASETUPSVC_CPP 1
// ============================================================================
/// include files
/// STD & ATL
#include <string>
#include <typeinfo>
/// GaudiKernel
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/System.h"
/// G4
#include "G4UserEventAction.hh"
#include "G4UserRunAction.hh"
#include "G4UserStackingAction.hh"
#include "G4UserSteppingAction.hh"
#include "G4UserTrackingAction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VUserPhysicsList.hh"
#include "G4VUserPrimaryGeneratorAction.hh"

/// GiGa
#include "GiGa/GiGaException.h"
#include "GiGa/GiGaUtil.h"
#include "GiGa/IGiGaRunManager.h"
/// local
#include "GiGa.h"

// ============================================================================
/**  @file
 *
 *   Implementation of class GiGa
 *   all methods from abstract interface IGiGaSetUpSvc
 *
 *   @author: Vanya Belyaev Ivan.Belyaev@itep.ru
 *   @date xx/xx/xxxx
 */
// ============================================================================

// ============================================================================
/** set detector constructon module
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through exception thrown
 *
 *  @param  obj      pointer to detector construction module
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4VUserDetectorConstruction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4VUserDetectorConstruction*)", Excpt )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4VUserDetectorConstruction*)", Excpt )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4VUserDetectorConstruction*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new world wolume
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through exception thrown
 *
 *  @param  obj    pointer to  new world volume
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4VPhysicalVolume* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4VPhysicalVolume*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4VPhysicalVolume*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4VPhysicalVolume*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new generator
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported throw exception
 *
 *  @param  obj        pointer to new generator
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4VUserPrimaryGeneratorAction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = Error( "operator<< : IGiGaRunManager* points to NULL!" ); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4VUserPrimaryGenerator*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4VUserPrimaryGenerator*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4VUserPrimaryGenerator*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new physics list
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through  exception thrown
 *
 *  @param  obj      pointer to physics list
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4VUserPhysicsList* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( 0 == runMgr() ) { sc = Error( "operator<< : IGiGaRunManager* points to NULL!" ); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4VUserPhysicsList*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4VUserPhysicsList*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4VUserPhysicsList*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new run action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through exception thrown
 *
 *  @param  obj     pointer to new run action
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4UserRunAction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( 0 == runMgr() ) { sc = Error( "operator<< : IGiGaRunManager* points to NULL!" ); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4UserRunAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4UserRunAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4UserRunAction*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new event action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through exception thrown
 *
 *  @param  obj     pointer to new event action
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4UserEventAction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( 0 == runMgr() ) { sc = Error( "operator<< : IGiGaRunManager* points to NULL!" ); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4UserEventAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4UserEventAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4UserEventAction*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new stacking action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through  exception thrown
 *
 *  @param  obj     pointer to new stacking action
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4UserStackingAction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4UserStackingAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4UserStackingAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4UserStackingAction*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new tracking  action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through exception thrown
 *
 *  @param  obj     pointer to new tracking action
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4UserTrackingAction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4UserTrackingAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4UserTrackingAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4UserTrackingAction*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set new stepping  action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  NB: errors are reported through exception thrown
 *
 *  @param  obj     pointer to new stepping action
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
IGiGaSetUpSvc& GiGa::operator<<( G4UserSteppingAction* obj ) {
  try {
    StatusCode sc = StatusCode::SUCCESS;
    if ( 0 == runMgr() ) { sc = retrieveRunManager(); }
    if ( sc.isFailure() ) {
      Exception( "Unable to create IGiGaRunManager!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    sc = runMgr()->declare( obj );
    if ( sc.isFailure() ) {
      Exception( "Unable to declare" + GiGaUtil::ObjTypeName( obj ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  } catch ( const GaudiException& Excpt ) {
    Exception( "operator<<(G4UserSteppingAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "operator<<(G4UserSteppingAction*)", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) {
    Exception( "operator<<(G4UserSteppingAction*)" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  ///
  return *this;
}

// ============================================================================
/** set detector constructon module
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj      pointer to detector construction module
 *  @return self-reference ot IGiGaSetUpSvc interface
 */
// ============================================================================
StatusCode GiGa::setConstruction( G4VUserDetectorConstruction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setConstruction()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setConstruction()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setConstruction()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new world wolume
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj    pointer to  new world volume
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setDetector( G4VPhysicalVolume* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setDetector()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setDetector()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setDetector()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new generator
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj        pointer to new generator
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setGenerator( G4VUserPrimaryGeneratorAction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setGenerator()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setGenerator()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setGenerator()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new physics list
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj      pointer to physics list
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setPhysics( G4VUserPhysicsList* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setPhysics()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setPhysics()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setPhysics()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new run action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj     pointer to new run action
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setRunAction( G4UserRunAction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setRunAction()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setRunAction()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setRunAction()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new event action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj     pointer to new event action
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setEvtAction( G4UserEventAction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setEvtAction()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setEvtAction()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setEvtAction()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new stacking action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj     pointer to new stacking action
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setStacking( G4UserStackingAction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setStacking()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setStacking()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setStacking()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new tracking  action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj     pointer to new tracking action
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setTracking( G4UserTrackingAction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setTracking()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setTracking()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setTracking()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/** set new stepping  action
 *               implementation of IGiGaSetUpSvc abstract interface
 *
 *  @param  obj     pointer to new stepping action
 *  @return status code
 */
// ============================================================================
StatusCode GiGa::setStepping( G4UserSteppingAction* obj ) {
  try {
    *this << obj;
  } catch ( const GaudiException& Excpt ) {
    Exception( "setStepping()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( const std::exception& Excpt ) {
    Exception( "setStepping()", Excpt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( ... ) { Exception( "setStepping()" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
// The END
// ============================================================================
