/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaRunManagerG4RM.cpp,v 1.4 2004-02-17 18:26:52 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2003/04/06 18:49:48  ibelyaev
//  see $GIGAROOT/doc/release.notes
//
// ============================================================================
// GiGa
#include "GiGa/GiGaUtil.h"
#include "GiGa/IGiGaGeoSrc.h"
// Local
#include "GiGaRunManager.h"
/// G4
#include "G4VUserDetectorConstruction.hh"

// ============================================================================
/** @file
 *  Implementation of virtual G4RunManager methods of class GiGaRunManager
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 30/07/2001
 */
// ============================================================================

// ============================================================================
/** overriden method from G4RunManager
 *  ONE NEVER SHOULD USE IT!!!
 */
// ============================================================================
void GiGaRunManager::BeamOn( int /* n_event   */, const char* /* macroFile */, int /* n_select  */ ) {
  Error( "The usage of BeamOn() method is dangerouse!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  StatusCode sc( StatusCode::SUCCESS );
  try {
    if ( !evt_Is_Processed() ) { sc = processTheEvent(); }
    if ( sc.isFailure() ) { Exception( " forbidden BeamOn() method!" ); }
  } catch ( const GaudiException& Excp ) { Exception( "BeamOn()", Excp ); } catch ( const std::exception& Excp ) {
    Exception( "BeamOn()", Excp );
  } catch ( ... ) { Exception( "BeamOn()" ); }
}

// ============================================================================
// Initialize the geometry
// ============================================================================
void GiGaRunManager::InitializeGeometry() {
  /// get root of geometry tree
  G4VPhysicalVolume* root = 0;
  if ( 0 != m_rootGeo ) {
    Print( " Already converted geometry will be used!" ).ignore();
    root = m_rootGeo;
  } else if ( 0 != geoSrc() ) {
    Print( " Geometry will be extracted from " + GiGaUtil::ObjTypeName( geoSrc() ) ).ignore();
    root = geoSrc()->world();
  } else if ( 0 != G4RunManager::userDetector ) {
    Print( " Geometry will be constructed using " + GiGaUtil::ObjTypeName( G4RunManager::userDetector ) ).ignore();
    root = G4RunManager::userDetector->Construct();
  } else {
    Error( " There are NO known sources of Geometry information!" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  //
  if ( 0 == root ) { Exception( "InitializeGeometry: NO 'geometry sources' abvailable" ); }
  ///
  DefineWorldVolume( root );

  if ( !m_par_worlds_facs.empty() ) {
    for ( auto& par_world_fac : m_par_worlds_facs ) {
      debug() << "Constructing parallel world" << endmsg;
      auto par_world = par_world_fac->construct();
      par_world->Construct();
      par_world->ConstructSD();
    }
    debug() << "Setting no of par worlds " << endmsg;
    kernel->SetNumberOfParallelWorld( m_par_worlds_facs.size() );
  }

  G4RunManager::geometryInitialized = true;
}

// ============================================================================
// Initialize
// ============================================================================
void GiGaRunManager::Initialize() {
  StatusCode sc( StatusCode::SUCCESS );
  try {
    if ( !krn_Is_Initialized() ) { sc = initializeKernel(); }
    if ( sc.isFailure() ) { Exception( "Initialize(): could not initializeKernel()" ); }
  } catch ( const GaudiException& Excp ) {
    Exception( "GiGaRunManager::Initialize()", Excp );
  } catch ( const std::exception& Excp ) { Exception( "GiGaRunManager::Initialize()", Excp ); } catch ( ... ) {
    Exception( "GiGaRunManager::Initialize()" );
  }
  ///
}

// ============================================================================
// The End
// ============================================================================
