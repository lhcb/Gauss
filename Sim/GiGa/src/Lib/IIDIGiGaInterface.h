/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaInterface.h,v 1.1 2004-02-20 18:58:21 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1  2002/05/07 12:21:32  ibelyaev
//  see $GIGAROOT/doc/release.notes  7 May 2002
//
// ============================================================================
#ifndef GIGA_IIDIGIGAINTERFACE_H
#  define GIGA_IIDIGIGAINTERFACE_H 1
// Include files
// GaudiKernel
#  include "GaudiKernel/IInterface.h"

/** @file
 *
 *  Definition of unique identifier for class IGiGaInterface
 *  @see IGiGaInterface
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   06/05/2002
 */

/** @var IID_IGiGaInterface
 *
 *  Definition of unique identifier for class IGiGaInterface
 *  @see IGiGaInterface
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   06/05/2002
 */
static const InterfaceID IID_IGiGaInterface( "IGiGaInterface", 1, 0 );

// ============================================================================
// The END
// ============================================================================
#endif // GIGA_IIDIGIGAINTERFACE_H
// ============================================================================
