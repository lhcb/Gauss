/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussRedecayCopyToService_H
#define GaussRedecayCopyToService_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// forward declarations
class IGaussRedecayStr; ///< GaussRedecay counter service
class IGaussRedecayCtr; ///< GaussRedecay counter service

/** @class GaussRedecayCopyToService GaussRedecayCopyToService.h
 *
 * Algorithm to copy all the MC objects into the service for later reuse.
 *
 *  @author Dominik Muller
 *  @date   2016-3-15
 */
class GaussRedecayCopyToService : public GaudiAlgorithm {
public:
  /// Standard constructor
  GaussRedecayCopyToService( const std::string& Name, ISvcLocator* SvcLoc );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

protected:
  /** accessor to GaussRedecay Service
   *  @return pointer to GaussRedecay Service
   */

private:
  std::string       m_gaussRDSvcName;
  IGaussRedecayCtr* m_gaussRDCtrSvc;
  IGaussRedecayStr* m_gaussRDStrSvc;

  std::string              m_particlesLocation; ///< Location in TES of output MCParticles.
  std::string              m_verticesLocation;  ///< Location in TES of output MCVertices.
  std::vector<std::string> m_hitsLocations;     ///< Location in TES of output MCHits.
  std::vector<std::string> m_calohitsLocations; ///< Location in TES of output MCCaloHits.
  std::string              m_richHitsLocation;
  std::string              m_richOpticalPhotonsLocation;
  std::string              m_richSegmentsLocation;
  std::string              m_richTracksLocation;
  std::string              m_GenCollisionsLocation;
};

#endif // GaussRedecayCopyToService_H
