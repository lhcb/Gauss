/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaKineCnvSvc.h,v 1.1 2004-02-20 19:27:27 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2002/01/22 18:24:42  ibelyaev
//  Vanya: update for newer versions of Geant4 and Gaudi
//
// Revision 1.2  2001/08/12 17:24:50  ibelyaev
// improvements with Doxygen comments
//
// Revision 1.1  2001/07/24 11:13:54  ibelyaev
// package restructurization(III) and update for newer GiGa
//
// ============================================================================
#ifndef GIGACNV_IIDIGIGAKINECNVSVC_H
#  define GIGACNV_IIDIGIGAKINECNVSVC_H 1
// ============================================================================

/** unique interface identifier
 */
static const InterfaceID IID_IGiGaKineCnvSvc( "IGiGaKineCnvSvc", 1, 0 );

// ============================================================================
// The End
// ============================================================================
#endif ///< GIGACNV_IIDIGIGAKINECNVSVC_H
// ============================================================================
