/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaGeomCnvSvc.h,v 1.1 2004-02-20 19:27:27 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.4  2002/05/04 20:39:35  ibelyaev
//  see $GIGACNVROOT/release.notes (4 May 2002)
//
// ============================================================================
#ifndef GIGACNV_IIDIGIGAGEOMCNVSVC_H
#  define GIGACNV_IIDIGIGAGEOMCNVSVC_H 1
// ============================================================================

/** @file
 *  declaration of unique identifier for IGiGaGeomCnvSvc
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 4 May 2002
 */

/** @var IID_IGiGaGeomCnvSvc
 *  declaration of unique identifier for IGiGaGeomCnvSvc
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 4 May 2002
 */
static const InterfaceID IID_IGiGaGeomCnvSvc( "IGiGaGeomCnvSvc", 4, 0 );

// ============================================================================
// The End
// ============================================================================
#endif ///< GIGACNV_IIDIGIGAGEOMCNVSVC_H
// ============================================================================
