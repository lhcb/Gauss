###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##  File to include BLS geometry and activate hits collecting
##
##  @author V.Talanov
##  @date 2009-10-30
##
from Gaudi.Configuration import *
from Gauss.Configuration import *

# -- Switch on the LHCb upstream geometry
#
importOptions("$GAUSSOPTS/BeforeVeloGeometry.py")

# -- Activate hits collecting from Beam Loss Scintillators
#
gtAlg1 = GetTrackerHitsAlgDetDesc(
    name="GetBlsHits1",
    CollectionName="BlsSDet/Hits",
    MCHitsLocation="/Event/MC/Bls1/Hits",
    Detector="/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/Bls1",
)
gtAlg1 = GetTrackerHitsAlgDetDesc(
    name="GetBlsHits2",
    CollectionName="BlsSDet/Hits",
    MCHitsLocation="/Event/MC/Bls2/Hits",
    Detector="/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/Bls2",
)
GaudiSequencer("DetectorsHits").Members += [gtAlg1, gtAlg2]

# -- Add hits to tape (GaussTape comes from Gauss-Job.py...)
#
myGaussTape = OutputStream("GaussTape")
myGaussTape.ItemList += ["/Event/MC/Bls1/Hits#1", "/Event/MC/Bls2/Hits#1"]

# BlsSim.py
