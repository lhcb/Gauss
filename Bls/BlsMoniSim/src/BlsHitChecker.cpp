/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BlsHitChecker.cpp,v 1.1.1.2 2010-03-10 17:38:47 vtalanov Exp $
// Include files

// from Gaudi
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Tuples.h"

// from STL
#include <map>
#include <string>

// Include files

// from Gaudi
#include "Event/MCHit.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/Stat.h"

// from STL
#include <map>

// from AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

/** @class BlsHitChecker BlsHitChecker.h
 *
 *
 *  @author Vadim Talanov
 *  @date   2010-02-06
 *
 *  MT modifications
 *  @author Dominik Muller
 *  @date   2020-07-23
 */

class BlsHitChecker : public Gaudi::Functional::Consumer<void( const LHCb::MCHits&, const LHCb::MCHeader& ),
                                                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /// Standard constructor
  BlsHitChecker( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {
                      KeyValue{ "HitsLocation", "MC/Bls/Hits" },
                      KeyValue{ "MCHeader", LHCb::MCHeaderLocation::Default },
                  } ){};

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( const LHCb::MCHits&, const LHCb::MCHeader& ) const override;

protected:
private:
  void                           bookHistograms();
  Gaudi::Property<bool>          m_blsAOn{ this, "BlsAOn", false };
  Gaudi::Property<bool>          m_blsCOn{ this, "BlsCOn", false };
  Gaudi::Property<std::string>   m_blsHTitlePrefix{ this, "HistogramTitlePrefix", "BLS: " };
  Gaudi::Property<double>        m_blsHEntryXMin{ this, "EntryXMin", -150.0 * Gaudi::Units::mm };
  Gaudi::Property<double>        m_blsHEntryXMax{ this, "EntryXMax", +150.0 * Gaudi::Units::mm };
  Gaudi::Property<unsigned long> m_blsHEntryXNbins{ this, "EntryXNbins", 300 };
  Gaudi::Property<double>        m_blsHEntryYMin{ this, "EntryYMin", -150.0 * Gaudi::Units::mm };
  Gaudi::Property<double>        m_blsHEntryYMax{ this, "EntryYMax", +150.0 * Gaudi::Units::mm };
  Gaudi::Property<unsigned long> m_blsHEntryYNbins{ this, "EntryYNbins", 300 };
  Gaudi::Property<double>        m_blsHEntryZMin{ this, "EntryZMin", -2200.0 * Gaudi::Units::mm };
  Gaudi::Property<double>        m_blsHEntryZMax{ this, "EntryZMax", -1900.0 * Gaudi::Units::mm };
  Gaudi::Property<unsigned long> m_blsHEntryZNbins{ this, "EntryZNbins", 300 };
  Gaudi::Property<double>        m_blsHEntryTimeOffset{ this, "EntryTimeOffset", 0.0 * Gaudi::Units::ns };
  Gaudi::Property<double>        m_blsHEntryTimeMin{ this, "EntryTimeMin", -50.0 * Gaudi::Units::ns };
  Gaudi::Property<double>        m_blsHEntryTimeMax{ this, "EntryTimeMax", +50.0 * Gaudi::Units::ns };
  Gaudi::Property<unsigned long> m_blsHEntryTimeNbins{ this, "EntryTimeNbins", 100 };
  Gaudi::Property<double>        m_blsHTrackEnDepMin{ this, "TrackEnDepMin", 0.0 };
  Gaudi::Property<double>        m_blsHTrackEnDepMax{ this, "TrackEnDepMax", 50.0 };
  Gaudi::Property<unsigned long> m_blsHTrackEnDepNbins{ this, "TrackEnDepNbins", 500 };
  Gaudi::Property<double>        m_blsHTrackLengthMin{ this, "TrackLengthMin", 0.0 };
  Gaudi::Property<double>        m_blsHTrackLengthMax{ this, "TrackLengthMax", 7.0 };
  Gaudi::Property<unsigned long> m_blsHTrackLengthNbins{ this, "TrackLengthNbins", 70 };
  Gaudi::Property<double>        m_blsHEventNumTracksMin{ this, "EventNumTracksMin", 0.0 };
  Gaudi::Property<double>        m_blsHEventNumTracksMax{ this, "EventNumTracksMax", 50.0 };
  Gaudi::Property<unsigned long> m_blsHEventNumTracksNbins{ this, "EventNumTracksNbins", 50 };
  Gaudi::Property<double>        m_blsHEventNumMin{ this, "EventNumMin", 0.0 };
  Gaudi::Property<double>        m_blsHEventNumMax{ this, "EventNumMax", 1000.0 };
  Gaudi::Property<unsigned long> m_blsHEventNumNbins{ this, "EventNumNbins", 1000 };
  mutable Gaudi::Accumulators::SummingCounter<size_t> m_blsHits{ this, "BlsHits" };
  mutable Gaudi::Accumulators::SummingCounter<size_t> m_blsTracks{ this, "BlsTracks" };

  typedef std::multimap<const LHCb::MCParticle*, LHCb::MCHit*> t_blsMCParticle2MCHitMultimap;

  AIDA::IHistogram2D* h_HitEntryXY{ nullptr };
  AIDA::IHistogram2D* h_HitEntryZX{ nullptr };
  AIDA::IHistogram2D* h_HitEntryZY{ nullptr };
  AIDA::IHistogram1D* h_HitEnDepTime{ nullptr };
  AIDA::IHistogram1D* h_TrackEnDep{ nullptr };
  AIDA::IHistogram1D* h_TrackLength{ nullptr };
  AIDA::IHistogram1D* h_TrackEnDepPerUnitLength{ nullptr };
  AIDA::IHistogram1D* h_EventNumHits{ nullptr };
  AIDA::IHistogram1D* h_EventNumTracks{ nullptr };
  AIDA::IHistogram1D* h_EventEnDep{ nullptr };
  AIDA::IHistogram1D* h_RunNumHits{ nullptr };
  AIDA::IHistogram1D* h_RunNumTracks{ nullptr };
  AIDA::IHistogram1D* h_RunEnDep{ nullptr };
};

//-----------------------------------------------------------------------------
// Implementation file for class : BlsHitChecker
//
// 2010-02-06 : Vadim Talanov
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BlsHitChecker )

//=============================================================================
// Initialization
//=============================================================================

StatusCode BlsHitChecker::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "BlsHitChecker::initialize()" << endmsg;
  bookHistograms();
  return StatusCode::SUCCESS;
} // end of: BlsHitChecker::initialize() {

//=============================================================================
// Main execution
//=============================================================================

void BlsHitChecker::operator()( const LHCb::MCHits& blsMCHits, const LHCb::MCHeader& myMCHeader ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "BlsHitChecker::execute()" << endmsg;
  // Just return if the number of hits in all BLSs is zero
  if ( 0 == blsMCHits.size() ) return;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "m_blsHitsLocation: " << blsMCHits.size() << " hit(s) found at event no. " << myMCHeader.evtNumber()
            << endmsg;
  // Define a multimap for Particle/Hit pairs
  t_blsMCParticle2MCHitMultimap myMultimap;
  // Loop over all hits got from m_blsHitsLocation
  for ( auto& hit : blsMCHits ) {
    // Get MC Particle that created this hit
    const LHCb::MCParticle* myMCParticle = hit->mcParticle();
    // If DEBUG is switched on
    if ( msgLevel( MSG::DEBUG ) ) {
      // Print some data for hit and particle
      LHCb::ParticleID      myParticleId       = myMCParticle->particleID();
      Gaudi::LorentzVector  myParticleMomentum = myMCParticle->momentum();
      const LHCb::MCVertex* myMCVertex         = myMCParticle->originVertex();
      debug() << "Particle ID and momentum: " << myParticleId.pid() << " " << myParticleMomentum.e() / Gaudi::Units::GeV
              << " " << myMCParticle << " " << hit << " " << endmsg;
      debug() << "Entry point: " << hit->entry().x() / Gaudi::Units::mm << " " << hit->entry().y() / Gaudi::Units::mm
              << " " << hit->entry().z() / Gaudi::Units::mm << endmsg;
      debug() << "Exit point: " << hit->exit().x() / Gaudi::Units::mm << " " << hit->exit().y() / Gaudi::Units::mm
              << " " << hit->exit().z() / Gaudi::Units::mm << endmsg;
      debug() << "Vertex position: " << myMCVertex->position().x() / Gaudi::Units::mm << " "
              << myMCVertex->position().y() / Gaudi::Units::mm << " " << myMCVertex->position().z() / Gaudi::Units::mm
              << " " << myMCVertex->type() << endmsg;
    } // end of: if ( msgLevel(MSG::DEBUG) )
    // Positive X goes to positive BLS and negative X to negative BLS
    if ( ( hit->entry().x() > 0 && m_blsCOn ) || ( hit->entry().x() < 0 && m_blsAOn ) ) {
      // Insert a pair of Particle-Hit into the map
      myMultimap.insert( t_blsMCParticle2MCHitMultimap::value_type( myMCParticle, hit ) );
    } // end of: if ( ( (*It)->entry().x() > 0 && m_blsCOn ) ||
  }   // end of: for ( LHCb::MCHits::iterator It = m_blsMCHits->begin();
  // Just return if no hits were found in a particular BLS
  if ( myMultimap.empty() ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "No hits in this BLS found at event no. " << myMCHeader.evtNumber() << endmsg;
    return;
  } // end of: if ( myMultimap.empty() ) {
  // Calculate hits/tracks and energy deposited in event
  unsigned short myEventNumHits   = 0;
  unsigned short myEventNumTracks = 0;
  double         myEventEnDep     = 0.0;
  // String for histogram titles
  std::string myTitle;
  // Scan Particle<->Hit map to get quantities per single track
  for ( t_blsMCParticle2MCHitMultimap::iterator anIt = myMultimap.begin(); anIt != myMultimap.end(); ) {
    // Count new track
    myEventNumTracks++;
    // Calculate energy deposited by a single track and this track length
    double myTrackEnDep  = 0.0;
    double myTrackLength = 0.0;
    // For each unique key (== MCParticle) in a map collect hits it has created
    const LHCb::MCParticle* myKey = ( *anIt ).first;
    for ( t_blsMCParticle2MCHitMultimap::iterator oneMoreIt = myMultimap.lower_bound( myKey );
          oneMoreIt != myMultimap.upper_bound( myKey ); oneMoreIt++ ) {
      // Count new hit
      myEventNumHits++;
      const LHCb::MCParticle* myMCParticle = ( *oneMoreIt ).first;
      LHCb::MCHit*            myMCHit      = ( *oneMoreIt ).second;
      // If DEBUG is switched on
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Particle2Hit map: " << myMCParticle << " " << myMCHit << " "
                << myMCHit->energy() / Gaudi::Units::MeV << " " << myMCHit->time() / Gaudi::Units::ns << endmsg;
      } // end of: if ( msgLevel(MSG::DEBUG) )
      // Add properties for current hit (== step) to track
      myTrackEnDep += myMCHit->energy();
      myTrackLength += myMCHit->pathLength();
      // Make histograms for a per hit quantities
      myTitle = m_blsHTitlePrefix + "Hit entry point in scintillator [mm], XY plane";
      h_HitEntryXY->fill( myMCHit->entry().x() / Gaudi::Units::mm, myMCHit->entry().y() / Gaudi::Units::mm );
      myTitle = m_blsHTitlePrefix + "Hit entry point in scintillator [mm], ZX plane";
      h_HitEntryZX->fill( myMCHit->entry().z() / Gaudi::Units::mm, myMCHit->entry().x() / Gaudi::Units::mm );
      myTitle = m_blsHTitlePrefix + "Hit entry point in scintillator [mm], ZY plane";
      h_HitEntryZY->fill( myMCHit->entry().z() / Gaudi::Units::mm, myMCHit->entry().y() / Gaudi::Units::mm );
      myTitle = m_blsHTitlePrefix + "Hit energy deposited in scintillator [MeV]";
      h_HitEnDepTime->fill( ( myMCHit->time() + m_blsHEntryTimeOffset ) / Gaudi::Units::ns,
                            myMCHit->energy() / Gaudi::Units::MeV );
      // This can as well be: anIt = upper_bound(myKey)
      anIt++;
    } // end of: for ( t_blsMCParticle2MCHitMultimap::iterator oneMoreIt
    // Add energy deposited by a single track to total energy deposited in event
    myEventEnDep += myTrackEnDep;
    // Make histograms for a per track quantities
    myTitle = m_blsHTitlePrefix + "Energy deposited per track [MeV]";
    h_TrackEnDep->fill( myTrackEnDep / Gaudi::Units::MeV );
    myTitle = m_blsHTitlePrefix + "Track length [cm]";
    h_TrackLength->fill( myTrackLength / Gaudi::Units::cm );
    myTitle = m_blsHTitlePrefix + "Energy deposited per track unit length [MeV/cm]";
    h_TrackEnDepPerUnitLength->fill( ( myTrackEnDep / Gaudi::Units::MeV ) / ( myTrackLength / Gaudi::Units::cm ) );
  } // end of: for ( t_blsMCParticle2MCHitMultimap::iterator anIt
  // If DEBUG is switched on
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Event no. " << myMCHeader.evtNumber() << ": Hits/Tracks = " << myEventNumHits << "/" << myEventNumTracks
            << ", EnDep = " << myEventEnDep / Gaudi::Units::MeV << endmsg;
  } // end of: if ( msgLevel(MSG::DEBUG) )
  // Make histograms for a per event quantities
  myTitle = m_blsHTitlePrefix + "Number of hits per event ";
  h_EventNumHits->fill( myEventNumHits );
  myTitle = m_blsHTitlePrefix + "Number of tracks per event ";
  h_EventNumTracks->fill( myEventNumTracks );
  myTitle = m_blsHTitlePrefix + "Energy deposited per event [MeV]";
  h_EventEnDep->fill( myEventEnDep / Gaudi::Units::MeV );
  // Count number of hits/tracks per run
  m_blsHits += myEventNumHits;
  m_blsTracks += myEventNumTracks;
  // Plot global per run quantities
  myTitle = m_blsHTitlePrefix + "Number of hits in event";
  h_RunNumHits->fill( myMCHeader.evtNumber(), myEventNumHits );
  myTitle = m_blsHTitlePrefix + "Number of tracks in event";
  h_RunNumTracks->fill( myMCHeader.evtNumber(), myEventNumTracks );
  myTitle = m_blsHTitlePrefix + "Energy deposited in event [MeV]";
  h_RunEnDep->fill( myMCHeader.evtNumber(), myEventEnDep / Gaudi::Units::MeV );
}

void BlsHitChecker::bookHistograms() {
  std::string myTitle;
  myTitle      = m_blsHTitlePrefix + "Hit entry point in scintillator [mm], XY plane";
  h_HitEntryXY = book2D( "HitEntryXY", myTitle, m_blsHEntryXMin / Gaudi::Units::mm, m_blsHEntryXMax / Gaudi::Units::mm,
                         m_blsHEntryXNbins, m_blsHEntryYMin / Gaudi::Units::mm, m_blsHEntryYMax / Gaudi::Units::mm,
                         m_blsHEntryYNbins );
  myTitle      = m_blsHTitlePrefix + "Hit entry point in scintillator [mm], ZX plane";
  h_HitEntryZX = book2D( "HitEntryZX", myTitle, m_blsHEntryZMin / Gaudi::Units::mm, m_blsHEntryZMax / Gaudi::Units::mm,
                         m_blsHEntryZNbins, m_blsHEntryXMin / Gaudi::Units::mm, m_blsHEntryXMax / Gaudi::Units::mm,
                         m_blsHEntryXNbins );
  myTitle      = m_blsHTitlePrefix + "Hit entry point in scintillator [mm], ZY plane";
  h_HitEntryZY = book2D( "HitEntryZY", myTitle, m_blsHEntryZMin / Gaudi::Units::mm, m_blsHEntryZMax / Gaudi::Units::mm,
                         m_blsHEntryZNbins, m_blsHEntryYMin / Gaudi::Units::mm, m_blsHEntryYMax / Gaudi::Units::mm,
                         m_blsHEntryYNbins );
  myTitle      = m_blsHTitlePrefix + "Hit energy deposited in scintillator [MeV]";
  h_HitEnDepTime = book( "HitEnDepTime", myTitle, m_blsHEntryTimeMin, m_blsHEntryTimeMax, m_blsHEntryTimeNbins );
  myTitle        = m_blsHTitlePrefix + "Energy deposited per track [MeV]";
  h_TrackEnDep   = book( "TrackEnDep", myTitle, m_blsHTrackEnDepMin, m_blsHTrackEnDepMax, m_blsHTrackEnDepNbins );
  myTitle        = m_blsHTitlePrefix + "Track length [cm]";
  h_TrackLength  = book( "TrackLength", myTitle, m_blsHTrackLengthMin, m_blsHTrackLengthMax, m_blsHTrackLengthNbins );
  myTitle        = m_blsHTitlePrefix + "Energy deposited per track unit length [MeV/cm]";
  h_TrackEnDepPerUnitLength =
      book( "TrackEnDepPerUnitLength", myTitle, m_blsHTrackEnDepMin, m_blsHTrackEnDepMax, m_blsHTrackEnDepNbins );
  myTitle = m_blsHTitlePrefix + "Number of hits per event ";
  h_EventNumHits =
      book( "EventNumHits", myTitle, m_blsHEventNumTracksMin, m_blsHEventNumTracksMax, m_blsHEventNumTracksNbins );
  myTitle = m_blsHTitlePrefix + "Number of tracks per event ";
  h_EventNumTracks =
      book( "EventNumTracks", myTitle, m_blsHEventNumTracksMin, m_blsHEventNumTracksMax, m_blsHEventNumTracksNbins );
  myTitle      = m_blsHTitlePrefix + "Energy deposited per event [MeV]";
  h_EventEnDep = book( "EventEnDep", myTitle, m_blsHTrackEnDepMin, m_blsHTrackEnDepMax, m_blsHTrackEnDepNbins );
  // Plot global per run quantities
  myTitle        = m_blsHTitlePrefix + "Number of hits in event";
  h_RunNumHits   = book( "RunNumHits", myTitle, m_blsHEventNumMin, m_blsHEventNumMax, m_blsHEventNumNbins );
  myTitle        = m_blsHTitlePrefix + "Number of tracks in event";
  h_RunNumTracks = book( "RunNumTracks", myTitle, m_blsHEventNumMin, m_blsHEventNumMax, m_blsHEventNumNbins );
  myTitle        = m_blsHTitlePrefix + "Energy deposited in event [MeV]";
  h_RunEnDep     = book( "RunEnDep", myTitle, m_blsHEventNumMin, m_blsHEventNumMax, m_blsHEventNumNbins );
}
