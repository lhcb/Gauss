Gauss
=====

In LHCb applications identically process events either collected by the real detector itself or events produced by the simulation software.
Gauss is the **main simulation framework in LHCb** that handles the **event generation step** and **tracking of particles through the detector material**.

This documentation refers to the newest version of the Gauss framework based on Gaussino and to be deployed for future `Sim11` production campaigns.

Documentation relative to earlier versions can be currently found from the `older Gauss Project web site <http://lhcbdoc.web.cern.ch/lhcbdoc/gauss/>`_.

For information on the Simulation Project and other simulation software and tools refer to the `Simulation Project web site <https://lhcb-simulation.web.cern.ch/index.html>`_


.. figure:: images/gaussino_lhcb_front.png
    :figwidth: 50%
    :align: center

    Simulations in LHCb detector with Gauss-on-Gaussino


.. figure:: images/gauss_phases.png
  :figwidth: 90%
  :align: center

  Generation and detector transport phases in Gauss


Gauss relies on **Gaussino**, which provides all the **core simulation functionalities**. If you wish to work with Gaussino only, please follow its documentation:

- `Gaussino, the experiment-independent, core simulation framework <https://gaussino.docs.cern.ch/>`_

.. figure:: images/gauss_Sim11_stack.png
    :figwidth: 50%
    :align: center

    Gauss (-on-Gaussino) dependencies for Sim11 and beyond.

Gauss is configured following the same modular structure as in Gaussino:

- the **generation** of events,
- the **detector simulation**,
- LHCb (DetDesc/DD4hep) **geometry** import,
- **monitoring** and saving of the **simulation output**.


.. figure:: images/gaussino_modules.png
  :figwidth: 70%
  :align: center

  Modular structure of Gauss.


.. toctree::
   :caption: Getting started
   :hidden:
   :maxdepth: 2

   getting_started/installation
   getting_started/first_simulation_job
   getting_started/contributing
   getting_started/further_reading

.. toctree::
   :caption: Configuration
   :hidden:
   :maxdepth: 2

   configuration/gauss
   configuration/generation
   configuration/simulation
   configuration/geometry

.. toctree::
   :caption: Examples & Tutorials
   :hidden:
   :maxdepth: 2

   examples/run3
   examples/adding_subdetector_in_dd4hep

.. toctree::
   :caption: Generators sets
   :hidden:
   :maxdepth: 1

   lcg_layers/LHCB_7

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
