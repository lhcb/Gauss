# Running your first simulation

Below you will find instructions on how to prepare a configuration file with a minimum working example. We will try to generate 5 events with a 1 MeV photon inside the 2022 LHCb detector.

## Prerequisites

1. Make sure that your Gauss is correctly installed following the instructions in [](./installation.md#installation) section.

2. Create and open a new python file. It will consists of all the options you need to configure the simulation job.

    ```shell
    vim your_gaussino_options.py
    ```

3. We will be now setting the properties of the Gauss configurables. There are 4 main configurables:

    ```{eval-rst}
    - :class:`Gauss <Gauss.Configuration.Gauss>` (main configurable)
    - :class:`GaussGeneration <Gauss.Generation.GaussGeneration>` (configuration of the generation phase)
    - :class:`GaussSimulation <Gauss.Simulation.GaussSimulation>` (configuration of the simulation phase)
    - :class:`GaussGeometry <Gauss.Geometry.GaussGeometry>` (configuration of the geometry)
    ```

    These configurables should be enough for most of the cases, but sometime you will also find a few other configurables, for example:

    ```{eval-rst}
    - :class:`ExternalDetectorEmbedder <ExternalDetector.Configuration.ExternalDetectorEmbedder>`
    ```

    and so on... Each of the configurable you can import from ``Configurables``. For example, for Gauss it will be:

    ```python
    from Configurables import Gauss
    ```

## Simple example

1. Specify the number of events by adding the following lines:

    ```python
    Gauss().EvtMax = 5
    ```

2. Do you want to run the generator phase and the simulation phase? Or just the generator? You can modify this by adding the following lines:

    ```python
    Gauss().Phases = ["Generator", "Simulation"]
    ```

    Please, note that the default is to have both the generator and the simulation phase.

3. Specify what to generate. In this example, we will try to generate 1 MeV photon per event. In order to do so, we have to add a `FixedMomentum` and `FlatNParticles` tools.

    ```python
    from GaudiKernel import SystemOfUnits as units
    from Configurables import (
        GaussGeneration,
        ParticleGun,
        FixedMomentum,
        FlatNParticles,
    )

    GaussGeneration().ParticleGun = True
    pgun = ParticleGun("ParticleGun")
    pgun.addTool(FixedMomentum, name="FixedMomentum")
    pgun.ParticleGunTool = "FixedMomentum"
    pgun.addTool(FlatNParticles, name="FlatNParticles")
    pgun.NumberOfParticlesTool = "FlatNParticles"
    pgun.FlatNParticles.MinNParticles = 1
    pgun.FlatNParticles.MaxNParticles = 1
    pgun.FixedMomentum.px = 0.0 * units.GeV
    pgun.FixedMomentum.py = 0.0 * units.GeV
    pgun.FixedMomentum.pz = 1.0 * units.GeV
    pgun.FixedMomentum.PdgCodes = [22]
    ```

4. Specify what data type you are going to use. This is normally a year, `2012`, `2022`, etc. Gauss will then internally set up all the default beam conditions for you.

    ```python
    from Gaudi.Configuration import importOptions
    importOptions("$GAUSSOPTS/General/2022.py")
    ```

5. (optional) Specify the geometry type. Normally this is done automatically for you, based on the data type provided in an earlier step.

    If you wish to choose the geoemtry service technology, you may choose either DD4hep or DetDesc. DetDesc is compatible only with Run 1, 2 & 3 data types. For Run 3 and beyond, DD4hep is the default option.

    ```python
    from Configurables import GaussGeometry
    GaussGeometry().Legacy = False # False => DD4hep, True => DetDesc
    ```

    You can also manually override, which sub-detectors will be used during the simulation. For example:

    ```python
    from Configurables import GaussGeometry
    GaussGeometry(
        # adds the geometry of FT and Magnet
        DetectorGeo={ "Detectors": ["FT", "Magnet"] },
        # adds the hit extraction classes for FT and turns on the magnetic field
        DetectorSim={ "Detectors": ["FT", "Magnet"] },
        # adds the monitoring algorithms for FT (histograms will be produced)
        DetectorMoni={ "Detectors": ["FT"] },
    )
    ```

## Running Gauss

Once your `your_gaussino_options.py` file is ready you can test it with Gauss!

Use either your local build:

```shell
./build.x86_64_v2-el9-gcc13-opt/run gaudirun.py your_gaussino_options.py
```

or `lb-run`:

```shell
lb-run --nightly lhcb-sim11/latest Gauss/master gaudirun.py your_options.py
```
