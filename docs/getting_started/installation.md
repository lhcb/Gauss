# Installation

Please note that Gauss is an LHCb-dependent package. If you are looking for an experiment-independent solutions, visit Gaussino:
- [Gaussino, the experiment-independent, core simulation framework](https://gaussino.docs.cern.ch/)

If you need to make changes to packages that Gauss depends on, such as `Detector`, please see the [whole stack](#whole-stack) section.

## Gauss only

### Running Gauss from the nightlies

```shell
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lb-set-platform x86_64_v2-el9-gcc13-opt
lb-run --nightly lhcb-sim11/latest Gauss/master gaudirun.py your_options.py
```

### Building Gauss with a local copy of the nightly build

```{eval-rst}
.. tip::
    In the examples below, we are going to use nightly build system in LHCb. You can check the status of nightly build by visiting the `LHCb nightly webpage <https://lhcb-nightlies.web.cern.ch/nightly/>`_. There are a few nightly slots currently available for Gauss:

    -  `lhcb-sim11 <https://lhcb-nightlies.web.cern.ch/nightly/lhcb-sim11/>`_ a stable slot for general development, as it includes what will be deployed in the next release,
    -  `lhcb-sim11-dev <https://lhcb-nightlies.web.cern.ch/nightly/lhcb-sim11-dev/>`_ for testing against new developments not yet merged for release,
    -  `lhcb-sim11-g4 <https://lhcb-nightlies.web.cern.ch/nightly/lhcb-sim11-g4/>`_ nightly slot for testing against future versions of Geant4,
    -  `lhcb-sim11-fastsim <https://lhcb-nightlies.web.cern.ch/nightly/lhcb-sim11-fastsim/>`_ nightly slot for testing against new fast simulation features,
    -  `lhcb-sim11-gen <https://lhcb-nightlies.web.cern.ch/nightly/lhcb-sim11-gen/>`_ nightly slot for testing against new generator features, e.g. EvtGen or Pythia versions.

    Currently, Gauss is built in each nightly slot on 2 platforms:

    - **x86_64_v2-el9-gcc13-opt** release build with **DetDesc & DD4Hep** geometry,
    - **x86_64_v2-el9-gcc13-dbg** debug build with **DetDesc & DD4Hep** geometry.
```

We're going to create a local copy of the projects used in the nightly slot. Gauss requires Gaussino, Gaudi, Detector, LHCb, Run2Support and GaussinoExtLibs (temporary project).

Make sure that you are using LbEnv on lxplus or similar and have a valid Grid proxy. You can run this to be sure:

```shell
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lhcb-proxy-init
```

The following will make sure that you have the most recent platform.

```shell
lb-set-platform x86_64_v2-el9-gcc13-opt
export LCG_VERSION=105c
```

Fetch the nightlies locally

```shell
lbn-install --verbose --projects=Gaudi,Geant4,DBASE,Detector,LHCb,Run2Support,GaussinoExtLibs,Gaussino --platforms=x86_64_v2-el9-gcc13-opt lhcb-sim11 Today
cd lhcb-sim11/Today
lb-set-workspace .
```

```{eval-rst}
.. attention::
    The ``Today`` version of the nightly slot might not always be available. Please, always check directly on the website if the build of Gaussino & Gauss was successful (cells in the `table <https://lhcb-nightlies.web.cern.ch/nightly/lhcb-sim11/>`_ should be either orange or green, but not red!). Always use the latest version that was successful (``yesterday``, ``404`` etc.).
```

Clone, configure, build and install Gauss:

```shell
git clone ssh://git@gitlab.cern.ch:7999/lhcb/Gauss.git
cd Gauss
git checkout master
lb-project-init .
make install
```

### Building only a subset of Gauss packages

The example below shows you how to work and build a subset of the packages of Gaussino. We will use `lb-dev` for this purpose. We are going to need one of the nightly slots mentioned in the previous section.

```shell
source /cvmfs/lhcb.cern.ch/lib/LbEnv
lb-dev --nightly lhcb-sim11/Today Gauss/master
cd GaussDev_master
git lb-use Gauss
```

Configure and build the package you'd like to modify. Below you will find an example for `Sim/Gauss`.

```shell
git lb-checkout Gauss/master Sim/Gauss
make
```

## Whole stack

As Gauss has a lot of project dependencies, building the whole stack by yourself might become not an easy task. Therefore, we recommend using the [lb-stack-setup](https://gitlab.cern.ch/rmatev/lb-stack-setup) tool. It already does a lot of work for you. You might also benefit from using intellisense for C++/python in VSCode and many more.

```shell
curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - stack
cd stack
make Gauss
```

```{eval-rst}
.. attention::

    Please, always follow the instructions available directly on `lb-stack-setup <https://gitlab.cern.ch/rmatev/lb-stack-setup/-/blob/master/README.md>`_.
```
