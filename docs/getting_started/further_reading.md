# Further reading

In LHCb, data collected by the detector is processed using a set of custom applications based on the Gaudi core software framework.
LHCb applications identically process events either collected by the detector itself or events produced by the simulation software.
Simulated events are first handled by Gauss that performs the event generation and tracking of particles
through the detector material.


One of the two main tasks of the Gauss framework is to control the generation of collisions, in most cases pp with Pythia, where a specific LHCb configuration is used. Gauss then makes use of EvtGen in order to model the decays of unstable particles. The other main task of Gauss is the propagation of the generated particles through the experimental apparatus and the simulation of physics processes occurring within the sub-detectors using the Geant4 toolkit. The information to be able to mimic the response of given sub-detectors when a particle intersects them (MC hits) and to understand trigger and reconstruction performance (MC truth) are written to a file for further processing.

Gauss is based on the Gaussino framework. It is a new experiment-independent core simulation framework that provides all the generic components to Gaussino and can act as an ideal testbed for all the new developments in Gauss.

You can find more information about Gaussino in the following papers:

```{eval-rst}
.. [GaussinoPaper1] D. Muller and B. G. Siddi, `Gaussino - a Gaudi-Based Core Simulation Framework <https://ieeexplore.ieee.org/document/9060074>`_,

.. [GaussinoPaper2] D. Muller, `Adopting new technologies in the LHCb Gauss simulation framework <https://www.epj-conferences.org/articles/epjconf/abs/2019/19/epjconf_chep2018_02004/epjconf_chep2018_02004.html>`_

.. [GaussinoPaper3] M. Mazurek, G.Corti, D. Muller, `New Simulation Software Technologies at the LHCb Experiment at CERN <http://www.cai.sk/ojs/index.php/cai/article/view/2021_4_815>`_
```
