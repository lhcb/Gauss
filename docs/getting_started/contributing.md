# Contributing

Gauss is the LHCb project based on top of core simulation framework Gaussino, and other LHCb projects.
Gauss contains all the LHCb-experiment-related components, whereas Gaussino contains all the core functionalities which should be independent of any experiment.

## Developing Gauss

Please make sure that you followed the recipe described in [](./installation.md#building-gauss) and make sure that you work on your local development branch.

Changes should be introduced via merge requests targeting the `master` branch.
If you wish to open a MR with your local branch (the name of the branch should start with your username, e.g. `username-some-changes`), then you can directly push the changes to the branch in the remote repository:

```{eval-rst}
.. code-block:: shell-session

    git add file1 file2
    git commit -m "The description of your commit"
    git push origin username-some-changes
```

In gitlab, open a MR targeting the `master` branch. Adding the `lhcb-sim11` label will include your changes in the next nightly build of `lhcb-sim11`.

## Documentation

1. Add a `Python Documentation` label to your MR. This will activate the gitlab CI pipeline.

2. Edit or add new markdown (`.md`) or reStructured Text (`.rst`) files in the `docs/` repository and make sure they are listed in the `index.rst` file. You can mix both by creating a markdown file and then putting rst in the following format:

```
```{eval-rst}
Your reStructured Text in markdown.
```{}
```

3. Your documentation should be accessible at `https://lhcb-gauss.docs.cern.ch/name-of-your-branch/index.html`

4. You can test the documentation locally by running `gitlab-runner exec shell build-docs`. Standard `make docs` will not work, as we need to add python paths of other projects.
