# LHCB_9

Generator     | Version
--------------|----------
crmc          | 2.0.1.p5
herwig3       | 7.2.3
lhapdf        | 6.5.3
madgraph5amc  | 3.5.2.atlas3
photos++      | 3.64
powheg-box-v2 | r3744.lhcb4.rdynamic
pythia6       | 429.2
pythia8       | 312
rivet         | 3.1.10
sherpa        | 3.0.0p1
starlight     | r313
superchic     | 5.3
tauola++      | 1.1.8
thepeg        | 2.2.3
yoda          | 1.9.10
