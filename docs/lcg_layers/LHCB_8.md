# LHCB_8

Generator     | Version
--------------|----------
crmc          | 1.8.0.lhcb
herwig3       | 7.2.3
lhapdf        | 6.2.3
madgraph5amc  | 2.9.3.atlas1
photos++      | 3.56.lhcb1
powheg-box-v2 | r3744.lhcb4.rdynamic
pythia6       | 427.2.lhcb
pythia8       | 244.lhcb4
rivet         | 3.1.10
starlight     | r313
superchic     | 5.1
tauola++      | 1.1.6b.lhcb
thepeg        | 2.2.3
yoda          | 1.9.10
