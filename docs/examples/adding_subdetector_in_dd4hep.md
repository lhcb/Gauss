# Adding a new DD4hep sub-detector with Detector

Updated with future upgrades detectors in mind

## Preparing Gauss for adding new sub-detectors

Make sure that before you start this tutorial you have followed the examples that show how to build Gauss with DD4hep support as described in the section [](../getting_started/installation.md#installation).

## Checklist to activate a sub-detector

Once you have correctly configured Gauss, you can start integrating your sub-detector with the simulation framework. We have prepared a list of the required tasks:

1. [](./adding_subdetector_in_dd4hep.md#xml-writer-basic-information)
2. [](./adding_subdetector_in_dd4hep.md#implement-python-configuration)
3. [](./adding_subdetector_in_dd4hep.md#check-if-extra-modifications-are-needed)
4. [](./adding_subdetector_in_dd4hep.md#create-a-mr-in-gauss-targeting-master-branch)
5. [](./adding_subdetector_in_dd4hep.md#put-the-sub-detector-in-the-nighlty-and-lhcbpr-tests)

### xml writer basic information


In order to load the geometry in Gauss from the Detector project, you have to make sure that your sub-detector is completely described in XML files in `Detector/compact/` directory of the Detector project. Therefore, Gauss has to know what is the hierarchy of the volumes in the geometry and this has to be implemented in [xml_writer.py](https://gitlab.cern.ch/lhcb/Gauss/-/blob/master/Sim/Gauss/python/Gauss/Detectors/xml_writer.py).

`xml_writer.py` is the tool that generates the custom `LHCb.xml` file used to completely describe the detector. It requires knowledge of what lines need to be added whenever a subdetecter is requested. This information lives inside each subdetector's relevant `[subdetector].py` script. Nowadays, `xml_writer.py` does not need to be touched.

A temporary XML file will be created in `/tmp` on your local machine when it runs.


### Implement python configuration


What happens in Gauss is that we use the python configuration to decide which sub-detectors should be included and which not. If a sub-detector is added, then all of its XML files have to be imported. Please note that Gauss has to support both DD4hep and DetDesc geometries and therefore we need a separate configuration for each of them. Python configuration of each sub-detector should be handled in a separate python file in `Sim/Gauss/python/Gauss/Detectors/`. We will use `VP` again as an example. In this case, a file `VP.py` is already there. If the sub-detector is completely new, you should create a new file and add the following import in [__init__.py](https://gitlab.cern.ch/lhcb/Gauss/-/blob/master/Sim/Gauss/python/Gauss/Detectors/__init__.py):

```diff
+ from Gauss.Detectors.VP import *
```

You have to make sure that your sub-detector class inherits from `det_base` and uses the `subdetector` decorator. There is also a set of functions that have to be implemented.

**For Future Upgrades detector, there is a slight caveat**
As there are no plans on supporting DetDesc with any Future Upgrades subdetector, the function `ApplyDetectorDetDesc` below can be left blank.

For `VP`:

```python
from Gauss.Detectors.det_base import det_base
from Gauss.Detectors.Helpers import subdetector

@subdetector
class VP(det_base):

    def ApplyDetectorDetDesc(self, basePieces, detPieces):
        # Add the necessary detector pieces for DetDesc

    def ApplyDetectorDD4hep(self, basePieces, detPieces):
        # Add a list of all the necessary DD4hep includes

    def SetupExtractionImpl(self, slot=''):
        # Configure the hit getter that transforms the Geant4 hit collection
        # to our event model. Wrapping function in base class configures SimConf.

    def SetupMonitor(self, slot=''):
        # Set up any necessary monitoring algorithms
```

Therefore, in order to use the DD4hep implementation of your sub-detector you have to implement `ApplyDetectorDetDesc` and populate the `LHCbGeo._listOfXMLIncludes_` list with the XML files required for your sub-detector. If your sub-detector is registering hits, then you should also provide a mapping between the name of the sensitive volume and the name of the factory used to construct the sensitive detector object. For `VP` we have:

```python
def ApplyDetectorDD4hep(self, compVersion, basePieces, detPieces):
    # Configuring the DD4hep detector conversion.
    # 1. Add the mapping to have the LHCbDD4hepCnvSvc instrument
    # the G4 volume with the correct sensdet factory if marked with a
    # dd4hep sensitive detector named VP
    from Configurables import LHCbDD4hepCnvSvc

    mappings = LHCbDD4hepCnvSvc().getProp("SensDetMappings")
    mappings["VP"] = "GiGaSensDetTrackerDD4hep/VPSDet"
    LHCbDD4hepCnvSvc().SensDetMappings = mappings

    # Add the necessary dd4hep includes for the VP. Also added all the
    # dependencies for material definitions that were identified using
    # trial and error. As the list is made unique before being added to
    # the xml this should be fine if they appear in multiple places
    from Gauss.Geometry import GaussGeometry

    GaussGeometry._listOfXMLIncludes_.append(f"VP/{compVersion}/VP.xml")
```

### Check if extra modifications are needed

This is more as a reminder. You have to make sure that the extraction hits classes and monitoring classes are implemented or modernized. Moreover, some sub-detectors use non-standard classes that might need additional changes for the Detector/DD4hep implementation. This has to be checked.

### Create a MR in Gauss targeting master branch

Simply follow the same procedure as in: [](../getting_started/contributing.md#developing-gauss).

### Put the sub-detector in the nightly and LHCbPR tests

```{eval-rst}
.. attention::
    The generic way of doing this is a work-in-progress.
```
