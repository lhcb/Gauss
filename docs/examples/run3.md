# Run 3 setup

Below you will find the minimum set of options to run 100 minbias events with either DetDesc or DD4hep

## DetDesc

```python
from Configurables import Gauss
Gauss.EvtMax = 100

from Gaudi.Configuration import importOptions
importOptions("$GAUSSOPTS/General/2022.py")
importOptions("$GAUSSOPTS/Geometry/DetDesc.py")
importOptions("$DECFILESROOT/options/30000000.py")

# (optional) activate multi-threading
importOptions("$GAUSSOPTS/General/Threads-8.py")
```

## DD4hep

```python
from Configurables import Gauss
Gauss.EvtMax = 100

from Gaudi.Configuration import importOptions
importOptions("$GAUSSOPTS/General/2022.py")
importOptions("$GAUSSOPTS/Geometry/DD4hep.py")
importOptions("$DECFILESROOT/options/30000000.py")

# (optional) activate multi-threading
importOptions("$GAUSSOPTS/General/Threads-8.py")
```
