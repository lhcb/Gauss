# Geometry

Configuration related to the geometry used during the simulation phase.

## Main Geometry Configuration

```{eval-rst}
.. currentmodule:: Gauss.Geometry

.. autoclass:: GaussGeometry
   :show-inheritance:
   :undoc-members:
   :special-members: __apply_configuration__
   :members:
   :private-members:
```

## Validating geometry

### Overlap checks

Geant4 provides an internal tool to look for the overlaps between volumes in the geometry description. It will scan the whole hierarchy of volumes and will print out in the console if any overlaps were found or not.

We have to add a few internal Geant4 UI commands that will be triggered at the beginning of the run:

```python
from Configurables import GaussSimulation
GaussSimulation().G4BeginRunCommand += [
    "/geometry/test/verbosity true", # increase verbosity
    "/geometry/test/tolerance 0 mm", # tolerance of overlaps
    "/geometry/test/recursion_start 0", # 0 - > beginning of the hierarchy
    "/geometry/test/recursion_depth 5", # -1 -> will scan the whole tree
    "/geometry/test/maximum_errors 1",
    "/geometry/test/run",
]
```

If there are no overlaps, you should see something like this:

```
G4WT0 > Checking overlaps for volume AV_3570!lvMuon_1#1!Copper30micBox_8#8 (G4Box) ... OK!
```

On the other hand, if an overlap was found, the output should look like:

```
-------- WWWW ------- G4Exception-START -------- WWWW -------
*** G4Exception : GeomVol1002
      issued by : G4PVPlacement::CheckOverlaps()
Overlap with volume already placed !
          Overlap is detected for volume AV_3570!lvMuon_1#1!SupportPanel_0#0:1502 (G4SubtractionSolid)
          with AV_3570!lvMuon_1#1!lvM5ASideBeamPlug_10#10:12 (G4SubtractionSolid) volume's
          local point (-80.8058,-221.315,-17.9971), overlapping by at least: 1.5685 cm
NOTE: Reached maximum fixed number -1- of overlaps reports for this volume !
*** This is just a warning message. ***
-------- WWWW -------- G4Exception-END --------- WWWW -------
```

## Available detectors

### BCM

```{eval-rst}
.. currentmodule:: Gauss.Detectors.BCM

.. autoclass:: BCM
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### BeamPipe

```{eval-rst}
.. currentmodule:: Gauss.Detectors.BeamPipe

.. autoclass:: BeamPipe
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### BLS

```{eval-rst}
.. currentmodule:: Gauss.Detectors.BLS

.. autoclass:: BLS
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### CALO

```{eval-rst}
.. currentmodule:: Gauss.Detectors.CALO

.. autoclass:: ECAL
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:

.. autoclass:: HCAL
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:

.. autoclass:: PRS
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:

.. autoclass:: SPD
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### FT

```{eval-rst}
.. currentmodule:: Gauss.Detectors.FT

.. autoclass:: FT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### HC

```{eval-rst}
.. currentmodule:: Gauss.Detectors.HC

.. autoclass:: HC
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### IT

```{eval-rst}
.. currentmodule:: Gauss.Detectors.IT

.. autoclass:: IT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### Magnet

```{eval-rst}
.. currentmodule:: Gauss.Detectors.Magnet

.. autoclass:: Magnet
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### Muon

```{eval-rst}
.. currentmodule:: Gauss.Detectors.Muon

.. autoclass:: Muon
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### OT

```{eval-rst}
.. currentmodule:: Gauss.Detectors.OT

.. autoclass:: OT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### PuVeto

```{eval-rst}
.. currentmodule:: Gauss.Detectors.PuVeto

.. autoclass:: PuVeto
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### RICH

```{eval-rst}
.. currentmodule:: Gauss.Detectors.RICH

.. autoclass:: RICH1
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:

.. autoclass:: RICH1PMT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:

.. autoclass:: RICH2
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:

.. autoclass:: RICH2PMT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### TT

```{eval-rst}
.. currentmodule:: Gauss.Detectors.TT

.. autoclass:: TT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```


### Shield

```{eval-rst}
.. currentmodule:: Gauss.Detectors.Shield

.. autoclass:: Shield
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### SL

```{eval-rst}
.. currentmodule:: Gauss.Detectors.SL

.. autoclass:: SL
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### UT

```{eval-rst}
.. currentmodule:: Gauss.Detectors.UT

.. autoclass:: UT
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### Velo

```{eval-rst}
.. currentmodule:: Gauss.Detectors.Velo

.. autoclass:: Velo
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```

### VP

```{eval-rst}
.. currentmodule:: Gauss.Detectors.VP

.. autoclass:: VP
   :show-inheritance:
   :undoc-members:
   :members:
   :private-members:
```
