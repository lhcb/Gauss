# Generation
Configuration related to the generation phase.

## Main Generation Configuration

```{eval-rst}
.. currentmodule:: Gauss.Generation

.. autoclass:: GaussGeneration
   :show-inheritance:
   :undoc-members:
   :special-members: __apply_configuration__
   :members:
   :private-members:
```
