# Simulation

Configuration related to the simulation phase.

## Main Simulation Configuration

```{eval-rst}
.. currentmodule:: Gauss.Simulation

.. autoclass:: GaussSimulation
   :show-inheritance:
   :undoc-members:
   :special-members: __apply_configuration__
   :members:
   :private-members:
```
