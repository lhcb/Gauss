# Gauss

## Main configuration

```{eval-rst}
.. currentmodule:: Gauss.Configuration

.. autoclass:: Gauss
   :show-inheritance:
   :undoc-members:
   :special-members: __apply_configuration__
   :members:
   :private-members:
```
