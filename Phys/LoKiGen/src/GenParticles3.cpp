/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Algs.h"
// ============================================================================
// LoKiGen
// ============================================================================
#include "HepMCUtils/HepMCUtils.h"
#include "HepMCUtils/Relatives.h"
#include "LoKi/GenAlgs.h"
#include "LoKi/GenExtract.h"
#include "LoKi/GenParticles3.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-01-28
 *
 */
// ============================================================================
namespace {
  // ==========================================================================
  inline std::size_t count_parts( const HepMC3::ConstGenVertexPtr& v, const HepMC3::Relatives& r,
                                  const LoKi::GenTypes::GCut& c ) {
    auto particles = r( v );
    return v ? LoKi::Algs::count_if( std::begin( particles ), std::end( particles ), c.func() ) : 0;
  }
  // ==========================================================================
  inline bool has_parts( const HepMC3::ConstGenVertexPtr& v, const HepMC3::Relatives& r,
                         const LoKi::GenTypes::GCut& c ) {
    auto particles = r( v );
    return v && LoKi::Algs::found( std::begin( particles ), std::end( particles ), c.func() );
  }
  // ==========================================================================
} // namespace

namespace LoKi {
  namespace GenParticles {
    // ============================================================================
    /*  constructor from the criteria and "range"
     *  @param cut the criteria
     *  @param range search region
     *  @see const HepMC3::Relatives &
     */
    // ============================================================================
    Count::Count( const LoKi::Types::GCuts& cut, const HepMC3::Relatives& range ) : m_cut( cut ), m_range( range ) {}
    Count::Count( const LoKi::Types::GCuts& cut, int range_idx )
        : Count( cut, HepMCUtils::RelativesFromID( range_idx ) ) {}
    // ============================================================================
    /*  constructor from the criteria and "range"
     *  @param cut the criteria
     *  @param range search region
     *  @see const HepMC3::Relatives &
     */
    // ============================================================================
    Count::Count( const HepMC3::Relatives& range, const LoKi::Types::GCuts& cut ) : m_cut( cut ), m_range( range ) {}
    Count::Count( int range_idx, const LoKi::Types::GCuts& cut )
        : Count( HepMCUtils::RelativesFromID( range_idx ), cut ) {}
    // ============================================================================
    // MANDATORY: clone method ("virtual contructor")
    // ============================================================================
    Count* Count::clone() const { return new Count( *this ); }
    // ============================================================================
    // MANDATORY: the only one essential method
    // ============================================================================
    double Count::operator()( const HepMC3::ConstGenParticlePtr& p ) const {
      //
      if ( !p ) {
        Error( "HepMC3::GenParticlePtr points to NULL, return -1000 " )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        return -1000;
      }
      // This should now automatically select the correct vertex
      return count_parts( p->production_vertex(), m_range, m_cut ); // RETURN
    }
    // ============================================================================
    //  "SHORT" representation, @see LoKi::AuxFunBase
    // ============================================================================
    std::ostream& Count::fillStream( std::ostream& stream ) const {
      stream << " GCOUNT(" << m_cut << ",";
      stream << HepMCUtils::RelationToString( m_range );
      return stream << ") ";
    }
    // ============================================================================

    // ============================================================================
    /*  constructor from the criteria and "range"
     *  @param cut the criteria
     *  @param range search region
     *  @see const HepMC3::Relatives &
     */
    // ============================================================================
    Has::Has( const LoKi::Types::GCuts& cut, const HepMC3::Relatives& range ) : m_cut( cut ), m_range( range ) {}
    Has::Has( const LoKi::Types::GCuts& cut, int range_idx ) : Has( cut, HepMCUtils::RelativesFromID( range_idx ) ) {}
    // ============================================================================
    /*  constructor from the criteria and "range"
     *  @param cut the criteria
     *  @param range search region
     *  @see const HepMC3::Relatives &
     */
    // ============================================================================
    Has::Has( const HepMC3::Relatives& range, const LoKi::Types::GCuts& cut ) : m_cut( cut ), m_range( range ) {}
    Has::Has( int range_idx, const LoKi::Types::GCuts& cut ) : Has( HepMCUtils::RelativesFromID( range_idx ), cut ) {}
    // ============================================================================
    // MANDATORY: clone method ("virtual contructor")
    // ============================================================================
    Has* Has::clone() const { return new Has( *this ); }
    // ============================================================================
    // MANDATORY: the only one essential method
    // ============================================================================
    bool Has::operator()( const HepMC3::ConstGenParticlePtr& p ) const {
      //
      if ( !p ) {
        Error( "HepMC3::GenParticlePtr points to NULL, return false" )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        return false;
      }
      return has_parts( p->production_vertex(), m_range, m_cut ); // RETURN
    }
    // ============================================================================
    //  "SHORT" representation, @see LoKi::AuxFunBase
    // ============================================================================
    std::ostream& Has::fillStream( std::ostream& stream ) const {
      stream << " GHAS(" << m_cut << ",";
      stream << HepMCUtils::RelationToString( m_range );
      return stream << ") ";
    }

  } // namespace GenParticles
} // namespace LoKi

// ============================================================================
// The END
// ============================================================================
