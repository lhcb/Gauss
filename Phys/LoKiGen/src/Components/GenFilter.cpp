/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "GaudiAlg/FilterPredicate.h"
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include <vector>
// ============================================================================
// LoKi
// ============================================================================
#include "Defaults/Locations.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "LoKi/FilterAlg.h"
#include "LoKi/GenTypes.h"
#include "LoKi/IGenHybridFactory.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 */
// ============================================================================
namespace LoKi {
  namespace {
    // ==========================================================================
    LoKi::BasicFunctors<LoKi::GenTypes::GenContainer>::BooleanConstant s_NONE{ false };
    // ==========================================================================
  } // namespace
  // ==========================================================================
  /** @class ODINFilter
   *  Simple filtering algorithm bases on LoKi/Bender "hybrid" framework
   *  for filtering according to Generator (HepMC3) information
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date 2011-06-02
   */
  class GenFilter : public Gaudi::Functional::FilterPredicate<bool( std::vector<HepMC3::GenEvent> const& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<LoKi::FilterAlg>> {
  public:
    // ========================================================================
    /// the main method: execute
    bool       operator()( std::vector<HepMC3::GenEvent> const& ) const override;
    StatusCode finalize() override;
    // ========================================================================
  public:
    // ========================================================================
    /** Decode the functor (use the factory)
     *  @see LoKi::FilterAlg
     *  @see LoKi::FilterAlg::decode
     *  @see LoKi::FilterAlg::i_decode
     */
    StatusCode decode() override {
      StatusCode sc = i_decode<LoKi::IGenHybridFactory>( m_cut );
      Assert( sc.isSuccess(), "Unable to decode the functor!" );
      return sc;
    }
    // ========================================================================
    /** standard constructor
     *  @see LoKi::FilterAlg
     *  @see GaudiAlgorithm
     *  @see      Algorithm
     *  @see      AlgFactory
     *  @see     IAlgFactory
     *  @param name the algorithm instance name
     *  @param pSvc pointer to Service Locator
     */
    GenFilter( const std::string& name, // the algorithm instance name
               ISvcLocator*       pSvc );     // pointer to the service locator
    // ========================================================================
  private:
    mutable Gaudi::Accumulators::BinomialCounter<> m_passed{ this, "#passed" };
    // ========================================================================
    /// the functor itself
    LoKi::Types::GCutVal m_cut{ s_NONE }; // the functor itself
    // ========================================================================
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// ============================================================================
/* standard constructor
 *  @see LoKi::FilterAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see      AlgFactory
 *  @see     IAlgFactory
 *  @param name the algorithm instance name
 *  @param pSvc pointer to Service Locator
 */
// ===========================================================================
LoKi::GenFilter::GenFilter( const std::string& name, // the algorithm instance name
                            ISvcLocator*       pSvc )      // pointer to the service locator
    : FilterPredicate{ name, pSvc, { "Location", Gaussino::HepMCEventLocation::Default } } {
  //
  StatusCode sc = setProperty( "Code", "~GEMPTY" );
  Assert( sc.isSuccess(), "Unable (re)set property 'Code'", sc );
  sc = setProperty( "Factory", "LoKi::Hybrid::GenTool/GenFactory:PUBLIC" );
  Assert( sc.isSuccess(), "Unable (re)set property 'Factory'", sc );
}
// ============================================================================
// finalize
// ============================================================================
StatusCode LoKi::GenFilter::finalize() {
  m_cut = s_NONE;
  return FilterPredicate::finalize();
}
// ============================================================================
// the main method: execute
// ============================================================================
bool LoKi::GenFilter::operator()( std::vector<HepMC3::GenEvent> const& events ) const {
  if ( updateRequired() ) {
    StatusCode sc = const_cast<LoKi::GenFilter*>( this )->decode();
    Assert( sc.isSuccess(), "Unable to decode the functor!" );
  }
  //
  // copy all particles into single vector
  //
  LoKi::GenTypes::GenContainer particles;
  for ( auto& event : events ) {
    particles.insert( particles.end(), std::begin( event.particles() ), std::end( event.particles() ) );
  }
  //
  // use the functor
  //
  const bool result = m_cut( particles );
  //
  // some statistics
  //
  m_passed += result;
  //
  // set the filter:
  //
  return result;
}
// ============================================================================
/// the factory (needed for instantiation)
DECLARE_COMPONENT( LoKi::GenFilter )
// ============================================================================
// The END
// ============================================================================
