/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// HepMC3
// ============================================================================
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
// ============================================================================
// local
// ============================================================================
#include "LoKi/GenOscillated.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 *  @date 2011-11-11
 *
 */
// ===========================================================================
/* check the oscillation for the particle
 *  @param p the particle
 *  @return the oscillated mother particle
 *  @author Vanya BELYAEV  Ivan.Belyaev@nikhef.nl
 *  @date 2008-06-03
 */
// ============================================================================
const HepMC3::ConstGenParticlePtr LoKi::GenParticles::oscillated1( const HepMC3::ConstGenParticlePtr& p ) {
  if ( !p ) { return nullptr; }
  auto pv = p->production_vertex();
  if ( !pv ) { return nullptr; }
  if ( 1 != pv->particles_in().size() ) { return nullptr; }
  auto m = *std::cbegin( pv->particles_in() );
  if ( !m ) { return nullptr; }
  // oscillated ?
  if ( p->pdg_id() != -m->pdg_id() ) { return nullptr; }
  //
  return m;
}
// ============================================================================
/*  check the oscillation for the particle
 *  @param p the particle
 *  @return the oscillated daughter particle
 *  @author Vanya BELYAEV  Ivan.Belyaev@nikhef.nl
 *  @date 2008-06-03
 */
const HepMC3::ConstGenParticlePtr LoKi::GenParticles::oscillated2( const HepMC3::ConstGenParticlePtr& p ) {
  if ( !p ) { return nullptr; }
  auto ev = p->end_vertex();
  if ( !ev ) { return nullptr; }
  if ( 1 != ev->particles_out().size() ) { return nullptr; }
  auto d = *std::cbegin( ev->particles_out() );
  if ( !d ) { return nullptr; }
  // oscillated ?
  if ( p->pdg_id() != -d->pdg_id() ) { return nullptr; }
  //
  return d;
}
// ==========================================================================
// The END
// ==========================================================================
