/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/GenAlgsDicts.h"
#include "LoKi/GenAlgs.h"
#include "LoKi/GenTypes.h"
// ============================================================================
/** @file
 *  Implementation file for namespace LoKi::Dicts::GenAlgs
 *  @date 2007-06-03
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
// count the particles in the event
// ============================================================================
std::size_t LoKi::Dicts::GenAlgs::count_if( const HepMC3::GenEvent* event, const LoKi::GenTypes::GCuts& cuts ) {
  return LoKi::GenAlgs::count_if( event, cuts );
}
// ============================================================================
// count the particles in the tree
// ============================================================================
std::size_t LoKi::Dicts::GenAlgs::count_if( const HepMC3::ConstGenParticlePtr& particle,
                                            const LoKi::GenTypes::GCuts&       cuts ) {
  return LoKi::GenAlgs::count_if( particle, cuts );
}
// ============================================================================
// count the particles in the tree
// ============================================================================
std::size_t LoKi::Dicts::GenAlgs::count_if( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GCuts& cuts,
                                            const HepMC3::Relatives& range ) {
  return LoKi::GenAlgs::count_if( vertex, cuts, range );
}
// ============================================================================
//  check the presence in the event
// ============================================================================
bool LoKi::Dicts::GenAlgs::found( const HepMC3::GenEvent* event, const LoKi::GenTypes::GCuts& cuts ) {
  return LoKi::GenAlgs::found( event, cuts );
}
// ============================================================================
//  check the presence in the tree
// ============================================================================
bool LoKi::Dicts::GenAlgs::found( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GCuts& cuts ) {
  return LoKi::GenAlgs::found( particle, cuts );
}
// ============================================================================
//  check the presence in the tree
// ============================================================================
bool LoKi::Dicts::GenAlgs::found( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GCuts& cuts,
                                  const HepMC3::Relatives& range ) {
  return LoKi::GenAlgs::found( vertex, cuts, range );
}
// ============================================================================
// accumulate through the addition
// ============================================================================
double LoKi::Dicts::GenAlgs::accumulate( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                                         const LoKi::GenTypes::GCuts& cut, double res ) {
  return LoKi::GenAlgs::accumulate( event, fun, cut, res, std::plus<>() );
}
// ============================================================================
// accumulate through the addition
// ============================================================================
double LoKi::Dicts::GenAlgs::accumulate( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GFunc& fun,
                                         const LoKi::GenTypes::GCuts& cut, const HepMC3::Relatives& range,
                                         double res ) {
  return LoKi::GenAlgs::accumulate( vertex, fun, cut, res, range, std::plus<>() );
}
// ============================================================================
// accumulate through the addition
// ============================================================================
double LoKi::Dicts::GenAlgs::accumulate( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GFunc& fun,
                                         const LoKi::GenTypes::GCuts& cut, const double res ) {
  return LoKi::GenAlgs::accumulate( particle, fun, cut, res, std::plus<>() );
}
// ============================================================================
// find minimal value over the event
// ============================================================================
double LoKi::Dicts::GenAlgs::min_value( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                                        const LoKi::GenTypes::GCuts& cut, double res ) {
  return LoKi::GenAlgs::min_value( event, fun, cut, res );
}
// ============================================================================
// find minimal value over the tree:
// ============================================================================
double LoKi::Dicts::GenAlgs::min_value( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GFunc& fun,
                                        const LoKi::GenTypes::GCuts& cut, const HepMC3::Relatives& range, double res ) {
  return LoKi::GenAlgs::min_value( vertex, fun, cut, range, res );
}
// ============================================================================
// find minimal value over the tree:
// ============================================================================
double LoKi::Dicts::GenAlgs::min_value( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GFunc& fun,
                                        const LoKi::GenTypes::GCuts& cut, double res ) {
  return LoKi::GenAlgs::min_value( particle, fun, cut, res );
}
// ============================================================================
// find the maximal value over the event
// ============================================================================
double LoKi::Dicts::GenAlgs::max_value( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                                        const LoKi::GenTypes::GCuts& cut, double res ) {
  return LoKi::GenAlgs::max_value( event, fun, cut, res );
}
// ============================================================================
// find the maximal value over the tree
// ============================================================================
double LoKi::Dicts::GenAlgs::max_value( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GFunc& fun,
                                        const LoKi::GenTypes::GCuts& cut, double res ) {
  return LoKi::GenAlgs::max_value( particle, fun, cut, res );
}
// ============================================================================
// find the maximal value over the tree:
// ============================================================================
double LoKi::Dicts::GenAlgs::max_value( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GFunc& fun,
                                        const LoKi::GenTypes::GCuts& cut, const HepMC3::Relatives& range, double res ) {
  return LoKi::GenAlgs::max_value( vertex, fun, cut, range, res );
}

// ============================================================================
// find the minimum element through the event
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::Dicts::GenAlgs::min_element( const HepMC3::GenEvent*      event,
                                                               const LoKi::GenTypes::GFunc& fun,
                                                               const LoKi::GenTypes::GCuts& cut ) {
  return LoKi::GenAlgs::min_element( event, fun, cut );
}
// ============================================================================
//  find the minimum element through the tree
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::Dicts::GenAlgs::min_element( const HepMC3::ConstGenParticlePtr& particle,
                                                               const LoKi::GenTypes::GFunc&       fun,
                                                               const LoKi::GenTypes::GCuts&       cut ) {
  return LoKi::GenAlgs::min_element( particle, fun, cut );
}
// ============================================================================
//  find the minimum element through the treec
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::Dicts::GenAlgs::min_element( const HepMC3::ConstGenVertexPtr& vertex,
                                                               const LoKi::GenTypes::GFunc&     fun,
                                                               const LoKi::GenTypes::GCuts&     cut,
                                                               const HepMC3::Relatives&         range ) {
  return LoKi::GenAlgs::min_element( vertex, fun, cut, range );
}

// ============================================================================
// find the minimum element through the event
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::Dicts::GenAlgs::max_element( const HepMC3::GenEvent*      event,
                                                               const LoKi::GenTypes::GFunc& fun,
                                                               const LoKi::GenTypes::GCuts& cut ) {
  return LoKi::GenAlgs::max_element( event, fun, cut );
}
// ============================================================================
//  find the minimum element through the tree
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::Dicts::GenAlgs::max_element( const HepMC3::ConstGenParticlePtr& particle,
                                                               const LoKi::GenTypes::GFunc&       fun,
                                                               const LoKi::GenTypes::GCuts&       cut ) {
  return LoKi::GenAlgs::max_element( particle, fun, cut );
}
// ============================================================================
//  find the minimum element through the treec
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::Dicts::GenAlgs::max_element( const HepMC3::ConstGenVertexPtr& vertex,
                                                               const LoKi::GenTypes::GFunc&     fun,
                                                               const LoKi::GenTypes::GCuts&     cut,
                                                               const HepMC3::Relatives&         range ) {
  return LoKi::GenAlgs::max_element( vertex, fun, cut, range );
}

// ============================================================================
// The END
// ============================================================================
