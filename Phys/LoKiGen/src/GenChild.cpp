/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BuildGenTrees.h"
#include "LoKi/GenChild.h"
#include "LoKi/GenTypes.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-01-23
 */
// ============================================================================
/*  get the number of children for the given HepMC3-particle
 *  @see HepMC3::GenParticle
 *  @param  mother pointer to HepMC3-particle
 *  @return number of chilren
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-06-02
 */
// ============================================================================
std::size_t LoKi::GenChild::nChildren( const HepMC3::ConstGenParticlePtr& mother ) {
  if ( 0 == mother ) { return 0; } // RETURN
  auto ev = mother->end_vertex();
  if ( 0 == ev ) { return 0; } // RETURN
  auto particles = HepMC3::Relatives::CHILDREN( ev );
  return particles.size();
}
// ============================================================================
/*  Trivial accessor to the daughter "decay" particles for the given
 *   HepMC3-particle
 *
 *  @attention index starts with 1 , null index corresponds
 *             to the original particle
 *
 *  @param  particle (const) pointer to mother particle
 *  @param  index    index   index of the child particle
 *  @return daughter particle with given index
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-06-02
 */
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::GenChild::child( const HepMC3::ConstGenParticlePtr& mother, const size_t index ) {
  if ( 0 == mother ) { return 0; }     // RETURN
  if ( 0 == index ) { return mother; } // RETURN
  auto ev = mother->end_vertex();
  if ( 0 == ev ) { return 0; } // RETURN
  auto   particles = HepMC3::Relatives::CHILDREN( ev );
  auto   begin     = std::begin( particles );
  auto   end       = std::end( particles );
  size_t curr      = index - 1;
  while ( begin != end && 0 < curr ) {
    ++begin;
    --curr;
  }
  if ( begin != end && 0 == curr ) { return *begin; } // REUTRN
  return 0;                                           // RETURN
}
// ============================================================================
/*  get all particles form the given vertex form the given range
 *  @see HepMC3::GenVertex::particles_begin
 *  @see HepMC3::GenVertex::particles_end
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
size_t LoKi::GenChild::particles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range,
                                  std::vector<HepMC3::ConstGenParticlePtr>& output ) {
  if ( !output.empty() ) { output.clear(); }
  if ( 0 == vertex ) { return output.size(); }
  LoKi::GenTypes::GenSet gset;
  particles( vertex, range, gset );
  output.insert( output.end(), std::begin( gset ), std::end( gset ) );
  return output.size();
}
// ============================================================================
/*  get all particles form the given vertex form the given range
 *  @see HepMC3::GenVertex::particles_begin
 *  @see HepMC3::GenVertex::particles_end
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
size_t LoKi::GenChild::particles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range,
                                  LoKi::GenTypes::GenSet& output ) {
  if ( !output.empty() ) { output.clear(); }
  if ( 0 == vertex ) { return output.size(); }
  auto particles = range( vertex );
  output.insert( std::begin( particles ), std::end( particles ) );
  return output.size();
}
// ============================================================================
/*  get all "in"-particles for the given vertex
 *  @see HepMC3::GenVertex::particles_in_const_begin()
 *  @see HepMC3::GenVertex::particles_in_const_end()
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenChild::particles_in( const HepMC3::ConstGenVertexPtr& vertex ) {
  if ( 0 == vertex ) { return std::vector<HepMC3::ConstGenParticlePtr>(); }
  return std::vector<HepMC3::ConstGenParticlePtr>( std::begin( vertex->particles_in() ),
                                                   std::end( vertex->particles_in() ) );
}
// ============================================================================
/*  get all "out"-particles for the given vertex
 *  @see HepMC3::GenVertex::particles_in_const_begin()
 *  @see HepMC3::GenVertex::particles_in_const_end()
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ===========================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenChild::particles_out( const HepMC3::ConstGenVertexPtr& vertex ) {
  if ( 0 == vertex ) { return std::vector<HepMC3::ConstGenParticlePtr>(); }
  return std::vector<HepMC3::ConstGenParticlePtr>( std::begin( vertex->particles_out() ),
                                                   std::end( vertex->particles_out() ) );
}
// ============================================================================
/*  get all particles form the given event
 *  @see HepMC3::GenEvent::particles_begin
 *  @see HepMC3::GenEvent::particles_end
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenChild::particles_all( const HepMC3::GenEvent* event ) {
  if ( 0 == event ) { return std::vector<HepMC3::ConstGenParticlePtr>(); }
  return event->particles();
}
// ============================================================================
/*  get all vertices form the given event
 *  @see HepMC3::GenEvent::vertices_begin
 *  @see HepMC3::GenEvent::vertices_end
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
std::vector<HepMC3::ConstGenVertexPtr> LoKi::GenChild::vertices_all( const HepMC3::GenEvent* event ) {
  if ( 0 == event ) { return std::vector<HepMC3::ConstGenVertexPtr>(); }
  return event->vertices();
}
// ============================================================================
/*  get all "children" particles form the given particle
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
size_t LoKi::GenChild::daughters( const HepMC3::ConstGenParticlePtr&        particle,
                                  std::vector<HepMC3::ConstGenParticlePtr>& output ) {
  if ( !output.empty() ) { output.clear(); }
  if ( 0 == particle ) { return output.size(); } // RETURN
  return daughters( particle->end_vertex(), output );
}
// ============================================================================
/*  get all "children" particles form the given particle
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
size_t LoKi::GenChild::daughters( const HepMC3::ConstGenParticlePtr& particle, LoKi::GenTypes::GenSet& output ) {
  if ( !output.empty() ) { output.clear(); }
  if ( 0 == particle ) { return output.size(); } // RETURN
  return daughters( particle->end_vertex(), output );
}
// ============================================================================
/*  get all "ancestors" particles from the given particle
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenChild::ancestors( const HepMC3::ConstGenParticlePtr& particle ) {
  if ( 0 == particle ) { return std::vector<HepMC3::ConstGenParticlePtr>(); }
  return LoKi::GenChild::ancestors( particle->production_vertex() );
}
// ============================================================================
/*  get all "descendant" particles form the given particle
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-05-26
 */
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenChild::descendants( const HepMC3::ConstGenParticlePtr& particle ) {
  if ( 0 == particle ) { return std::vector<HepMC3::ConstGenParticlePtr>(); }
  return LoKi::GenChild::descendants( particle->end_vertex() );
}
// ============================================================================
/* get all independent decay trees from HepMC3::GenEvent
 *  @see LoKi::GenTrees::buildTrees
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2016-01-17
 */
// ============================================================================
LoKi::GenTypes::ConstVector LoKi::GenChild::trees( const HepMC3::GenEvent* event ) {
  if ( 0 == event ) { return LoKi::GenTypes::ConstVector(); }
  LoKi::GenTypes::ConstVector result;
  result.reserve( 128 );
  LoKi::GenTrees::buildTrees( std::begin( event->particles() ), std::end( event->particles() ),
                              std::back_inserter( result ) );
  return result;
}
// ========================================================================
/*  get all independent decay trees from container of particles
 *  @see LoKi::GenTrees::buildTrees
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2016-01-17
 */
// ============================================================================
LoKi::GenTypes::ConstVector LoKi::GenChild::trees( const LoKi::GenTypes::ConstVector& particles ) {
  LoKi::GenTypes::ConstVector result;
  result.reserve( particles.size() );
  LoKi::GenTrees::buildTrees( particles.begin(), particles.end(), std::back_inserter( result ) );
  return result;
}
// ========================================================================
/*  get all independent decay trees from container of particles
 *  @see LoKi::GenTrees::buildTrees
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2016-01-17
 */
// ============================================================================
LoKi::GenTypes::ConstVector LoKi::GenChild::trees( const LoKi::GenTypes::GRange& particles ) {
  LoKi::GenTypes::ConstVector result;
  result.reserve( particles.size() );
  LoKi::GenTrees::buildTrees( particles.begin(), particles.end(), std::back_inserter( result ) );
  return result;
}
// ========================================================================
namespace {
  // ==========================================================================
  template <class INDEX>
  HepMC3::ConstGenParticlePtr _child_( const HepMC3::ConstGenParticlePtr& particle, INDEX begin, INDEX end ) {
    //
    if ( 0 == particle ) { return 0; }       // RETURN
    if ( begin == end ) { return particle; } // RETURN
    //
    HepMC3::ConstGenParticlePtr daug = LoKi::GenChild::child( particle, *begin );
    //
    if ( 0 == daug ) { return 0; } // RETURN
    //
    return _child_( daug, begin + 1, end );
  }
  // ==========================================================================
} // namespace
// ============================================================================
HepMC3::ConstGenParticlePtr LoKi::GenChild::child( const HepMC3::ConstGenParticlePtr& particle,
                                                   const std::vector<unsigned int>&   indices ) {
  return _child_( particle, indices.begin(), indices.end() );
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
