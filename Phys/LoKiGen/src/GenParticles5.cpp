/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/LHCbMath.h"
// ============================================================================
// LoKi
// ============================================================================
#include "Defaults/HepMCAttributes.h"
#include "HepMCUser/VertexAttribute.h"
#include "LoKi/Constants.h"
#include "LoKi/GenParticles5.h"
#include "LoKi/Geometry.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2013-04-14
 *
 */
namespace LoKi {
  namespace GenParticles {
    // ============================================================================
    /** constructor from one
     *  @param primary use primary vertex, otherwise use own vertex
     */
    // ============================================================================
    Flight::Flight( const bool primary ) : m_primary( primary ) {}
    // ============================================================================
    // MANDATORY: clone method ("virtual destructor")
    // ============================================================================
    Flight* Flight::clone() const { return new Flight( *this ); }
    // ============================================================================
    // OPTIONAL: nice printout
    // ============================================================================
    std::ostream& Flight::fillStream( std::ostream& s ) const { return s << ( m_primary ? "GPVFLIGHT" : "GFLIGHT" ); }
    // ============================================================================
    // MANDATORY: the only one essential method
    // ============================================================================
    double Flight::operator()( const HepMC3::ConstGenParticlePtr& p ) const {
      //
      if ( !p ) {
        Error( "HepMC3::GenParticlePtr points to NULL, return -1*km " )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        return -1 * Gaudi::Units::km;
      }
      // end vertex
      HepMC3::ConstGenVertexPtr ev = p->end_vertex();
      //
      if ( !ev ) {
        Error( "HepMC3::GenParticle::end_vertex points to NULL, return +1*km " )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        return Gaudi::Units::km;
      }
      //
      HepMC3::ConstGenVertexPtr v0 = nullptr;
      if ( primary() ) {
        //
        const HepMC3::GenEvent* evt = p->parent_event();
        if ( !evt ) {
          Error( "HepMC3::GenParticle::parent_event points to NULL, return -1*km " )
              .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
          return -1 * Gaudi::Units::km;
        }
        //
        v0 = evt->attribute<HepMC3::VertexAttribute>( Gaussino::HepMC::Attributes::SignalProcessVertex )->value();
        if ( !v0 ) {
          Error( "HepMC3::GenEvent::signal_process_vertex points to NULL, return -1*km " )
              .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
          return -1 * Gaudi::Units::km;
        }
        //
      } else {
        //
        v0 = p->production_vertex();
        if ( !v0 ) {
          Error( "HepMC3::GenParticle::production_vertex points to NULL, return -1*km " )
              .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
          return -1 * Gaudi::Units::km;
        }
        //
      }
      //
      // calculate the distance
      if ( ev == v0 ) { return 0; }
      //
      const LoKi::Point3D ed( ev->position() );
      const LoKi::Point3D eo( v0->position() );
      //
      return ( ed - eo ).R();
    }

  } // namespace GenParticles
} // namespace LoKi
// ============================================================================
// The END
// ============================================================================
