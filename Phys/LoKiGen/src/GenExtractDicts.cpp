/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// HepMC3
// ============================================================================
#ifdef __INTEL_COMPILER
#  pragma warning( disable : 1572 ) // floating-point equality and inequality comparisons are unreliable
#  pragma warning( push )
#endif
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMCUtils/Relatives.h"
#ifdef __INTEL_COMPILER
#  pragma warning( pop )
#endif
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/GenExtractDicts.h"
#include "LoKi/Objects.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Dicts::GenExtract
 *  @date 2007-06-03
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
// extract the particles from the event
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenExtract::genParticles( const HepMC3::GenEvent*      event,
                                                                         const LoKi::GenTypes::GCuts& cuts ) {
  std::vector<HepMC3::ConstGenParticlePtr> result;
  LoKi::Extract::genParticles( event, std::back_inserter( result ), cuts );
  return result;
}
// ============================================================================
// extract the particles from the vertex
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenExtract::genParticles( const HepMC3::ConstGenVertexPtr& vertex,
                                                                         const LoKi::GenTypes::GCuts&     cuts,
                                                                         const HepMC3::Relatives&         range ) {
  std::vector<HepMC3::ConstGenParticlePtr> result;
  LoKi::Extract::genParticles( vertex, range, std::back_inserter( result ), cuts );
  return result;
}
// ============================================================================
// extract the particles from the vertex
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenExtract::genParticles( const HepMC3::ConstGenVertexPtr& vertex,
                                                                         const HepMC3::Relatives&         range ) {
  std::vector<HepMC3::ConstGenParticlePtr> result;
  LoKi::Extract::genParticles( vertex, range, std::back_inserter( result ), LoKi::Objects::_VALID_ );
  return result;
}
// ============================================================================
// extract the particles from the event
// ============================================================================
std::vector<HepMC3::ConstGenParticlePtr> LoKi::GenExtract::genParticles( const HepMC3::ConstGenParticlePtr& particle,
                                                                         const LoKi::GenTypes::GCuts&       cuts ) {
  std::vector<HepMC3::ConstGenParticlePtr> result;
  LoKi::Extract::genParticles( particle, std::back_inserter( result ), cuts );
  return result;
}
// ============================================================================
// The END
// ============================================================================
