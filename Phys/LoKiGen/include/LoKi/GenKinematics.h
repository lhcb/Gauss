/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENKINEMATICS_H
#define LOKI_GENKINEMATICS_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Kinematics.h"
// ============================================================================
// HepMC3
// ============================================================================
#ifdef __INTEL_COMPILER
#  pragma warning( disable : 1572 ) // floating-point equality and inequality comparisons are unreliable
#  pragma warning( push )
#endif
#include "HepMC3/GenParticle.h"
#ifdef __INTEL_COMPILER
#  pragma warning( pop )
#endif
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2001-01-23
 */
// ============================================================================
namespace LoKi {
  namespace GenKinematics {
    // ========================================================================
    /** The most trivial function.
     *  It seems to be almost useless from the first sight, but
     *  effectivel it is useful in conjunction with
     *  algorithms, acting as "converter" of the particle
     *  into the 4-momentum
     *  @param p particle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline LoKi::LorentzVector momentum( const HepMC3::ConstGenParticlePtr& p ) {
      if ( 0 == p ) { return LoKi::LorentzVector(); }
      return LoKi::LorentzVector( p->momentum() );
    }
    // ========================================================================
    /** @struct Gen4Momentum
     *  the simple object which acts as a converter of HepMC3::GenPartile
     *  to LoKi::LorentzVector
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    struct Gen4Momentum {
      LoKi::LorentzVector operator()( const HepMC3::ConstGenParticlePtr& p ) const { return momentum( p ); }
    };
    // ========================================================================
    /** trivial function to evaluate the mass HepMC3::GenParticle
     *  @param  p particle
     *  @return invariant mass
     *  @see HepMC3::GenParticle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-01-17
     */
    double mass( const HepMC3::ConstGenParticlePtr& p );
    // ========================================================================
    /** trivial function to evaluate the mass HepMC3::GenParticle
     *  @param  p1 the first particle
     *  @param  p2 the second particle
     *  @return invariant mass
     *  @see HepMC3::GenParticle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-01-17
     */
    double mass( const HepMC3::ConstGenParticlePtr& p1, HepMC3::ConstGenParticlePtr p2 );
    // ========================================================================
    /** trivial function to evaluate the mass HepMC3::GenParticle
     *  @param  p1 the first particle
     *  @param  p2 the third particle
     *  @param  p3 the second particle
     *  @return invariant mass
     *  @see HepMC3::GenParticle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-01-17
     */
    double mass( const HepMC3::ConstGenParticlePtr& p1, HepMC3::ConstGenParticlePtr p2,
                 HepMC3::ConstGenParticlePtr p3 );
    // ========================================================================
    /** trivial function to evaluate the mass HepMC3::GenParticle
     *  @param  p1 the first particle
     *  @param  p2 the second particle
     *  @param  p3 the third  particle
     *  @param  p4 the fourth  particle
     *  @return invariant mass
     *  @see HepMC3::GenParticle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-01-17
     */
    double mass( const HepMC3::ConstGenParticlePtr& p1, HepMC3::ConstGenParticlePtr p2, HepMC3::ConstGenParticlePtr p3,
                 HepMC3::ConstGenParticlePtr p4 );
    // ========================================================================
  } // namespace GenKinematics
  // ==========================================================================
  namespace Kinematics {
    // ========================================================================
    // export the namespace into more general scope
    using namespace LoKi::GenKinematics;
    // ========================================================================
  } // namespace Kinematics
  // ==========================================================================
} // end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_GENKINEMATICS_H
