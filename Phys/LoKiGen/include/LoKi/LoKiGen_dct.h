/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIGEN_DCT_H
#define LOKI_LOKIGEN_DCT_H 1
// ============================================================================
// Include files
// ============================================================================
// HepMC3
// ============================================================================
#ifdef __INTEL_COMPILER
#  pragma warning( disable : 1572 ) // floating-point equality and inequality comparisons are unreliable
#  pragma warning( push )
#endif
// ============================================================================
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
// ============================================================================
#ifdef __INTEL_COMPILER
#  pragma warning( pop )
#endif
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Keeper.h"
#include "LoKi/LoKiGen.h"
#include "LoKi/Monitoring.h"
#include "LoKi/Operators.h"
#include "LoKi/TreeOps.h"
#include "LoKi/Trees.h"
#include "LoKi/UniqueKeeper.h"
// ============================================================================
#include "LoKi/Dicts.h"
#include "LoKi/GenAlgsDicts.h"
#include "LoKi/GenChildSelector.h"
#include "LoKi/GenDecayChain.h"
#include "LoKi/GenDecays.h"
#include "LoKi/GenDump.h"
#include "LoKi/GenExtractDicts.h"
#include "LoKi/GenMoniDicts.h"
#include "LoKi/GenParticles.h"
#include "LoKi/GenParticles2.h"
#include "LoKi/GenParticles3.h"
#include "LoKi/GenParticles4.h"
#include "LoKi/GenParticles5.h"
// ============================================================================
#include "LoKi/FinderDict.h"
#include "LoKi/IGenDecay.h"
// ============================================================================
#include "LoKi/GenHybridEngine.h"
#include "LoKi/IGenHybridTool.h"
// ============================================================================
/** @file
 *  The dictionaries for the package Phys/LoKiGen
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-01
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Functors {
    // ========================================================================
    // the specialized printout
    // ========================================================================
    template <>
    inline std::ostream& Empty<HepMC3::ConstGenVertexPtr>::fillStream( std::ostream& s ) const {
      return s << "GVEMPTY";
    }
    // ========================================================================
    // the specialized printout
    // ========================================================================
    template <>
    inline std::ostream& Empty<HepMC3::ConstGenParticlePtr>::fillStream( std::ostream& s ) const {
      return s << "GEMPTY ";
    }
    // ========================================================================
    // the specialized printout
    // ========================================================================
    template <>
    inline std::ostream& Size<HepMC3::ConstGenVertexPtr>::fillStream( std::ostream& s ) const {
      return s << "GVSIZE";
    }
    // ========================================================================
    // the specialized printout
    // ========================================================================
    template <>
    inline std::ostream& Size<HepMC3::ConstGenParticlePtr>::fillStream( std::ostream& s ) const {
      return s << "GSIZE ";
    }
    // ========================================================================
  } // namespace Functors
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Dicts {
    // ========================================================================
    template <>
    class FunCalls<HepMC3::GenParticle> {
    private:
      // ======================================================================
      typedef HepMC3::ConstGenParticlePtr                                Type;
      typedef LoKi::BasicFunctors<HepMC3::ConstGenParticlePtr>::Function Fun;
      typedef LoKi::details::result_t<Fun>                               result_type;
      // ======================================================================
    public:
      // ======================================================================
      // __call__
      static result_type __call__( const Fun& fun, const Type& o ) { return fun( o ); }
      // ======================================================================
      // __rrshift__
      static result_type __rrshift__( const Fun& fun, const Type& o ) { return o >> fun; }
      // __rrshift__
      static std::vector<result_type> __rrshift__( const Fun& fun, const LoKi::GenTypes::GenContainer& o ) {
        return o >> fun;
      }
      // ======================================================================
      // _rshift_
      static LoKi::FunctorFromFunctor<Type, double> __rshift__( const Fun&                           fun,
                                                                const LoKi::Functor<double, double>& o ) {
        return fun >> o;
      }
      // _rshift_
      static LoKi::FunctorFromFunctor<Type, bool> __rshift__( const Fun& fun, const LoKi::Functor<double, bool>& o ) {
        return fun >> o;
      }
      // ======================================================================
    };
    // ========================================================================
    template <>
    class CutCalls<HepMC3::GenParticle> {
    private:
      // ======================================================================
      typedef HepMC3::ConstGenParticlePtr          Type;
      typedef LoKi::BasicFunctors<Type>::Predicate Fun;
      typedef LoKi::details::result_t<Fun>         result_type;
      // ======================================================================
    public:
      // ======================================================================
      // __call__
      static result_type __call__( const Fun& fun, const Type& o ) { return fun( o ); }
      //
      // __call__ as filter
      //
      // __call__
      static LoKi::GenTypes::GenContainer __call__( const Fun& fun, const LoKi::GenTypes::GenContainer& o ) {
        return o >> fun;
      }
      // __call__
      static LoKi::GenTypes::GenContainer __call__( const Fun& fun, const LoKi::GenTypes::GRange& o ) {
        return o >> fun;
      }
      // __call__
      static LoKi::GenTypes::GenContainer __call__( const Fun& fun, const HepMC3::GenEvent* e ) { return e >> fun; }
      // ======================================================================
    public:
      // ======================================================================
      // __rrshift__
      static result_type __rrshift__( const Fun& fun, const Type& o ) { return o >> fun; }
      //
      // rrshift as "filter"
      //
      // __rrshift__
      static LoKi::GenTypes::GenContainer __rrshift__( const Fun& fun, const LoKi::GenTypes::GenContainer& o ) {
        return o >> fun;
      }
      // __rrshift__
      static LoKi::GenTypes::GenContainer __rrshift__( const Fun& fun, const LoKi::GenTypes::GRange& o ) {
        return o >> fun;
      }
      // __rrshift__
      static LoKi::GenTypes::GenContainer __rrshift__( const Fun& fun, const HepMC3::GenEvent* e ) { return e >> fun; }
      // ======================================================================
    public:
      // ======================================================================
      // __rshift__
      static LoKi::FunctorFromFunctor<Type, bool> __rshift__( const Fun& fun, const Fun& o ) { return fun >> o; }
      // ======================================================================
    };
    // ========================================================================
    template <>
    class FunCalls<HepMC3::GenVertex> {
    private:
      // ======================================================================
      typedef HepMC3::ConstGenVertexPtr           Type;
      typedef LoKi::BasicFunctors<Type>::Function Fun;
      typedef LoKi::details::result_t<Fun>        result_type;
      // ======================================================================
    public:
      // ======================================================================
      // __call__
      static result_type __call__( const Fun& fun, const Type& o ) { return fun( o ); }
      // ======================================================================
    public:
      // ======================================================================
      // __rrshift__
      static result_type __rrshift__( const Fun& fun, const Type& o ) { return o >> fun; }
      // __rrshift__
      static std::vector<result_type> __rrshift__( const Fun& fun, const LoKi::GenTypes::GenVContainer& o ) {
        return o >> fun;
      }
      // ======================================================================
    public:
      // ======================================================================
      // _rshift_
      static LoKi::FunctorFromFunctor<Type, double> __rshift__( const Fun&                           fun,
                                                                const LoKi::Functor<double, double>& o ) {
        return fun >> o;
      }
      // _rshift_
      static LoKi::FunctorFromFunctor<Type, bool> __rshift__( const Fun& fun, const LoKi::Functor<double, bool>& o ) {
        return fun >> o;
      }
      // ======================================================================
    };
    // ========================================================================
    template <>
    class CutCalls<HepMC3::GenVertex> {
    private:
      // ======================================================================
      typedef HepMC3::ConstGenVertexPtr            Type;
      typedef LoKi::BasicFunctors<Type>::Predicate Fun;
      typedef LoKi::details::result_t<Fun>         result_type;
      // ======================================================================
    public:
      // ======================================================================
      // __call__
      static result_type __call__( const Fun& fun, const Type& o ) { return fun( o ); }
      // ======================================================================
    public:
      // ======================================================================
      // __rrshift__
      static result_type __rrshift__( const Fun& fun, const Type& o ) { return o >> fun; }
      // __rrshift__
      static const LoKi::GenTypes::GenVContainer __rrshift__( const Fun& fun, const LoKi::GenTypes::GenVContainer& o ) {
        return o >> fun;
      }
      // ======================================================================
    public:
      // ======================================================================
      // __rrshift__
      static LoKi::FunctorFromFunctor<Type, bool> __rshift__( const Fun& fun, const Fun& o ) { return fun >> o; }
      // ======================================================================
    };
    // ========================================================================
  } // namespace Dicts
  // ==========================================================================
} // namespace LoKi
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Dicts {
    // ========================================================================
    template <>
    struct FinderDicts_<HepMC3::ConstGenParticlePtr> {
      // ======================================================================
      /// get the actual "stripped" type
      typedef HepMC3::ConstGenParticlePtr              TYPE;
      typedef HepMC3::ConstGenParticlePtr              Type;
      typedef std::vector<HepMC3::ConstGenParticlePtr> ConstVector;
      // ======================================================================
    public:
      // ======================================================================
      static bool __hasDecay__( const Decays::Finder_<TYPE>& finder, const ConstVector& input ) {
        return finder.hasDecay( input.begin(), input.end() );
      }
      // ======================================================================
      static bool __hasDecay__( const Decays::Finder_<TYPE>& finder, const HepMC3::GenEvent* event ) {
        return event && finder.hasDecay( std::begin( event->particles() ), std::end( event->particles() ) );
      }
      // ======================================================================
      static bool __hasDecay__( const Decays::Finder_<TYPE>& finder, HepMC3::ConstGenVertexPtr vertex,
                                const HepMC3::Relatives& range ) {
        if ( !vertex ) { return false; }
        auto particles = range( vertex );
        return finder.hasDecay( std::begin( particles ), std::end( particles ) );
      }
      // ======================================================================
    public:
      // ======================================================================
      static size_t __findDecay__( const Decays::Finder_<TYPE>& finder, const ConstVector& input,
                                   ConstVector& output ) {
        return finder.findDecay( input.begin(), input.end(), output );
      }
      // ======================================================================
      static size_t __findDecay__( const Decays::Finder_<TYPE>& finder, const HepMC3::GenEvent* event,
                                   ConstVector& output ) {
        return event ? finder.findDecay( std::begin( event->particles() ), std::end( event->particles() ), output ) : 0;
      }
      // ======================================================================
      static size_t __findDecay__( const Decays::Finder_<TYPE>& finder, HepMC3::ConstGenVertexPtr vertex,
                                   const HepMC3::Relatives& range, ConstVector& output ) {
        if ( !vertex ) { return false; }
        auto particles = range( vertex );
        return finder.findDecay( std::begin( particles ), std::end( particles ), output );
      }
      // ======================================================================
    public:
      // ======================================================================
      static ConstVector __findDecay__( const Decays::Finder_<TYPE>& finder, const ConstVector& input ) {
        ConstVector output;
        __findDecay__( finder, input, output );
        return output;
      }
      // ======================================================================
      static ConstVector __findDecay__( const Decays::Finder_<TYPE>& finder, const HepMC3::GenEvent* event ) {
        ConstVector output;
        __findDecay__( finder, event, output );
        return output;
      }
      // ======================================================================
      static ConstVector __findDecay__( const Decays::Finder_<TYPE>& finder, HepMC3::ConstGenVertexPtr vertex,
                                        const HepMC3::Relatives& range ) {
        ConstVector output;
        __findDecay__( finder, vertex, range, output );
        return output;
      }
      // ======================================================================
    };
    // ========================================================================
  } // namespace Dicts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace {
  // ==========================================================================
  struct _Instantiations {
    // ========================================================================
    /// fictive constructor
    _Instantiations();
    // ========================================================================
    // the basic types
    LoKi::Types::GRange  m_r1;
    LoKi::Types::GVRange m_r2;
    // rangelists:
    LoKi::Types::GRangeL  m_l1;
    LoKi::Types::GVRangeL m_l2;
    // keepers:
    LoKi::Keeper<HepMC3::GenParticle> m_k1;
    LoKi::Keeper<HepMC3::GenVertex>   m_k2;
    // unique keepers
    LoKi::UniqueKeeper<HepMC3::GenParticle> m_u1;
    LoKi::UniqueKeeper<HepMC3::GenVertex>   m_u2;
    // the basic functions
    LoKi::Dicts::Funcs<HepMC3::ConstGenParticlePtr>  m_f1;
    LoKi::Dicts::Funcs<HepMC3::ConstGenVertexPtr>    m_f2;
    LoKi::Dicts::VFuncs<HepMC3::ConstGenParticlePtr> m_f3;
    LoKi::Dicts::VFuncs<HepMC3::ConstGenVertexPtr>   m_f4;
    // operators
    LoKi::Dicts::FuncOps<HepMC3::ConstGenParticlePtr> m_o1;
    LoKi::Dicts::FuncOps<HepMC3::ConstGenVertexPtr>   m_o2;
    LoKi::Dicts::CutsOps<HepMC3::ConstGenParticlePtr> m_o3;
    LoKi::Dicts::CutsOps<HepMC3::ConstGenVertexPtr>   m_o4;
    // functional parts:
    LoKi::Dicts::MapsOps<HepMC3::ConstGenParticlePtr>   m_fo1;
    LoKi::Dicts::PipeOps<HepMC3::ConstGenParticlePtr>   m_fo2;
    LoKi::Dicts::FunValOps<HepMC3::ConstGenParticlePtr> m_fo3;
    LoKi::Dicts::CutValOps<HepMC3::ConstGenParticlePtr> m_fo31;
    LoKi::Dicts::MapsOps<HepMC3::ConstGenVertexPtr>     m_fo5;
    LoKi::Dicts::PipeOps<HepMC3::ConstGenVertexPtr>     m_fo6;
    LoKi::Dicts::FunValOps<HepMC3::ConstGenVertexPtr>   m_fo7;
    LoKi::Dicts::CutValOps<HepMC3::ConstGenVertexPtr>   m_fo71;
    LoKi::Dicts::SourceOps<HepMC3::ConstGenVertexPtr>   m_fo9;
    LoKi::Dicts::SourceOps<HepMC3::ConstGenParticlePtr> m_fo10;
    /// mathematics:
    LoKi::Dicts::FunCalls<HepMC3::GenParticle> m_c1;
    LoKi::Dicts::FunCalls<HepMC3::GenVertex>   m_c2;
    LoKi::Dicts::CutCalls<HepMC3::GenParticle> m_c3;
    LoKi::Dicts::CutCalls<HepMC3::GenVertex>   m_c4;
    /// the special operators for identifiers
    LoKi::Dicts::PIDOps<LoKi::GenParticles::Identifier>    m_i1;
    LoKi::Dicts::PIDOps<LoKi::GenParticles::AbsIdentifier> m_i2;
    /// same ?
    LoKi::TheSame<HepMC3::ConstGenParticlePtr> m_s1;
    LoKi::TheSame<HepMC3::ConstGenVertexPtr>   m_s2;
    /// trivia
    LoKi::Functors::Empty<HepMC3::ConstGenParticlePtr> m_e1;
    LoKi::Functors::Size<HepMC3::ConstGenParticlePtr>  m_si1;
    LoKi::Functors::Empty<HepMC3::ConstGenVertexPtr>   m_e2;
    LoKi::Functors::Size<HepMC3::ConstGenVertexPtr>    m_si2;
    // decay funders:
    Decays::Tree_<HepMC3::ConstGenParticlePtr>          m_tree1;
    Decays::Trees::Any_<HepMC3::ConstGenParticlePtr>    m_tree2;
    Decays::Trees::None_<HepMC3::ConstGenParticlePtr>   m_tree3;
    Decays::Trees::Stable_<HepMC3::ConstGenParticlePtr> m_tree4;
    LoKi::Dicts::TreeOps<HepMC3::ConstGenParticlePtr>   m_trops;
    // ========================================================================
    // Decay Finder
    // ========================================================================
    Decays::IGenDecay::Finder                              m_finder;
    Decays::IGenDecay::Tree                                m_tree;
    LoKi::Dicts::FinderDicts_<HepMC3::ConstGenParticlePtr> m_finderDicts;
    // ========================================================================
  };
  // ==========================================================================
} // end of anonymous namespace
// ============================================================================

// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIGEN_DCT_H
