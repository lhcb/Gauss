/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENALGSDICTS_H
#define LOKI_GENALGSDICTS_H
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "HepMC3/GenEvent.h"
#include "HepMCUtils/Relatives.h"

#include "LoKi/Constants.h"
#include "LoKi/GenTypes.h"
// ============================================================================
namespace LoKi {
  namespace Dicts {
    namespace GenAlgs {
      // ======================================================================
      /// count the particles in the event
      std::size_t count_if( const HepMC3::GenEvent* event, const LoKi::GenTypes::GCuts& cuts );
      // ======================================================================
      /// count the particles in the tree
      std::size_t count_if( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GCuts& cuts );
      // ======================================================================
      /// count the particles in the tree
      std::size_t count_if( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GCuts& cuts,
                            const HepMC3::Relatives& range = HepMCUtils::WrapperRelatives::DESCENDANTS );
      // ======================================================================
      /// check the presence in the event
      bool found( const HepMC3::GenEvent* event, const LoKi::GenTypes::GCuts& cuts );
      // ======================================================================
      /// check the presence in the tree
      bool found( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GCuts& cuts );
      // ======================================================================
      /// check the presence in the tree
      bool found( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GCuts& cuts,
                  const HepMC3::Relatives& range = HepMCUtils::WrapperRelatives::DESCENDANTS );
      // ======================================================================
      /// accumulate through the addition
      double accumulate( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                         const LoKi::GenTypes::GCuts& cut, double res = 0.0 );
      // ======================================================================
      /// accumulate through the addition
      double accumulate( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GFunc& fun,
                         const LoKi::GenTypes::GCuts& cut,
                         const HepMC3::Relatives& range = HepMCUtils::WrapperRelatives::DESCENDANTS, double res = 0.0 );
      // ======================================================================
      /// accumulate through the addition
      double accumulate( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GFunc& fun,
                         const LoKi::GenTypes::GCuts& cut, double res = 0.0 );
      // ======================================================================
      /// find minimal value over the event
      double min_value( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                        const LoKi::GenTypes::GCuts& cut, double res = LoKi::Constants::PositiveInfinity );
      // ======================================================================
      /// find minimal value over the tree:
      double min_value( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GFunc& fun,
                        const LoKi::GenTypes::GCuts& cut, double res = LoKi::Constants::PositiveInfinity );
      // ======================================================================
      /// find minimal value over the tree:
      double min_value( const HepMC3::ConstGenVertexPtr& veretx, const LoKi::GenTypes::GFunc& fun,
                        const LoKi::GenTypes::GCuts& cut,
                        const HepMC3::Relatives&     range = HepMCUtils::WrapperRelatives::DESCENDANTS,
                        double                       res   = LoKi::Constants::PositiveInfinity );
      // ======================================================================
      /// find maximum value over the event
      double max_value( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                        const LoKi::GenTypes::GCuts& cut, double res = LoKi::Constants::NegativeInfinity );
      // ======================================================================
      /// find maximum value over the tree
      double max_value( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenTypes::GFunc& fun,
                        const LoKi::GenTypes::GCuts& cut, double res = LoKi::Constants::NegativeInfinity );
      // ======================================================================
      /// find maximum value over the tree
      double max_value( const HepMC3::ConstGenVertexPtr& vertex, const LoKi::GenTypes::GFunc& fun,
                        const LoKi::GenTypes::GCuts& cut,
                        const HepMC3::Relatives&     range = HepMCUtils::WrapperRelatives::DESCENDANTS,
                        double                       res   = LoKi::Constants::NegativeInfinity );
      // ======================================================================
      /// find the minimum element through the event
      HepMC3::ConstGenParticlePtr min_element( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                                               const LoKi::GenTypes::GCuts& cut );
      /// find the minimum element through the tree
      HepMC3::ConstGenParticlePtr min_element( const HepMC3::ConstGenParticlePtr& particle,
                                               const LoKi::GenTypes::GFunc& fun, const LoKi::GenTypes::GCuts& cut );
      // ======================================================================
      /// find the minimum element through the tree
      HepMC3::ConstGenParticlePtr
      min_element( const HepMC3::ConstGenVertexPtr& veretx, const LoKi::GenTypes::GFunc& fun,
                   const LoKi::GenTypes::GCuts& cut,
                   const HepMC3::Relatives&     range = HepMCUtils::WrapperRelatives::DESCENDANTS );
      // ======================================================================
      /// find the maximal element through the event
      HepMC3::ConstGenParticlePtr max_element( const HepMC3::GenEvent* event, const LoKi::GenTypes::GFunc& fun,
                                               const LoKi::GenTypes::GCuts& cut );
      // ======================================================================
      /// find the minimum element through the tree
      HepMC3::ConstGenParticlePtr max_element( const HepMC3::ConstGenParticlePtr& particle,
                                               const LoKi::GenTypes::GFunc& fun, const LoKi::GenTypes::GCuts& cut );
      // ======================================================================
      /// find the minimum element through the tree
      HepMC3::ConstGenParticlePtr
      max_element( const HepMC3::ConstGenVertexPtr& veretx, const LoKi::GenTypes::GFunc& fun,
                   const LoKi::GenTypes::GCuts& cut,
                   const HepMC3::Relatives&     range = HepMCUtils::WrapperRelatives::DESCENDANTS );
      // ======================================================================
    } // namespace GenAlgs
  }   // namespace Dicts
} // end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_GENALGSDICTS_H
