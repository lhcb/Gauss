/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENALGS_H
#define LOKI_GENALGS_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
#include <numeric>
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Algs.h"
// ============================================================================
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/Relatives.h"
#include "HepMCUtils/Relatives.h"
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-01-23
 *
 */
// ============================================================================
namespace LoKi {
  /** @namespace  LoKi::GenAlgs  LoKi/GenAlgs.h
   *
   *  Collection of  few helper functions/algoritgms  "a'la STL"
   *  to simplify the operations with HepMC3 graphs
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-01-23
   */
  namespace GenAlgs {
    template <class OBJECT, class PREDICATE>
    inline size_t count_if( OBJECT first, OBJECT last, const PREDICATE& cut );
    // ========================================================================
    /** useful helper function (a'la STL) to count a number of
     *  (HepMC3) particles, which satisfies the certain criteria,
     *  e.e. count number of leptons in LHCb acceptance :
     *
     *  @code
     *
     *  using namespace LoKi::GenAlgs
     *  using namespace LoKi::Cuts
     *
     *  const HepMC3::GenEvent* event = ... ;
     *
     *  const size_t nLeptons =
     *    count_if ( event , GLEPTON && GTHETA < 400 * mrad ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::GLEPTON
     *  @see LoKi::Cuts::GTHETA
     *
     *  @param event pointer to HepMC3 event
     *  @param cut   the condidion/predicate/criterion
     *  @return number of elements which satisfy the criterion
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-23
     */
    template <class PREDICATE>
    inline size_t count_if( const HepMC3::GenEvent* event, const PREDICATE& cut ) {
      return ( 0 == event )
                 ? 0
                 : LoKi::Algs::count_if( std::begin( event->particles() ), std::end( event->particles() ), cut );
    }
    // ========================================================================
    /** simple function to count number of "good" paricles in the HepMC3-graph
     *  @param vertex pointer to the vertex
     *  @param cut    predicate which defines "good" particle
     *  @param range  range of HepMC3-graph
     *  @see const HepMC3::Relatives &
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-03
     */
    template <class PREDICATE>
    inline size_t count_if( const HepMC3::ConstGenVertexPtr& vertex, const PREDICATE& cut,
                            const HepMC3::Relatives& range ) {
      if ( 0 == vertex ) { return 0; } // RETURN
      auto particles = range( vertex );
      return LoKi::Algs::count_if( std::begin( particles ), std::end( particles ), cut ); // RETURN
    }
    // ========================================================================
    /** simple function to count number of "good" paricles in the decay tree
     *  of th egiven particlee
     *  @param particle pointer to the mother particle
     *  @param cut    predicate which defines "good" particle
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-03
     */
    template <class PREDICATE>
    inline size_t count_if( const HepMC3::ConstGenParticlePtr& particle, const PREDICATE& cut ) {
      size_t result = 0;
      if ( 0 != particle ) {
        result = LoKi::GenAlgs::count_if( particle->end_vertex(), cut, HepMCUtils::WrapperRelatives::DESCENDANTS );
      }
      if ( cut( particle ) ) { ++result; }
      // check the particle itself
      return result;
    }
    // ========================================================================
    /** useful helper function (a'la STL) to count a number of
     *  (HepMC3) particles, which satisfy certain criteria,
     *  e.e. count number of beauty particles
     *
     *  @code
     *
     *  using namespace LoKi::GenAlgs
     *  using namespace LoKi::Cuts
     *
     *  // any sequence of objects, convertible to
     *  // const LHCb::HepMC3Events*, const LHCb::HepMC3Event*
     *  // or const HepMC3::GenEvent*
     *  SEQUENCE objects = ... ;
     *
     *  const size_t nBeauty  =
     *    count_if ( objects.begin() , objects.end() , BEAUTY ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::BEAUTY
     *
     *  @param  first begin of the sequence
     *  @param  last  end of the sequence
     *  @param  cut   the predicate
     *  @return number of elements which satisfy the criterion
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-23
     */
    template <class OBJECT, class PREDICATE>
    inline size_t count_if( OBJECT first, OBJECT last, const PREDICATE& cut ) {
      using arg_t = decltype( *first );
      return std::accumulate( first, last, size_t( 0 ),
                              [&]( size_t n, arg_t arg ) { return n + LoKi::GenAlgs::count_if( arg, cut ); } );
    }
    // ========================================================================
    /** usefull helper function (a'la STL) to efficiently check the
     *  presence of at least one (HepMC3) particle, which satisfies the
     *  certain criteria, e.g. b-buarks from Higgs decays:
     *
     *  @code
     *
     *  using namespace LoKi::GenAlgs
     *  using namespace LoKi::Cuts
     *
     *  const HepMC3::GenEvent* event = ... ;
     *
     *  const bool good =
     *    found ( event , ( "b" == GABSID ) &&
     *       0 < GNINTREE ( "H_10" == GABSID , HepMC3::parents ) ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::Cuts::GNINTREE
     *
     *  @param event pointer to HepMC3 event
     *  @param cut   the condidion/predicate/criterion
     *  @return true if the particle, which satisfies the criterion is found
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-23
     */
    template <class PREDICATE>
    inline bool found( const HepMC3::GenEvent* event, const PREDICATE& cut ) {
      return event && ( std::end( event->particles() ) !=
                        LoKi::Algs::find_if( std::begin( event->particles() ), std::end( event->particles() ), cut ) );
    }
    // ========================================================================
    /** simple function to check the presence of "good" particle in HepMC3-graph
     *  @param vertex pointer to the vertex
     *  @param cut defintion of "good" particle
     *  @param range HepMC3-graph
     *  @return true if at least one "good" particle is in a graph.
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class PREDICATE>
    inline bool found( const HepMC3::ConstGenVertexPtr& vertex, const PREDICATE& cut, const HepMC3::Relatives& range ) {
      if ( !vertex ) { return false; }
      auto particles = range( vertex );
      return LoKi::Algs::found( std::begin( particles ), std::end( particles ), cut );
    }
    // ========================================================================
    /** simple function to check the presence of "good" particle in the decay
     *  tree of the mother particle
     *  @param particle pointer to mother particle
     *  @param cut defintion of "good" particle
     *  @return true if at least one "good" particle is in a decay tree
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class PREDICATE>
    inline bool found( const HepMC3::ConstGenParticlePtr& particle, const PREDICATE& cut ) {
      return cut( particle ) || ( particle && LoKi::GenAlgs::found( particle->end_vertex(), cut,
                                                                    HepMCUtils::WrapperRelatives::DESCENDANTS ) );
    }
    // =====================================================================
    template <class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr foundFirst( const HepMC3::ConstGenVertexPtr& vertex,
                                                         const PREDICATE&                 cut );
    // ======================================================================
    template <class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr foundFirst( const HepMC3::ConstGenParticlePtr& particle,
                                                         const PREDICATE&                   cut );
    // ======================================================================
    template <class PARTICLE, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr foundFirst( PARTICLE begin, PARTICLE end, const PREDICATE& cut ) {
      //
      HepMC3::ConstGenParticlePtr result = nullptr;
      //
      for ( ; !result && begin != end; ++begin ) { result = foundFirst( *begin, cut ); }
      //
      return result;
    }
    // ======================================================================
    /** simple function to get the first matched element in tree
     *  tree of the mother particle
     *  @param particle pointer to mother particle
     *  @param cut defintion of "good" particle
     *  @return good particle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr foundFirst( const HepMC3::ConstGenParticlePtr& particle,
                                                         const PREDICATE&                   cut ) {
      if ( 0 == particle ) { return nullptr; }    // RETURN
      if ( cut( particle ) ) { return particle; } // RETURN
      //
      if ( 0 == particle->end_vertex() ) { return nullptr; } // RETURN
      //
      return foundFirst( particle->end_vertex(), cut );
    }
    // ========================================================================
    template <class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr foundFirst( const HepMC3::ConstGenVertexPtr& vertex,
                                                         const PREDICATE&                 cut ) {
      //
      if ( !vertex ) { return nullptr; } // RETURN
      //
      auto particles = HepMC3::Relatives::CHILDREN( vertex );
      return foundFirst( std::begin( particles ), std::end( particles ), cut );
    }
    // ========================================================================
    /** useful helper function (a'la STL) to efficiently check the
     *  presence of at least one (HepMC3) particle, which satisfies the
     *  certain criteria, e.g. for beauty particles
     *
     *  @code
     *
     *  using namespace LoKi::GenAlgs
     *  using namespace LoKi::Cuts
     *
     *  // any sequence of objects, convertible to
     *  // const LHCb::HepMC3Events*, const LHCb::HepMC3Event*
     *  // or const HepMC3::GenEvent*
     *  SEQUENCE objects = ... ;
     *
     *  const bool good =
     *    found ( objects.begin() , objects.end() , BEAUTY ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::BEAUTY
     *
     *  @param  first begin of the sequence
     *  @param  last  end of the sequence
     *  @param  cut   the predicate
     *  @return number of elements which satisfy the criterion
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-23
     */
    template <class OBJECT, class PREDICATE>
    inline bool found( OBJECT first, OBJECT last, const PREDICATE& cut ) {
      using arg_t = decltype( *first );
      return std::any_of( first, last, [&]( arg_t arg ) { return found( arg, cut ); } );
    }
    // ========================================================================
    /** Simple algorithm for accumulation of
     *  the function value through the  HepMC3 graph
     *
     *  E.g. one can 'accumulate' the total
     *  4-momentum of all daughter muons:
     *
     *  @code
     *
     *  const HepMC3::GenEvent* event = ... ;
     *
     *  LoKi::LorentzVector result = LoKi::GenAlgs::accumulate
     *   ( event                            ,   // the event
     *     LoKi::Kinematics::Gen4Momentum() ,   // get 4-mometumm
     *     "mu+" == GABSID                  ,   // consider only muons
     *     LoKi::LorentzVector()            ,   // the initial value
     *     std::plus<LoKi::LorentzVector>() ) ; // operation
     *
     *  @endcode
     *
     *  @see HepMC3::GenEvent
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::LorentzVector
     *  @see LoKi::Kinematics::Gen4Momentum
     *
     *  @param  event     pointer to HepMC3::GenEvent
     *  @param  functor   function to be evaluated
     *  @param  predicate selection criteria
     *  @param  result    the initial-value for the result
     *  @return the result of accumulation
     *
     *  @author Vanya BELYAEV ibelyaev@physucs.syr.edu
     *  @date 2006-02-09
     */
    template <class RESULT, class FUNCTOR, class PREDICATE, class OPERATION>
    inline RESULT accumulate( const HepMC3::GenEvent* event, const FUNCTOR& functor, const PREDICATE& predicate,
                              RESULT result, OPERATION binop ) {
      return event ? LoKi::Algs::accumulate( std::begin( event->particles() ), std::end( event->particles() ), functor,
                                             predicate, result, binop )
                   : result;
    }
    // ========================================================================
    template <class RESULT, class FUNCTOR, class PREDICATE>
    inline RESULT accumulate( const HepMC3::GenEvent* event, const FUNCTOR& functor, const PREDICATE& predicate,
                              RESULT result ) {
      return accumulate( event, functor, predicate, result, std::plus<RESULT>() );
    }
    // ========================================================================
    /** simple function for accumulatio throught the HepMC3-graph
     *  @param vertex pointer to HepMC3-graph
     *  @param functor    function to be accumulated
     *  @param predicate  restriction/selector for particles
     *  @param result     the initial value for accumulation
     *  @param range      the type of HepMC3-graph
     *  @param binop      the operation used for accumulation
     *  @return updated accumulation result
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class RESULT, class FUNCTOR, class PREDICATE, class OPERATION>
    inline RESULT accumulate( const HepMC3::ConstGenVertexPtr& vertex, const FUNCTOR& functor,
                              const PREDICATE& predicate, RESULT result, const HepMC3::Relatives& range,
                              OPERATION binop ) {
      if ( !vertex ) { return result; }
      auto particles = range( vertex );
      return LoKi::Algs::accumulate( std::begin( particles ), std::end( particles ), functor, predicate, result,
                                     binop );
    }
    // ========================================================================
    template <class RESULT, class FUNCTOR, class PREDICATE>
    inline RESULT accumulate( const HepMC3::ConstGenVertexPtr& vertex, const FUNCTOR& functor,
                              const PREDICATE& predicate, RESULT result, const HepMC3::Relatives& range ) {
      return accumulate( vertex, functor, predicate, result, range, std::plus<RESULT>() );
    }
    // ========================================================================
    /** simple function for accumulatio throught the decay tree of the particle
     *  @param particle the pointer to mother particle
     *  @param functor    function to be accumulated
     *  @param predicate  restriction/selector for particles
     *  @param result     the initial value for accumulation
     *  @param binop      the operation used for accumulation
     *  @return updated accumulation result
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class RESULT, class FUNCTOR, class PREDICATE, class OPERATION>
    inline RESULT accumulate( const HepMC3::ConstGenParticlePtr& particle, const FUNCTOR& functor,
                              const PREDICATE& predicate, RESULT result, OPERATION binop ) {
      if ( particle ) {
        result = LoKi::GenAlgs::accumulate( particle->end_vertex(), functor, predicate, result,
                                            HepMCUtils::WrapperRelatives::DESCENDANTS, binop );
      }
      if ( predicate( particle ) ) { result = binop( result, functor( particle ) ); }
      return result;
    }
    // ========================================================================
    template <class RESULT, class FUNCTOR, class PREDICATE>
    inline RESULT accumulate( const HepMC3::ConstGenParticlePtr& particle, const FUNCTOR& functor,
                              const PREDICATE& predicate, RESULT result ) {
      return accumulate( particle, functor, predicate, result, std::plus<RESULT>() );
    }
    // ========================================================================
    /** simple function to extract the minimum value of certain function
     *  form HepMC3-graph
     *  @param vertex     pointer to HepMC3-graph
     *  @param functor    function to be evaluated
     *  @param predicate  restriction/selector for particles
     *  @param range      the type of HepMC3-graph
     *  @param minval     the initial value
     *  @return updated accumulation result
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE, class RESULT>
    inline RESULT min_value( const HepMC3::ConstGenVertexPtr& vertex, const FUNCTION& fun, const PREDICATE& cut,
                             const HepMC3::Relatives& range, RESULT minval ) {
      if ( !vertex ) { return minval; } // RETURN
      // investigate the vertex
      auto particles = range( vertex );
      auto ifound    = LoKi::Algs::select_min( std::begin( particles ), std::end( particles ), fun, cut );
      if ( std::end( particles ) == ifound ) { return minval; } // RETURN
      // check the minimum:
      minval = std::min( minval, fun( *ifound ) );
      return minval; // RETURN
    }
    // ========================================================================
    /** The trivial algorithm which scans the decay
     *  tree of the particle and searches for the the
     *  minimal value for some functions for
     *  particles which satisfy the certain criteria
     *
     *  @param particle the particle
     *  @param fun      function to be evaluated
     *  @param cut      the criteria
     *  @param minval   minimal value
     *  @return updated minimal value
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-02-20
     */
    template <class FUNCTION, class PREDICATE, class RESULT>
    inline RESULT min_value( const HepMC3::ConstGenParticlePtr& particle, const FUNCTION& fun, const PREDICATE& cut,
                             RESULT minval ) {
      // (1) traverse the tree if possible
      if ( particle ) {
        // check all end-vertices :
        minval = LoKi::GenAlgs::min_value( particle->end_vertex(), fun, cut, HepMCUtils::WrapperRelatives::DESCENDANTS,
                                           minval ); // RECURSION!
      }
      // (2) check itself
      if ( cut( particle ) ) { minval = std::min( minval, fun( particle ) ); }
      //
      return minval; // RETURN
    }
    // ========================================================================
    /** simple function to extract the maximum value of certain function
     *  form HepMC3-graph
     *  @param vertex     pointer to HepMC3-graph
     *  @param functor    function to be evaluated
     *  @param predicate  restriction/selector for particles
     *  @param range      the type of HepMC3-graph
     *  @param minval     the initial value
     *  @return updated maximum
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE, class RESULT>
    inline RESULT max_value( const HepMC3::ConstGenVertexPtr& vertex, const FUNCTION& fun, const PREDICATE& cut,
                             const HepMC3::Relatives& range, RESULT maxval ) {
      if ( !vertex ) { return maxval; } // RETURN
      // investigate the vertex
      auto particles = range( vertex );
      auto ifound    = LoKi::Algs::select_max( std::begin( particles ), std::end( particles ), fun, cut );
      // check the maximum
      return ( std::end( particles ) != ifound ) ? std::max( maxval, fun( *ifound ) ) : maxval;
    }
    // ========================================================================
    /** The trivial algorithm which scans the decay
     *  tree of the particle and searches for the the
     *  maximal value for some functions for
     *  particles which satisfy the certain criteria
     *
     *  @param particle the particle
     *  @param fun      function to be evaluated
     *  @param cut      the criteria
     *  @param minval   minimal value
     *  @return updated minimal value
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-02-20
     */
    template <class FUNCTION, class PREDICATE, class RESULT>
    inline RESULT max_value( const HepMC3::ConstGenParticlePtr& particle, const FUNCTION& fun, const PREDICATE& cut,
                             RESULT maxval ) {
      // (1) traverse the tree if possible
      if ( particle ) {
        // check for endvertices
        maxval = LoKi::GenAlgs::max_value( particle->end_vertex(), fun, cut, HepMCUtils::WrapperRelatives::DESCENDANTS,
                                           maxval ); // RECURSION!
      }
      // (2) check itself
      return cut( particle ) ? std::max( maxval, fun( particle ) ) : maxval;
    }
    // ========================================================================
    /** simple function to extract the mminimul value of certain function
     *  for whole HepMC3-event
     *  @param event      pointer to event
     *  @param fun        function to be evaluated
     *  @param cut        restriction/selector for particles
     *  @param minval     the initial value
     *  @return updated minimum
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE, class RESULT>
    inline RESULT min_value( const HepMC3::GenEvent* event, const FUNCTION& fun, const PREDICATE& cut, RESULT minval ) {
      if ( !event ) { return minval; } // RETURN
      //
      auto ifound =
          LoKi::Algs::select_min( std::begin( event->particles() ), std::end( event->particles() ), fun, cut );
      return ifound != std::end( event->particles() ) ? std::min( minval, fun( *ifound ) ) : minval;
    }
    // ========================================================================
    /** simple function to extract the maximum value of certain function
     *  for whole HepMC3 event
     *  @param event      pointer to event
     *  @param fun        function to be evaluated
     *  @param cut        restriction/selector for particles
     *  @param maxval     the initial value of maximum
     *  @return updated maximum
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE, class RESULT>
    inline RESULT max_value( const HepMC3::GenEvent* event, const FUNCTION& fun, const PREDICATE& cut, RESULT maxval ) {
      if ( event ) { return maxval; } // RETURN
      //
      auto ifound =
          LoKi::Algs::select_max( std::begin( event->particles() ), std::end( event->particles() ), fun, cut );
      return ifound != std::end( event->particles() ) ? std::max( maxval, fun( *ifound ) ) : maxval;
    }
    // ========================================================================
    /** simple function to extract the particle, which minimizes a
     *  certain function
     *  @param event      pointer to event
     *  @param fun        function to be evaluated
     *  @param cut        restriction/selector for particles
     *  @return particle which mimimizes the function
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr min_element( const HepMC3::GenEvent* event, const FUNCTION& fun,
                                                          const PREDICATE& cut ) {
      if ( !event ) { return nullptr; } // RETURN
      auto ifound =
          LoKi::Algs::select_min( std::begin( event->particles() ), std::end( event->particles() ), fun, cut );
      return ifound != std::end( event->particles() ) ? *ifound : nullptr;
    }
    // ========================================================================
    /** simple function to extract the particle, which minimizes a
     *  certain function
     *  @param vertex     pointer to HepMC3-graph
     *  @param fun        function to be evaluated
     *  @param cut        restriction/selector for particles
     *  @param range      type of HepMC3-graph
     *  @return particle which mimimizes the function
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr min_element( const HepMC3::ConstGenVertexPtr& vertex, const FUNCTION& fun,
                                                          const PREDICATE& cut, const HepMC3::Relatives& range ) {
      if ( !vertex ) { return nullptr; } // RETURN
      auto particles = range( vertex );
      auto ifound    = LoKi::Algs::select_min( std::begin( particles ), std::end( particles ), fun, cut );
      return ifound != std::end( particles ) ? *ifound : nullptr; // RETURN
    }
    // ========================================================================
    /** simple function to extract the particle, which minimizes a
     *  certain function
     *  @param particle pointer to mother particle
     *  @param fun      function to be evaluated
     *  @param cut      restriction/selector for particles
     *  @return particle which mimimizes the function
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr min_element( const HepMC3::ConstGenParticlePtr& particle,
                                                          const FUNCTION& fun, const PREDICATE& cut ) {
      if ( !particle ) { return nullptr; } // RETURN
      const HepMC3::ConstGenParticlePtr& tmp =
          LoKi::GenAlgs::min_element( particle->end_vertex(), fun, cut, HepMCUtils::WrapperRelatives::DESCENDANTS );
      if ( !tmp && cut( particle ) ) { return particle; } // RETURN
      if ( !tmp ) { return nullptr; }                     // RETURN
      return fun( tmp ) < fun( particle ) ? tmp : particle;
    }
    // ========================================================================
    /** simple function to extract the particle, which maximizes a
     *  certain function
     *  @param event pointer to the event
     *  @param fun   function to be evaluated
     *  @param cut   restriction/selector for particles
     *  @return particle which maximizes the function
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr max_element( const HepMC3::GenEvent* event, const FUNCTION& fun,
                                                          const PREDICATE& cut ) {
      if ( !event ) { return nullptr; } // RETURN
      auto ifound =
          LoKi::Algs::select_max( std::begin( event->particles() ), std::end( event->particles() ), fun, cut );
      return ifound != std::end( event->particles() ) ? *ifound : nullptr; // RETURN
    }
    // ========================================================================
    /** simple function to extract the particle, which maximizes a
     *  certain function
     *  @param vertex pointer to HepMC3-graph
     *  @param fun    function to be evaluated
     *  @param cut    restriction/selector for particles
     *  @param range      type of HepMC3-graph
     *  @return particle which maximizes the function
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr max_element( const HepMC3::ConstGenVertexPtr& vertex, const FUNCTION& fun,
                                                          const PREDICATE& cut, const HepMC3::Relatives& range ) {
      if ( 0 == vertex ) { return 0; } // RETURN
      auto particles = range( vertex );
      auto ifound    = LoKi::Algs::select_max( std::begin( particles ), std::end( particles ), fun, cut );
      if ( std::end( particles ) == ifound ) { return 0; } // RETURN
      return *ifound;                                      // RETURN
    }
    // ========================================================================
    /** simple function to extract the particle, which maximizes a
     *  certain function
     *  @param particle pointer to the mother particle
     *  @param fun      function to be evaluated
     *  @param cut      restriction/selector for particles
     *  @return particle which maximizes the function
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     */
    template <class FUNCTION, class PREDICATE>
    inline const HepMC3::ConstGenParticlePtr max_element( const HepMC3::ConstGenParticlePtr& particle,
                                                          const FUNCTION& fun, const PREDICATE& cut ) {
      if ( 0 == particle ) { return 0; } // RETURN
      auto tmp =
          LoKi::GenAlgs::max_element( particle->end_vertex(), fun, cut, HepMCUtils::WrapperRelatives::DESCENDANTS );
      if ( 0 == tmp && cut( particle ) ) { return particle; } // RETURN
      if ( 0 == tmp ) { return 0; }                           // RETURN
      return fun( tmp ) < fun( particle ) ? tmp : particle;
    }
    // ========================================================================
  } // namespace GenAlgs
  // ==========================================================================
} // namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_GENALGS_H
