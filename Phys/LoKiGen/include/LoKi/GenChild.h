/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENCHILD_H
#define LOKI_GENCHILD_H 1
// ============================================================================
// Include files
// ============================================================================
// HepMC3
// ============================================================================
#ifdef __INTEL_COMPILER
// floating-point equality and inequality comparisons are unreliable
#  pragma warning( disable : 1572 )
#  pragma warning( push )
#endif
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/Relatives.h"
#include "HepMCUtils/Relatives.h"
#ifdef __INTEL_COMPILER
#  pragma warning( pop )
#endif
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/CmpBarCode.h"
#include "LoKi/GenTypes.h"
// ============================================================================
/** @file
 *  Set of functions to access daughtr particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace GenChild {
    // ========================================================================
    /** get the number of children for the given HepMC3-particle
     *  @see HepMC3::GenParticle
     *  @param  mother pointer to HepMC3-particle
     *  @return number of chilren
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-02
     */
    GAUDI_API
    std::size_t nChildren( const HepMC3::ConstGenParticlePtr& mother );
    // ========================================================================
    /** Trivial accessor to the daughter "decay" particles for
     *  the given HepMC3-particle
     *
     *  @attention index starts with 1 , null index corresponds
     *             to the original particle
     *
     *  @param  mother   (const) pointer to mother particle
     *  @param  index            index of the child particle
     *  @return daughter particle with given index
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-02
     */
    GAUDI_API
    HepMC3::ConstGenParticlePtr child( const HepMC3::ConstGenParticlePtr& mother, const size_t index );
    // ========================================================================
    /** Trivial accessor to the daughter "decay" particles for
     *  the given HepMC3-particle
     *
     *  @attention index starts with 1 , null index corresponds
     *             to the original particle
     *
     *  @param  particle (const) pointer to mother particle
     *  @param  index1   index   index of the child particle
     *  @param  index2   index   index of the child particle
     *  @return daughter particle with given index
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-02
     */
    inline HepMC3::ConstGenParticlePtr child( const HepMC3::ConstGenParticlePtr& particle, const size_t index1,
                                              const size_t index2 ) {
      return child( child( particle, index1 ), index2 );
    }
    // ========================================================================
    /** Trivial accessor to the daughter "decay" particles for
     *  the given HepMC3-particle
     *
     *  @attention index starts with 1 , null index corresponds
     *             to the original particle
     *
     *  @param  particle (const) pointer to mother particle
     *  @param  index1   index   index of the child particle
     *  @param  index2   index   index of the child particle
     *  @param  index3   index   index of the child particle
     *  @return daughter particle with given index
     *  @author Vanya BELYAEV ibelyaev@phsycis.syr.edu
     *  @date 2007-06-02
     */
    inline HepMC3::ConstGenParticlePtr child( const HepMC3::ConstGenParticlePtr& particle, const size_t index1,
                                              const size_t index2, const size_t index3 ) {
      return child( child( particle, index1 ), index2, index3 );
    }
    // ========================================================================
    /** Trivial accessor to the daughter "decay" particles for
     *  the given HepMC3-particle
     *
     *  @attention index starts with 1 , null index corresponds
     *             to the original particle
     *
     *  @param  particle (const) pointer to mother particle
     *  @param  index1   index   index of the child particle
     *  @param  index2   index   index of the child particle
     *  @param  index3   index   index of the child particle
     *  @param  index4   index   index of the child particle
     *  @return daughter particle with given index
     *  @author Vanya BELYAEV ibelyaev@phsycis.syr.edu
     *  @date 2007-06-02
     */
    inline HepMC3::ConstGenParticlePtr child( const HepMC3::ConstGenParticlePtr& particle, const size_t index1,
                                              const size_t index2, const size_t index3, const size_t index4 ) {
      return child( child( particle, index1 ), index2, index3, index4 );
    }
    // ========================================================================
    GAUDI_API
    HepMC3::ConstGenParticlePtr child( const HepMC3::ConstGenParticlePtr& particle,
                                       const std::vector<unsigned int>&   indices );
    // ========================================================================
    /*  get all "in"-particles for the given vertex
     *  @see HepMC3::GenVertex::particles_in_const_begin()
     *  @see HepMC3::GenVertex::particles_in_const_end()
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    std::vector<HepMC3::ConstGenParticlePtr> particles_in( const HepMC3::ConstGenVertexPtr& vertex );
    // ========================================================================
    /*  get all "out"-particles for the given vertex
     *  @see HepMC3::GenVertex::particles_in_const_begin()
     *  @see HepMC3::GenVertex::particles_in_const_end()
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    std::vector<HepMC3::ConstGenParticlePtr> particles_out( const HepMC3::ConstGenVertexPtr& vertex );
    // ========================================================================
    /*  get all particles form the given event
     *  @see HepMC3::GenEvent::particles_begin
     *  @see HepMC3::GenEvent::particles_end
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    std::vector<HepMC3::ConstGenParticlePtr> particles_all( const HepMC3::GenEvent* event );
    // ========================================================================
    /** get all vertices form the given event
     *  @see HepMC3::GenEvent::vertices_begin
     *  @see HepMC3::GenEvent::vertices_end
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    std::vector<HepMC3::ConstGenVertexPtr> vertices_all( const HepMC3::GenEvent* event );
    // ========================================================================
    /** get all particles from the given vertex from the given range
     *  @see HepMC3::GenVertex::particles_begin
     *  @see HepMC3::GenVertex::particles_end
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    size_t particles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range,
                      std::vector<HepMC3::ConstGenParticlePtr>& output );
    // ========================================================================
    /** get all particles from the given vertex from the given range
     *  @see HepMC3::GenVertex::particles_begin
     *  @see HepMC3::GenVertex::particles_end
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    size_t particles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range,
                      LoKi::GenTypes::GenSet& output );
    // ========================================================================
    /** get all particles form the given vertex form the given range
     *  @see HepMC3::GenVertex::particles_begin
     *  @see HepMC3::GenVertex::particles_end
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline std::vector<HepMC3::ConstGenParticlePtr> particles( const HepMC3::ConstGenVertexPtr& vertex,
                                                               const HepMC3::Relatives&         range ) {
      LoKi::GenTypes::GenSet result;
      particles( vertex, range, result );
      return std::vector<HepMC3::ConstGenParticlePtr>( result.begin(), result.end() );
    }
    // ========================================================================
    /** get all "parents" particles form the given vertxex
     *  @see LoKi::Child::particles
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline std::vector<HepMC3::ConstGenParticlePtr> parents( const HepMC3::ConstGenVertexPtr& vertex ) {
      return particles( vertex, HepMC3::Relatives::PARENTS );
    }
    // ========================================================================
    /** get all "daughter" particles form the given vertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline size_t daughters( const HepMC3::ConstGenVertexPtr&          vertex,
                             std::vector<HepMC3::ConstGenParticlePtr>& output ) {
      return particles( vertex, HepMC3::Relatives::CHILDREN, output );
    }
    // ========================================================================
    /** get all "daughter" particles form the given vertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline size_t daughters( const HepMC3::ConstGenVertexPtr& vertex, LoKi::GenTypes::GenSet& output ) {
      return particles( vertex, HepMC3::Relatives::CHILDREN, output );
    }
    // ========================================================================
    /** get all "daughter" particles form the given particle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    size_t daughters( const HepMC3::ConstGenParticlePtr& particle, std::vector<HepMC3::ConstGenParticlePtr>& output );
    // ========================================================================
    /** get all "daughter" particles form the given particle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    size_t daughters( const HepMC3::ConstGenParticlePtr& particle, LoKi::GenTypes::GenSet& output );
    // ========================================================================
    /** get all "children" particles form the given vertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline std::vector<HepMC3::ConstGenParticlePtr> children( const HepMC3::ConstGenVertexPtr& vertex ) {
      return particles( vertex, HepMC3::Relatives::CHILDREN );
    }
    // ========================================================================
    /** get all "children" particles form the given particle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline std::vector<HepMC3::ConstGenParticlePtr> children( const HepMC3::ConstGenParticlePtr& particle ) {
      std::vector<HepMC3::ConstGenParticlePtr> result;
      daughters( particle, result );
      return result;
    }
    // ========================================================================
    /** get all "ancestors" particles form the given vertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline std::vector<HepMC3::ConstGenParticlePtr> ancestors( const HepMC3::ConstGenVertexPtr& vertex ) {
      return particles( vertex, HepMCUtils::WrapperRelatives::ANCESTORS );
    }
    // ========================================================================
    /** get all "ancestors" particles form the givel particlle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    std::vector<HepMC3::ConstGenParticlePtr> ancestors( const HepMC3::ConstGenParticlePtr& particle );
    // ========================================================================
    /** get all "descendants" particles form the given vertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    inline std::vector<HepMC3::ConstGenParticlePtr> descendants( const HepMC3::ConstGenVertexPtr& vertex ) {
      return particles( vertex, HepMCUtils::WrapperRelatives::DESCENDANTS );
    }
    // ========================================================================
    /** get all "descendant" particles form the given particle
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    GAUDI_API
    std::vector<HepMC3::ConstGenParticlePtr> descendants( const HepMC3::ConstGenParticlePtr& particle );
    // ========================================================================
    /** get all "relatives" particles from the given vertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2007-05-26
     */
    // inline std::vector<HepMC3::ConstGenParticlePtr> relatives( const HepMC3::ConstGenVertexPtr & vertex ) {
    // return particles( vertex, HepMC3::relatives );
    //}
    // ========================================================================
    /// forward declaration
    class Selector; // forward declaration
    // ========================================================================
    /** Trivial accessor to the daughter particles for the given particle.
     *  @param  particle (const) pointer to mother particle
     *  @param  selector the selector
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2012-01-26
     */
    GAUDI_API
    HepMC3::ConstGenParticlePtr child( const HepMC3::ConstGenParticlePtr& particle,
                                       const LoKi::GenChild::Selector&    selector );
    // ========================================================================
    /** accessor to certain children particles for the given particle
     *  @param  particle (INPUT) pointer to mother particle
     *  @param  selector (INPUT) the selector
     *  @param  result   (OUTPUT) the container of found particles
     *  @return number of found particles
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2012-01-26
     */
    GAUDI_API
    unsigned int children( const HepMC3::ConstGenParticlePtr& particle, const LoKi::GenChild::Selector& selector,
                           LoKi::GenTypes::ConstVector& daughters );
    // ========================================================================
    /** accessor to certain children particles for the given particle
     *  @param  particle (INPUT) pointer to mother particle
     *  @param  selector (INPUT) the selector
     *  @return the container of found particles
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2010-05-29
     */
    GAUDI_API
    LoKi::GenTypes::ConstVector children( const HepMC3::ConstGenParticlePtr& particle,
                                          const LoKi::GenChild::Selector&    selector );
    // ========================================================================
    /** get all independent decay trees from HepMC3::GenEvent
     *  @see LoKi::GenTrees::buildTrees
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-01-17
     *  @attention  it could be a bit slow
     */
    GAUDI_API
    LoKi::GenTypes::ConstVector trees( const HepMC3::GenEvent* event );
    // ========================================================================
    /** get all independent decay trees from container of particles
     *  @see LoKi::GenTrees::buildTrees
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-01-17
     *  @attention  it could be a bit slow
     */
    GAUDI_API
    LoKi::GenTypes::ConstVector trees( const LoKi::GenTypes::ConstVector& particles );
    // ========================================================================
    /** get all independent decay trees from container of particles
     *  @see LoKi::GenTrees::buildTrees
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-01-17
     *  @attention  it could be a bit slow
     */
    GAUDI_API
    LoKi::GenTypes::ConstVector trees( const LoKi::GenTypes::GRange& particles );
    // ========================================================================
  } // namespace GenChild
  // ==========================================================================
  namespace Child {
    // ========================================================================
    using namespace LoKi::GenChild;
    // ========================================================================
  } // namespace Child
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_GENCHILD_H
