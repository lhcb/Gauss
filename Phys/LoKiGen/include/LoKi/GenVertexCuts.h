/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENVERTEXCUTS_H
#define LOKI_GENVERTEXCUTS_H 1
// ============================================================================
// Include files
// ============================================================================
// HepMC3
// ============================================================================
#include "HepMC3/GenVertex.h"
// ============================================================================
// LoKiGen
// ============================================================================
#include "LoKi/GenTypes.h"
#include "LoKi/GenVertices.h"
//
#include "LoKi/Filters.h"
// ============================================================================
/** @file LoKi/GenVertexCuts.h
 *
 *  Collection of useful functions/predicates to deal with
 *  HepMC3 generator information
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-08
 *
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @var GVTRUE
     *  trivial predicate which always returns "true"
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVTRUE = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::BooleanConstant{ true };
    // ========================================================================
    /** @var GVFALSE
     *  trivial predicate which always returns "false"
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVFALSE = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::BooleanConstant{ false };
    // ========================================================================
    /** @var GVALL
     *  trivial predicate which always returns "true"
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVALL = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::BooleanConstant{ true };
    // ========================================================================
    /** @var GVNONE
     *  trivial predicate which always returns "false"
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVNONE = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::BooleanConstant{ false };
    // ========================================================================
    /** @var GVONE
     *  trivial function which always returns 1
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVONE = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::Constant{ 1 };
    // ========================================================================
    /** @var GVNULL
     *  trivial function which always returns 0
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVNULL = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::Constant{ 0 };
    // ========================================================================
    /** @var GVZERO
     *  trivial function which always returns 0
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVZERO = LoKi::BasicFunctors<HepMC3::ConstGenVertexPtr>::Constant{ 0 };
    // ========================================================================
    /** Minimum from 2 functions
     *
     *  @code
     *
     *  GVFun f1 = ... ;
     *  GVFun f2 = ... ;
     *
     *  GVFun f = GVMIN( f1 , f2 ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Min
     */
    typedef LoKi::Min<HepMC3::ConstGenVertexPtr> GVMIN;
    // ========================================================================
    /** Maximum from 2 functions
     *
     *  @code
     *
     *  GVFun f1 = ... ;
     *  GVFun f2 = ... ;
     *
     *  GVFun f = GVMAX( f1 , f2 ) ;
     *
     *  @endcode
     *
     *  @see LoKi::MAX
     */
    typedef LoKi::Min<HepMC3::ConstGenVertexPtr> GVMAX;
    // ========================================================================
    /** @var GVBAR
     *  trivial function which returns the "bar-code" for
     *  HepMC3::GenVertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVBAR = LoKi::GenVertices::BarCode{};
    // ========================================================================
    /** @var GVBARCODE
     *  trivial function which returns the "bar-code" for
     *  HepMC3::GenVertex
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVBARCODE = LoKi::GenVertices::BarCode{};
    // ========================================================================
    /** @var GVX
     *  trivial function which returns the X-position of
     *  HepMC3::GenVertex
     *  @see HepMC3::GenVertex
     *  @see LoKi::GenVertices::PositionX
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVX = LoKi::GenVertices::PositionX{};
    // ========================================================================
    /** @var GVY
     *  trivial function which returns the X-position of
     *  HepMC3::GenVertex
     *  @see HepMC3::GenVertex
     *  @see LoKi::GenVertices::PositionY
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVY = LoKi::GenVertices::PositionY{};
    // ========================================================================
    /** @var GVZ
     *  trivial function which returns the Z-position of
     *  HepMC3::GenVertex
     *  @see HepMC3::GenVertex
     *  @see LoKi::GenVertices::PositionZ
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVZ = LoKi::GenVertices::PositionZ{};
    // ========================================================================
    /** @var GVRHO
     *  trivial function which returns the rho(zylindrical)-position of
     *  HepMC3::GenVertex
     *  @see HepMC3::GenVertex
     *  @see LoKi::GenVertices::Rho
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-02-13
     */
    inline const auto GVRHO = LoKi::GenVertices::Rho{};
    // ========================================================================
    /** @var GVT
     *  trivial function which returns the T-position of
     *  HepMC3::GenVertex
     *  @see HepMC3::GenVertex
     *  @see LoKi::GenVertices::PositionT
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVT = LoKi::GenVertices::PositionT{};
    // ========================================================================
    /** @var GVTIME
     *  trivial function which returns the T-position of
     *  HepMC3::GenVertex
     *  @see HepMC3::GenVertex
     *  @see LoKi::GenVertices::PositionT
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    inline const auto GVTIME = LoKi::GenVertices::PositionT{};
    // ========================================================================
    /** the adapter function which counts number of particles
     *  which satisfy certain criteria within the specified range
     *
     *  E.g. find the decay H -> b b~ X :
     *
     *  @code
     *
     *  // number of b-quarks withing children particles
     *  GVFun num1 =  GVCOUNT( "b"  == GID , HepMC3::children ) ;
     *
     *  // number of b~-quarks withing children particles
     *  GVFun num2 =  GVCOUNT( "b~" == GID , HepMC3::children ) ;
     *
     *  // use adapter function to End vertex
     *  GFun num = GFAEVX ( num1 + num2 , 0 ) ;
     *
     *  // find the decay H -> b b :
     *
     *  const LHCB::HepMC3Events* events = ... ;
     *
     *  GCut decay = ( 2 == num ) && ( "H_10" == GID ) ;
     *
     *  const bool good = LoKi::GenAlgs::found ( events , decay ) ;
     *
     *  @endcode
     *
     *  @see HepMC3::GenVertex
     *  @see HepMC3::GenParticle
     *  @see LoKi::GenVertices::CountIF
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    typedef LoKi::GenVertices::CountIF GVCOUNT;
    // ========================================================================
    /** the adapter function which accumulated the value of
     *  given function over the particles which satisfy
     *  certain criteria within the specified range
     *
     *  Total energy of all stable daughter charged leptons:
     *
     *  @code
     *
     *  GVFun e1 = GVSUM( GE , HepMC3::descendants ,
     *                           GCHARGED && GLEPTON ) ;
     *
     *  GFun eLep = GFAEVX( e1 )
     *
     *  HepMC3::ConstGenParticlePtr p = ... ;
     *
     *  const double eTot = eLep( p ) ;
     *
     *  @endcode
     *
     *  @see HepMC3::GenVertex
     *  @see HepMC3::GenParticle
     *  @see LoKi::Cuts::GFAEVX
     *  @see LoKi::GenVertices::SumIF
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-08
     */
    typedef LoKi::GenVertices::SumIF GVSUM;
    // ========================================================================
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_GENVERTEXCUTS_H
