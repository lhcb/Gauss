/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HepMC3/Relatives.h"
#include <utility>
// ============================================================================
namespace Gaudi {
  // ==========================================================================
  namespace Utils {
    std::string toCpp( const int o );
    // ========================================================================
    // strings and chars
    // ========================================================================
    inline std::string toCpp( const HepMC3::Relatives& range ) {
      if ( dynamic_cast<const HepMC3::Parents*>( &range ) ) {
        const int i = 0;
        return toCpp( i );
      }
      if ( dynamic_cast<const HepMC3::Children*>( &range ) ) {
        const int i = 1;
        return toCpp( i );
      }
      if ( dynamic_cast<const HepMC3::Ancestors*>( &range ) ) {
        const int i = 3;
        return toCpp( i );
      }
      if ( dynamic_cast<const HepMC3::Descendants*>( &range ) ) {
        const int i = 4;
        return toCpp( i );
      }
      const int i = -1;
      return toCpp( i );
    }
  } // namespace Utils
} // namespace Gaudi
