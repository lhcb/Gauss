/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENPARTICLES3_H
#define LOKI_GENPARTICLES3_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/Relatives.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/GenTypes.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-01-28
 *
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace GenParticles {
    // ========================================================================
    /** @class Count
     *  simple class which counts how many particles
     *  satisfy the certain criteria
     *
     *  @see LoKi::Cuts::GCOUNT
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@itep.ru
     *  @date   2012-01-28
     */
    class GAUDI_API Count : public LoKi::GenTypes::GFunc {
    public:
      // ======================================================================
      /** constructor from the criteria and "range"
       *  @param cut the criteria
       *  @param range search region
       *  @see const HepMC3::Relatives &
       */
      Count( const LoKi::Types::GCuts& cut, const HepMC3::Relatives& range );
      Count( const LoKi::Types::GCuts& cut, int range_idx );
      /** constructor from the criteria and "range"
       *  @param range search region
       *  @param cut the criteria
       *  @see const HepMC3::Relatives &
       */
      Count( const HepMC3::Relatives& range, const LoKi::Types::GCuts& cut );
      Count( int range_idx, const LoKi::Types::GCuts& cut );
      // ======================================================================
      /// MANDATORY: clone method ("virtual contructor")
      Count* clone() const override;
      /// MANDATORY: the only one essential method
      double operator()( const HepMC3::ConstGenParticlePtr& v ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      LoKi::Types::GCut        m_cut;
      const HepMC3::Relatives& m_range;
      // ======================================================================
    };
    // ========================================================================
    /** @class Has
     *  simple class which check the presence of particles
     *  satisfy the certain criteria
     *
     *  @see LoKi::Cuts::GHAS
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@itep.ru
     *  @date   2012-01-28
     */
    class GAUDI_API Has : public LoKi::GenTypes::GCuts {
    public:
      // ======================================================================
      /** constructor from the criteria and "range"
       *  @param cut the criteria
       *  @param range search region
       *  @see const HepMC3::Relatives &
       */
      Has( const LoKi::Types::GCuts& cut, const HepMC3::Relatives& range );
      Has( const LoKi::Types::GCuts& cut, int range_idx );
      /** constructor from the criteria and "range"
       *  @param range search region
       *  @param cut the criteria
       *  @see const HepMC3::Relatives &
       */
      Has( const HepMC3::Relatives& range, const LoKi::Types::GCuts& cut );
      Has( int range_idx, const LoKi::Types::GCuts& cut );
      // ======================================================================
      /// MANDATORY: clone method ("virtual contructor")
      Has* clone() const override;
      /// MANDATORY: the only one essential method
      bool operator()( const HepMC3::ConstGenParticlePtr& v ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      LoKi::Types::GCut        m_cut;
      const HepMC3::Relatives& m_range;
      // ======================================================================
    };
    // ========================================================================
  } // namespace GenParticles
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef GCOUNT
     *  simple functor to count certain particles in HepMC3-graph
     *
     *  @code
     *
     *   const GCOUNT cnt = GCOUNT ( "pi+" == GABSID , HepMC3.descendents ) ;
     *
     *   HepMC3::ConstGenParticlePtr p = ... ;
     *
     *   const double numPi = cnt ( p ) ;
     *
     *  @endcode
     *
     *  @see LoKi:GenParticles::Count
     *  @see LoKi:GenVertices::CountIF
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-01-28
     */
    typedef LoKi::GenParticles::Count GCOUNT;
    // ========================================================================
    /** @typedef GHAS
     *  simple functor to check certain particles in HepMC3-graph
     *  @code
     *
     *   const GHAS chk = GHAS ( "pi+" == GABSID , HepMC3.descendents ) ;
     *
     *   HepMC3::ConstGenParticlePtr p = ... ;
     *
     *   const bool hasPi = chk ( p ) ;
     *
     *  @endcode
     *
     *  @see LoKi:GenParticles::Has
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-01-28
     */
    typedef LoKi::GenParticles::Has GHAS;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_GENPARTICLES3_H
