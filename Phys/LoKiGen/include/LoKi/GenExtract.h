/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GENEXTRACT_H
#define LOKI_GENEXTRACT_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// HepMC3
// ============================================================================
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/Relatives.h"
#include "HepMCUtils/Relatives.h"
#include "LoKi/GenTypes.h"
// ============================================================================
#include <algorithm>
// ============================================================================
/** @file
 *
 *  Collection of useful function to extract the information from  HepMC3
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2001-01-23
 *
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Extract {
    // ========================================================================
    /** simple function which allows to extract a certain
     *  particles from HepMC3 vertex
     *
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT genParticles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range, OUTPUT output,
                                const PREDICATE& predicate );
    // ========================================================================
    /** simple function which allows to extract a certain
     *  particles from HepMC3 vertex
     *
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT genParticles( const HepMC3::ConstGenParticlePtr& particle, OUTPUT output,
                                const PREDICATE& predicate );
    // ========================================================================
    /** simple function which allow to extract a certain
     *  particles from HepMC3 graph.
     *
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::Cuts::GNINTREE
     *  @see HepMC3::GenEvent
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class INPUT, class OUTPUT, class PREDICATE>
    inline OUTPUT getGenParticles( INPUT first, INPUT last, OUTPUT output, const PREDICATE& predicate );
    // ========================================================================
    /** Simple function which allow to extract a certain
     *  particles from HepMC3 graph.
     *
     *  e.g. one can get all  b(and anti-b)quarks from
     *  Higgs decay
     *
     *  @code
     *
     *  const HepMC3::GenEvent* event = ... ;
     *
     *  SEQUENCE bquarks ;
     *  LoKi::Extract::getGenParticles
     *     ( event                          ,
     *       std::back_inserter( bquarks )  ,
     *       ( "b" == GABSID ) &&
     *       0 != GNINTREE( "H_10" == GABSID , HepMC3::parents ) ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::Cuts::GNINTREE
     *  @see HepMC3::GenEvent
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT getGenParticles( const HepMC3::GenEvent* event, OUTPUT output, const PREDICATE& predicate ) {
      if ( 0 == event ) { return output; }
      for ( auto& p : event->particles() ) {
        if ( !LoKi::GenTypes::_ptr_wrap( predicate )( p ) ) { continue; } // CONTINUE
        //
        const HepMC3::ConstGenParticlePtr particle = p;
        *output                                    = particle;
        ++output; // NB!
        //
      }
      return output;
    }
    // ========================================================================
    /** simple function which allows to extract a certain
     *  particles from HepMC3 vertex
     *
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT getGenParticles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range,
                                   OUTPUT output, const PREDICATE& predicate ) {
      if ( !vertex ) { return output; } // RETURN
      //
      auto particles         = range( vertex );
      auto wrapped_predicate = LoKi::GenTypes::_ptr_wrap( predicate );
      return std::copy_if( std::begin( particles ), std::end( particles ), output, wrapped_predicate );
    }
    // ========================================================================
    /** simple function which allows to extract a certain
     *  particles from HepMC3 particle tree
     *
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT getGenParticles( const HepMC3::ConstGenParticlePtr& particle, OUTPUT output,
                                   const PREDICATE& predicate ) {
      if ( particle ) {
        output = genParticles( particle->end_vertex(), HepMCUtils::WrapperRelatives::DESCENDANTS, output,
                               predicate ); // RECURSION
      }
      /// check the particle
      if ( predicate( particle ) ) {
        *output = particle;
        ++output; // NB!
      }
      return output; // RETURN
    }
    // ========================================================================
    /** simple function which allow to extract a certain
     *  particles from HepMC3 graph.
     *
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::Cuts::GNINTREE
     *  @see HepMC3::GenEvent
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class INPUT, class OUTPUT, class PREDICATE>
    inline OUTPUT getGenParticles( INPUT first, INPUT last, OUTPUT output, const PREDICATE& predicate ) {
      for ( ; first != last; ++first ) { output = getGenParticles( *first, output, predicate ); }
      return output;
    }
    // ========================================================================
    /** simple function which allow to extract a certain
     *  particles from HepMC3 graph.
     *
     *  e.g. one can get all  b(and anti-b)quarks from
     *  Higgs decay
     *
     *  @code
     *
     *  const HepMC3::GenEvent* event = ... ;
     *
     *  SEQUENCE bquarks ;
     *  LoKi::Extract::genParticles
     *     ( event                          ,
     *       std::back_inserter( bquarks )  ,
     *       ( "b" == GABSID ) &&
     *       0 != GNINTREE( "H_10" == GABSID , HepMC3::parents ) ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::Cuts::GNINTREE
     *  @see HepMC3::GenEvent
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT genParticles( const HepMC3::GenEvent* event, OUTPUT output, const PREDICATE& predicate ) {
      return getGenParticles( event, output, predicate );
    }
    // ========================================================================
    /** simple function which allows to extract a certain
     *  particles from HepMC3 vertex
     *
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT genParticles( const HepMC3::ConstGenVertexPtr& vertex, const HepMC3::Relatives& range, OUTPUT output,
                                const PREDICATE& predicate ) {
      return getGenParticles( vertex, range, output, predicate );
    }
    // ========================================================================
    /** simple function which allows to extract a certain
     *  particles from HepMC3 vertex
     *
     *  @see HepMC3::GenParticle
     *  @see HepMC3::GenVertex
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class OUTPUT, class PREDICATE>
    inline OUTPUT genParticles( const HepMC3::ConstGenParticlePtr& particle, OUTPUT output,
                                const PREDICATE& predicate ) {
      return getGenParticles( particle, output, predicate );
    }
    // ========================================================================
    /** simple function which allow to extract a certain
     *  particles from HepMC3 graph.
     *
     *  @see LoKi::Cuts::GABSID
     *  @see LoKi::Cuts::GNINTREE
     *  @see HepMC3::GenEvent
     *
     *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
     *  @date   2005-03-26
     */
    template <class INPUT, class OUTPUT, class PREDICATE>
    inline OUTPUT genParticles( INPUT first, INPUT last, OUTPUT output, const PREDICATE& predicate ) {
      return getGenParticles( first, last, output, predicate );
    }
    // ========================================================================
  } // namespace Extract
} // end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_GENEXTRACT_H
