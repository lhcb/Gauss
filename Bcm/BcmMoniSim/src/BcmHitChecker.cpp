/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "BcmDet/DeBcm.h"
#include "BcmDet/DeBcmSens.h"

#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/MCHeader.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"

namespace LHCb {

  /**
   *  @author Tomasz Szumlak & Chris Parkes
   *  @date   2005-12-13
   */
  struct BcmHitChecker : public Gaudi::Functional::Consumer<void( MCHits const&, DeBcm const&, MCHeader const& ),
                                                            DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeBcm>> {
    BcmHitChecker( const std::string& name, ISvcLocator* pSvcLocator );
    void                  operator()( MCHits const&, DeBcm const&, MCHeader const& ) const override;
    bool                  checkStation( MCHit const& Hit, DeBcm const& ) const;
    Gaudi::Property<bool> m_detailedMonitor{ this, "MonitorInDetail", false, "MonitorInDetail" };
  };

  DECLARE_COMPONENT_WITH_ID( BcmHitChecker, "BcmHitChecker" )

} // namespace LHCb

LHCb::BcmHitChecker::BcmHitChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { { "BcmDetLocation", DeBcmLocation::Default },
                  { "MCHeaderLocation", MCHeaderLocation::Default },
                  { "BcmHitsLocation", "MC/Bcm/Hits" } } ) {}

void LHCb::BcmHitChecker::operator()( LHCb::MCHits const& bcmMCHits, DeBcm const& bcmDet,
                                      LHCb::MCHeader const& evt ) const {
  for ( auto& bcmHit : bcmMCHits ) {
    // Check if hit is in the correct bcm station
    if ( checkStation( *bcmHit, bcmDet ) ) {
      plot( bcmHit->energy() / Gaudi::Units::eV, "enDep", "Energy deposited in Diamond [eV]", 0., 500000., 100 );
      plot2D( bcmHit->entry().x() / Gaudi::Units::mm, bcmHit->entry().y() / Gaudi::Units::mm, "entryXY",
              "Particle entry point in Diamond [mm] - XY plane", -80., 80., -80., 80., 160, 160 );
      // MC particle energy
      const MCParticle* myMCParticle = bcmHit->mcParticle();
      if ( 0 != myMCParticle ) {
        Gaudi::LorentzVector fMom = myMCParticle->momentum();
        plot( fMom.e() / Gaudi::Units::GeV, "eMCPart", "Particle energy [GeV]", 0., 50., 100 );
      }

      // Tuple
      if ( m_detailedMonitor.value() ) {
        long  nEvt  = evt.evtNumber();
        Tuple tuple = nTuple( 100, "Hit info" );
        // fill N-Tuple with hit data:
        tuple->column( "Event", nEvt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        // "~(~0u << N)" is equivalent to "(1 << N) - 1", i.e. a mask selecting the bits from 0 to N-1
        tuple->column( "Sensor", ( ~( ~0u << DeBcmShifts::shiftStationID ) ) & ( bcmHit->sensDetID() ) )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "Path", bcmHit->pathLength() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "EnDep", bcmHit->energy() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "xIn", bcmHit->entry().x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "yIn", bcmHit->entry().y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "zIn", bcmHit->entry().z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple
            ->column( "rIn", ( sqrt( bcmHit->entry().x() * bcmHit->entry().x() +
                                     bcmHit->entry().y() * bcmHit->entry().y() ) ) )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "xOut", bcmHit->exit().x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "yOut", bcmHit->exit().y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "zOut", bcmHit->exit().z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple
            ->column( "rOut",
                      ( sqrt( bcmHit->exit().x() * bcmHit->exit().x() + bcmHit->exit().y() * bcmHit->exit().y() ) ) )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "arTime", bcmHit->time() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

        // fill N-Tuple with particle data
        tuple->column( "pid", bcmHit->mcParticle()->particleID().pid() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "charge", bcmHit->mcParticle()->particleID().threeCharge() / 3 )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "px", bcmHit->mcParticle()->momentum().px() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "py", bcmHit->mcParticle()->momentum().py() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "pz", bcmHit->mcParticle()->momentum().pz() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "E", bcmHit->mcParticle()->momentum().E() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

        // fill N-tuple with primary vertex data
        tuple->column( "primx", bcmHit->mcParticle()->primaryVertex()->position().x() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "primy", bcmHit->mcParticle()->primaryVertex()->position().y() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->column( "primz", bcmHit->mcParticle()->primaryVertex()->position().z() )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      }
    }
  }
}

/// Check if hit is in the monitored BCM station
bool LHCb::BcmHitChecker::checkStation( LHCb::MCHit const& hit, DeBcm const& bcmDet ) const {
  int nStation   = bcmDet.stationNumber();
  int hitSensor  = hit.sensDetID();
  int hitStation = ( hitSensor >> DeBcmShifts::shiftStationID );
  return nStation == hitStation;
}
