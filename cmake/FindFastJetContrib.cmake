###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# - Locate FastJetContrib package
# Defines:
#
#  FASTJETCONTRIB_FOUND
#  FASTJETCONTRIB_INCLUDE_DIR
#  FASTJETCONTRIB_INCLUDE_DIRS
#  FASTJETCONTRIB_<component>_FOUND
#  FASTJETCONTRIB_<component>_LIBRARY
#  FASTJETCONTRIB_LIBRARIES
#  FASTJETCONTRIB_LIBRARY_DIRS
#
# Can be steered by FASTJETCONTRIB_ROOT.
#

find_path(FASTJETCONTRIB_INCLUDE_DIR fastjet/contrib/ScJet.hh
          HINTS $ENV{FASTJETCONTRIB_ROOT_DIR}/include ${FASTJETCONTRIB_ROOT_DIR}/include)

find_library(FASTJETCONTRIB_LIBRARY NAMES fastjetcontribfragile
             HINTS $ENV{FASTJETCONTRIB_ROOT_DIR}/lib ${FASTJETCONTRIB_ROOT_DIR}/lib)

# handle the QUIETLY and REQUIRED arguments and set FASTJET_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FastJetContrib DEFAULT_MSG FASTJETCONTRIB_INCLUDE_DIR FASTJETCONTRIB_LIBRARY)

mark_as_advanced(FASTJETCONTRIB_FOUND FASTJETCONTRIB_INCLUDE_DIR FASTJETCONTRIB_LIBRARY)

set(FASTJETCONTRIB_INCLUDE_DIRS ${FASTJETCONTRIB_INCLUDE_DIR})
set(FASTJETCONTRIB_LIBRARIES ${FASTJETCONTRIB_LIBRARY})
get_filename_component(FASTJETCONTRIB_LIBRARY_DIRS ${FASTJETCONTRIB_LIBRARY} PATH)
