/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ConditionAccessorHolder.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#ifdef USE_DD4HEP
#  include "Detector/Muon/DeMuon.h"
#  include "Detector/Muon/Namespace.h"
#  include "LbDD4hep/ConditionAccessorHolder.h"
#endif

#include "Event/MCHeader.h"
#include "Event/MCHit.h"

#include "AIDA/IHistogram1D.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"

#include <string>

namespace Gauss::Muon {
  /**
   *  @author A Sarti
   *  @date   2005-05-20
   *  @author Dominik Muller
   *  @date   2005-05-20
   */

  using DetDescBase = LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeMuonDetector>;

#ifdef USE_DD4HEP
  using DD4hepBase = LHCb::Det::LbDD4hep::usesBaseAndConditions<GaudiTupleAlg, LHCb::Detector::DeMuon>;
#endif

  template <class TBase, class TDeMuonDetector>
  using Consumer =
      Gaudi::Functional::Consumer<void( const LHCb::MCHits&, const LHCb::MCHeader&, const TDeMuonDetector& ), TBase>;

  template <class TBase, class TDeMuonDetector, const std::string& TDeMuonLocation>
  class HitChecker : public Consumer<TBase, TDeMuonDetector> {
  public:
    HitChecker( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer<TBase, TDeMuonDetector>( name, pSvcLocator,
                                            { { "MuonHits", LHCb::MCHitLocation::Muon },
                                              { "MCHeader", LHCb::MCHeaderLocation::Default },
                                              { "DeMuonDetector", TDeMuonLocation } } ){};

    void       operator()( LHCb::MCHits const&, LHCb::MCHeader const&, TDeMuonDetector const& ) const override;
    StatusCode finalize() override;
    StatusCode initialize() override;

  private:
    Gaudi::Property<bool> m_isRun1Or2{ this, "IsRun1Or2", false };
    Gaudi::Property<bool> m_detailedMonitor{ this, "DetailedMonitor", false };

    static constexpr size_t regionsNo       = 4;
    static constexpr size_t maxStationsNo   = 5;
    size_t                  m_nBookStations = 5;

    mutable boost::container::static_vector<std::array<int, regionsNo>, maxStationsNo> nhit{};
    mutable boost::container::static_vector<std::array<int, regionsNo>, maxStationsNo> cnt{};

    mutable boost::container::static_vector<int, maxStationsNo> nhit_ri{};
    mutable boost::container::static_vector<int, maxStationsNo> cnt_ri{};

    mutable std::mutex                                  m_lazy_lock;
    mutable std::map<unsigned int, AIDA::IHistogram1D*> m_histos;
    mutable std::atomic_uint                            m_hit_outside_gaps{ 0 };
  };

  using MuonHitCheckerDetDesc = HitChecker<DetDescBase, DeMuonDetector, DeMuonLocation::Default>;
  DECLARE_COMPONENT_WITH_ID( MuonHitCheckerDetDesc, "MuonHitCheckerDetDesc" )

#ifdef USE_DD4HEP
  using MuonHitCheckerDD4hep = HitChecker<DD4hepBase, LHCb::Detector::DeMuon, LHCb::Detector::Muon::Location::Default>;
  DECLARE_COMPONENT_WITH_ID( MuonHitCheckerDD4hep, "MuonHitCheckerDD4hep" )
#endif

} // namespace Gauss::Muon

template <class TBase, class TDeMuonDetector, const std::string& TDeMuonLocation>
StatusCode Gauss::Muon::HitChecker<TBase, TDeMuonDetector, TDeMuonLocation>::initialize() {
  return Consumer<TBase, TDeMuonDetector>::initialize().andThen( [&]() -> StatusCode {
    // Initialize quantities Station & Region related
    if ( !m_isRun1Or2.value() ) { m_nBookStations = 4; }
    for ( size_t i = 0; i < m_nBookStations; i++ ) {
      nhit_ri.push_back( 0 );
      cnt_ri.push_back( 0 );
      cnt.push_back( { 0, 0, 0, 0 } );
      nhit.push_back( { 0, 0, 0, 0 } );
    }

    if ( this->fullDetail() ) {
      for ( unsigned int station = 0; station < m_nBookStations; station++ ) {
        for ( unsigned int region = 0; region < regionsNo; region++ ) {
          int hh              = station * regionsNo + region;
          m_histos[hh + 2000] = this->book( hh + 2000, "Radial Multiplicity", 0., 6000., 200 );
          m_histos[hh + 1000] = this->book( hh + 1000, "Time multiplicity", 0., 100., 200 );
        }
      }
    }

    return StatusCode::SUCCESS;
  } );
}

template <class TBase, class TDeMuonDetector, const std::string& TDeMuonLocation>
void Gauss::Muon::HitChecker<TBase, TDeMuonDetector, TDeMuonLocation>::operator()(
    const LHCb::MCHits& hits, const LHCb::MCHeader& header, const TDeMuonDetector& muonD ) const {

  long                        local_evt = header.evtNumber();
  std::lock_guard<std::mutex> guard_lock( m_lazy_lock );

  this->debug() << "==> Execute MuonHitChecker" << endmsg;

  boost::container::static_vector<std::array<int, regionsNo>, maxStationsNo> tnhit{};
  boost::container::static_vector<int, maxStationsNo>                        tnhit_ri{};
  boost::container::static_vector<int, maxStationsNo>                        tmhit_ri{};

  for ( size_t i = 0; i < m_nBookStations; i++ ) {
    tnhit_ri.push_back( 0 );
    tmhit_ri.push_back( 0 );
    tnhit.push_back( { 0, 0, 0, 0 } );
  }

  std::vector<float> m_sta, m_reg, m_con, m_x, m_y, m_z, m_time;
  std::vector<float> m_id, m_px, m_py, m_pz, m_E, m_xv, m_yv, m_zv, m_tv, m_mom;
  std::vector<float> m_ple, m_hen, m_dix, m_dxz, m_dyz;

  // get the MCHits
  int MyDetID;
  // Loop over Muon Hits of given type
  for ( auto& hit : hits ) {
    MyDetID = hit->sensDetID();
    if ( MyDetID < 0 ) m_hit_outside_gaps++;
    if ( MyDetID < 0 ) continue;

    // Needs to extract info from sens ID
    int station = muonD.stationID( MyDetID );
    int region  = muonD.regionID( MyDetID );

    this->debug() << " Station, Region :: " << station << " " << region << endmsg;

    float xpos = ( hit->entry().x() + hit->exit().x() ) / 2.0;
    float ypos = ( hit->entry().y() + hit->exit().y() ) / 2.0;
    float zpos = ( hit->entry().z() + hit->exit().z() ) / 2.0;
    float time = hit->time();

    // New monitored quantities
    float plen = hit->pathLength();
    float hene = hit->energy();
    float hdis = hit->displacement().x();
    float hdxz = hit->dxdz();
    float hdyz = hit->dydz();

    // Temporary monitoring (need to check if already available)
    m_ple.push_back( plen );
    m_hen.push_back( hene );
    m_dix.push_back( hdis );
    m_dxz.push_back( hdxz );
    m_dyz.push_back( hdyz );

    double tof = time - sqrt( xpos * xpos + ypos * ypos + zpos * zpos ) / 300.0;
    if ( tof < 0.1 ) tof = 0.1;
    float r = sqrt( xpos * xpos + ypos * ypos );

    m_sta.push_back( station );
    m_reg.push_back( region );
    // Only Geant Container available
    m_con.push_back( 0 );

    m_x.push_back( xpos );
    m_y.push_back( ypos );
    m_z.push_back( zpos );
    m_time.push_back( time );

    // Fill some histos
    unsigned int hh = station * regionsNo + region;
    if ( this->fullDetail() == true ) {
      if ( m_histos.find( hh + 2000 ) != std::end( m_histos ) ) { m_histos[hh + 2000]->fill( r ); }
      if ( m_histos.find( hh + 1000 ) != std::end( m_histos ) ) { m_histos[hh + 1000]->fill( tof ); }
    }

    // MC truth
    const LHCb::MCParticle* particle = hit->mcParticle();
    if ( particle ) {
      if ( abs( particle->particleID().pid() ) < 100000 ) { m_id.push_back( particle->particleID().pid() ); }

      m_px.push_back( particle->momentum().px() );
      m_py.push_back( particle->momentum().py() );
      // Pz sign tells you the particle direction
      m_pz.push_back( particle->momentum().pz() );
      m_E.push_back( particle->momentum().e() );

      // Particle Vertex studies
      m_xv.push_back( particle->originVertex()->position().x() );
      m_yv.push_back( particle->originVertex()->position().y() );
      m_zv.push_back( particle->originVertex()->position().z() );
      m_tv.push_back( particle->originVertex()->time() );

      const LHCb::MCParticle* moth = particle->mother();
      if ( moth && ( abs( moth->particleID().pid() ) < 100000 ) ) {
        m_mom.push_back( moth->particleID().pid() );
      } else {
        m_mom.push_back( 0 );
      }
    } else {
      m_id.push_back( 0 );
      m_px.push_back( 0 );
      m_py.push_back( 0 );
      m_pz.push_back( 0 );
      m_E.push_back( 0 );
      m_xv.push_back( 0 );
      m_yv.push_back( 0 );
      m_zv.push_back( 0 );
      m_tv.push_back( 0 );
      m_mom.push_back( 0 );
    }

    if ( abs( particle->particleID().pid() ) == 13 ) { tmhit_ri[station]++; }
    tnhit[station][region]++;
    tnhit_ri[station]++;
  }
  for ( unsigned int s = 0; s < m_nBookStations; s++ ) {
    // Looking at mean number of hits (intregrated over regions)
    cnt_ri[s]++;
    nhit_ri[s] += tnhit_ri[s];

    for ( unsigned int r = 0; r < regionsNo; r++ ) {
      // Looking at mean number of hits
      cnt[s][r]++;
      nhit[s][r] += tnhit[s][r];
    }
  }

  // book vectors of histos
  if ( m_detailedMonitor.value() == true ) {
    typename Consumer<TBase, TDeMuonDetector>::Tuple nt1 = this->nTuple( 1, "MC HITS", CLID_ColumnWiseTuple );
    this->info() << "Doing ntuple stuff" << endmsg;
    //    nt1->column("Run", m_run,0,1000000);
    int pippo = local_evt;
    nt1->column( "Event", pippo, 0, 10000 ).ignore();

    nt1->farray( "is", m_sta, "Nhits", 1000 ).ignore();
    nt1->farray( "ir", m_reg, "Nhits", 1000 ).ignore();
    nt1->farray( "ic", m_con, "Nhits", 1000 ).ignore();
    nt1->farray( "x", m_x, "Nhits", 1000 ).ignore();
    nt1->farray( "y", m_y, "Nhits", 1000 ).ignore();
    nt1->farray( "z", m_z, "Nhits", 1000 ).ignore();
    nt1->farray( "t", m_time, "Nhits", 1000 ).ignore();
    nt1->farray( "id", m_id, "Nhits", 1000 ).ignore();
    nt1->farray( "px", m_px, "Nhits", 1000 ).ignore();
    nt1->farray( "py", m_py, "Nhits", 1000 ).ignore();
    nt1->farray( "pz", m_pz, "Nhits", 1000 ).ignore();
    nt1->farray( "E", m_E, "Nhits", 1000 ).ignore();
    nt1->farray( "xv", m_xv, "Nhits", 1000 ).ignore();
    nt1->farray( "yv", m_yv, "Nhits", 1000 ).ignore();
    nt1->farray( "zv", m_zv, "Nhits", 1000 ).ignore();
    nt1->farray( "tv", m_tv, "Nhits", 1000 ).ignore();
    nt1->farray( "idm", m_mom, "Nhits", 1000 ).ignore();
    nt1->farray( "pl", m_ple, "Nhits", 1000 ).ignore();
    nt1->farray( "he", m_hen, "Nhits", 1000 ).ignore();
    nt1->farray( "dx", m_dix, "Nhits", 1000 ).ignore();
    nt1->farray( "xz", m_dxz, "Nhits", 1000 ).ignore();
    nt1->farray( "yz", m_dyz, "Nhits", 1000 ).ignore();
    nt1->write().ignore();
  }
}

template <class TBase, class TDeMuonDetector, const std::string& TDeMuonLocation>
StatusCode Gauss::Muon::HitChecker<TBase, TDeMuonDetector, TDeMuonLocation>::finalize() {
  this->info() << "-----------------------------------------------------------------" << endmsg;
  this->info() << "       Muon Monitoring Table " << endmsg;
  this->info() << "-----------------------------------------------------------------" << endmsg;

  if ( !m_isRun1Or2.value() ) {
    this->info() << " M2        M3        M4        M5 " << endmsg;
  } else {
    this->info() << " M1        M2        M3        M4        M5 " << endmsg;
  }

  for ( unsigned int r = 0; r < regionsNo; r++ ) {
    for ( unsigned int s = 0; s < m_nBookStations; s++ ) {
      if ( cnt[s][r] ) {
        this->info() << format( "%5.3lf  ", (double)nhit[s][r] / cnt[s][r] );
      } else {
        this->info() << "0.000  ";
      }
    }
    this->info() << " R" << r + 1 << endmsg;
  }
  this->info() << "-----------------------------------------------------------------" << endmsg;
  this->info() << "-------    Integrated over regions    ---------------------------" << endmsg;
  this->info() << "-----------------------------------------------------------------" << endmsg;
  for ( unsigned int s = 0; s < m_nBookStations; s++ ) {
    if ( cnt_ri[s] ) {
      this->info() << format( "%5.3lf  ", (double)nhit_ri[s] / cnt_ri[s] );
    } else {
      this->info() << "0.000  ";
    }
  }

  this->info() << " allR" << endmsg;
  this->info() << " number of hits generated outside gaps volume " << m_hit_outside_gaps << endmsg;

  return Consumer<TBase, TDeMuonDetector>::finalize();
}
