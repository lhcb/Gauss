/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHeader.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

#include "DetDesc/ConditionAccessorHolder.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#ifdef USE_DD4HEP
#  include "Detector/Muon/DeMuon.h"
#  include "Detector/Muon/Namespace.h"
#  include "LbDD4hep/ConditionAccessorHolder.h"
#endif

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"

#include <boost/lexical_cast.hpp>

namespace LHCb {

  using DetDescBase = LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeMuonDetector>;

#ifdef USE_DD4HEP
  using DD4hepBase = LHCb::Det::LbDD4hep::usesBaseAndConditions<GaudiTupleAlg, LHCb::Detector::DeMuon>;
#endif

  template <class TBase, class TDeMuonDetector>
  using Consumer = Gaudi::Functional::Consumer<void( MCHits const&, MCHeader const&, TDeMuonDetector const& ), TBase>;

  /**
   *  @author Stefania Vecchi
   *  @date   2009-03-12
   */
  template <class TBase, class TDeMuonDetector, const std::string& TDeMuonLocation>
  struct MuonMultipleScatteringChecker : public Consumer<TBase, TDeMuonDetector> {

    MuonMultipleScatteringChecker( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer<TBase, TDeMuonDetector>( name, pSvcLocator,
                                            { { "MuonHits", MCHitLocation::Muon },
                                              { "MCHeader", MCHeaderLocation::Default },
                                              { "MuonLocation", TDeMuonLocation } } ) {}

    void                  operator()( MCHits const&, MCHeader const&, TDeMuonDetector const& ) const override;
    Gaudi::Property<bool> m_fillNtuple{ this, "fillNtuple", false, "Fill the ntuple" };
  };

  using MuonMSCheckerDetDesc = MuonMultipleScatteringChecker<DetDescBase, DeMuonDetector, DeMuonLocation::Default>;
  DECLARE_COMPONENT_WITH_ID( MuonMSCheckerDetDesc, "MuonMultipleScatteringCheckerDetDesc" )

#ifdef USE_DD4HEP
  using MuonMSCheckerDD4hep =
      MuonMultipleScatteringChecker<DD4hepBase, Detector::DeMuon, Detector::Muon::Location::Default>;
  DECLARE_COMPONENT_WITH_ID( MuonMSCheckerDD4hep, "MuonMultipleScatteringCheckerDD4hep" )
#endif
} // namespace LHCb

template <class TBase, class TDeMuonDetector, const std::string& TDeMuonLocation>
void LHCb::MuonMultipleScatteringChecker<TBase, TDeMuonDetector, TDeMuonLocation>::operator()(
    LHCb::MCHits const& hits, LHCb::MCHeader const& evt, TDeMuonDetector const& muonD ) const {
  int nEvt = evt.evtNumber();

  int                MyDetID;
  std::string        name, title, prof, part;
  std::vector<float> m_station, m_chamber, m_region, m_x, m_y, m_z, m_time, m_dxdz, m_dydz, m_ene;
  std::vector<float> m_p, m_pz, m_id, m_tx, m_ty;

  // Loop over Muon Hits of given type
  for ( auto& hit : hits ) {

    MyDetID = hit->sensDetID();
    if ( MyDetID < 0 ) continue;

    // Needs to extract info from sens ID
    int station = muonD.stationID( MyDetID );
    int region  = muonD.regionID( MyDetID );
    int chamber = muonD.chamberID( MyDetID );

    float xpos = hit->midPoint().x();
    float ypos = hit->midPoint().y();
    float zpos = hit->midPoint().z();
    float time = hit->time();

    float ene  = hit->energy();
    float dxdz = hit->dxdz();
    float dydz = hit->dydz();

    m_station.push_back( station );
    m_region.push_back( region );
    m_chamber.push_back( chamber );

    this->debug() << " Station " << station << " chamber " << chamber << "x= " << xpos << ", y= " << ypos
                  << " ,z=" << zpos << endmsg;

    m_x.push_back( xpos );
    m_y.push_back( ypos );
    m_z.push_back( zpos );
    m_time.push_back( time );
    m_ene.push_back( ene );
    m_dxdz.push_back( dxdz );
    m_dydz.push_back( dydz );

    // MC truth
    const MCParticle* particle = hit->mcParticle();
    if ( particle ) {
      if ( abs( particle->particleID().pid() ) < 100000 ) {
        m_id.push_back( particle->particleID().pid() );
      } else
        m_id.push_back( 99999 );

      m_tx.push_back( particle->momentum().px() / particle->momentum().pz() );
      m_ty.push_back( particle->momentum().py() / particle->momentum().pz() );
      // Pz sign tells you the particle direction
      m_pz.push_back( particle->momentum().pz() );
      m_p.push_back( particle->p() );

    } else {
      m_id.push_back( 99999 );
      m_tx.push_back( 99999 );
      m_ty.push_back( 99999 );
      m_pz.push_back( 99999 );
      m_p.push_back( 99999 );
    }
  }

  std::vector<float> c_station, c_chamber, c_region, c_x, c_y, c_z, c_time, c_dxdz, c_dydz, c_ene;
  std::vector<float> c_p, c_pz, c_id, c_tx, c_ty;
  this->debug() << " m-vector size " << m_x.size() << endmsg;
  for ( int i1 = 0; i1 < int( m_station.size() ); ++i1 ) {
    this->debug() << " " << m_x[i1] << " " << m_y[i1] << " " << m_z[i1] << " " << m_station[i1] << " " << m_p[i1] << " "
                  << m_id[i1] << " -- " << m_time[i1] << endmsg;
    bool flag = true;
    for ( int i2 = 0; i2 < int( c_station.size() ); ++i2 ) {
      if ( m_station[i1] == c_station[i2] && m_p[i1] == c_p[i2] ) flag = false;
    }
    if ( flag ) {
      c_station.push_back( m_station[i1] );
      c_region.push_back( m_region[i1] );
      c_chamber.push_back( m_chamber[i1] );
      c_x.push_back( 0 );
      c_y.push_back( 0 );
      c_z.push_back( 0 );
      c_dxdz.push_back( 0 );
      c_dydz.push_back( 0 );
      c_time.push_back( 0 );
      c_ene.push_back( 0 );
      c_p.push_back( m_p[i1] );
      c_pz.push_back( m_pz[i1] );
      c_id.push_back( m_id[i1] );
      c_tx.push_back( m_tx[i1] );
      c_ty.push_back( m_ty[i1] );
    }
  }
  for ( int i2 = 0; i2 < int( c_station.size() ); ++i2 ) {
    float ngap = 0;
    for ( int i1 = 0; i1 < int( m_station.size() ); ++i1 ) {
      if ( m_station[i1] == c_station[i2] && m_p[i1] == c_p[i2] ) {

        c_x[i2] += m_x[i1];
        c_y[i2] += m_y[i1];
        c_z[i2] += m_z[i1];
        c_dxdz[i2] += m_dxdz[i1];
        c_dydz[i2] += m_dydz[i1];
        c_time[i2] += m_time[i1];
        c_ene[i2] += m_ene[i1];
        ++ngap;
      }
    }
    c_x[i2]    = c_x[i2] / ngap;
    c_y[i2]    = c_y[i2] / ngap;
    c_z[i2]    = c_z[i2] / ngap;
    c_dxdz[i2] = c_dxdz[i2] / ngap;
    c_dydz[i2] = c_dydz[i2] / ngap;
    c_time[i2] = c_time[i2] / ngap;
  }

  this->debug() << " c-vector size " << c_x.size() << endmsg;
  for ( int i1 = 0; i1 < int( c_station.size() ); ++i1 ) {
    this->debug() << " " << c_x[i1] << " " << c_y[i1] << " " << c_z[i1] << " " << c_station[i1] << " " << c_p[i1] << " "
                  << c_id[i1] << " -- " << c_time[i1] << endmsg;

    for ( int i2 = i1 + 1; i2 < int( c_station.size() ); ++i2 ) {
      if ( c_p[i1] == 99999 ) continue;
      if ( fabs( c_id[i1] ) != 13 ) continue;
      if ( c_p[i1] == c_p[i2] && c_pz[i1] == c_pz[i2] ) {
        if ( c_station[i2] - c_station[i1] == 1 ) {
          double deltax = c_x[i2] - ( c_x[i1] + tan( c_dxdz[i1] ) * ( c_z[i2] - c_z[i1] ) );
          double deltay = c_y[i2] - ( c_y[i1] + tan( c_dydz[i1] ) * ( c_z[i2] - c_z[i1] ) );
          ;
          double deltaTx = c_dxdz[i2] - c_dxdz[i1];
          double deltaTy = c_dydz[i2] - c_dydz[i1];
          this->debug() << "(" << c_station[i1] << "," << c_station[i2] << ") deltaX= " << deltax
                        << " deltaY= " << deltay << "deltaTx= " << deltaTx << " deltaTy= " << deltaTy << " p "
                        << c_p[i1] << endmsg;

          int ista = int( c_station[i1] );
          if ( c_station[i2] > ista ) ista = int( c_station[i2] );
          prof     = "_prof";
          part     = " MF" + boost::lexical_cast<std::string>( ista - 1 );
          int calo = 1; // calo is first station here
          if ( ista == calo ) part = " Calo";
          name  = "dxdTx_MF" + boost::lexical_cast<std::string>( ista );
          title = "delta x vs delta theta x after" + part;
          double xmax, xmin;
          xmax = 20;
          xmin = -xmax;
          double ymax, ymin;
          ymax = 5;
          if ( ista == 1 ) ymax = 20;
          ymin = -ymax;
          this->plot2D( deltaTx * 1000, deltax, name, title, xmin, xmax, ymin, ymax, 201, 201 );
          this->profile1D( deltaTx * 1000, deltax, name + prof, title + prof, xmin, xmax, 201 );

          name  = "dydTy_MF" + boost::lexical_cast<std::string>( ista );
          title = "delta y vs delta theta y after" + part;
          this->plot2D( deltaTy * 1000, deltay, name, title, xmin, xmax, ymin, ymax, 201, 201 );
          this->profile1D( deltaTy * 1000, deltay, name + prof, title + prof, xmin, xmax, 201 );

          name  = "pdx_MF" + boost::lexical_cast<std::string>( ista );
          title = "p vs delta x after" + part;
          this->plot2D( deltax, c_p[i1] / Gaudi::Units::GeV, name, title, ymin, ymax, 0., 100., 201, 20 );
          this->profile1D( deltax, c_p[i1] / Gaudi::Units::GeV, name + prof, title + prof, ymin, ymax, 201 );

          name  = "pdy_MF" + boost::lexical_cast<std::string>( ista );
          title = "p vs delta y after" + part;
          this->plot2D( deltay, c_p[i1] / Gaudi::Units::GeV, name, title, ymin, ymax, 0., 100., 201, 20 );
          this->profile1D( deltay, c_p[i1] / Gaudi::Units::GeV, name + prof, title + prof, ymin, ymax, 201 );

          name  = "pdTx_MF" + boost::lexical_cast<std::string>( ista );
          title = "p vs delta theta x after" + part;
          this->plot2D( deltaTx * 1000, c_p[i1] / Gaudi::Units::GeV, name, title, xmin, xmax, 0., 100., 201, 20 );
          this->profile1D( deltaTx * 1000, c_p[i1] / Gaudi::Units::GeV, name + prof, title + prof, xmin, xmax, 201 );

          name  = "pdTy_MF" + boost::lexical_cast<std::string>( ista );
          title = "p vs delta theta y after" + part;
          this->plot2D( deltaTy * 1000, c_p[i1] / Gaudi::Units::GeV, name, title, xmin, xmax, 0., 100., 201, 20 );
          this->profile1D( deltaTy * 1000, c_p[i1] / Gaudi::Units::GeV, name + prof, title + prof, xmin, xmax, 201 );
        }
      }
    }
  }

  // book vectors of histos
  if ( m_fillNtuple.value() ) {
    typename Consumer<TBase, TDeMuonDetector>::Tuple nt1 = this->nTuple( 1, "MC Muon HITS", CLID_ColumnWiseTuple );
    // nt1->column("Event",nEvt,(short int) 0,(short int) 10000);
    nt1->column( "Event", nEvt, 0, 10000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "station", c_station, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "region", c_region, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "chamber", c_chamber, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "x", c_x, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "y", c_y, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "z", c_z, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "dxdz", c_dxdz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "dydz", c_dydz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "time", c_time, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ene", c_ene, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "id", c_id, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "p", c_p, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "pz", c_pz, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "tx", c_tx, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->farray( "ty", c_ty, "Nhits", 1000 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    nt1->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
}
