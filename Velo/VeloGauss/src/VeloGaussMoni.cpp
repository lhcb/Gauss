/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

#include "VeloDet/DeVelo.h"

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiKernel/Vector4DTypes.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

namespace LHCb {

  /**
   *  @author Tomasz Szumlak & Chris Parkes
   *  @date   2005-12-13
   */
  class VeloGaussMoni : public Gaudi::Functional::Consumer<void( MCHits const&, MCHits const&, DeVelo const& ),
                                                           DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeVelo>> {
  public:
    VeloGaussMoni( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { { "VeloMCHits", LHCb::MCHitLocation::Velo },
                      { "PuVetoMCHits", LHCb::MCHitLocation::PuVeto },
                      { "VeloDetLocation", DeVeloLocation::Default } } ){};

    StatusCode initialize() override;
    StatusCode finalize() override;

    void operator()( LHCb::MCHits const&, LHCb::MCHits const&, DeVelo const& ) const override;

  protected:
    StatusCode veloMCHitMonitor( LHCb::MCHits const&, DeVelo const& veloDet ) const;
    StatusCode veloPileUpMCHitMonitor( LHCb::MCHits const& ) const;

  private:
    void bookHistograms();

    Gaudi::Property<int>      m_print{ this, "Print", 0 };
    Gaudi::Property<bool>     m_printInfo{ this, "PrintInfo", false };
    Gaudi::Property<bool>     m_detailedMonitor{ this, "DetailedMonitor", true };
    Gaudi::Property<bool>     m_testMCHit{ this, "TestMCHit", true };
    Gaudi::Property<bool>     m_testPileUpMCHit{ this, "TestPileupMCHit", true };
    mutable std::atomic_ulong m_nMCH{ 0 };
    mutable std::atomic_ulong m_nMCH2{ 0 };
    mutable std::atomic_ulong m_nPUMCH{ 0 };
    mutable std::atomic_ulong m_nPUMCH2{ 0 };
    mutable std::atomic_uint  m_nEvent{ 0 };
    mutable std::mutex        m_lazy_lock;

    AIDA::IHistogram1D* m_hist_nMCHits{ nullptr };
    AIDA::IHistogram1D* m_hist_eDepSi{ nullptr };
    AIDA::IHistogram1D* m_hist_TOF{ nullptr };

    AIDA::IHistogram2D* m_hist_entryZX{ nullptr };
    AIDA::IHistogram2D* m_hist_entryXY{ nullptr };
    AIDA::IHistogram2D* m_hist_exitZX{ nullptr };
    AIDA::IHistogram2D* m_hist_exitXY{ nullptr };

    AIDA::IHistogram2D* m_hist_entryRRXY{ nullptr };
    AIDA::IHistogram2D* m_hist_entryRRZX{ nullptr };
    AIDA::IHistogram2D* m_hist_entryPDRXY{ nullptr };
    AIDA::IHistogram2D* m_hist_entryPURXY{ nullptr };
    AIDA::IHistogram2D* m_hist_entryRLXY{ nullptr };
    AIDA::IHistogram2D* m_hist_entryRLZX{ nullptr };
    AIDA::IHistogram2D* m_hist_entryPDLXY{ nullptr };
    AIDA::IHistogram2D* m_hist_entryPULXY{ nullptr };

    AIDA::IHistogram1D* m_hist_eMCPart{ nullptr };

    AIDA::IHistogram1D* m_hist_nMCPUHits{ nullptr };
    AIDA::IHistogram1D* m_hist_eDepSiPU{ nullptr };
    AIDA::IHistogram1D* m_hist_TOFPU{ nullptr };

    AIDA::IHistogram2D* m_hist_entryZXPU{ nullptr };
    AIDA::IHistogram2D* m_hist_entryXYPU{ nullptr };
    AIDA::IHistogram2D* m_hist_exitZXPU{ nullptr };
    AIDA::IHistogram2D* m_hist_exitXYPU{ nullptr };

    AIDA::IHistogram1D* m_hist_eMCPartPU{ nullptr };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( VeloGaussMoni, "VeloGaussMoni" )

} // namespace LHCb

StatusCode LHCb::VeloGaussMoni::initialize() {
  return Consumer::initialize().andThen( [&] {
    setHistoTopDir( "Velo/" );
    bookHistograms();
    if ( !( m_testMCHit || m_testPileUpMCHit ) ) {
      error() << " ==> VeloGaussMoni asked to monitor nothing! " << endmsg;
    }
    return StatusCode::SUCCESS;
  } );
}

void LHCb::VeloGaussMoni::operator()( LHCb::MCHits const& veloMCHits, LHCb::MCHits const& veloPileUpMCHits,
                                      DeVelo const& veloDet ) const {
  // FIXME: Cannot have the threads interfere with each others plotting
  // so instead of doing something smart we just lock this for now ...
  std::lock_guard<std::mutex> guard_lock( m_lazy_lock );
  m_nEvent++;
  if ( m_testMCHit ) veloMCHitMonitor( veloMCHits, veloDet ).ignore();
  if ( m_testPileUpMCHit ) veloPileUpMCHitMonitor( veloPileUpMCHits ).ignore();
}

StatusCode LHCb::VeloGaussMoni::finalize() {
  double ave_nMCH    = (double)m_nMCH / m_nEvent;
  double ave_nMCH2   = (double)m_nMCH2 / m_nEvent;
  double err_nMCH    = sqrt( ( ave_nMCH2 - ( ave_nMCH * ave_nMCH ) ) / m_nEvent );
  double ave_nPUMCH  = (double)m_nPUMCH / m_nEvent;
  double ave_nPUMCH2 = (double)m_nPUMCH2 / m_nEvent;
  double err_nPUMCH  = sqrt( ( ave_nPUMCH2 - ( ave_nPUMCH * ave_nPUMCH ) ) / m_nEvent );

  info() << "------------------------------------------------------" << endmsg;
  info() << "                - VeloGaussMoni table -                 " << endmsg;
  info() << "------------------------------------------------------" << endmsg;
  if ( m_nMCH > 0 ) {
    info() << "| Number of MCHits/Event:       " << ave_nMCH << "+/-" << err_nMCH << endmsg;
    info() << "| Number of PileUpMCHits/Event: " << ave_nPUMCH << "+/-" << err_nPUMCH << endmsg;
  } else {
    info() << "| ==> No MCHits found! " << endmsg;
  }
  info() << "------------------------------------------------------" << endmsg;

  return Consumer::finalize(); // must be called after all other actions
}

StatusCode LHCb::VeloGaussMoni::veloMCHitMonitor( LHCb::MCHits const& veloMCHits, DeVelo const& veloDet ) const {
  auto size = veloMCHits.size();
  m_nMCH += size;
  m_nMCH2 += size * size;

  m_hist_nMCHits->fill( size );

  // loop over all MCHits stored into the container
  for ( auto& veloMCHit : veloMCHits ) {
    if ( m_printInfo ) {
      info() << " ==> Test MCHit: \n"
             << " sensor: " << ( veloMCHit->sensDetID() )
             << "\nentry: x= " << ( veloMCHit->entry() ).x() / Gaudi::Units::mm << " mm"
             << "\nentry: y= " << ( veloMCHit->entry() ).y() / Gaudi::Units::mm << " mm"
             << "\nentry: z= " << ( veloMCHit->entry() ).z() / Gaudi::Units::mm << " mm"
             << "\nexit: x= " << ( veloMCHit->exit() ).x() / Gaudi::Units::mm << " mm"
             << "\nexit: y= " << ( veloMCHit->exit() ).y() / Gaudi::Units::mm << " mm"
             << "\nexit: z= " << ( veloMCHit->exit() ).z() / Gaudi::Units::mm << " mm"
             << "energy: " << ( veloMCHit->energy() ) / Gaudi::Units::eV << " eV"
             << "TOF: " << ( veloMCHit->time() ) / Gaudi::Units::ns << " ns" << endmsg;
    }

    if ( m_detailedMonitor ) {
      m_hist_eDepSi->fill( veloMCHit->energy() / Gaudi::Units::eV );
      m_hist_entryZX->fill( veloMCHit->entry().z() / Gaudi::Units::cm, veloMCHit->entry().x() / Gaudi::Units::cm );
      m_hist_entryXY->fill( veloMCHit->entry().x() / Gaudi::Units::cm, veloMCHit->entry().y() / Gaudi::Units::cm );
      m_hist_exitZX->fill( veloMCHit->exit().z() / Gaudi::Units::cm, veloMCHit->exit().x() / Gaudi::Units::cm );
      m_hist_exitXY->fill( veloMCHit->exit().x() / Gaudi::Units::cm, veloMCHit->exit().y() / Gaudi::Units::cm );
      m_hist_TOF->fill( veloMCHit->time() / Gaudi::Units::ns );

      const DeVeloSensor* sensor = veloDet.sensor( veloMCHit->sensDetID() );
      double              x      = veloMCHit->entry().x() / Gaudi::Units::cm;
      double              y      = veloMCHit->entry().y() / Gaudi::Units::cm;
      double              z      = veloMCHit->entry().z() / Gaudi::Units::cm;
      if ( sensor->isRight() ) {
        if ( sensor->isR() ) {
          m_hist_entryRRXY->fill( x, y );
          m_hist_entryRRZX->fill( z, x );
        } else if ( sensor->isPhi() ) {
          if ( sensor->isDownstream() ) {
            m_hist_entryPDRXY->fill( x, y );
          } else {
            m_hist_entryPURXY->fill( x, y );
          }
        }
      } else {
        if ( sensor->isR() ) {
          m_hist_entryRLXY->fill( x, y );
          m_hist_entryRLZX->fill( z, x );
        } else if ( sensor->isPhi() ) {
          if ( sensor->isDownstream() ) {
            m_hist_entryPDLXY->fill( x, y );
          } else {
            m_hist_entryPULXY->fill( x, y );
          }
        }
      }
    }
    // get access to the MCParticle which made the hit, and write out 4-mom
    const MCParticle* myMCParticle = veloMCHit->mcParticle();
    if ( 0 != myMCParticle ) {
      Gaudi::LorentzVector fMom = myMCParticle->momentum();
      m_hist_eMCPart->fill( fMom.e() / Gaudi::Units::GeV );
      if ( m_printInfo ) {
        info() << " ==> MCHit - MCParticle: "
               << "\np_x = " << fMom.px() / Gaudi::Units::GeV << "\np_y = " << fMom.py() / Gaudi::Units::GeV
               << "\np_z = " << fMom.pz() / Gaudi::Units::GeV << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode LHCb::VeloGaussMoni::veloPileUpMCHitMonitor( LHCb::MCHits const& veloPileUpMCHits ) const {
  debug() << " ==> VeloGaussMoni::VeloPileUpMCHitMonitor " << endmsg;
  auto size = veloPileUpMCHits.size();
  m_nPUMCH += size;
  m_nPUMCH2 += size * size;

  m_hist_nMCPUHits->fill( size );

  // loop over all hits sotred into the Pile Up Hits container
  for ( auto& veloPileUpMCHit : veloPileUpMCHits ) {
    if ( m_printInfo ) {
      info() << " ==> Test Pile Up MCHit: \n"
             << " sensor: " << ( veloPileUpMCHit->sensDetID() )
             << "\nentry: x= " << ( veloPileUpMCHit->entry() ).x() / Gaudi::Units::mm << " mm"
             << "\nentry: y= " << ( veloPileUpMCHit->entry() ).y() / Gaudi::Units::mm << " mm"
             << "\nentry: z= " << ( veloPileUpMCHit->entry() ).z() / Gaudi::Units::mm << " mm"
             << "\nexit: x= " << ( veloPileUpMCHit->exit() ).x() / Gaudi::Units::mm << " mm"
             << "\nexit: y= " << ( veloPileUpMCHit->exit() ).y() / Gaudi::Units::mm << " mm"
             << "\nexit: z= " << ( veloPileUpMCHit->exit() ).z() / Gaudi::Units::mm << " mm"
             << "energy: " << ( veloPileUpMCHit->energy() ) / Gaudi::Units::eV << " eV"
             << "TOF: " << ( veloPileUpMCHit->time() ) / Gaudi::Units::ns << " ns" << endmsg;
    }
    //
    if ( m_detailedMonitor ) {
      m_hist_eDepSiPU->fill( veloPileUpMCHit->energy() / Gaudi::Units::eV );
      m_hist_entryXYPU->fill( veloPileUpMCHit->entry().x() / Gaudi::Units::cm,
                              veloPileUpMCHit->entry().y() / Gaudi::Units::cm );
      m_hist_entryZXPU->fill( veloPileUpMCHit->entry().z() / Gaudi::Units::cm,
                              veloPileUpMCHit->entry().x() / Gaudi::Units::cm );
      m_hist_exitXYPU->fill( veloPileUpMCHit->exit().x() / Gaudi::Units::cm,
                             veloPileUpMCHit->exit().y() / Gaudi::Units::cm );
      m_hist_exitZXPU->fill( veloPileUpMCHit->exit().z() / Gaudi::Units::cm,
                             veloPileUpMCHit->exit().x() / Gaudi::Units::cm );
      m_hist_TOFPU->fill( veloPileUpMCHit->time() / Gaudi::Units::ns );
    }
    // get access to the MCParticle which made the hit, and write out 4-mom
    const MCParticle* myMCParticle = veloPileUpMCHit->mcParticle();
    if ( 0 != myMCParticle ) {
      Gaudi::LorentzVector fMom = myMCParticle->momentum();
      plot( fMom.e() / Gaudi::Units::GeV, "eMCPartPU", "Particle energy [GeV]", 0., 50., 100 );
      if ( m_printInfo ) {
        info() << " ==> MCHit - MCParticle: "
               << "\np_x = " << fMom.px() / Gaudi::Units::GeV << "\np_y = " << fMom.py() / Gaudi::Units::GeV
               << "\np_z = " << fMom.pz() / Gaudi::Units::GeV << "\nE = " << fMom.e() / Gaudi::Units::GeV << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

void LHCb::VeloGaussMoni::bookHistograms() {
  m_hist_nMCHits   = book1D( "nMCHits", "Number of hits in Velo per event", 0., 3000., 100 );
  m_hist_eDepSi    = book( "eDepSi", "Energy deposited in Si [eV]", 0., 300000., 100 );
  m_hist_TOF       = book( "TOF", "Time Of Flight [ns]", 0., 50., 100 );
  m_hist_entryZX   = book2D( "entryZX", "Particle entry point in Si [cm] - ZX plane", -20., 80., 1000, -5., 5., 50 );
  m_hist_entryXY   = book2D( "entryXY", "Particle entry point in Si [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_exitZX    = book2D( "exitZX", "Particle exit point in Si [cm] - ZX plane", -20., 80., 1000, -5., 5., 50 );
  m_hist_exitXY    = book2D( "exitXY", "Particle exit point in Si [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_entryRRXY = book2D( "entryRRXY", "Particle entry point in RRight [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_entryRRZX =
      book2D( "entryRRZX", "Particle entry point in RRight [cm] - ZX plane", -20., 80., 1000, -5., 5., 50 );
  m_hist_entryPDRXY = book2D( "entryPDRXY", "Particle entry point in PhiDR [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_entryPURXY = book2D( "entryPURXY", "Particle entry point in PhiUR [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_entryRLXY  = book2D( "entryRLXY", "Particle entry point in RLeft [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_entryRLZX = book2D( "entryRLZX", "Particle entry point in RLeft [cm] - ZX plane", -20, 80, 1000, -5., 5., 50 );
  m_hist_entryPDLXY = book2D( "entryPDLXY", "Particle entry point in PhiDR [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_entryPULXY = book2D( "entryPULXY", "Particle entry point in PhiUL [cm] - XY plane", -5., 5., 50, -5., 5., 50 );

  m_hist_eMCPart = book( "eMCPart", "Particle energy [GeV]", 0., 50., 100 );

  m_hist_nMCPUHits = book1D( "nMCPUHits", "PileUp: Number of hits in Velo per event", 0., 3000., 100 );
  m_hist_eDepSiPU  = book( "eDepSiPU", "PileUp: Energy deposited in Si [eV]", 0., 300000., 100 );
  m_hist_TOFPU     = book( "TOFPU", "PileUp: Time Of Flight [ns]", 0., 50., 100 );
  m_hist_entryZXPU =
      book2D( "entryZXPU", "PileUp: Particle entry point in Si [cm] - ZX plane", -20., 80., 1000, -5., 5., 50 );
  m_hist_entryXYPU =
      book2D( "entryXYPU", "PileUp: Particle entry point in Si [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_exitZXPU =
      book2D( "exitZXPU", "PileUp: Particle exit point in Si [cm] - ZX plane", -20., 80., 1000, -5., 5., 50 );
  m_hist_exitXYPU = book2D( "exitXYPU", "PileUp: Particle exit point in Si [cm] - XY plane", -5., 5., 50, -5., 5., 50 );

  m_hist_eMCPartPU = book( "eMCPartPU", "PileUp: Particle energy [GeV]", 0., 50., 100 );
}
