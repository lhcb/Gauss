###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Event Type: 70000000
#
# ASCII decay Descriptor: p p > mu+ mu-
#
from Configurables import Generation, Special

Generation().EventType = 70000000
Generation().SampleGenerationTool = "Special"
from Configurables import Special

Generation().addTool(Special)
Generation().Special.ProductionTool = "Pythia8Production"

# Generation options.
Generation().PileUpTool = "FixedLuminosityForRareProcess"
Generation().DecayTool = ""
Generation().SampleGenerationTool = "Special"

# Special options.
Generation().addTool(Special)
Generation().Special.CutTool = ""
Generation().Special.DecayTool = ""

# Madgraph options.
from Configurables import Gauss

Gauss().SampleGenerationToolOptions = {"Commands": ["generate p p > mu+ mu-"]}
