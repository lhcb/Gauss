###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/LbGenXicc
-------------
#]=======================================================================]

string(APPEND CMAKE_Fortran_FLAGS " -fsecond-underscore")

gaudi_add_tests(QMTest)

gaudi_add_library(LbGenXiccLib
    SOURCES
        src/Lib/Confine.cpp
        src/Lib/Counter.cpp
        src/Lib/Funtrans.cpp
        src/Lib/GenXicc.cpp
        src/Lib/Loggrade.cpp
        src/Lib/Mixevnt.cpp
        src/Lib/Mtypeofxi.cpp
        src/Lib/Outpdf.cpp
        src/Lib/Subopen.cpp
        src/Lib/Upcom.cpp
        src/Lib/Usertran.cpp
        src/Lib/Valmatrix.cpp
        src/Lib/Vegasbin.cpp
        src/Lib/Vegasinf.cpp
        src/Lib/Vegcross.cpp
        src/Lib/Wbstate.cpp
        src/Lib/gconfine.F
        src/Lib/gcounter.F
        src/Lib/gfuntrans.F
        src/Lib/gloggrade.F
        src/Lib/gmixevnt.F
        src/Lib/gmtypeofxi.F
        src/Lib/goutpdf.F
        src/Lib/gsubopen.F
        src/Lib/gupcom.F
        src/Lib/gusertran.F
        src/Lib/gvalmatrix.F
        src/Lib/gvegasbin.F
        src/Lib/gvegasinf.F
        src/Lib/gvegcross.F
        src/Lib/gwbstate.F
    LINK
        PUBLIC
            Gauss::GenXicc
            Gauss::LbPythiaLib
)

gaudi_add_module(LbGenXicc
    SOURCES
        src/Components/GenXiccProduction.cpp
        src/Components/LHAupGenXicc.cpp
    LINK
        Gauss::LbHardLib
        Gauss::LbGenXiccLib
)
