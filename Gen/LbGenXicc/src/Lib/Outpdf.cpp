/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common Outpdf ! F. Zhang 01-04-11
#include "LbGenXicc/Outpdf.h"

// set pointer to zero at start
Outpdf::OUTPDF* Outpdf::s_outpdf = 0;

// Constructor
Outpdf::Outpdf() {}

// Destructor
Outpdf::~Outpdf() {}

// access ioutpdf in common
int& Outpdf::ioutpdf() {
  init(); // check COMMON is initialized
  return s_outpdf->ioutpdf;
}

// access ipdfnum in common
int& Outpdf::ipdfnum() {
  init(); // check COMMON is initialized
  return s_outpdf->ipdfnum;
}
