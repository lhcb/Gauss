/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common Valmatrix
#include "LbGenXicc/Valmatrix.h"

// set pointer to zero at start
Valmatrix::VALMATRIX* Valmatrix::s_valmatrix = 0;

// Constructor
Valmatrix::Valmatrix() {}

// Destructor
Valmatrix::~Valmatrix() {}

// access cmfactor in common
double& Valmatrix::cmfactor() {
  init(); // check COMMON is initialized
  return s_valmatrix->cmfactor;
}
