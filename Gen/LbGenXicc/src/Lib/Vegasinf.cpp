/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common Vegasinf
#include "LbGenXicc/Vegasinf.h"

// set pointer to zero at start
Vegasinf::VEGASINF* Vegasinf::s_vegasinf = 0;

// Constructor
Vegasinf::Vegasinf() {}

// Destructor
Vegasinf::~Vegasinf() {}

// access number in common
int& Vegasinf::number() {
  init(); // check COMMON is initialized
  return s_vegasinf->number;
}

// access nitmx in common
int& Vegasinf::nitmx() {
  init(); // check COMMON is initialized
  return s_vegasinf->nitmx;
}
