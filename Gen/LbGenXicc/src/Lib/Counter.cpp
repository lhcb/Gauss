/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common Pydat1
#include "LbGenXicc/Counter.h"

// set pointer to zero at start
Counter::COUNTER* Counter::s_counter = 0;

// Constructor
Counter::Counter() {}

// Destructor
Counter::~Counter() {}

// access ibcstate in common
// int& Counter::ibcstate() {
int& Counter::ixiccstate() {
  init(); // check COMMON is initialized
  return s_counter->ixiccstate;
}

// access nev in common
int& Counter::nev() {
  init(); // check COMMON is initialized
  return s_counter->nev;
}

// GG 16.02.2012
double& Counter::xmaxwgt() {
  init(); // check COMMON is initialized
  return s_counter->xmaxwgt;
}
