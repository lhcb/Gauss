###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Import the necessary modules.
from Configurables import (
    Generation,
    HerwigppProduction,
    Inclusive,
    MinimumBias,
    SignalPlain,
    SignalRepeatedHadronization,
    Special,
)

# Add Herwig++ as minimum bias production tool.
Generation().addTool(MinimumBias)
Generation().MinimumBias.ProductionTool = "HerwigppProduction"
Generation().MinimumBias.addTool(HerwigppProduction)

# Add Herwig++ as inclusive production tool.
Generation().addTool(Inclusive)
Generation().Inclusive.ProductionTool = "HerwigppProduction"
Generation().Inclusive.addTool(HerwigppProduction)

# Add Herwig++ as special production tool.
Generation().addTool(Special)
Generation().Special.ProductionTool = "HerwigppProduction"
Generation().Special.addTool(HerwigppProduction)

# Add Herwig++ as plain signal production tool.
Generation().addTool(SignalPlain)
Generation().SignalPlain.ProductionTool = "HerwigppProduction"
Generation().SignalPlain.addTool(HerwigppProduction)

# Add Herwig++ as repeated signal hadronization production tool.
Generation().addTool(SignalRepeatedHadronization)
Generation().SignalRepeatedHadronization.ProductionTool = "HerwigppProduction"
Generation().SignalRepeatedHadronization.addTool(HerwigppProduction)
