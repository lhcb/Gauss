###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    Generation,
    HijingProduction,
    Inclusive,
    MinimumBias,
    SignalPlain,
    SignalRepeatedHadronization,
    Special,
)

# Tuning for pPb/Pbp collisions
hijingTune = [
    "hiparnt hipr1 2 0.65",
    "hiparnt hipr1 6 0.12",
    "hijinginit bmin 0",  # default 0.
    "hijinginit bmax 20",  # default 0.
]

gen = Generation()
gen.addTool(Special)
gen.addTool(MinimumBias)
gen.addTool(Inclusive)
gen.addTool(SignalPlain)
gen.addTool(SignalRepeatedHadronization)

gen.Special.ProductionTool = "HijingProduction"
gen.MinimumBias.ProductionTool = "HijingProduction"
gen.Inclusive.ProductionTool = "HijingProduction"
gen.SignalPlain.ProductionTool = "HijingProduction"
gen.SignalRepeatedHadronization.ProductionTool = "HijingProduction"

gen.Special.addTool(HijingProduction)
gen.MinimumBias.addTool(HijingProduction)
gen.Inclusive.addTool(HijingProduction)
gen.SignalPlain.addTool(HijingProduction)
gen.SignalRepeatedHadronization.addTool(HijingProduction)

gen.Special.HijingProduction.Commands += hijingTune
gen.MinimumBias.HijingProduction.Commands += hijingTune
gen.Inclusive.HijingProduction.Commands += hijingTune
gen.SignalPlain.HijingProduction.Commands += hijingTune
gen.SignalRepeatedHadronization.HijingProduction.Commands += hijingTune
