###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Generation
from Gaudi.Configuration import *

Generation().CommonVertex = True
Generation().SampleGenerationTool = "Special"
from Configurables import DaughtersInLHCbKeepOnlySignal, FixedNInteractions, Special

Generation().addTool(Special)
Generation().PileUpTool = "FixedNInteractions"
Generation().addTool(FixedNInteractions)
Generation().FixedNInteractions.NInteractions = 2
Generation().Special.CutTool = "DaughtersInLHCbKeepOnlySignal"
Generation().Special.PileUpProductionTool = "Pythia8Production"
Generation().Special.ReinitializePileUpGenerator = False
Generation().Special.addTool(DaughtersInLHCbKeepOnlySignal)
Generation().Special.DaughtersInLHCbKeepOnlySignal.SignalPID = 443
