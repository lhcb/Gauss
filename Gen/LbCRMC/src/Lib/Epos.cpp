/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Epos.cpp, 2015-08-28 14:26:00 Laure Massacrier Exp $
// Include files

// local
#include "LbCRMC/Epos.h"

// STL
#include <cstring>

//-----------------------------------------------------------------------------
// Implementation file for class : Epos
//
// 2005-08-19 : Laure Massacrier
//-----------------------------------------------------------------------------
Cevt   Epos::s_cevt;
C2evt  Epos::s_c2evt;
Cptl   Epos::s_cptl;
Hadr5  Epos::s_hadr5;
Accum  Epos::s_accum;
Nucl1  Epos::s_nucl1;
Othe1  Epos::s_othe1;
Appli  Epos::s_appli;
Drop7  Epos::s_drop7;
Hadr25 Epos::s_hadr25;
