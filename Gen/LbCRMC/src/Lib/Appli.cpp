/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:Appli.cpp,v 1., 29-07-2015, Laure Massacrier $
// access epos common block Appli.h
#include "LbCRMC/Appli.h"

// set pointer to zero at start
Appli::APPLI* Appli::s_appli = 0;

// Constructor
Appli::Appli() {}

// Destructor
Appli::~Appli() {}

// access iappl in COMMON
int& Appli::iappl() {
  init();
  return s_appli->iappl;
}

// access model in COMMON
int& Appli::model() {
  init();
  return s_appli->model;
}
