###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    CRMCProduction,
    Generation,
    Inclusive,
    MinimumBias,
    SignalPlain,
    Special,
)


def EPOS_setCentrality(minIP, maxIP):
    """function to set impact parameters for EPOS"""

    Generation().addTool(MinimumBias)
    Generation().MinimumBias.ProductionTool = "CRMCProduction"
    Generation().MinimumBias.addTool(CRMCProduction)

    Generation().addTool(Special)
    Generation().Special.addTool(CRMCProduction)
    Generation().addTool(Inclusive)
    Generation().Inclusive.addTool(CRMCProduction)
    Generation().addTool(SignalPlain)
    Generation().SignalPlain.addTool(CRMCProduction)

    Generation().MinimumBias.CRMCProduction.ImpactParameter = True
    Generation().MinimumBias.CRMCProduction.MinImpactParameter = minIP
    Generation().MinimumBias.CRMCProduction.MaxImpactParameter = maxIP

    Generation().Special.CRMCProduction.ImpactParameter = True
    Generation().Special.CRMCProduction.MinImpactParameter = minIP
    Generation().Special.CRMCProduction.MaxImpactParameter = maxIP

    Generation().Inclusive.CRMCProduction.ImpactParameter = True
    Generation().Inclusive.CRMCProduction.MinImpactParameter = minIP
    Generation().Inclusive.CRMCProduction.MaxImpactParameter = maxIP

    Generation().SignalPlain.CRMCProduction.ImpactParameter = True
    Generation().SignalPlain.CRMCProduction.MinImpactParameter = minIP
    Generation().SignalPlain.CRMCProduction.MaxImpactParameter = maxIP
