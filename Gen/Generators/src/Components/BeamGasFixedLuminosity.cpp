/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "BeamGasFixedLuminosity.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// From Event
#include "Event/GenCountersFSR.h"

// From Generators
#include "Generators/GenCounters.h"
#include "Generators/ICounterLogFile.h"

// Declaration of the Tool Factory

DECLARE_COMPONENT( BeamGasFixedLuminosity )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BeamGasFixedLuminosity::BeamGasFixedLuminosity( const std::string& type, const std::string& name,
                                                const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_xmlLogTool( 0 )
    , m_numberOfZeroInteraction( 0 )
    , m_nEvents( 0 )
    , m_randSvc( 0 ) {
  declareInterface<IPileUpTool>( this );
}

//=============================================================================
// Destructor
//=============================================================================
BeamGasFixedLuminosity::~BeamGasFixedLuminosity() { ; }

//=============================================================================
// Initialize method
//=============================================================================
StatusCode BeamGasFixedLuminosity::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // Initialize the number generator
  m_randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );

  //  XMl log file
  m_xmlLogTool = tool<ICounterLogFile>( "XmlCounterLogFile" );

  return sc;
}

//=============================================================================
// Compute the number of pile up to generate according to beam parameters
//=============================================================================
unsigned int BeamGasFixedLuminosity::numberOfPileUp() {
  LHCb::BeamParameters* beam = get<LHCb::BeamParameters>( m_beamParameters );
  if ( 0 == beam ) Exception( "No beam parameters registered" );
  double nu = m_fixedTargetLuminosity.value() * m_fixedTargetXSection.value() / beam->revolutionFrequency();

  if ( nu < 0.001 ) { // negligible pileup
    if ( m_zeroAllowed.value() ) return 0;
    return 1;
  }

  LHCb::GenFSR* genFSR = nullptr;
  if ( m_FSRName.value() != "" ) {
    IDataProviderSvc* fileRecordSvc = svc<IDataProviderSvc>( "FileRecordDataSvc", true );
    genFSR                          = getIfExists<LHCb::GenFSR>( fileRecordSvc, m_FSRName.value(), false );
    if ( !genFSR ) warning() << "Could not find GenFSR at " << m_FSRName << endmsg;
  }

  unsigned int result = 0;
  while ( 0 == result ) {
    m_nEvents++;
    if ( genFSR ) genFSR->incrementGenCounter( LHCb::GenCountersFSR::AllEvt, 1 );
    Rndm::Numbers poissonGenerator( m_randSvc, Rndm::Poisson( nu ) );
    result = (unsigned int)poissonGenerator();
    if ( 0 == result ) {
      m_numberOfZeroInteraction++;
      if ( genFSR ) genFSR->incrementGenCounter( LHCb::GenCountersFSR::ZeroInt, 1 );
    }
    if ( m_rareProcess.value() ) result += 1;
    if ( m_zeroAllowed.value() ) break;
  }
  return result;
}

//=============================================================================
// Print the specific pile up counters
//=============================================================================
void BeamGasFixedLuminosity::printPileUpCounters() {
  using namespace GenCounters;
  printCounter( m_xmlLogTool, "all events (including empty events)", m_nEvents );
  printCounter( m_xmlLogTool, "events with 0 interaction", m_numberOfZeroInteraction );
}

//=============================================================================
// Finalize method
//=============================================================================
StatusCode BeamGasFixedLuminosity::finalize() {
  release( m_randSvc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  return GaudiTool::finalize();
}
