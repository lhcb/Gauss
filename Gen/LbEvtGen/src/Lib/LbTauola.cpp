/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// local
#include "LbEvtGen/LbTauola.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LbTauola
//
// 2013-05-31 : Patrick Robbe
//-----------------------------------------------------------------------------

extern "C" {
#ifdef WIN32
void __stdcall SETTAUOLAOUTPUTUNIT( int* );
#else
void settauolaoutputunit_( int* );
#endif
}

void LbTauola::setOutputUnit( int outputUnit ) {
#ifdef WIN32
  SETTAUOLAOUTPUTUNIT( &outputUnit );
#else
  settauolaoutputunit_( &outputUnit );
#endif
}
