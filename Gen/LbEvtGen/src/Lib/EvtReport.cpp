/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: EvtReport.cpp,v 1.3 2007-10-10 20:07:24 robbep Exp $
// Overwrite EvtGen output messages

#include "EvtGenBase/EvtReport.hh"
#include "Generators/StreamForGenerator.h"

std::ostringstream dummyStr;

//=============================================================================
// Reimplementation of print facility of EvtGen
//=============================================================================
std::ostream& EvtGenReport( EvtGenSeverity severity, const char* facility ) {
  dummyStr.str( "" );
  if ( severity < EVTGEN_WARNING ) {
    if ( 0 != facility[0] ) {
      ( *StreamForGenerator::getStream() ) << MSG::ERROR << facility << " Error from EvtGen" << endmsg;
    } else
      ( *StreamForGenerator::getStream() ) << MSG::ERROR << "Error from EvtGen" << endmsg;
  } else if ( severity < EVTGEN_INFO ) {
    if ( 0 != facility[0] ) {
      ( *StreamForGenerator::getStream() ) << MSG::INFO;
      if ( StreamForGenerator::getStream()->isActive() ) std::cout << facility << ":";
    } else
      ( *StreamForGenerator::getStream() ) << MSG::INFO;
  } else {
    if ( 0 != facility[0] ) {
      ( *StreamForGenerator::getStream() ) << MSG::DEBUG;
      if ( StreamForGenerator::getStream()->isActive() ) std::cout << facility << ":";
    } else
      ( *StreamForGenerator::getStream() ) << MSG::DEBUG;
  }
  if ( StreamForGenerator::getStream()->isActive() )
    return std::cout;
  else
    return dummyStr;
}
