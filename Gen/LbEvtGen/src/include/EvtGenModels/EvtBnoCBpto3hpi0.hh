//--------------------------------------------------------------------------
//
// Copyright Information: See EvtGen/COPYRIGHT
//
// Module: EvtBnoCBpto3hpi0
//
// Description: Model for generating flat phase space events in the charmless
//              VV region. Necessary due to limitations with generator-level
//              cuts in LoKi; inability to handle Bose-symmetric final states
//              with PHOTOS
//
// Modification history:
//
//    Jeremy Dalseno     Mar 2020     Module created
//
//------------------------------------------------------------------------

#ifndef EVTBNOCBPTO3HPI0_HH
#define EVTBNOCBPTO3HPI0_HH

#include <string>

#include "EvtGenBase/EvtComplex.hh"
#include "EvtGenBase/EvtDecayAmp.hh"

class EvtParticle;

class EvtBnoCBpto3hpi0 : public EvtDecayAmp {

public:
  EvtBnoCBpto3hpi0();

  std::string getName() override { return "BNOCBPTO3HPI0"; }

  EvtDecayBase* clone() override { return new EvtBnoCBpto3hpi0; }

  void initProbMax() override { setProbMax( 1.0 ); }

  void init() override;

  void decay( EvtParticle* p ) override;

private:
  bool       _bose_symm;
  double     _m2hp_ll, _m2hp_ul; // Lower and upper (2h)+ invariant mass limits
  double     _m2h0_ll, _m2h0_ul; // Lower and upper (2h)0 invariant mass limits
  EvtComplex _I;
};

#endif // EVTBNOCBPTO3HPI0_HH
