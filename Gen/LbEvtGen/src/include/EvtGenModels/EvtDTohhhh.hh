//--------------------------------------------------------------------------
//
// Environment:
//      This software is part of the EvtGen package developed jointly
//      for the BaBar and CLEO collaborations.  If you use all or part
//      of it, please give an appropriate acknowledgement.
//
// Copyright Information: See EvtGen/COPYRIGHT
//      Copyright (C) 1998      Caltech, UCSB
//
// Module: EvtGen/EvtToDhhhh.hh
//
// Description:
//
// Modification history:
//
//    DJL/RYD     August 11, 1998         Module created
//
//------------------------------------------------------------------------

#ifndef EvtDHHHH_HH
#define EvtDHHHH_HH

#include "EvtGenBase/EvtDecayIncoherent.hh"

// MINT
#include "Mint/D_hhhh.h"
#include "Mint/IMintGen.h"
// #include "Mint/counted_ptr.h"
#include "Mint/EvtTRandom.h"

class EvtParticle;

namespace MINT {
  class EvtRand : virtual public IEvtRandom {
  public:
    /// Standard constructor
    EvtRand();
    EvtRand( const MINT::EvtRand& other );
    ~EvtRand();
    void         SetSeed( unsigned int seed ) override;
    unsigned int GetSeed() const override;
    double       Rndm() override;

  private:
    unsigned int m_nCalls; // number of random numbers generated
  };
} // namespace MINT

class EvtDTohhhh : public EvtDecayIncoherent {
public:
  EvtDTohhhh() {}

  ~EvtDTohhhh();

  std::string   getName() override;
  EvtDecayBase* clone() override;

  void init() override;
  void decay( EvtParticle* p ) override;

private:
  MINT::IMintGen*   m_MintGen;
  MINT::EvtTRandom* m_rnd;
};

#endif
