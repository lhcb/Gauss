//--------------------------------------------------------------------------
//
// Copyright Information: See EvtGen/COPYRIGHT
//
// Module: EvtBnoCBptoKshhh
//
// Description: Model for generating flat phase space for the charmless
//              region. Necessary due to limitations with the
//              combinatorial in the construction of the
//              invariant mass of the intermediate states.
//
// Modification history:
//
//    Pablo Baladrón     Dec 2019     Module created
//
//------------------------------------------------------------------------

#ifndef EVTBNOCBPTOKSHHH_HH
#define EVTBNOCBPTOKSHHH_HH

#include <string>

#include "EvtGenBase/EvtComplex.hh"
#include "EvtGenBase/EvtDecayAmp.hh"

class EvtParticle;

class EvtBnoCBptoKshhh : public EvtDecayAmp {

public:
  EvtBnoCBptoKshhh();

  std::string getName() override { return "BNOCBPTOKSHHH"; }

  EvtDecayBase* clone() override { return new EvtBnoCBptoKshhh; }

  void initProbMax() override { setProbMax( 4.0 ); }

  void init() override;

  void decay( EvtParticle* p ) override;

private:
  double     _m2pi_ll, _m2pi_ul; // Lower and upper 2h invariant mass limits
  double     _m3pi_ll, _m3pi_ul; // Lower and upper 3h invariant mass limits
  EvtComplex _I;
};

#endif // EVTBNOCBPTOKSHHH_HH
