/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATORS_LBTAUOLA_H
#define GENERATORS_LBTAUOLA_H 1

// Include files

/** @class LbTauola LbTauola.h Generators/LbTauola.h
 *
 *
 *  @author Patrick Robbe
 *  @date   2013-05-31
 */
class LbTauola {
public:
  /// Set output unit of Tauola
  static void setOutputUnit( int outputUnit );
};
#endif // GENERATORS_LBTAUOLA_H
