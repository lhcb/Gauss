/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Avoid contamination with WINDOWS symbols
#ifdef WIN32
#  define NOATOM
#  define NOGDI
#  define NOGDICAPMASKS
#  define NOMETAFILE
#  define NOMINMAX
#  define NOMSG
#  define NOOPENFILE
#  define NORASTEROPS
#  define NOSCROLL
#  define NOSOUND
#  define NOSYSMETRICS
#  define NOTEXTMETRIC
#  define NOWH
#  define NOCOMM
#  define NOKANJI
#  define NOCRYPT
#  define NOMCX
#endif

// Include files
// from STL
#include <string>

// from boost
#include "boost/filesystem/path.hpp"

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector4DTypes.h"
// from MCInterfaces
#include "GenInterfaces/IDecayTool.h"

// from EvtGen
#include "EvtGenBase/EvtId.hh"

// from HepMC
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMCUser/Status.h"

// from EvtGen
#include "EvtGenBase/EvtRandomEngine.hh"
#include "TRandom.h"

class RandomEngineWrapper : public EvtRandomEngine {
public:
  virtual ~RandomEngineWrapper() = default;
  // return a random number using gRandom from ROOT.
  // The Gaussino global random engine replaces gRandom
  // to use the correct thread-local engine of the
  // respective algorithm call.
  double random() override { return gRandom->Rndm(); }
};

// forward declarations
namespace LHCb {
  class ParticleID;
}

class EvtGen;
class EvtParticle;

/** @class EvtGenDecay EvtGenDecay.h "EvtGenDecay.h"
 *
 *  Tool to interface to EvtGen generator. Concrete implementation of
 *  a IDecayTool.
 *
 *  @author Patrick Robbe
 *  @date   2003-10-15
 */
class EvtGenDecay : public extends<GaudiTool, IDecayTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Initialize method.
   *  In initialization:
   *  -# Create a temporary evt.pdl file to transfer particle properties
   *     from the particle property service to EvtGen.
   *  -# Read the main DECAY.DEC decay file and the user signal decay file.
   *  -# Manages Pythia and PHOTOS print-out according to message level.
   *  -# Initializes Pythia for EvtGen.
   */
  StatusCode initialize() override;

  /// Finalize method
  StatusCode finalize() override;

  StatusCode generateDecay( const HepMC3::GenParticlePtr& theMother, HepRandomEnginePtr& engine ) const override;

  StatusCode generateSignalDecay( const HepMC3::GenParticlePtr& theMother, bool& flip,
                                  HepRandomEnginePtr& engine ) const override;

  StatusCode generateDecayWithLimit( const HepMC3::GenParticlePtr& theMother, const int targetId,
                                     HepRandomEnginePtr& engine ) const override;

  /// Implements IDecayTool::enableFlip
  void enableFlip() const override;

  /// Implements IDecayTool::disableFlip
  void disableFlip() const override;

  /// Implements IDecayTool::isKnownToDecayTool
  bool isKnownToDecayTool( const int pdgId ) const override;

  /// Implements IDecayTool::getSignalBr
  double getSignalBr() const override;

  /// Implements IDecayTool::checkSignalPresence
  bool checkSignalPresence() const override;

  /// Implements IDecayTool::setSignal
  void setSignal( const int ) override;

protected:
  /** Make a HepMCTree tree from an EvtGen particle stopping at the PDG
   *  Id targetId
   *
   *  @param theParticle pointer to the EvtGen particle to put in HepMC format
   *  @param theMother pointer to the HepMC particle associated to theParticle
   *  @param theOrigin position of the first particle in the decay tree
   *                (the root of the decay tree)
   *  @param targetId int pdgId of the last particle to generate (not used if
   *         negative)
   */
  StatusCode makeHepMC( EvtParticle* theParticle, const HepMC3::GenParticlePtr& theMother,
                        const Gaudi::LorentzVector& theOrigin, int targetId = -999 ) const;

  /** Check if HepMC Particle is valid for decay by EvtGen and to fill a
   *  HepMC event (checks if it is not already decayed and ensures that
   *  it has a defined production vertex)
   *  @param[in] thePart HepMC::GenParticle to analyze
   *  @return StatusCode::SUCCESS if no problem
   *  @return StatusCode::ERROR if particle has already an end vertex
   */
  void checkParticle( const HepMC3::GenParticlePtr& theParticle ) const;

  /** Call EvtGen to decay a particle
   *  @param[out] thePart the EvtParticle to produce
   *  @param[in]  theHepMCParticle the HepMC::GenParticle to decay with
   *              EvtGen
   *  @param[in]  eid if different from EvtId(-1,-1), specify the EvtId of
   *              the particle to generate (in case of an EvtGen alias for
   *              example)
   */
  StatusCode callEvtGen( EvtParticle*& thePart, const HepMC3::GenParticlePtr& theHepMCParticle,
                         const EvtId& eid ) const;

  /// Return the id of the alias corresponding to the pdg code pdgId
  virtual const EvtId getSignalAlias( int pdgId ) const;

  static std::unique_ptr<EvtGen> m_gen; ///< EvtGen engine

  std::unique_ptr<EvtRandomEngine> m_randomEngine; ///< Random Engine to use in EvtGeni

  // Recursive mutex to be called at the beginning of any of the three public functions for
  // decaying
  static std::recursive_mutex m_mutex;

private:
  /** Create a temporary evt.pdl file filled with Gaudi Particle Svc
   *  properties to update EvtGen particle properties
   *
   *  @param tempFileName name of the temporary created file
   */
  StatusCode createTemporaryEvtFile( const boost::filesystem::path& tempFileName ) const;

  /** Get 2J+1 spin for particles not supported in LHCbKernel/ParticleID
   *
   *  @param theId ParticleID of the particle for which to calculate 2J+1
   *  @return 2J+1 of the particle
   */
  int getParticleSpin( const LHCb::ParticleID& theId ) const;

  /// returns branching fraction of the given Id
  double branching( const EvtId& id ) const;

  /// check if id exists in generic decay table
  bool checkGeneric( const EvtId& id ) const;

  ///< Generic decay file name (set by options)
  Gaudi::Property<std::string> m_decayFile{ this, "DecayFile", "empty", "Generic decay file name" };

  ///< User decay file name (set by options)
  Gaudi::Property<std::string> m_userDecay{ this, "UserDecayFile", "empty", "User decay file name" };

  EvtId m_signalId{ -1, -1 }; /// EvtGen Id of signal ID

  /// Do not erase temporary evt.pdl particle property file (set by options)
  Gaudi::Property<bool> m_keepTempEvtFile{ this, "KeepTempEvtFile", false,
                                           "Do not erase temporary evt.pdl particle property file" };

  /// Minimum value for ctau. Below ctau is set to 0.
  double m_minctau{ 1.e-4 * Gaudi::Units::mm };

  /// Minimum value for ctau. Above ctau is set to 0.
  double m_maxctau{ 1.e+16 * Gaudi::Units::mm };

  /// Minimum value for width. Below it is set to 0.
  double m_minwidth{ 1.5e-6 * Gaudi::Units::GeV };

  /// Unit for TAUOLA output
  int m_tauolaUnit{ 6 };

  /// Unit for PHOTOS output
  int m_photosUnit{ 6 };

  /// Flag for polarized Lambda_b production (set by options)
  Gaudi::Property<bool> m_generatePolLambdab{ this, "PolarizedLambdad", false,
                                              "Flag for polarized Lambda_b production" };

  /// Flag for polarized spin 1/2 baryons production (set by options)
  Gaudi::Property<bool> m_generatePolBaryons{ this, "PolarizedBaryons", false,
                                              "Flag for polarized spin 1/2 baryons production" };

  /// spin 1/2 baryons polarisation
  Gaudi::Property<double> m_BaryonPol{ this, "BaryonPol", 1.0, "spin 1/2 baryons polarisation" };

  /// Flag for polarized charmonium production (set by options)
  Gaudi::Property<bool> m_generatePolCharmonium{ this, "PolarizedCharmonium", false,
                                                 "Flag for polarized charmonium production" };

  /// Real part of spin matrix for helicity 0 for charmonium polarization
  Gaudi::Property<double> m_realHelZero{ this, "RealHelZero", 1.,
                                         "Real part of spin matrix for helicity 0 for charmonium polarization" };

  /// Imaginary part of spin matrix for helicity 0 for charmonium polarization
  Gaudi::Property<double> m_imHelZero{ this, "ImHelZero", 0.,
                                       "Imaginary part of spin matrix for helicity 0 for charmonium polarization" };

  /// Real part of spin matrix for helicity 1 for charmonium polarization
  Gaudi::Property<double> m_realHelOne{ this, "RealHelOne", 1.,
                                        "Real part of spin matrix for helicity 1 for charmonium polarization" };

  /// Imaginary part of spin matrix for helicity 1 for charmonium polarization
  Gaudi::Property<double> m_imHelOne{ this, "ImHelOne", 0.,
                                      "Imaginary part of spin matrix for helicity 1 for charmonium polarization" };
};

// #include "EvtGenBase/EvtIncoherentMixing.hh"

//=============================================================================
// Enable the possibility to flip the flavour for CP decay modes in EvtGen
//=============================================================================
inline void EvtGenDecay::enableFlip() const {
  //  TODO: look how to implement in EvtGen
  //  EvtIncoherentMixing::enableFlip() ;
}

//=============================================================================
// Disable the possibility to flip the flavour for CP decay modes in EvtGen
//=============================================================================
inline void EvtGenDecay::disableFlip() const {
  // TODO: look how to implement in EvtGen
  //  EvtIncoherentMixing::disableFlip() ;
}
