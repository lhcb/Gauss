/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RivetAnalysisHandler.h, v 2.1 2021-10-07 18:00:00 agrecu $
#ifndef COMPONENT_RIVETANALYSISHANDLER_H
#  define COMPONENT_RIVETANALYSISHANDLER_H 1

// from Gaudi
#  include <Gaudi/Property.h>
#  include <GaudiAlg/GaudiAlgorithm.h>
// from RIVET
#  include <Rivet/AnalysisHandler.hh>

// from GenEvent/Event
#  include <Event/HepMCEvent.h>

#  include <sstream>
#  include <string>
#  include <utility>
#  include <vector>

/** @class RivetAnalysisHandler RivetAnalysisHandler.h component/RivetAnalysisHandler.h
 *
 *  Component that interfaces Rivet to Gauss in order to run Rivet plug-ins
 *  inside Gauss jobs. The interface is heavily inspired by the ATLAS API for
 *  Rivet which is developed and maintained by Andy Buckley and James Monk.
 *
 *  @author Alex Grecu <alexandru.grecu@nipne.ro>
 *  @date   2011-08-12
 */
class RivetAnalysisHandler : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

  enum idStatusLog {
    negMassParticles = 0, // 0: count particles with negative! rest mass
    nbXAngleBoosts,       // 1: times x-Angle boost(s) was done
    nbUnitConversions,    // 2: times internal unit conversion was applied
    nbPIDAdjustments,     // 3: times particleId was adjusted
    nbGenXSChanges        // 4: nb of generator provided xsection changes
  };

private:
  // Indicates how is the interaction cross-section set/determined for each event
  enum XS_SourceType {
    InvalidNotSet = 0, // default value
    FromHepMCEvent,    // 1: xs and uncertainty read from HepMC event stream
    FromExternalOpt,   // 2: xs and uncertainty provided by algorithm options (w/ or w/o forcing the value)
    FromBeamParams     // 3: xs with 0.0 uncertainty is read from LHCb::BeamParameters
  };

  std::pair<Rivet::ConstGenParticlePtr, Rivet::ConstGenParticlePtr> findBeamParticles( HepMC::GenEvent* hEvent );

  double getTotalXSection();

  bool detectBeamCrossingAngles( HepMC::GenEvent* hEvent );

  /// The main Rivet analysis handler
  Rivet::AnalysisHandler* _analysisManager{ nullptr };

  /// Property: The base file name (prefix of filenames) to write results to ("MyRivet").
  Gaudi::Property<std::string> m_filename{
      this, "BaseFileName", "MyRivet", "The base file name (prefix of filenames) to write results to (\"MyRivet\")" };

  Gaudi::Property<std::string> m_runname{
      this, "RunName", "",
      "The name of the run to prepended to YODA histogram paths (keep empty string unless expert advised)" };

  /// Property: A list of names of the analyses to run ([]).
  Gaudi::Property<std::vector<std::string>> m_analysisNames{
      this, "Analyses", {}, "A list of names of the analyses to run ([])" };

  /// Property: Location on TES where the HepMC events are read from (LHCb::HepMCEventLocation::Default).
  Gaudi::Property<std::string> m_mcEvtLocation{
      this, "MCEventLocation", LHCb::HepMCEventLocation::Default,
      "Location on TES where the HepMC events are read from (LHCb::HepMCEventLocation::Default)" };

  /// Property: List of additional file paths where analyses should be looked for, e.g. add os.path.abspath('.') when
  /// analysis lib is in option file directory ([]).
  Gaudi::Property<std::vector<std::string>> m_analysisPaths{
      this,
      "AnalysisPath",
      {},
      "List of additional file paths where analysis plugins should be looked for, e.g. add os.path.abspath('.') when "
      "analysis lib(*.so) is in the option file directory ([])" };

  /// Property: Switch that controls the transformation of status ID of particles (given by EvtGen) back to Pythia
  /// defaults (False).
  Gaudi::Property<bool> m_modStatusID{ this, "CorrectStatusID", false,
                                       "Switch that controls the transformation of status ID of particles (given by "
                                       "EvtGen) back to Pythia defaults (False)" };

  /// Property: Instructs the algorithm to automatically detect and correct for beam crossing angles (True).
  Gaudi::Property<bool> m_xAngleDetect{
      this, "CorrectCrossingAngles", true,
      "Instructs the algorithm to automatically detect and correct for beam crossing angles (True)" };

  /// Property: The externally provided cross-section for the present run [picobarn]; ignored when read from data (-1.).
  Gaudi::Property<double> m_XSection{ this, "xSectionValue", -1.,
                                      "The externally provided cross-section for the present run (in picobarns); "
                                      "ignored when present in HepMC and not forced (-1.)" };

  /// Property: The optional uncertainty on the externally provided cross-section [picobarn] (0.0)
  Gaudi::Property<double> m_XSectionErr{ this, "xSectionError", 0.,
                                         "The uncertainty on the externally provided cross-section for the present "
                                         "run; ignored when present in HepMC and not forced (0.)" };

  /// Property: Forces GenTune to set the cross-section to the value provided externally in each event (False).
  Gaudi::Property<bool> m_forceCrossSection{
      this, "forceXSection", false, "Forces GenTune to set the provided cross-section value for each event (False)" };

  /// Property: Forces GenTune to instruct RIVET to ignore the beams defined in the analysis metadata (False). Modules
  /// may not work or give false results.
  Gaudi::Property<bool> m_ignoreBeams{ this, "forceIgnoreBeams", false,
                                       "Forces GenTune to instruct RIVET to ignore the beams defined in the analysis "
                                       "metadata (False). Modules may not work or give false results." };

  /// Property: Instruct analysis handler to skip any defined event weights
  Gaudi::Property<bool> m_skipWeights{ this, "skipWeights", true,
                                       "Specify whether HepMC event weigths are to ignored or not (True)." };

  /// Property: Define a pattern to select specific weights only
  Gaudi::Property<std::string> m_wgtSelPat{
      this, "weightSelectPattern", "",
      "Defined pattern to select only specific named weights in HepMC events (\"\")." };

  /// Property: Define a pattern to de-select specific weights
  Gaudi::Property<std::string> m_wgtUSelPat{
      this, "weightDeselectPattern", "",
      "Defined pattern to de-select specific named weights in HepMC events (\"\")." };

  /// Property: Set name of nominal weight to consider in the HepMC events
  Gaudi::Property<std::string> m_defWgtName{ this, "nominalWeightName", "",
                                             "Set the name of weight to be taken as nominal in HepMC events (\"\")." };

  /// Property: Internal statistical messages print-out suppression soft limit (30).
  Gaudi::Property<long unsigned int> m_logSoftLimit{
      this, "LogSuppressionSoftLimit", 30, "Internal statistical messages print-out suppression soft limit (30)" };

  /// Property: Internal statistical message print-out suppression frequency (10).
  Gaudi::Property<long unsigned int> m_logSuppressFreq{
      this, "LogSuppressedOutputFrequency", 10, "Internal statistical message print-out suppression frequency (10)" };

  /// Property: Internal statistical message print-out suppression hard limit (200).
  Gaudi::Property<long unsigned int> m_logHardLimit{
      this, "LogSuppressionHardLimit", 200, "Internal statistical message print-out suppression hard limit (200)" };

  /// Internal event counter to ensure proper event numbering in HepMC
  int _currentEventNumber{ -1 };

  /// Indicates whether the cross-section is needed by any of the analyses run in job (false).
  bool _reqCrossSection{ false };

  /// Indicates which source should be queried for cross-section value when needed.
  XS_SourceType _xsectionSource{ XS_SourceType::InvalidNotSet };

  /// When crossing angle presence is detected these flags are true.
  bool _xHAngleCorrection{ false };
  bool _xVAngleCorrection{ false };

  // The medium horizontal crossing angle value (Ox).
  double _mHxAngle{ 0. };
  // The medium vertical crossing angle value (Oy).
  double _mVxAngle{ 0. };

  /// This flag is true only if the component was linked against HepMC 2.03 or when scaling to LHCb units is needed.
  bool _needsUnitConv{ false };

  /// Toggles to false after first event processed to allow dynamic setup of flags above (true).
  bool _isFirstEvent{ true };

  /// Booleean controlling whether event number corrective mechanism is enabled
  bool _checkEvtNb{ true };

  /// Energy conversion factor (1.).
  double _scaleFactorEnergy{ 1.0 };

  /// Length conversion factor (1.) - usually 1., but ...
  double _scaleFactorLength{ 1.0 };

  /// The description for each internal statistical counter
  static const char* const _statDescriptors[];

  /// Number of internal statistical counters
  static const unsigned int COUNTERS_NB = 5;

  /// Various statistics (debugging mode and not only) -- see idStatusLog
  std::vector<long unsigned int> _myStats{ 0, 0, 0, 0, 0 };

  /// Internal function: Set cross-section for each event when cross-section value provided in options
  void compatSetCrossSection( HepMC::GenEvent* );

  /// Checks whether messages specific to internal flag statId are suppressed or not.
  bool statLogEnabled( idStatusLog statId );
  bool statLogEnabled( idStatusLog statId, MSG::Level lvl );

  void prepareHepMCEvent( HepMC::GenEvent* );
};

/// sorts GenParticles by descending energy
bool cmpGenParticleByEDesc( Rivet::ConstGenParticlePtr a, Rivet::ConstGenParticlePtr b ) {
  return ( a->momentum().e() > b->momentum().e() );
}

/// taken from RIVET framework
bool fuzzyEq( double a, double b, double eps = 1.e-6 ) {
  const double absavg  = fabs( a + b ) / 2.0;
  const double absdiff = fabs( a - b );
  const bool   rtn     = ( ( absavg == 0.0 && absdiff == 0.0 ) || ( absdiff < eps * absavg ) );
  return rtn;
}

/// computes invariant mass for particles boosted along z-axis
double invariantMass( const HepMC::FourVector& mom ) {
  double mass = ( mom.e() - mom.pz() ) * ( mom.e() + mom.pz() ) - mom.px() * mom.px() - mom.py() * mom.py();
  mass        = Rivet::sign( mass ) * sqrt( fabs( mass ) );
  return mass;
}

#endif
// COMPONENT_RIVETANALYSISHANDLER_H
