/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RivetAnalysisHandler.cpp $

//-----------------------------------------------------------------------------
// Implementation file for class : RivetAnalysisHandler
//
// Responsible: Alex Grecu <alexandru.grecu@nipne.ro>
//-----------------------------------------------------------------------------

// from Gaudi/LHCb
#include <Event/BeamParameters.h>
#include <Gaudi/Property.h>
#include <GaudiKernel/CommonMessaging.h>
// #include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/KeyedContainer.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/StatusCode.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <GaudiKernel/Vector3DTypes.h>
// HepMC
#include <HepMC/GenEvent.h>
#include <HepMC/GenParticle.h>
#include <HepMC/SimpleVector.h>
#ifdef HEPMC_HAS_UNITS
#  include <HepMC/Units.h>
#endif
#ifdef HEPMC_HAS_CROSS_SECTION
#  include <HepMC/GenCrossSection.h>
#endif
// system
// #include <sys/types.h>
// #include <cassert>
// #include <cmath>
// #include <cstdlib>
// #include <iterator>
// #include <memory>
// #include <type_traits>
// from boost
#include <sys/stat.h>

// disable unused parameter warning (appears in libRivet)
// #pragma GCC diagnostic ignored "-Wunused-parameter"
// #pragma GCC diagnostic ignored "-Wunused-local-typedefs"

// from Rivet (LCG)
#include "Rivet/Math/MathUtils.hh"
#include "Rivet/Tools/RivetHepMC.hh"
#include <Rivet/Analysis.hh>
#include <Rivet/AnalysisInfo.hh>
#include <Rivet/AnalysisLoader.hh>
#include <Rivet/Rivet.hh>
#include <Rivet/Tools/Logging.hh>
#include <Rivet/Tools/RivetPaths.hh>
#include <Rivet/Tools/Utils.hh>

// local
#include "LogLevelMap.h"
#include "RivetAnalysisHandler.h"

using namespace std;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RivetAnalysisHandler )

class RivetAnalysisHandler;

// Initializing static members
const char* const RivetAnalysisHandler::_statDescriptors[] = {
    "Particles with negative rest mass (in DEBUG mode only)", "Boost angle corrections", "Unit conversions",
    "ParticleID adjustments", "MC generator interaction XS changes" };

//=============================================================================
// Initialization
//=============================================================================
StatusCode RivetAnalysisHandler::initialize() {
  struct stat  mybuff;
  unsigned int nAna = 0;
  StatusCode   sc   = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                  // error printed already by GaudiAlgorithm
  info() << this->name() << " initializing using Rivet v. " << Rivet::version() << endmsg;
  // check at least one analysis in queue
  if ( m_analysisNames.value().size() == 0 ) {
    fatal() << "Please, add at least one analysis module name to 'Analyses' property. Nothing to do!" << endmsg;
    return StatusCode::FAILURE;
  };
  if ( m_XSectionErr.value() < 0.0 ) m_XSectionErr.setValue( -m_XSectionErr.value() );
  if ( m_forceCrossSection.value() && m_XSection.value() < 0.0 ) {
    fatal() << "Cannot force setting negative cross-section value." << endmsg;
    return StatusCode::FAILURE;
  };
#ifdef HEPMC_HAS_UNITS
  info() << "Event unit conversion performed in HEPMC." << endmsg;
#else
  info() << "Event unit conversion performed by RivetAnalysisHandler." << endmsg;
#endif
  // Create Rivet::AnalysisHandler instance
  _analysisManager = new Rivet::AnalysisHandler( m_runname.value() );
  assert( _analysisManager );
  info() << "Created Rivet analysis handler instance w/ run name " << m_runname.value() << " ..." << endmsg;
  _analysisManager->setIgnoreBeams( m_ignoreBeams.value() ); // in future change to checkBeams when logic returns!
  if ( m_defWgtName.value().size() > 0 ) _analysisManager->setNominalWeightName( m_defWgtName.value() );
  if ( m_wgtSelPat.value().size() > 0 ) _analysisManager->selectMultiWeights( m_wgtSelPat.value() );
  if ( m_wgtUSelPat.value().size() > 0 ) _analysisManager->deselectMultiWeights( m_wgtUSelPat.value() );
  if ( m_skipWeights.value() ) _analysisManager->skipMultiWeights( true );
  // Append valid additional paths provided by user to $RIVET_ANALYSIS_PATH
  vector<string> all_paths = Rivet::getAnalysisLibPaths();
  if ( m_analysisPaths.value().size() > 0 ) {
    for ( const string& ps : m_analysisPaths.value() ) {
      if ( ps.size() == 0 ) continue;
      if ( stat( ps.c_str(), &mybuff ) == 0 )
        all_paths.push_back( ps );
      else
        warning() << "Ignoring bad/unaccessible path " << ps << "." << endmsg;
    };
    Rivet::setAnalysisLibPaths( all_paths );
  };
  // identify platform as minimal check-up of set environment
  char* binTag = getenv( "BINARY_TAG" );
  if ( binTag == NULL ) {
    warning() << "$BINARY_TAG env variable not set: main analysis plugin directory may not be found..." << endmsg;
    binTag = getenv( "CMTCONFIG" );
    if ( binTag == NULL ) {
      warning() << "$CMTCONFIG variable not set: main analysis plugin directory may not be found..." << endmsg;
    }
  };
  // attempt to locate our lib in analysis paths - beware of lib name clashes!
  if ( binTag != NULL ) {
    for ( const string& p : all_paths ) {
      const std::string lbPluginsPath = p + "/RivetLHCbPlugins.so";
      if ( stat( lbPluginsPath.c_str(), &mybuff ) == 0 )
        info() << "Located LbRivetPlugins lib in " << p << "." << endmsg;
    };
  };
  // read back RIVET_ANALYSIS_PATH from environment and warn if missing
  // TODO: Read other environment variables and print them if defined!
  // if (msgLevel(MSG::DEBUG))
  string env_rap = getenv( "RIVET_ANALYSIS_PATH" );
  if ( env_rap.size() > 0 ) {
    debug() << "RIVET_ANALYSIS_PATH = " << env_rap << endmsg;
  } else {
    warning() << "$RIVET_ANALYSIS_PATH missing or empty!" << endmsg;
  };

  // Setting log level for Rivet & analyses according to Gauss (LHCbApp) log level
  MSG::Level jobMsgLvl = ( MSG::Level )( *msgSvc() ).outputLevel();
  always() << "Gauss log level: " << (int)jobMsgLvl << endmsg;
  Rivet::Log::setLevel( "Rivet.Projection.PVertex", rivetLevel( jobMsgLvl ) );
  always() << "Rivet.Projection.PVertex log level: " << Rivet::Log::getLog( "Rivet.Projection.PVertex" ).getLevel()
           << endmsg;
  Rivet::Log::setLevel( "Rivet", rivetLevel( jobMsgLvl ) );
  if ( msgLevel( MSG::VERBOSE ) ) { // list all available analysis names
    vector<string> analysisNames = Rivet::AnalysisLoader::analysisNames();
    verbose() << "Listing available Rivet analyses:" << std::endl;
    for ( const string& a : analysisNames ) verbose() << " " << a << std::endl;
    verbose() << endmsg;
  };
  // Determine if cross-section is needed
  _reqCrossSection = false;
  for ( const string& a : m_analysisNames.value() ) {
    string         ananame = a;
    vector<string> anaopt  = Rivet::split( a, ":" );
    if ( anaopt.size() > 1 ) ananame = anaopt[0];
    always() << "Loading Rivet analysis " << ananame;
    auto analysis = Rivet::AnalysisLoader::getAnalysis( ananame );
    if ( 0 == analysis ) {
      always() << " ... [ \x1B[31mFAILED\x1B[0m; Skipping... ]" << endmsg;
      continue;
    } else {
      if ( anaopt.size() > 1 ) {
        vector<string> anaopt2 = std::vector<string>( anaopt.begin() + 1, anaopt.end() );
        always() << " with options " << Rivet::join( anaopt2, ", " ) << " ... ";
      };
      always() << "[ \x1B[32mOK\x1B[0m ]" << endmsg;
      nAna++;
    };
    if ( analysis->info().needsCrossSection() ) {
      _reqCrossSection = true;
      info() << "Analysis " << a << " requires valid production cross section value." << endmsg;
    };
    _analysisManager->addAnalysis( a );
    string logName = "Rivet.Analysis." + a;
    // TODO: Sincronizat cu nivelul de logging de la Rivet odata ce s-a produs separarea
    Rivet::Log::setLevel( logName, rivetLevel( jobMsgLvl ) );
  };                 // end analysis loop
  if ( nAna == 0 ) { // verify non-zero analysis plugins loaded
    fatal() << "No analysis plugin left to run in current Rivet job. Exiting..." << endmsg;
    return StatusCode::FAILURE;
  };
  if ( m_forceCrossSection.value() && ( m_XSection.value() < 0.0 ) ) { // fail if user tries to force invalid
                                                                       // cross-section value
    if ( _reqCrossSection ) {
      fatal() << "Invalid external cross-section value was forced." << endmsg;
      return StatusCode::FAILURE;
    } else {
      warning() << "Forced cross-section value is invalid, but not needed." << endmsg;
    };
  };
  // Initialize Rivet 2.x before processing first event
  debug() << "<== Initialize done." << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode RivetAnalysisHandler::execute() {
  debug() << "==> Enter Execute ";
  if ( _isFirstEvent )
    debug() << "first event";
  else
    debug() << "event #" << ( _analysisManager->numEvents() + 1 );
  debug() << " ..." << endmsg;
  /// get HepMC event from TES
  LHCb::HepMCEvent::Container* mcEvents = get<LHCb::HepMCEvent::Container>( m_mcEvtLocation.value() );
  if ( NULL == mcEvents ) {
    fatal() << "Cannot retrieve HepMC events from '" << m_mcEvtLocation.value() << "' on TES." << endmsg;
    return StatusCode::FAILURE;
  };
  if ( mcEvents->size() == 0 ) {
    warning() << "No HepMC events in TES container. Skipping execute sequence..." << endmsg;
    return StatusCode::SUCCESS;
  };
  /// take only the first event and thus ignore pile-up ?!
  if ( mcEvents->size() > 1 ) {
    warning() << "Multiple {" << mcEvents->size()
              << "} HepMC events in TES container. Will process only first HepMC event in container." << endmsg;
  };
  LHCb::HepMCEvent::Container::iterator ievent    = mcEvents->begin();
  LHCb::HepMCEvent*                     lhcbEvent = ( *ievent );
  if ( NULL == lhcbEvent ) {
    warning() << "Skipping NULL primary HepMC event and execute sequence..." << endmsg;
    return StatusCode::SUCCESS;
  };
  // using GenEvent's deep copy constructor
  HepMC::GenEvent* gevEvent = new HepMC::GenEvent( *lhcbEvent->pGenEvt() );

  // determine needed scaling factors and xangles from first event
  if ( _isFirstEvent ) {
    _currentEventNumber = gevEvent->event_number();
    if ( _currentEventNumber < 0 ) _currentEventNumber = 0;
#ifdef HEPMC_HAS_UNITS
    // proper unit conversion is done in Rivet::Event by rebuilding GenEvent (deep copy)
    info() << "HepMC event units are  [" << HepMC::Units::name( gevEvent->momentum_unit() ) << ", "
           << HepMC::Units::name( gevEvent->length_unit() ) << ", ns]." << endmsg;
    _scaleFactorEnergy = HepMC::Units::conversion_factor( gevEvent->momentum_unit(), HepMC::Units::GEV );
    _scaleFactorLength = HepMC::Units::conversion_factor( gevEvent->length_unit(), HepMC::Units::MM );
    // here force conversion using HepMC code
#else
    warning()
        << "Old HepMC library detected! Assuming LHCb units [MeV, mm, ns] for conversion to RIVET units [GeV, mm, ns]."
        << endmsg;
    _scaleFactorEnergy = Gaudi::Units::MeV / Gaudi::Units::GeV;
    _scaleFactorLength = 1.0;
#endif
    if ( ( !fuzzyEq( _scaleFactorEnergy, 1.0 ) ) || ( !fuzzyEq( _scaleFactorLength, 1.0 ) ) ) {
      info() << "Conversion of units to [GeV, mm, ns] is needed." << endmsg;
      _needsUnitConv = true;
    } else {
      _needsUnitConv = false;
    };
    info() << "Internal conversion of HepMC event units... [\x1B[1m" << ( _needsUnitConv ? "enabled" : "disabled" )
           << "\x1B[0m]." << endmsg;
    // Cross-section info follows:
#ifdef HEPMC_HAS_CROSS_SECTION
    if ( nullptr == gevEvent->cross_section() ) { // useful info for MC generators!
      warning() << "MC event generator provides NULL cross-section in HepMC." << endmsg;
    } else {
      info() << "HepMC cross-section: " << gevEvent->cross_section()->cross_section() << " +/- "
             << gevEvent->cross_section()->cross_section_error() << " picobarn(s)." << endmsg;
      if ( !m_forceCrossSection.value() ) {
        _xsectionSource = XS_SourceType::FromHepMCEvent;
        m_XSection.setValue( gevEvent->cross_section()->cross_section() ); // copy value so that is not default
      };
    };
#else
    warning() << "HepMC does not support cross-section entries." << endmsg;
#endif
    if ( _reqCrossSection ) {
      if ( m_forceCrossSection.value() ) {
        _xsectionSource = XS_SourceType::FromExternalOpt;
        info() << "Forced cross-section: " << m_XSection.value() << " +/- " << m_XSectionErr.value() << " pb."
               << endmsg;
      } else {
        if ( ( _xsectionSource == XS_SourceType::InvalidNotSet ) && ( m_XSection.value() < 0.0 ) ) {
          warning() << "Invalid external cross-section value [pb]...Trying to use total cross section value." << endmsg;
          m_XSection.setValue( getTotalXSection() );
          if ( m_XSection.value() < 0.0 ) {
            fatal()
                << "Invalid total cross-section value read from BeamParameters. No valid cross-section value available."
                << endmsg;
            return StatusCode::FAILURE;
          } else {
            m_XSectionErr.setValue( 0.0 );
            warning() << "Using total cross-section value from BeamParameters: " << m_XSection.value() << " +/- 0.0 pb."
                      << endmsg;
            _xsectionSource = XS_SourceType::FromBeamParams;
          };
        };
      };
    } else {
      info() << "Cross-section value not needed by any of the currently selected plug-ins." << endmsg;
    }; // end cross-section source selection
    /// set beam particles if not present in Event (e.g. when reading from external sources)
    /// TODO: Look for cases of event generators for which beam particles need to be set at each event ?! If yes, this
    /// may go into prepareHepMCEvent.
    pair<Rivet::ConstGenParticlePtr, Rivet::ConstGenParticlePtr> beamParticles = findBeamParticles( gevEvent );
    if ( ( 0 != beamParticles.first ) && ( 0 != beamParticles.second ) ) {
      Gaudi::XYZVector b1;
      Gaudi::XYZVector b2;
      b1.SetXYZ( beamParticles.first->momentum().x(), beamParticles.first->momentum().y(),
                 beamParticles.first->momentum().z() );
      b2.SetXYZ( beamParticles.second->momentum().x(), beamParticles.second->momentum().y(),
                 beamParticles.second->momentum().z() );
      b1 += b2;
      double hangle = b1.x() / b2.z() / Rivet::sign( b2.z() ) / 2.0 / Gaudi::Units::mrad;
      double vangle = b1.y() / b2.z() / Rivet::sign( b2.Z() ) / 2.0 / Gaudi::Units::mrad;
      info() << "Smeared horizontal crossing angle: " << hangle << " mrad." << endmsg;
      info() << "Smeared vertical crossing angle: " << vangle << " mrad." << endmsg;
    };
    /// if detection is requested and it fails then algorithm fails!
    if ( ( m_xAngleDetect.value() ) && ( detectBeamCrossingAngles( gevEvent ) ) ) return StatusCode::FAILURE;
  }; // end preprocessing first event !
  // apply all/most mods to GenEvent in this function
  prepareHepMCEvent( gevEvent );
  if ( _isFirstEvent ) {
    _analysisManager->init( *gevEvent );
    _isFirstEvent = false;
    // haveNamedWeigths() has correct meaning only after analysis handler is initialized
    _checkEvtNb = m_skipWeights.value() || !_analysisManager->haveNamedWeights() || ( gevEvent->weights().size() == 1 );
  };
  _analysisManager->analyze( *gevEvent );
  delete gevEvent;
  debug() << "<== Leave Execute" << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode RivetAnalysisHandler::finalize() {
  debug() << "==> Finalize" << endmsg;
  compatSetCrossSection( NULL );
  if ( _myStats[nbGenXSChanges] >= m_logHardLimit.value() )
    info() << "Last xsection value received from generator is: " << m_XSection.value() << " pb." << endmsg;
  // print debugging counters
  info() << "Internal counter values:" << std::endl;
  for ( uint i = 0; i < this->_myStats.size(); i++ )
    info() << "Counter[" << i << "] <<\"" << RivetAnalysisHandler::_statDescriptors[i] << "\">> = " << this->_myStats[i]
           << std::endl;
  info() << endmsg;
  string histFileName = ( m_filename.value() + ".yoda" );
  _analysisManager->finalize();
  _analysisManager->writeData( histFileName );
  debug() << "<== Finalize" << endmsg;
  return GaudiAlgorithm::finalize(); // must be called after all other actions
}

//=============================================================================
// Helper functions and additional non-public methods
//=============================================================================

/// sets manager cross section value when it cannot be read from data automatically or a specific value is forced
void RivetAnalysisHandler::compatSetCrossSection( HepMC::GenEvent* pEvent ) {
  if ( !( _reqCrossSection || m_forceCrossSection.value() ) ) return;
  if ( NULL == pEvent ) {
    _analysisManager->setCrossSection( m_XSection.value(), m_XSectionErr.value(), true );
    return;
  };
#ifdef HEPMC_HAS_CROSS_SECTION
  HepMC::GenCrossSection* pxs = pEvent->cross_section();
  if ( ( 0 == pxs ) || m_forceCrossSection.value() ) { // try setting the cross-section to "externally" provided value
    pxs = new HepMC::GenCrossSection();
    pxs->set_cross_section( m_XSection.value(), m_XSectionErr.value() );
    pEvent->set_cross_section( *pxs );
  } else {
    if ( !fuzzyEq( m_XSection.value(), pxs->cross_section() ) ) {
      _myStats[nbGenXSChanges]++;
      if ( statLogEnabled( nbGenXSChanges, MSG::INFO ) )
        info() << "Generator provided xsection has changed from " << m_XSection.value() << " to "
               << pxs->cross_section() << endmsg;
      m_XSection.setValue( pxs->cross_section() ); // (re)set xsection to value given by generator (backup value!)
    };
  };
#else
  _analysisManager->setCrossSection( m_XSection.value(), m_XSectionErr.value(), true );
#endif
}

/// verifies if the log messages for a specific internal statistics flag is suppressed
bool RivetAnalysisHandler::statLogEnabled( idStatusLog statId ) { return statLogEnabled( statId, MSG::DEBUG ); }
bool RivetAnalysisHandler::statLogEnabled( idStatusLog statId, MSG::Level lvl ) {
  if ( !msgLevel( lvl ) ) return false;
  bool printOn =
      ( ( _myStats[statId] < m_logSoftLimit.value() ) ||
        ( ( _myStats[statId] <= m_logHardLimit.value() ) && ( _myStats[statId] % m_logSuppressFreq.value() == 0 ) ) );
  if ( _myStats[statId] == m_logSoftLimit.value() )
    always() << "Messages for internal stat flag \"" << _statDescriptors[statId] << "\" [id:" << statId
             << "] are softly suppressed from now on." << endmsg;
  if ( _myStats[statId] == m_logHardLimit.value() )
    always() << "Messages for internal stat flag \"" << _statDescriptors[statId] << "\" [id:" << statId
             << "] are totally supressed from now on." << endmsg;
  return printOn;
}

/// verifies that HepMC event has valid beam particles and if not try to detect them //HepMC::GenParticle*
std::pair<Rivet::ConstGenParticlePtr, Rivet::ConstGenParticlePtr>
RivetAnalysisHandler::findBeamParticles( HepMC::GenEvent* hEvent ) {
  if ( hEvent->valid_beam_particles() ) return hEvent->beam_particles();
  HepMC::GenParticle* pBeam1 = nullptr;
  HepMC::GenParticle* pBeam2 = nullptr;
  int                 pid    = 0;
  // for (HepMC::GenEvent::particle_const_iterator pi = hEvent->particles_begin();
  //        pi != hEvent->particles_end(); ++pi) { // use (*pi) to get GenParticle*
  for ( Rivet::ConstGenParticlePtr pi : Rivet::HepMCUtils::particles( hEvent ) ) {
    pid = pi->pdg_id();
    // consider only gamma and non partons as beam particles (enforce p--><--p ?)
    //  second condition pid != 0 was too lax!
    if ( !pi->production_vertex() && ( ( pid == 22 ) || ( pid > 100 ) ) ) {
      if ( pid != 2212 ) {
        warning() << "Beam particle " << pid << " (status=" << pi->status() << ") detected but not proton. Skipping..."
                  << endmsg;
        continue;
      };
      if ( pBeam2 ) {
        error() << "More than 2 beams detected!" << endmsg;
        break;
      };
      if ( pBeam1 ) {
        pBeam2 = (HepMC::GenParticle*)pi;
      } else {
        pBeam1 = (HepMC::GenParticle*)pi;
      };
    };
  };
  if ( ( pBeam2 == nullptr ) || ( pBeam1 == nullptr ) ) {
    error() << "Not all proton beams were found in HepMC event:" << std::endl;
    hEvent->print( error().stream() );
    error() << endmsg;
    return make_pair( nullptr, nullptr );
  };
  // is this really needed for two protons of same energy?!
  if ( !cmpGenParticleByEDesc( (Rivet::ConstGenParticlePtr)pBeam1, (Rivet::ConstGenParticlePtr)pBeam2 ) ) {
    HepMC::GenParticle* tt = pBeam2;
    pBeam2                 = pBeam1;
    pBeam1                 = tt;
  };
  hEvent->set_beam_particles( pBeam1, pBeam2 );
  return std::make_pair( (Rivet::ConstGenParticlePtr)pBeam1, (Rivet::ConstGenParticlePtr)pBeam2 );
}

/// Read total cross-section from beam parameters. PYTHIA & LHCb use mb while HepMC uses pb!
double RivetAnalysisHandler::getTotalXSection() {
  LHCb::BeamParameters* beamParams = get<LHCb::BeamParameters>( LHCb::BeamParametersLocation::Default );
  if ( 0 == beamParams ) return -1.0;
  return ( beamParams->totalXSec() / Gaudi::Units::picobarn );
}

/// Detect beam crossing angles if they exist
bool RivetAnalysisHandler::detectBeamCrossingAngles( HepMC::GenEvent* hEvent ) {
  if ( 0 == hEvent ) {
    error() << "NULL GenEvent pointer. Skip beam crossing angles detection..." << endmsg;
    return true;
  };
  LHCb::BeamParameters* beamParams = get<LHCb::BeamParameters>( LHCb::BeamParametersLocation::Default );
  _mHxAngle                        = 0.0;
  _mVxAngle                        = 0.0;
  if ( 0 == beamParams ) {
    error() << "No beam parameters found in TES at /Event/" << LHCb::BeamParametersLocation::Default << endmsg;
    return true;
  } else {
    info() << "Maximal angle smearing: " << ( beamParams->angleSmear() / Gaudi::Units::mrad ) << " mrad." << endmsg;
    if ( !fuzzyEq( beamParams->angleSmear(), 0.0 ) )
      debug() << "Please, use Gauss().BeamBetaStar = 0.0 to eliminate angle smearing if nedeed." << endmsg;
    _mHxAngle = beamParams->horizontalCrossingAngle();
    _mVxAngle = beamParams->verticalCrossingAngle();
  };
  if ( 0.0 != _mHxAngle ) {
    _xHAngleCorrection = true;
    info() << "Horizontal crossing angle: " << ( _mHxAngle / Gaudi::Units::mrad ) << " mrad." << endmsg;
  };
  if ( 0.0 != _mVxAngle ) {
    _xVAngleCorrection = true;
    info() << "Vertical crossing angle: " << ( _mVxAngle / Gaudi::Units::mrad ) << " mrad." << endmsg;
  };
  return false;
}

void RivetAnalysisHandler::prepareHepMCEvent( HepMC::GenEvent* hEvent ) {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "HepMC event before internal modifications:" << std::endl;
    hEvent->print( debug().stream() );
    debug() << endmsg;
  };
  // prevent processing if multiple weights defined and sub-events might/should appear
  if ( _checkEvtNb && !_isFirstEvent ) {
    // step event counter; do nothing in first event
    _currentEventNumber++;
    debug() << "HepMC event #" << hEvent->event_number() << " vs. internal counter #" << _currentEventNumber << "."
            << endmsg;
    if ( hEvent->event_number() != _currentEventNumber ) hEvent->set_event_number( _currentEventNumber );
  };
  if ( _needsUnitConv || _xHAngleCorrection || _xVAngleCorrection || m_modStatusID.value() ) {
    stringstream       dess;
    bool               negmm = false;
    double             px, py, pz, e;
    double             mm = 0.0;
    HepMC::FourVector* tmom;
    if ( _needsUnitConv ) _myStats[nbUnitConversions]++; // pre-update internal counter
#ifdef HEPMC_HAS_UNITS
    if ( _needsUnitConv ) hEvent->use_units( HepMC::Units::GEV, HepMC::Units::MM );
#else
    // reproduce event translation from HepMC code
    if ( _needsUnitConv && ( _scaleFactorLength != 1.0 ) )
      for ( Rivet::ConstGenVertexPtr vtx : Rivet::HepMCUtils::vertices( hEvent ) )
        vtx->convert_position( _scaleFactorLength );
#endif
    for ( HepMC::GenEvent::particle_iterator pi = hEvent->particles_begin(); pi != hEvent->particles_end(); ++pi ) {
      HepMC::GenParticle* p = ( *pi );
      px                    = p->momentum().px();
      py                    = p->momentum().py();
      pz                    = p->momentum().pz();
      e                     = p->momentum().e();
      if ( msgLevel( MSG::DEBUG ) ) {
        dess.str( std::string() );
        // from Rivet::FourMomentum::mass(); HepMC::FourVector::m() not optimised for Oz beam line
        mm    = invariantMass( p->momentum() );
        negmm = ( mm < 0.0 );
      };
      tmom = new HepMC::FourVector( px, py, pz, e );
      if ( m_modStatusID.value() ) { // erases trace-back to particles decayed by EvtGen
        int psid = p->status();
        if ( psid > LHCb::HepMCEvent::DocumentationParticle ) { // following HepMC 2.05 particle status code
                                                                // recommendation
          if ( p->is_beam() ) {
            ; // beam particle (status code == 4)
          } else if ( psid == LHCb::HepMCEvent::StableInDecayGen ) {
            p->set_status( 1 );
          } else if ( ( psid > 10 ) && ( psid < 201 ) ) {
            p->set_status( 0 );
          } else {
            p->set_status( 2 );
          };
          _myStats[3]++;
        };
      };
      // detect if particle rest mass is negative (ignore documentation particles)
      if ( msgLevel( MSG::DEBUG ) && ( p->status() != 3 ) && negmm ) {
        dess << "Negative rest mass particle: " << std::endl
             << "id=" << p->pdg_id() << "; status=" << p->status() << ";" << std::endl;
        dess << "Before boost: " << std::endl
             << "P = (" << px << ", " << py << ", " << pz << ", " << e << ") --> m0 = " << mm << " [MeV]." << std::endl;
      };
#ifndef HEPMC_HAS_UNITS
      if ( _needsUnitConv && ( _scaleFactorEnergy != 1.0 ) ) {
        // modify when length(time) unit conversion also needed (HepMC 2.03)
        // see GeneratorUtils::scale in LHCb framework if needed!
        tmom->setPx( px * _scaleFactorEnergy );
        tmom->setPy( py * _scaleFactorEnergy );
        tmom->setPz( pz * _scaleFactorEnergy );
        tmom->setE( e * _scaleFactorEnergy );
      };
#endif
      if ( _xHAngleCorrection && _xVAngleCorrection ) {
        tmom->setE( e - px * _mHxAngle - py * _mVxAngle );
        tmom->setPx( px - e * _mHxAngle );
        tmom->setPy( py - e * _mVxAngle );
        if ( statLogEnabled( nbXAngleBoosts ) )
          debug() << "After XY-boost P' = (" << tmom->px() << ", " << tmom->py() << ", " << tmom->pz() << ", "
                  << tmom->e() << ") [MeV]..." << endmsg;
        _myStats[nbXAngleBoosts]++;
      } else {
        if ( _xHAngleCorrection ) {
          tmom->setE( e - px * _mHxAngle );
          tmom->setPx( px - e * _mHxAngle );
          if ( statLogEnabled( nbXAngleBoosts ) )
            debug() << "After X-boost P' = (" << tmom->px() << ", " << tmom->py() << ", " << tmom->pz() << ", "
                    << tmom->e() << ") [MeV]..." << endmsg;
          _myStats[nbXAngleBoosts]++;
        };
        if ( _xVAngleCorrection ) {
          tmom->setE( e - py * _mVxAngle );
          tmom->setPy( py - e * _mVxAngle );
          if ( statLogEnabled( nbXAngleBoosts ) )
            debug() << "After Y-boost P' = (" << tmom->px() << ", " << tmom->py() << ", " << tmom->pz() << ", "
                    << tmom->e() << ") [MeV]..." << endmsg;
          _myStats[nbXAngleBoosts]++;
        };
      };
      if ( _needsUnitConv || _xHAngleCorrection || _xVAngleCorrection )
        p->set_momentum( (const HepMC::FourVector&)( *tmom ) );
      if ( msgLevel( MSG::DEBUG ) && negmm ) { // re-test for negative rest mass
        if ( _xHAngleCorrection || _xVAngleCorrection ) {
          mm = invariantMass( p->momentum() );
          if ( mm < 0. ) {
            if ( statLogEnabled( negMassParticles ) )
              debug() << dess.str() << "After boost: " << std::endl
                      << "P' = (" << tmom->px() << ", " << tmom->py() << ", " << tmom->pz() << ", " << tmom->e()
                      << ") --> m0 = " << mm << " [MeV]." << endmsg;
            _myStats[negMassParticles]++;
          };
        } else {
          debug() << dess.str() << endmsg;
          _myStats[negMassParticles]++;
        };
      }; // end 2nd test negative squared mass
      delete tmom;
    }; // end particle iteration
  };   // end big if (corrections/transforms needed)
  compatSetCrossSection( hEvent );
  // Analyze HepMC GenEvent in debugging mode
  if ( msgLevel( MSG::DEBUG ) &&
       ( _needsUnitConv || _xHAngleCorrection || _xVAngleCorrection || m_modStatusID.value() ) ) {
    debug() << "HepMC event after internal modifications:" << std::endl;
    hEvent->print( debug().stream() );
    debug() << endmsg;
  };
}
