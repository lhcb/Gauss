/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// -*- C++ -*-
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Tools/Logging.hh"
#include "YODA/Config/YodaConfig.h"
#include <Rivet/Analysis.hh>

using namespace std;

namespace Rivet {

  /// Generic analysis looking at various distributions of final state particles
  class MC_LHCb_GENERIC : public Analysis {
  public:
    /// Constructor
    MC_LHCb_GENERIC()
        : Analysis( "MC_LHCb_GENERIC" )
        , nerrinf( 0 )
        , nerrnan( 0 )
        , nphotons( 0 )
        , minPhtPx( 0. )
        , maxPhtPx( 0. )
        , minPhtPz( 0. )
        , maxPhtPz( 0. )
        , minPhtM( 0. )
        , maxPhtM( 0. )
        , npions( 0 )
        , minPi0Px( 0. )
        , maxPi0Px( 0. )
        , minPi0Pz( 0. )
        , maxPi0Pz( 0. )
        , minPi0M( 0. )
        , maxPi0M( 0. ) {}

  public:
    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      MSG_WARNING( "Initializing analysis module " << this->name() << " w/ RIVET " << RIVET_VERSION << " & YODA "
                                                   << YODA_VERSION << " ..." );
      // Projections
      const FinalState cnfs( Cuts::abseta < 10.0 && Cuts::pT > 150 * MeV );
      declare( UnstableParticles( Cuts::abseta < 10.0 && Cuts::pT > 100 * MeV ), "UFS" );
      declare( cnfs, "FS" );
      declare( ChargedFinalState( Cuts::abseta < 10.0 && Cuts::pT > 150 * MeV ), "CFS" );

      // Histograms
      // @todo Choose E/pT ranged based on input energies... can't do anything about kin. cuts, though
      book( _histMult, "Mult", 100, -0.5, 199.5 );
      book( _histMultCh, "MultCh", 100, -0.5, 199.5 );
      MSG_WARNING( "Booked multiplicity histos..." );

      book( _huMesPt, "uMesPt", 300, 0, 30 );
      book( _huMesEta, "uMesEta", 100, -10, 10 );
      book( _huMesRap, "uMesRap", 100, -10, 10 );
      book( _huMesP, "uMesP", 100, 0, 200 );

      book( _hsMesPt, "sMesPt", 300, 0, 30 );
      book( _hsMesEta, "sMesEta", 100, -10, 10 );
      book( _hsMesRap, "sMesRap", 100, -10, 10 );
      book( _hsMesP, "sMesP", 100, 0, 200 );

      book( _hcMesPt, "cMesPt", 300, 0, 30 );
      book( _hcMesEta, "cMesEta", 100, -10, 10 );
      book( _hcMesRap, "cMesRap", 100, -10, 10 );
      book( _hcMesP, "cMesP", 100, 0, 200 );

      book( _hbMesPt, "bMesPt", 300, 0, 30 );
      book( _hbMesEta, "bMesEta", 100, -10, 10 );
      book( _hbMesRap, "bMesRap", 100, -10, 10 );
      book( _hbMesP, "bMesP", 100, 0, 200 );

      book( _huBarPt, "uBarPt", 300, 0, 30 );
      book( _huBarEta, "uBarEta", 100, -10, 10 );
      book( _huBarRap, "uBarRap", 100, -10, 10 );
      book( _huBarP, "uBarP", 100, 0, 200 );

      book( _hsBarPt, "sBarPt", 300, 0, 30 );
      book( _hsBarEta, "sBarEta", 100, -10, 10 );
      book( _hsBarRap, "sBarRap", 100, -10, 10 );
      book( _hsBarP, "sBarP", 100, 0, 200 );

      book( _hcBarPt, "cBarPt", 300, 0, 30 );
      book( _hcBarEta, "cBarEta", 100, -10, 10 );
      book( _hcBarRap, "cBarRap", 100, -10, 10 );
      book( _hcBarP, "cBarP", 100, 0, 200 );

      book( _hbBarPt, "bBarPt", 300, 0, 30 );
      book( _hbBarEta, "bBarEta", 100, -10, 10 );
      book( _hbBarRap, "bBarRap", 100, -10, 10 );
      book( _hbBarP, "bBarP", 100, 0, 200 );

      MSG_WARNING( "Booked flavour hadron histos..." );
      book( _histPt, "Pt", 300, 0, 30 );
      book( _histPtCh, "PtCh", 300, 0, 30 );

      book( _histPx, "Px", 100, -50, 50 );
      book( _histPy, "Py", 100, -50, 50 );

      book( _hPhtPx, "PhtPxM", 20, -1., 1., 30, -15., 15. );
      book( _hPhtPy, "PhtPyM", 20, -1., 1., 30, -15., 15. );
      book( _hPi0Px, "Pi0PxM", 40, 115, 155, 30, -15., 15. );
      book( _hPi0Pz, "Pi0PzM", 40, 115, 155, 200, -sqrtS() / GeV, sqrtS() / GeV );

      book( _histE, "E", 100, 0, 200 );
      book( _histECh, "ECh", 100, 0, 200 );
      MSG_WARNING( "Booked momenta and energy histos..." );

      book( _histEta, "Eta", 100, -10, 10 );
      book( _histEtaCh, "EtaCh", 100, -10, 10 );
      book( _tmphistEtaPlus, "_tmp_histEtaPlus", 50, 0, 10 );
      book( _tmphistEtaMinus, "_tmp_histEtaMinus", 50, 0, 10 );
      book( _tmphistEtaChPlus, "_tmp_histEtaChPlus", 50, 0, 10 );
      book( _tmphistEtaChMinus, "_tmp_histEtaChMinus", 50, 0, 10 );
      book( _hEtaRatio, "EtaPMRatio" );     //, 50, 0., 10.);
      book( _hEtaChRatio, "EtaChPMRatio" ); //, 50, 0., 10.);

      book( _histEtaSumEt, "EtaSumEt", 50, 0, 10 );
      MSG_WARNING( "Booked pseudorapidity histos..." );

      book( _histRapidity, "Rapidity", 100, -10, 10 );
      book( _histRapidityCh, "RapidityCh", 100, -10, 10 );
      book( _tmphistRapPlus, "_tmp_histRapPlus", 50, 0, 10 );
      book( _tmphistRapMinus, "_tmp_histRapMinus", 50, 0, 10 );
      book( _tmphistRapChPlus, "_tmp_histRapChPlus", 50, 0, 10 );
      book( _tmphistRapChMinus, "_tmp_histRapChMinus", 50, 0, 10 );
      book( _hRapRatio, "RapidityPMRatio" );     //, 50, 0, 10);
      book( _hRapChRatio, "RapidityChPMRatio" ); //, 50, 0, 10);

      book( _histPhi, "Phi", 50, 0, TWOPI );
      book( _histPhiCh, "PhiCh", 50, 0, TWOPI );
      MSG_WARNING( "Booked particle geometry histos (etarap, rap, phi) ..." );
    }

    /// Perform the per-event analysis
    void analyze( const Event& event ) {
      // const double weight = event.weight();
      double mm, px, py, pz, pT; // @suppress("Multiple variable declaration")
      bool   isBaryon = false;
      bool   isMeson  = false;
      // Iterate through unstable particle list
      const UnstableParticles& ufs = applyProjection<UnstableParticles>( event, "UFS" );
      for ( const Particle& p : ufs.particles() ) {
        int pid = p.pid();
        if ( pid == 111 ) { // pi^0
          px = p.px() / GeV;
          pz = p.pz() / GeV;
          mm = p.mass() / MeV;
          _hPi0Px->fill( mm, px );
          _hPi0Pz->fill( mm, pz );
          npions++;
          if ( minPi0Px > px ) minPi0Px = px;
          if ( maxPi0Px < px ) maxPi0Px = px;
          if ( minPi0Pz > pz ) minPi0Pz = pz;
          if ( maxPi0Pz < pz ) maxPi0Pz = pz;
          if ( minPi0M > mm ) minPi0M = mm;
          if ( maxPi0M < mm ) maxPi0M = mm;
        };
        isBaryon = PID::isBaryon( pid );
        isMeson  = PID::isMeson( pid );
        if ( isBaryon || isMeson ) {
          mm                = p.p() / GeV;
          const double feta = p.eta();
          const double frap = p.rapidity();
          // skip problematic particles ?!
          if ( isnan( feta ) || isinf( feta ) ) continue;
          if ( isnan( frap ) || isinf( frap ) ) continue;
          pT = p.pT() / GeV;
          if ( PID::hasBottom( pid ) ) {
            if ( isBaryon ) {
              _hbBarPt->fill( pT );
              _hbBarEta->fill( feta );
              _hbBarRap->fill( frap );
              _hbBarP->fill( mm );
            };
            if ( isMeson ) {
              _hbMesPt->fill( pT );
              _hbMesEta->fill( feta );
              _hbMesRap->fill( frap );
              _hbMesP->fill( mm );
            };
            continue;
          };
          if ( PID::hasCharm( pid ) ) {
            if ( isBaryon ) {
              _hcBarPt->fill( pT );
              _hcBarEta->fill( feta );
              _hcBarRap->fill( frap );
              _hcBarP->fill( mm );
            };
            if ( isMeson ) {
              _hcMesPt->fill( pT );
              _hcMesEta->fill( feta );
              _hcMesRap->fill( frap );
              _hcMesP->fill( mm );
            };
            continue;
          };
          if ( PID::hasStrange( pid ) ) {
            if ( isBaryon ) {
              _hsBarPt->fill( pT );
              _hsBarEta->fill( feta );
              _hsBarRap->fill( frap );
              _hsBarP->fill( mm );
            };
            if ( isMeson ) {
              _hsMesPt->fill( pT );
              _hsMesEta->fill( feta );
              _hsMesRap->fill( frap );
              _hsMesP->fill( mm );
            };
            continue;
          };
          if ( isBaryon ) {
            _huBarPt->fill( pT );
            _huBarEta->fill( feta );
            _huBarRap->fill( frap );
            _huBarP->fill( mm );
          };
          if ( isMeson ) {
            _huMesPt->fill( pT );
            _huMesEta->fill( feta );
            _huMesRap->fill( frap );
            _huMesP->fill( mm );
          };
        };
      }

      // Charged + neutral final state
      const FinalState& cnfs = applyProjection<FinalState>( event, "FS" );
      MSG_DEBUG( "Total event multiplicity = " << cnfs.particles().size() );
      _histMult->fill( cnfs.particles().size() );
      for ( const Particle& p : cnfs.particles() ) {
        const double eta = p.eta();
        const double pet = p.Et() / GeV;
        // MSG_DEBUG("Filling eta!");
        _histEta->fill( eta );
        // MSG_DEBUG("Filling eta sum E_T!");
        _histEtaSumEt->fill( fabs( eta ), pet );
        if ( eta > 0 ) {
          // MSG_DEBUG("Filling eta plus!");
          _tmphistEtaPlus->fill( fabs( eta ) );
        } else {
          // MSG_DEBUG("Filling eta minus!");
          _tmphistEtaMinus->fill( fabs( eta ) );
        }
        const double rapidity = p.rapidity();
        // MSG_DEBUG("Filling rapidity (" << std::setw(6) << std::fixed << rapidity << std::scientific << ")!");
        if ( isinf( rapidity ) ) {
          if ( nerrinf < 100 )
            MSG_ERROR( "Rapidity is Inf for particle " << p.pid() << " (HEPMC status=" << p.genParticle()->status()
                                                       << ")..." );
          nerrinf++;
          continue;
        };
        if ( isnan( rapidity ) ) {
          if ( nerrnan < 100 )
            MSG_ERROR( "Rapidity is NaN for particle " << p.pid() << " (status=" << p.genParticle()->status()
                                                       << ")..." );
          nerrnan++;
          continue;
        };
        _histRapidity->fill( rapidity );
        if ( rapidity > 0 ) {
          // MSG_DEBUG("Filling rapidity plus!");
          _tmphistRapPlus->fill( rapidity );
        } else {
          // MSG_DEBUG("Filling rapidity minus!");
          _tmphistRapMinus->fill( -rapidity );
        }
        if ( ( &p ) == NULL ) {
          MSG_ERROR( "Null particle reference!" );
          continue;
        };
        px = p.px() / GeV;
        py = p.py() / GeV;
        pT = p.pT() / GeV;
        // MSG_DEBUG("px = " << px << " GeV/c");
        // MSG_DEBUG("py = " << py << " GeV/c");
        // MSG_DEBUG("Filling px!");
        _histPx->fill( px );
        // MSG_DEBUG("Filling py!");
        _histPy->fill( py );
        // MSG_DEBUG("Filling pT!");
        _histPt->fill( p.pT() / GeV );
        // MSG_DEBUG("Filling E!");
        _histE->fill( p.E() / GeV );
        // MSG_DEBUG("Filling phi!");
        _histPhi->fill( p.phi() );
        if ( p.pid() == 22 ) { // final-state photons
          pz = p.pz() / GeV;
          py = p.py() / GeV;
          mm = p.mass() / MeV;
          _hPhtPx->fill( mm, px );
          _hPhtPy->fill( mm, py );
          nphotons++;
          if ( minPhtPx > px ) minPhtPx = px;
          if ( maxPhtPx < px ) maxPhtPx = px;
          if ( minPhtPz > pz ) minPhtPz = pz;
          if ( maxPhtPz < pz ) maxPhtPz = px;
          if ( minPhtM > mm ) minPhtM = mm;
          if ( maxPhtM < mm ) maxPhtM = mm;
        };
      }
      // Process charged final state particles
      const ChargedFinalState& cfs = applyProjection<ChargedFinalState>( event, "CFS" );
      MSG_DEBUG( "Total event charged multiplicity = " << cfs.particles().size() );
      _histMultCh->fill( cfs.particles().size() );
      for ( const Particle& p : cfs.particles() ) {
        const double eta = p.pseudorapidity();
        pT               = p.pT() / GeV;
        _histEtaCh->fill( eta );
        if ( eta > 0. ) {
          _tmphistEtaChPlus->fill( eta );
        } else {
          _tmphistEtaChMinus->fill( -eta );
        }
        const double rapidity = p.momentum().rapidity();
        if ( isnan( rapidity ) || isinf( rapidity ) ) continue; // avoid events with bad data!
        _histRapidityCh->fill( rapidity );
        if ( rapidity > 0. ) {
          _tmphistRapChPlus->fill( rapidity );
        } else {
          _tmphistRapChMinus->fill( -rapidity );
        }
        _histPtCh->fill( pT );
        _histECh->fill( p.E() / GeV );
        _histPhiCh->fill( p.phi() );
      }
    }

    /// Finalize
    void finalize() {
      MSG_WARNING( "Entering " << this->name() << " finalize method..." );
      // make qmtest not fail!?
      if ( _hPhtPx->effNumEntries( false ) < 1.e-9 ) _hPhtPx->fill( 0., 0. );
      if ( _hPhtPy->effNumEntries( false ) < 1.e-9 ) _hPhtPy->fill( 0., 0. );
      if ( _hPi0Px->effNumEntries( false ) < 1.e-9 ) _hPi0Px->fill( 134.98, 0. );
      if ( _hPi0Pz->effNumEntries( false ) < 1.e-9 ) _hPi0Pz->fill( 134.98, 0. );
      const double unitFact = 1. / sumOfWeights();
      MSG_WARNING( "Normalize flavoured hadron histos..." );
      // Flavoured mesons
      scale( _huMesEta, unitFact );
      scale( _huMesRap, unitFact );
      scale( _huMesPt, unitFact );
      scale( _huMesP, unitFact );
      scale( _hsMesEta, unitFact );
      scale( _hsMesRap, unitFact );
      scale( _hsMesPt, unitFact );
      scale( _hsMesP, unitFact );
      scale( _hcMesEta, unitFact );
      scale( _hcMesRap, unitFact );
      scale( _hcMesPt, unitFact );
      scale( _hcMesP, unitFact );
      scale( _hbMesEta, unitFact );
      scale( _hbMesRap, unitFact );
      scale( _hbMesPt, unitFact );
      scale( _hbMesP, unitFact );
      // Flavoured baryons
      scale( _huBarEta, unitFact );
      scale( _huBarRap, unitFact );
      scale( _huBarPt, unitFact );
      scale( _huBarP, unitFact );
      scale( _hsBarEta, unitFact );
      scale( _hsBarRap, unitFact );
      scale( _hsBarPt, unitFact );
      scale( _hsBarP, unitFact );
      scale( _hcBarEta, unitFact );
      scale( _hcBarRap, unitFact );
      scale( _hcBarPt, unitFact );
      scale( _hcBarP, unitFact );
      scale( _hbBarEta, unitFact );
      scale( _hbBarRap, unitFact );
      scale( _hbBarPt, unitFact );
      scale( _hbBarP, unitFact );

      scale( _histMult, unitFact );
      scale( _histMultCh, unitFact );

      scale( _histEta, unitFact );
      scale( _histEtaCh, unitFact );

      scale( _histRapidity, unitFact );
      scale( _histRapidityCh, unitFact );

      scale( _histPt, unitFact );
      scale( _histPtCh, unitFact );

      scale( _histPx, unitFact );
      scale( _histPy, unitFact );

      scale( _histE, unitFact );
      scale( _histECh, unitFact );

      scale( _histPhi, unitFact );
      scale( _histPhiCh, unitFact );

      MSG_WARNING( "Prepare ratio plots..." );
      divide( _tmphistEtaPlus, _tmphistEtaMinus, _hEtaRatio );
      divide( _tmphistEtaChPlus, _tmphistEtaChMinus, _hEtaChRatio );
      divide( _tmphistRapPlus, _tmphistRapMinus, _hRapRatio );
      divide( _tmphistRapChPlus, _tmphistRapChMinus, _hRapChRatio );

      MSG_WARNING( std::endl
                   << "=======================================" << std::endl
                   << "Number of photons: " << nphotons << std::endl
                   << "min px, max px [GeV/c]: " << minPhtPx << " , " << maxPhtPx << std::endl
                   << "min pz, max pz [GeV/c]: " << minPhtPz << " , " << maxPhtPz << std::endl
                   << "min M, max M [MeV/c^2]: " << minPhtM << " , " << maxPhtM << std::endl
                   << "=======================================" << std::endl
                   << "Number of neutral pions: " << npions << std::endl
                   << "min px, max px [GeV/c]: " << minPi0Px << " , " << maxPi0Px << std::endl
                   << "min pz, max pz [GeV/c]: " << minPi0Pz << " , " << maxPi0Pz << std::endl
                   << "min M, max M [MeV/c^2]: " << minPi0M << " , " << maxPi0M << std::endl
                   << "=======================================" << std::endl
                   << "Number of particles w/ rapidity=NaN: " << nerrnan << std::endl
                   << "Number of particles w/ rapidity=Inf: " << nerrinf << std::endl
                   << "=======================================" << std::endl );

      /*
      removeAnalysisObject(_tmphistEtaPlus);
      removeAnalysisObject(_tmphistEtaMinus);
      removeAnalysisObject(_tmphistEtaChPlus);
      removeAnalysisObject(_tmphistEtaChMinus);

      removeAnalysisObject(_tmphistRapPlus);
      removeAnalysisObject(_tmphistRapMinus);
      removeAnalysisObject(_tmphistRapChPlus);
      removeAnalysisObject(_tmphistRapChMinus);
      */
    }

    //@}

  private:
    /// Temporary histos used to calculate eta+/eta- ratio plot
    Histo1DPtr _tmphistEtaPlus, _tmphistEtaMinus;     // @suppress("Multiple variable declaration")
    Histo1DPtr _tmphistEtaChPlus, _tmphistEtaChMinus; // @suppress("Multiple variable declaration")
    Histo1DPtr _tmphistRapPlus, _tmphistRapMinus;     // @suppress("Multiple variable declaration")
    Histo1DPtr _tmphistRapChPlus, _tmphistRapChMinus; // @suppress("Multiple variable declaration")
    /// Counters for error suppression
    unsigned int nerrinf;
    unsigned int nerrnan;

    /// Temp variables:
    unsigned int nphotons;
    double       minPhtPx, maxPhtPx, minPhtPz, maxPhtPz, minPhtM, maxPhtM; // @suppress("Multiple variable declaration")
    unsigned int npions;
    double       minPi0Px, maxPi0Px, minPi0Pz, maxPi0Pz, minPi0M, maxPi0M; // @suppress("Multiple variable declaration")

    /// @name Histograms
    //@{
    Histo1DPtr   _histMult, _histMultCh; // @suppress("Multiple variable declaration")
    Profile1DPtr _histEtaSumEt;
    Histo1DPtr   _huMesEta, _huMesRap, _huMesPt, _huMesP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _hsMesEta, _hsMesRap, _hsMesPt, _hsMesP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _hcMesEta, _hcMesRap, _hcMesPt, _hcMesP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _hbMesEta, _hbMesRap, _hbMesPt, _hbMesP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _huBarEta, _huBarRap, _huBarPt, _huBarP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _hsBarEta, _hsBarRap, _hsBarPt, _hsBarP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _hcBarEta, _hcBarRap, _hcBarPt, _hcBarP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _hbBarEta, _hbBarRap, _hbBarPt, _hbBarP;            // @suppress("Multiple variable declaration")
    Histo1DPtr   _histEta, _histEtaCh;                               // @suppress("Multiple variable declaration")
    Histo1DPtr   _histRapidity, _histRapidityCh;                     // @suppress("Multiple variable declaration")
    Histo1DPtr   _histPt, _histPtCh;                                 // @suppress("Multiple variable declaration")
    Histo1DPtr   _histPx, _histPy;                                   // @suppress("Multiple variable declaration")
    Histo1DPtr   _histE, _histECh;                                   // @suppress("Multiple variable declaration")
    Histo1DPtr   _histPhi, _histPhiCh;                               // @suppress("Multiple variable declaration")
    Histo2DPtr   _hPhtPx, _hPi0Px;                                   // @suppress("Multiple variable declaration")
    Histo2DPtr   _hPhtPy, _hPi0Pz;                                   // @suppress("Multiple variable declaration")
    Scatter2DPtr _hEtaRatio, _hEtaChRatio, _hRapRatio, _hRapChRatio; // @suppress("Multiple variable declaration")
    //@}
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN( MC_LHCb_GENERIC );

} // namespace Rivet
