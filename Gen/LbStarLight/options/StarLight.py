###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

# Dictionary of known nucleons with entries which take the form:
# name : (Z,  A, Delta mass in MeV.)
# Delta masses (in keV) are from http://amdc.in2p3.fr/nubase/nubase2016.txt.
nucleons = {
    "p": (1, 1, None),
    "He": (2, 4, 2.4249156),  # Alias for 4He.
    "Ne": (10, 20, -7.0419305),  # Alias for 20Ne.
    "Ar": (18, 40, -35.0398946),  # Alias for 40Ar.
    "Kr": (36, 84, -82.439335),  # Alias for 84Kr.
    "Xe": (54, 132, -89.278962),  # Alias for 132Xe.
    "Pb": (82, 208, -21.7486),  # Alias for 208Pb.
    "3He": (2, 3, 14.9312179),
    "4He": (2, 4, 2.4249156),
    "20Ne": (10, 20, -7.0419305),
    "21Ne": (10, 21, -5.73178),
    "22Ne": (10, 22, -8.024719),
    "38Ar": (18, 38, -34.71482),
    "39Ar": (18, 39, -33.242),
    "40Ar": (18, 40, -35.0398946),
    "84Kr": (36, 84, -82.439335),
    "128Xe": (54, 128, -89.8603),
    "132Xe": (54, 132, -89.278962),
    "208Pb": (82, 208, -21.7486),
}


# Set the beam, given an ID (1 or 2) and a list of commands to
# append. This returns the beam four-vector. Atomic mass unit is from
# https://physics.nist.gov/cgi-bin/cuu/Value?uev.
def setBeam(beam, cmds):
    from math import sin, sqrt

    from Configurables import Gauss, Generation, StarLightProduction

    # Get the crossings angles.
    hCrossingAngle = Gauss().getProp("BeamHCrossingAngle")
    hBeamlineAngle = Gauss().getProp("BeamLineAngles")[0]
    vCrossingAngle = Gauss().getProp("BeamVCrossingAngle")
    vBeamlineAngle = Gauss().getProp("BeamLineAngles")[1]

    # Calculate the momenta.
    sn = -1.0 if beam == 2 else 1.0
    pz = Gauss().getProp("%sMomentum" % ("B2" if beam == 2 else "Beam"))
    px = abs(pz) * sin(hCrossingAngle + sn * hBeamlineAngle)
    py = abs(pz) * sin(vCrossingAngle + sn * vBeamlineAngle)

    ## pp case here for beam 2
    # if beam == 2 and Gauss().getProp('%sMomentum' % ('B2') ) > 0.0 :
    #    pz = sn*pz
    #    px = sn*px
    #    py = sn*py

    print(px, py, pz, "momentum", beam)

    # Append the beam particle Z and A.
    nucleon = Gauss().getProp("B%iParticle" % beam)
    if nucleon in nucleons:
        z, a, dm = nucleons[nucleon]
    else:
        print("Unknown nucleon %s for beam %i." % (nucleon, beam))
        z, a, dm = None, None, None
        Generation().Special.addTool(StarLightProduction)
        for cmd in Generation().Special.StarLightProduction.Commands:
            if ("BEAM_%i_Z" % beam) in cmd:
                z = int(cmd.split("=")[-1])
            if ("BEAM_%i_A" % beam) in cmd:
                a = int(cmd.split("=")[-1])
        if z == None:
            print("Beam %i Z not set, defaulting to 1.")
            z = 1
        if a == None:
            print("Beam %i A not set, defaulting to 1.")
            a = 1
    cmds += ["BEAM_%i_Z = %i" % (beam, z)]
    cmds += ["BEAM_%i_A = %i" % (beam, a)]

    # Calculate the mass and beam gamma, append beam gamma.
    if dm:
        m = a * 931.4940954 + dm
    else:
        m = (a - z) * 939.565413 + z * 938.27281
    gamma = (a * sqrt(px**2 + py**2 + pz**2) + m) / m
    cmds += ["BEAM_%i_GAMMA = %f" % (beam, gamma)]

    # Return the beam momentum.
    return px, py, pz, sqrt(m**2 + px**2 + py**2 + pz**2)


def configStarLight():
    from Configurables import BoostForEpos, GaudiSequencer

    # Set the beam particles.
    cmds = []
    px1, py1, pz1, e1 = setBeam(1, cmds)
    px2, py2, pz2, e2 = setBeam(2, cmds)

    # Append the commands.
    Generation().Special.ProductionTool = "StarLightProduction"
    Generation().Special.PileUpProductionTool = "Pythia8Production"
    Generation().Special.ReinitializePileUpGenerator = False
    Generation().Special.addTool(StarLightProduction)
    Generation().Special.StarLightProduction.Commands += cmds

    # Set the boost.
    GaudiSequencer("GeneratorSlotMainSeq").Members += [
        BoostForEpos(p_x=px1 + px2, p_y=py1 + py2, p_z=pz1 + pz2, e=e1 + e2)
    ]


# Add StarLight as a special production tool.
from Configurables import Generation, Special, StarLightProduction

Generation().addTool(Special)
Generation().Special.ProductionTool = "StarLightProduction"
Generation().Special.addTool(StarLightProduction)

# Append the configuration as a final action.
from Gaudi.Configuration import appendPostConfigAction

appendPostConfigAction(configStarLight)
