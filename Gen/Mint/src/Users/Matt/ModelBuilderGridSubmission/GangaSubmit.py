###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

import os
import subprocess
import sys
import time

os.system("echo $ROOTSYS")

print("ROOT environment configured")

fileout = "out_" + sys.argv[1] + ".txt"

print(fileout)

print(sys.argv[2])

os.system("./ampFit " + sys.argv[1] + " < " + sys.argv[2] + " > " + fileout)
