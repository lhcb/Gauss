/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:07 GMT
#include "DKsPiPi.h"
#include "DKsPiPiResonances.h"
using namespace DKsPiPiResonances;

ClassImp( DKsPiPi )

    Dalitz3Body::DKsPiPi( const char* name, const char* title, RooAbsReal& _m12sq, RooAbsReal& _m23sq,
                          RooArgList& ResonanceList )
    : AbsComplexPdf( name, title )
    , _result( 0, 0 )
    , m13sq( "m13sq", "m13sq", m12sq, m23sq, DZeroM, piPlusM, KsM, piMinusM )
    , resSum( "resSum", "resSum", ResonanceList ) {}

Dalitz3Body::DKsPiPi( const Dalitz3Body& other, const char* name )
    : AbsComplexPdf( other, name ), _result( other._result )
}
//
