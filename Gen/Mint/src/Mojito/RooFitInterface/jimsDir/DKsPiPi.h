/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:07 GMT
#ifndef DALITZDKSPIPI
#  define DALITZDKSPIPI

#  include "Rtypes.h"
#  include "TObject.h"

#  include "RooAbsPdf.h"
#  include "RooAbsReal.h"
#  include "RooListProxy.h"
#  include "RooRealProxy.h"

#  include "AbsComplexPdf.h"
#  include "RooM13.h"
#  include <complex>

class DKsPiPi : public AbsComplexPdf {
  mutable std::complex<Double_t> _result;

public:
  DKsPiPi( const char* name, const char* title, RooAbsReal& _m12sq, RooAbsReal& _m23sq );
  DKsPiPi( const DKsPiPi& copyMe, const char* name = 0 );
  virtual TObject* clone( const char* newname ) const { return new DKsPiPi( *this, newname ); }
  inline virtual ~DKsPiPi() {}

protected:
  RooM13     m13sq;
  ComplexSum resSum;

  virtual std::complex<Double_t> getCVal() const {
    getVal();
    return _result;
  }
  Double_t evaluate() const {
    _result = resSum.getCVal();
    return _result.real() * _result.real() + _result.imag() * _result.imag();
  }

private:
  ClassDef( DKsPiPi, 0 )
};
#endif
//
