/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:06 GMT
#ifndef JONASDALITZ3BODY
#  define JONASDALITZ3BODY

#  include "Rtypes.h"
#  include "TObject.h"

#  include "RooAbsPdf.h"
#  include "RooAbsReal.h"
#  include "RooListProxy.h"
#  include "RooRealProxy.h"

#  include "AbsComplexPdf.h"
#  include "ComplexSum.h"
#  include <complex>

class Dalitz3Body : public AbsComplexPdf {
  mutable std::complex<Double_t> _result;

public:
  Dalitz3Body( const char* name, const char* title, RooAbsReal& _m12sq, RooAbsReal& _m23sq );
  Dalitz3Body( const Dalitz3Body& other, const char* name = 0 );
  virtual TObject* clone( const char* newname ) const { return new Dalitz3Body( *this, newname ); }
  inline virtual ~Dalitz3Body() {}

protected:
  static RooRealVar piM rhoM, rhoW, BM;
  RooM13                m13sq;
  ComplexBW             bw12;
  ComplexBW             bw13;
  ComplexBW             bw23;

  ComplexSum         resonances;
  Roo3BodyPhaseSpace phaseSpace;

  virtual std::complex<Double_t> getCVal() const {
    getVal();
    return _result;
  }
  Double_t evaluate() const;

private:
  ClassDef( Dalitz3Body, 0 )
};
#endif
//
