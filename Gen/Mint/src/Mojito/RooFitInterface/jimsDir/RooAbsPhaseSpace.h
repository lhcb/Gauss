/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:09 GMT
#ifndef ROOABSPHASESPACE
#  define ROOABSPHASESPACE

#  include "RooAbsPdf.h"
#  include "RooArgList.h"
#  include "RooArgSet.h"

class RooAbsPhaseSpace : public RooAbsPdf {
public:
  RooAbsPhaseSpace( const char* name, const char* title ) : RooAbsPdf( name, title ) {}
  RooAbsPhaseSpace( const RooAbsPhaseSpace& other, const char* name ) : RooAbsPdf( other, name ) {}
  RooAbsPhaseSpace( const RooAbsPdf& other, const char* name ) : RooAbsPdf( other, name ) {}

  virtual RooArgSet  defaultParameterSet() const  = 0;
  virtual RooArgList defaultParameterList() const = 0;
  RooDataSet*        generateDefault( int nEvents ) { return generate( defaultParameterSet(), nEvents ); }
  Double_t           getNormDefault() const { return getNorm( defaultParameterSet() ); }

private:
  ClassDef( RooAbsPhaseSpace, 0 )
};
#endif
//
