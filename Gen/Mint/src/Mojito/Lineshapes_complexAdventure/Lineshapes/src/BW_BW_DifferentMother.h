/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BW_BW_DIFFERENT_MOTHER_HH
#  define BW_BW_DIFFERENT_MOTHER_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:04 GMT

#  include "BW_BW.h"

// used for CrystalBarrelRhoOmega
class ParticlePropeties;

class BW_BW_DifferentMother : public BW_BW, virtual public ILineshape {
protected:
  int                               _alternativeMumPDG;
  virtual const ParticleProperties* mumsProperties() const;

public:
  BW_BW_DifferentMother( const AssociatedDecayTree& decay, IDalitzEventAccess* events, int newMumID );
};

#endif
//
