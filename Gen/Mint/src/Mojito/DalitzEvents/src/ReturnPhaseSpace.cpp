/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:00 GMT
#include "Mint/ReturnPhaseSpace.h"
using namespace MINT;

ReturnPhaseSpace::ReturnPhaseSpace( IDalitzEventAccess* evts )
    : IDalitzEventAccess(), IReturnReal(), DalitzEventAccess( evts ) {}

ReturnPhaseSpace::ReturnPhaseSpace( IDalitzEventList* evts )
    : IDalitzEventAccess(), IReturnReal(), DalitzEventAccess( evts ) {}

ReturnPhaseSpace::ReturnPhaseSpace( const ReturnPhaseSpace& other )
    : IBasicEventAccess<IDalitzEvent>()
    , IEventAccess<IDalitzEvent>()
    , IDalitzEventAccess()
    , IReturnReal()
    , IGetRealEvent<IDalitzEvent>()
    , DalitzEventAccess( other ) {}

double ReturnPhaseSpace::RealVal() {
  if ( 0 == getEvent() ) return -9999;
  return getEvent()->phaseSpace();
}
