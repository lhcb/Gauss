/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:01 GMT
#include "Mint/ReturnWeight.h"
using namespace MINT;

ReturnWeight::ReturnWeight( IDalitzEventAccess* evts )
    : IDalitzEventAccess(), IReturnReal(), DalitzEventAccess( evts ) {}

ReturnWeight::ReturnWeight( IDalitzEventList* evts ) : IDalitzEventAccess(), IReturnReal(), DalitzEventAccess( evts ) {}

ReturnWeight::ReturnWeight( const ReturnWeight& other )
    : IBasicEventAccess<IDalitzEvent>()
    , IEventAccess<IDalitzEvent>()
    , IDalitzEventAccess()
    , IReturnReal()
    , IGetRealEvent<IDalitzEvent>()
    , DalitzEventAccess( other ) {}

double ReturnWeight::RealVal() {
  if ( 0 == getEvent() ) return -9999;
  return getEvent()->getWeight();
}
