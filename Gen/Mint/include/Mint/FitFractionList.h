/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINT_DALITZ_FIT_FRACTION_LIST_HH
#  define MINT_DALITZ_FIT_FRACTION_LIST_HH

#  include "Mint/FitFraction.h"
#  include <cmath>
#  include <iostream>
#  include <vector>

/* this is a dumb class
   that only stores the result
   of a fit fraction calculation
   but doesn not calculate anytthig
   by itself
*/

class FitFractionList : public std::vector<FitFraction> {
protected:
  FitFraction _sum;

public:
  FitFractionList();
  FitFractionList( const FitFractionList& other );

  void add( const FitFraction& f );

  const FitFraction& sum() const { return _sum; }
  void               setSumFitError( double sfe );
  void               setSumIntegError( double sie );

  void print( std::ostream& os ) const;

  void sortBySizeAscending();
  void sortBySizeDecending();
  void sortByMagnitudeAscending();
  void sortByMagnitudeDecending();
  void sortByName();

  // helper classes for sorting:
  class smallerFitFractionBySize {
  public:
    bool operator()( const FitFraction& a, const FitFraction& b ) { return a.frac() < b.frac(); }
  };
  class antiSmallerFitFractionBySize {
  public:
    bool operator()( const FitFraction& a, const FitFraction& b ) { return ( a.frac() > b.frac() ); }
  };
  class smallerFitFractionByMagnitude {
  public:
    bool operator()( const FitFraction& a, const FitFraction& b ) { return fabs( a.frac() ) < fabs( b.frac() ); }
  };
  class antiSmallerFitFractionByMagnitude {
  public:
    bool operator()( const FitFraction& a, const FitFraction& b ) { return ( fabs( a.frac() ) > fabs( b.frac() ) ); }
  };
  class smallerFitFractionByName {
  public:
    bool operator()( const FitFraction& a, const FitFraction& b ) { return a.name() < b.name(); }
  };
};

std::ostream& operator<<( std::ostream& os, const FitFractionList& f );
#endif
//
