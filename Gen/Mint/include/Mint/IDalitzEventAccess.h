/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IDALITZEVENTACCESS_HH
#  define IDALITZEVENTACCESS_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:00 GMT

#  include "Mint/IDalitzEvent.h"
#  include "Mint/IDalitzIntegrator.h"
#  include "Mint/IEventAccess.h"

class IDalitzEventAccess : virtual public MINT::IEventAccess<IDalitzEvent> {
public:
  //  virtual IDalitzIntegrator* makeIntegratorForOwner()=0;
};
#endif
//
