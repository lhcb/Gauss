/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IEVENT_GENERATOR_HH
#  define IEVENT_GENERATOR_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:54 GMT

#  include "Mint/counted_ptr.h"

namespace MINT {

  template <typename RETURN_TYPE>
  class IEventGenerator {
  public:
    virtual counted_ptr<RETURN_TYPE> newEvent()          = 0;
    virtual bool                     exhausted() const   = 0;
    virtual bool                     ensureFreshEvents() = 0; // will normally randomise the seed
    virtual ~IEventGenerator() {}
  };

} // namespace MINT
#endif
//
