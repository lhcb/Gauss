/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FIT_COMPLEX_POLAR_HH
#  define FIT_COMPLEX_POLAR_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT

#  include <complex>
#  include <iostream>
#  include <string>

#  include "Mint/FitParameter.h"
#  include "Mint/NamedParameter.h"
#  include "Mint/NamedParameterBase.h"
#  include "Mint/Phase.h"

#  include "Mint/FitComplex.h"

namespace MINT {

  class FitComplexPolar : public FitComplex {
    //  FitComplexPolar(const FitComplexPolar& ){};
    // no copying for now.
    // dangerous because for each
    // FitParameter, a pointer
    // is held in MinuitParametSet.
    // Anyway, what would the copied parameter
    // mean?
  protected:
    FitParameter _amp;
    FitParameter _phase;

    void defaultInit();

    static double _degFac;
    static void   calculateDegFac();

  public:
    inline const FitParameter& amp() const { return _amp; }
    inline const FitParameter& phase() const { return _phase; }

    FitParameter& amp() { return _amp; }
    FitParameter& phase() { return _phase; }
    static double degFac() {
      if ( _degFac < 0 ) calculateDegFac();
      return _degFac;
    }

    FitComplex::TYPE type() const override { return FitComplex::POLAR; }

    FitParameter&       p1() override { return _amp; }
    FitParameter&       p2() override { return _phase; }
    const FitParameter& p1() const override { return _amp; }
    const FitParameter& p2() const override { return _phase; }

    static std::string makeAmpName( const std::string& varName );
    static std::string makePhaseName( const std::string& varName );

    void set( std::complex<double> z ) override;

    FitComplexPolar( const std::string& varName, const char* fname = 0, MinuitParameterSet* pset = 0,
                     FitParameter::FIX_OR_WHAT     fow = FitParameter::FIX,
                     NamedParameterBase::VERBOSITY vb  = NamedParameterBase::VERBOSE );
    FitComplexPolar( const std::string& varName, MinuitParameterSet* pset,
                     FitParameter::FIX_OR_WHAT     fow = FitParameter::FIX,
                     NamedParameterBase::VERBOSITY vb  = NamedParameterBase::VERBOSE );
    virtual ~FitComplexPolar();

    std::complex<double> getVal() const override;
    std::complex<double> getValInit() const override;
    void                 print( std::ostream& os = std::cout ) const override;
    bool                 gotInitialised() const override;

    operator std::complex<double>() const override { return getVal(); }

    bool isZero() const override { return ( 0.0 == amp() ); }
  };
} // namespace MINT
#endif
//
