/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMINTGEN_HH
#  define IMINTGEN_HH
// author: Matt Coombes
// status:  17 Aug 2011

#  include "TRandom.h"
#  include <string>
#  include <vector>

namespace MINT {
  class IMintGen {
  public:
    virtual ~IMintGen(){};

    virtual void SetInputTextFile( std::string inputFile )              = 0;
    virtual void Initalize( const std::vector<int>& pat, TRandom* rnd ) = 0;

    // Decay Event in parent Rest Frame
    virtual std::vector<std::vector<double>> DecayEventRFVec() = 0;
  };
} // namespace MINT

#endif
//
