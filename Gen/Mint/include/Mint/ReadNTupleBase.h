/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef READNTUPLEBASE_HH
#define READNTUPLEBASE_HH

#include "TBranch.h"
#include "TEntryList.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TTree.h"

#include <vector>

#include <string>

#include "Mint/DalitzEvent.h"
#include "Mint/DalitzEventList.h"
#include "Mint/DalitzEventPattern.h"
#include "Mint/DiskResidentEventList.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/counted_ptr.h"

using namespace std;
using namespace MINT;

class ReadNTupleBase {
private:
public:
  ReadNTupleBase() {}

  ~ReadNTupleBase() {}
};

#endif
