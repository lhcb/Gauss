/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IUNWEIGHTED_EVENT_GENERATOR_HH
#  define IUNWEIGHTED_EVENT_GENERATOR_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:54 GMT

#  include "Mint/IEventGenerator.h"
#  include "Mint/counted_ptr.h"
// #include "TRandom.h"

class TRandom;

namespace MINT {
  // (maybe this belongs in the fitter, not in mint)

  template <typename RETURN_TYPE>
  class IUnweightedEventGenerator : public virtual IEventGenerator<RETURN_TYPE> {
  public:
    counted_ptr<RETURN_TYPE>         newEvent() override  = 0;
    virtual counted_ptr<RETURN_TYPE> newUnweightedEvent() = 0;

    bool         ensureFreshEvents() override = 0;
    virtual bool setRnd( TRandom* rnd )       = 0;

    virtual ~IUnweightedEventGenerator() {}
  };

} // namespace MINT
#endif
//
