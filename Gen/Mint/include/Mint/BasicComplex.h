/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINT_BASIC_COMPLEX_CLASS_HH
#  define MINT_BASIC_COMPLEX_CLASS_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT
#  include "Mint/IReturnComplex.h"
#  include <complex>
namespace MINT {
  class BasicComplex : virtual public IReturnComplex {
    std::complex<double> _z;

  public:
    BasicComplex( double initVal = 0 );
    BasicComplex( const std::complex<double>& initVal );
    BasicComplex( const BasicComplex& other );
    BasicComplex( IReturnComplex* other );

    std::complex<double>         ComplexVal() override;
    virtual void                 setVal( std::complex<double>& val );
    virtual std::complex<double> getVal() const;

    virtual ~BasicComplex(){};
  };
} // namespace MINT
#endif
//
