/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FIT_AMP_PAIR_LIST_HH
#  define FIT_AMP_PAIR_LIST_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:03 GMT

#  include "Mint/DalitzHistoSet.h"
#  include "Mint/FitAmpPair.h"
#  include "Mint/FitAmpPairCovariance.h"
#  include "Mint/FitFractionList.h"

#  include "Mint/IGetDalitzEvent.h"
#  include "Mint/IIntegrationCalculator.h"

#  include "Mint/counted_ptr.h"
#  include <vector>

class FitAmplitude;
class IDalitzEvent;
namespace MINT {
  class Minimiser;
}

class FitAmpPairList : public std::vector<FitAmpPair>, virtual public IIntegrationCalculator {
  int    _Nevents;
  double _sum;
  double _sumsq;

  double _psSum;
  double _psSumSq;

  mutable FitAmpPairCovariance _cov;

  MINT::counted_ptr<IGetDalitzEvent> _efficiency;

  FitFractionList _singleAmpFractions, _interferenceFractions;

  double phaseSpaceIntegral() const; // only for debug

  std::string    dirName() const;
  bool           makeDirectory( const std::string& asSubdirOf = "." ) const;
  virtual double oldVariance() const;

  bool reset();

public:
  FitAmpPairList();
  FitAmpPairList( const FitAmpPairList& other );
  MINT::counted_ptr<IIntegrationCalculator> clone_IIntegrationCalculator() const override;

  virtual void addAmps( FitAmplitude* a1, FitAmplitude* a2 );
  void         addEvent( IDalitzEvent* evtPtr, double weight = 1 ) override;
  void         addEvent( MINT::counted_ptr<IDalitzEvent> evtPtr, double weight = 1 ) override;

  bool         isCompatibleWith( const FitAmpPairList& other ) const;
  virtual bool add( const FitAmpPairList& otherList );
  virtual bool add( const FitAmpPairList* otherListPtr );
  virtual bool add( MINT::const_counted_ptr<FitAmpPairList> otherListPtr );

  virtual bool append( const FitAmpPairList& otherListPtr );
  virtual bool append( const FitAmpPairList* otherListPtr );
  virtual bool append( MINT::const_counted_ptr<FitAmpPairList> otherListPtr );

  int    numEvents() const override;
  double integral() const override;
  double variance() const override;
  double sumOfVariances() const;

  FitFractionList getFractions() const override { return _singleAmpFractions; }
  FitFractionList getInterferenceTerms() const { return _interferenceFractions; }

  void   setEfficiency( MINT::counted_ptr<IGetDalitzEvent> eff );
  void   unsetEfficiency();
  double efficiency( IDalitzEvent* evtPtr );

  bool makeAndStoreFractions( MINT::Minimiser* mini = 0 ) override {
    return makeAndStoreFractions( "FitAmpResults.txt", mini );
  }
  virtual bool makeAndStoreFractions( const std::string& fname, MINT::Minimiser* min = 0 );
  double       getFractionChi2() const override;

  DalitzHistoSet histoSet() const override;
  void           saveEachAmpsHistograms( const std::string& prefix ) const override;

  std::vector<DalitzHistoSet> GetEachAmpsHistograms();

  void doFinalStats( MINT::Minimiser* min = 0 ) override;

  bool save( const std::string& asSubdirOf = "." ) const override;
  bool retrieve( const std::string& asSubdirOf = "." ) override;

  void print( std::ostream& os = std::cout ) const override;

  FitAmpPairList& operator+=( const FitAmpPairList& other );
  FitAmpPairList  operator+( const FitAmpPairList& other ) const;
};

std::ostream& operator<<( std::ostream& os, const FitAmpPairList& fap );

#endif
//
