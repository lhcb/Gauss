/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SPINFACTOR_TRIVIAL_HH
#  define SPINFACTOR_TRIVIAL_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:13 GMT

#  include "Mint/CLHEPSystemOfUnits.h"

#  include "Mint/ISpinFactor.h"

// 2-body and trivial spin factor:
class SpinFactorTrivial : virtual public ISpinFactor {
public:
  double getVal() override { return 1.0; }

  double RealVal() override { return getVal(); }

  std::string name() const override { return "TrivialSpinFactor=1"; }
};

#endif
//
