/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Mint/IEventAccess.h>
#include <Mint/IGetRealEvent.h>
#include <Mint/IReturnRealWithSmoothy.h>

namespace MINT {

  template <typename EVENT>
  class IGetRealEventWithSmoothy : virtual public IEventAccess<EVENT>,
                                   virtual public IGetRealEvent<EVENT>,
                                   virtual public IReturnRealWithSmoothy {
  public:
    //  virtual double getVal()=0;
    //  virtual double RealVal()=0;
    // virtual double SmootherLargerRealVal()=0;
    virtual ~IGetRealEventWithSmoothy() {}
  };

} // namespace MINT
