/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FAST_AMPLITUDE_HH
#  define FAST_AMPLITUDE_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:57 GMT

#  include <complex>
#  include <map>

#  include "Mint/IGetRealEvent.h"
#  include "Mint/IReturnComplex.h"

#  include "Mint/Amplitude.h"
#  include "Mint/IDalitzEvent.h"

class FastAmplitude : public Amplitude,
                      virtual public MINT::IGetRealEvent<IDalitzEvent>,
                      virtual public MINT::IReturnComplex {
protected:
  std::map<IDalitzEvent*, std::complex<double>> _resultMap;

  bool knownEvent( std::complex<double>& value );

  mutable long int _rememberNumber; // for cashing
  long int         rememberNumber() const;

public:
  FastAmplitude( const DecayTree& decay, IDalitzEventAccess* events, char SPD_Wave = '?', const std::string& opt = "" );

  FastAmplitude( const DecayTree& decay, IDalitzEventList* events, char SPD_Wave = '?', const std::string& opt = "" );

  FastAmplitude( const AmpInitialiser& ampInit, IDalitzEventAccess* events );
  FastAmplitude( const AmpInitialiser& ampInit, IDalitzEventList* events );

  FastAmplitude( const FastAmplitude& other );
  FastAmplitude( const FastAmplitude& other, IDalitzEventAccess* newEvents );
  FastAmplitude( const FastAmplitude& other, IDalitzEventList* newEvents );

  FastAmplitude( const Amplitude& other );
  FastAmplitude( const Amplitude& other, IDalitzEventAccess* newEvents );
  FastAmplitude( const Amplitude& other, IDalitzEventList* newEvents );

  std::complex<double> getVal() override;
  std::complex<double> getVal( IDalitzEvent* evt ) override;
};

#endif
//
