/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef NEG_TWO_LL_SUM_HH
#  define NEG_TWO_LL_SUM_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:56 GMT

#  include "Mint/Minimisable.h"

#  include <vector>

namespace MINT {

  class IMinimisable;
  class MinuitParameterSet;

  class Neg2LLSum : public Minimisable {

    std::vector<IMinimisable*> _likList;

  public:
    Neg2LLSum( MinuitParameterSet* mps = 0 );

    Neg2LLSum( const std::vector<IMinimisable*>& likList_in, MinuitParameterSet* mps = 0 );

    Neg2LLSum( IMinimisable* ll_1, MinuitParameterSet* mps = 0 );

    Neg2LLSum( IMinimisable* ll_1, IMinimisable* ll_2, MinuitParameterSet* mps = 0 );

    Neg2LLSum( IMinimisable* ll_1, IMinimisable* ll_2, IMinimisable* ll_3, MinuitParameterSet* mps = 0 );

    Neg2LLSum( IMinimisable* ll_1, IMinimisable* ll_2, IMinimisable* ll_3, IMinimisable* ll_4,
               MinuitParameterSet* mps = 0 );

    Neg2LLSum( IMinimisable* ll_1, IMinimisable* ll_2, IMinimisable* ll_3, IMinimisable* ll_4, IMinimisable* ll_5,
               MinuitParameterSet* mps = 0 );

    Neg2LLSum( IMinimisable* ll_1, IMinimisable* ll_2, IMinimisable* ll_3, IMinimisable* ll_4, IMinimisable* ll_5,
               IMinimisable* ll_6, MinuitParameterSet* mps = 0 );

    Neg2LLSum( const Neg2LLSum& other );

    bool add( IMinimisable* llPtr );

    void beginFit() override;

    void parametersChanged() override;

    void endFit() override;

    double getVal() override;
  };
} // namespace MINT
#endif
//
