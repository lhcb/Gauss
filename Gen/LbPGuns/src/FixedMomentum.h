/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedMomentum.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_FIXEDMOMENTUM_H
#define PARTICLEGUNS_FIXEDMOMENTUM_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
// from ParticleGuns
#include "GaudiKernel/RndmGenerators.h"
#include "LbPGuns/IParticleGunTool.h"

/** @class FixedMomentum FixedMomentum.h "FixedMomentum.h"
 *
 *  Particle gun with fixed momentum
 *
 *  @author Patrick Robbe
 *  @date   2008-05-18
 */
class FixedMomentum : public extends<GaudiTool, IParticleGunTool> {
public:
  /// Constructor
  using extends::extends;

  /// Initialize particle gun parameters
  StatusCode initialize() override;

  /// Generation of particles
  void generateParticle( Gaudi::LorentzVector& momentum, Gaudi::LorentzVector& origin, int& pdgId ) override;

  /// Print counters
  void printCounters() override { ; };

private:
  Gaudi::Property<double> m_px{ this, "px", 1.0 * Gaudi::Units::GeV, "px (Set by options)" }; ///< px (Set by options)
  Gaudi::Property<double> m_py{ this, "py", 1.0 * Gaudi::Units::GeV, "py (Set by options)" }; ///< py (Set by options)
  Gaudi::Property<double> m_pz{ this, "pz", 1.0 * Gaudi::Units::GeV, "pz (Set by options)" }; ///< pz (Set by options)

  /// Pdg Codes of particles to generate (Set by options)
  Gaudi::Property<std::vector<int>> m_pdgCodes{
      this, "PdgCodes", { -211 }, "Pdg Codes of particles to generate (Set by options)" };

  /// Masses of particles to generate
  std::vector<double> m_masses;

  /// Names of particles to generate
  std::vector<std::string> m_names;

  /// Flat random number generator
  Rndm::Numbers m_flatGenerator;
};

#endif // PARTICLEGUNS_FIXEDMOMENTUM_H
