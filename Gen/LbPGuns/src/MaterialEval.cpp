/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MaterialEval.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
// Include files

// local
#include "MaterialEval.h"

// From STL
#include <cmath>

// From Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MaterialEval
// Description:
//   Allows the user to "shoot" Monte Carlo particles and store the result
//   in the Transient Store.
//
// 2007-08-21 : Gloria Corti
//-----------------------------------------------------------------------------

// Declaration of the tool Factory
DECLARE_COMPONENT( MaterialEval )

//=============================================================================
// Initialize Method
//=============================================================================
StatusCode MaterialEval::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  sc                   = m_flatGenerator.initialize( randSvc, Rndm::Flat( 0., 1. ) );
  if ( !sc.isSuccess() ) return Error( "Could not initialize random number generator" );

  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  const LHCb::ParticleProperty* particle = ppSvc->find( LHCb::ParticleID( m_pdgCode.value() ) );
  if ( !particle ) {
    error() << "Information not found for pdgID " << m_pdgCode.value() << endmsg;
    return StatusCode::FAILURE;
  }
  m_mass = particle->mass();

  release( ppSvc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  if ( m_useGrid.value() ) {
    info() << "Generating in a grid of regular steps in ";
    if ( m_etaPhi.value() ) {
      m_dEtastep = ( m_maxEta.value() - m_minEta.value() ) / m_nEtastep.value();
      m_dPhistep = ( m_maxPhi.value() - m_minPhi.value() ) / m_nPhistep.value();
      info() << "eta-phi plane" << endmsg;
      info() << "  with " << m_nEtastep.value() << " steps in " << m_minEta.value() << " <= eta <= " << m_maxEta.value()
             << " and " << m_nPhistep.value() << " steps in " << m_minPhi.value() << " <= phi <= " << m_maxPhi.value()
             << endmsg;
      info() << "  ==> DEta step = " << m_dEtastep << ", Dphi Step = " << m_dPhistep << endmsg;
    } else {
      m_dXstep = ( m_xmax.value() - m_xmin.value() ) / m_nXstep.value();
      m_dYstep = ( m_ymax.value() - m_ymin.value() ) / m_nYstep.value();
      info() << "x-y plane at z = " << m_zplane.value() / Gaudi::Units::m << " m " << endmsg;
      info() << "  with " << m_nXstep.value() << " steps in " << m_xmin.value() / Gaudi::Units::cm
             << " cm <= x <= " << m_xmax.value() / Gaudi::Units::cm << " cm and " << m_nYstep.value() << " steps in "
             << m_ymin.value() / Gaudi::Units::cm << " cm <= y <= " << m_ymax.value() / Gaudi::Units::cm << " cm "
             << endmsg;
      info() << "  ==> Dx step = " << m_dXstep / Gaudi::Units::cm << " cm , Dy Step = " << m_dYstep / Gaudi::Units::cm
             << " cm " << endmsg;
    }
  } else {
    info() << "Generating a uniform distribution in ";
    if ( m_etaPhi.value() ) {
      info() << "eta-phi plane" << endmsg;
      info() << "  with " << m_minEta.value() << " <= eta <= " << m_maxEta.value() << " and " << m_minPhi.value()
             << " <= phi <= " << m_maxPhi.value() << endmsg;
    } else {
      info() << "x-y plane at z = " << m_zplane.value() / Gaudi::Units::m << " m " << endmsg;
      info() << "  with " << m_xmin.value() / Gaudi::Units::cm << " cm <= x <= " << m_xmax.value() / Gaudi::Units::cm
             << " cm and " << m_ymin.value() / Gaudi::Units::cm << " cm <= y <= " << m_ymax.value() / Gaudi::Units::cm
             << " cm " << endmsg;
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// generateParticle method
//=============================================================================
void MaterialEval::generateParticle( Gaudi::LorentzVector& fourMomentum, Gaudi::LorentzVector& origin, int& pdgId ) {
  double     px( 0. ), py( 0. ), pz( 0. );
  StatusCode sc;

  if ( m_useGrid.value() ) {
    if ( m_etaPhi.value() ) {
      sc = generateGridEtaPhi( px, py, pz );
      if ( sc.isFailure() ) Exception( "Error in generating eta-phi grid" );
    } else {
      sc = generateGridXY( px, py, pz );
      if ( sc.isFailure() ) Exception( "Error in generting x-y grid" );
    }
  } else {
    if ( m_etaPhi.value() )
      generateUniformEtaPhi( px, py, pz );
    else
      generateUniformXY( px, py, pz );
  }

  fourMomentum.SetPx( px );
  fourMomentum.SetPy( py );
  fourMomentum.SetPz( pz );
  fourMomentum.SetE( std::sqrt( m_mass * m_mass + fourMomentum.P2() ) );
  pdgId = m_pdgCode.value();
  origin.SetCoordinates( m_xVtx.value(), m_yVtx.value(), m_zVtx.value(), 0. );
}

//=============================================================================
// Generation of a uniformly flat distribution in x-y plane
//=============================================================================
void MaterialEval::generateUniformXY( double& px, double& py, double& pz ) {

  double x = m_flatGenerator() * ( m_xmax.value() - m_xmin.value() ) + m_xmin.value() - m_xVtx.value();
  double y = m_flatGenerator() * ( m_ymax.value() - m_ymin.value() ) + m_ymin.value() - m_yVtx.value();

  double z = m_zplane.value() - m_zVtx.value();
  double r = sqrt( x * x + y * y + z * z );

  px = m_ptotal.value() * x / r;
  py = m_ptotal.value() * y / r;
  pz = m_ptotal.value() * z / r;

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "====> Generate Uniform XY  " << endmsg;
    verbose() << " X = " << x << " mm, Y = " << y << " mm " << endmsg;
  }

  return;
}

//=============================================================================
// Generate 3-momentum for a regular grid in x-y plane
//=============================================================================
StatusCode MaterialEval::generateGridXY( double& px, double& py, double& pz ) {

  double x, y, z, r;

  if ( m_counterY < m_nYstep.value() ) {
    if ( m_counterX < m_nXstep.value() ) {

      x = ( m_xmin.value() + ( m_dXstep / 2 ) ) + ( m_counterX * m_dXstep ) - m_xVtx.value();
      y = ( m_ymin.value() + ( m_dYstep / 2 ) ) + ( m_counterY * m_dYstep ) - m_yVtx.value();
      z = m_zplane.value() - m_zVtx.value();
      r = sqrt( x * x + y * y + z * z );

      px = m_ptotal.value() * x / r;
      py = m_ptotal.value() * y / r;
      pz = m_ptotal.value() * z / r;

      m_counterX++;
    }
    if ( m_counterX == m_nXstep.value() ) {
      m_counterX = 0;
      m_counterY++;
    }
  } else if ( m_counterY >= m_nYstep.value() ) {
    return StatusCode::FAILURE;
  }

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "====> Generate Grig XY  " << endmsg;
    verbose() << " X = " << x << " mm,  Y = " << y << " mm" << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Generate 3-momentum for a uniformly flat distribution in eta-phi plane
//=============================================================================
void MaterialEval::generateUniformEtaPhi( double& px, double& py, double& pz ) {

  double eta, phi, theta;

  eta   = m_flatGenerator() * ( m_maxEta.value() - m_minEta.value() ) + m_minEta.value();
  phi   = m_flatGenerator() * ( m_maxPhi.value() - m_minPhi.value() ) + m_minPhi.value();
  theta = 2. * atan( exp( -eta ) );

  px = m_ptotal.value() * sin( theta ) * cos( phi );
  py = m_ptotal.value() * sin( theta ) * sin( phi );
  pz = m_ptotal.value() * cos( theta );

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "====> GenerateUniform EtaPhi " << endmsg;
    verbose() << " Eta = " << eta << " Phi = " << phi << " rad " << endmsg;
  }

  return;
}

//=============================================================================
// Generate 3-momentum for a regular grid in eta-phi plane
//=============================================================================
StatusCode MaterialEval::generateGridEtaPhi( double& px, double& py, double& pz ) {

  double eta, phi, theta;
  phi = 0.0;
  if ( m_counterEta < m_nEtastep.value() ) {
    if ( m_counterPhi < m_nPhistep.value() ) {

      phi   = ( m_minPhi.value() + ( m_dPhistep / 2 ) ) + ( m_counterPhi * m_dPhistep );
      eta   = ( m_minEta.value() + ( m_dEtastep / 2 ) ) + ( m_counterEta * m_dEtastep );
      theta = 2. * atan( exp( -eta ) );

      px = m_ptotal.value() * sin( theta ) * cos( phi );
      py = m_ptotal.value() * sin( theta ) * sin( phi );
      pz = m_ptotal.value() * cos( theta );

      m_counterPhi++;
    }
    if ( m_counterPhi == m_nPhistep.value() ) {
      m_counterPhi = 0;
      m_counterEta++;
    }
  } else if ( m_counterEta >= m_nEtastep.value() ) {
    return StatusCode::FAILURE;
  }

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "======>Generate Grid Eta Phi" << endmsg;
    verbose() << " Eta = " << eta << " Phi = " << phi << " rad " << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
