/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatPtRapidity.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_MOMENTUMRANGE_H
#define PARTICLEGUNS_MOMENTUMRANGE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
// from ParticleGuns
#include "GaudiKernel/RndmGenerators.h"
#include "LbPGuns/IParticleGunTool.h"

/** @class FlatPtRapidity FlatPtRapidity.h "FlatPtRapidity.h"
 *
 *  Particle gun with given momentum range
 *
 *  @author Dan Johnson
 *  @date   2016-02-19
 */
class FlatPtRapidity : public extends<GaudiTool, IParticleGunTool> {
public:
  /// Constructor
  using extends::extends;

  /// Initialize particle gun parameters
  StatusCode initialize() override;

  /// Generation of particles
  void generateParticle( Gaudi::LorentzVector& momentum, Gaudi::LorentzVector& origin, int& pdgId ) override;

  /// Print counters
  void printCounters() override { ; };

private:
  Gaudi::Property<double> m_minPt{ this, "PtMin", 1.0 * Gaudi::Units::GeV,
                                   "Minimum pT" }; ///< Minimum pT (Set by options)
  Gaudi::Property<double> m_minRapidity{ this, "RapidityMin", 1.8,
                                         "Minimum rapidity" }; ///< Minimum rapidity (Set by options)

  Gaudi::Property<double> m_maxPt{ this, "PtMax", 10.0 * Gaudi::Units::GeV,
                                   "Maximum pT" }; ///< Maximum pT (Set by options)
  Gaudi::Property<double> m_maxRapidity{ this, "RapidityMax", 4.7,
                                         "Maximum rapidity" }; ///< Maximum rapidity (Set by options)

  /// Pdg Codes of particles to generate (Set by options)
  Gaudi::Property<std::vector<int>> m_pdgCodes{ this, "PdgCodes", { -211 }, "Pdg Codes of particles to generate" };

  /// Masses of particles to generate
  std::vector<double> m_masses{};

  /// Names of particles to generate
  std::vector<std::string> m_names{};

  /// Flag if want to sample particle mass and range
  Gaudi::Property<bool>   m_sampleMass{ this, "SampleMass", false, "Flag to sample mass of generated particle" };
  Gaudi::Property<double> m_MassRange_min{ this, "MassRange_min", -1., "Min of Mass Range" };
  Gaudi::Property<double> m_MassRange_max{ this, "MassRange_max", -1., "Max of Mass Range" };

  /// Flat random number generator
  Rndm::Numbers m_flatGenerator;
};

#endif // PARTICLEGUNS_MOMENTUMRANGE_H
