/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GaussianTheta.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
// Include files

// This class
#include "GaussianTheta.h"

// From STL
#include <cmath>

// From Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

//==========================================================================
// Implementation file for class: GaussianTheta
//
// 2008-05-18: Patrick Robbe, rewrite in tool format particle gun algorithm
//==========================================================================

DECLARE_COMPONENT( GaussianTheta )

//==========================================================================
// Initialize Generator
//==========================================================================
StatusCode GaussianTheta::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( !sc.isSuccess() ) return sc;

  // Create the flat and gaussian generators
  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  sc                   = m_flatGenerator.initialize( randSvc, Rndm::Flat( 0., 1. ) );
  if ( !sc.isSuccess() ) return Error( "Cannot initialize flat generator" );

  sc = m_gaussGenerator.initialize( randSvc, Rndm::Gauss( m_meanTheta.value(), m_sigmaTheta.value() ) );
  if ( !sc.isSuccess() ) return Error( "Cannot initialize Gaussian generator" );

  // Get the mass of the particle to be generated
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  const LHCb::ParticleProperty* particle = ppSvc->find( LHCb::ParticleID( m_pdgCode.value() ) );
  m_mass                                 = particle->mass();

  release( ppSvc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  if ( m_maxMom.value() < m_minMom.value() ) return Error( "Invalid options for momentum range" );

  return sc;
}

//===========================================================================
// Generate Particle
//===========================================================================
void GaussianTheta::generateParticle( Gaudi::LorentzVector& fourMomentum, Gaudi::LorentzVector& origin, int& pdgId ) {

  const double theta = m_gaussGenerator();
  const double phi   = m_flatGenerator() * Gaudi::Units::twopi;

  double px, py, pz;

  const double momentum = m_minMom.value() + m_flatGenerator() * ( m_maxMom.value() - m_minMom.value() );

  ///       Transform to x,y,z coordinates
  const double pt = momentum * sin( theta );
  px              = pt * cos( phi );
  py              = pt * sin( phi );
  pz              = momentum * cos( theta );

  // defining x & y rotation matrices
  Gaudi::XYZVector momVect( px, py, pz );
  Gaudi::RotationX rotationx( m_x_axis.value() );

  momVect = rotationx * momVect;

  Gaudi::RotationY rotationy( m_y_axis.value() );

  momVect = rotationy * momVect;

  origin.SetCoordinates( 0., 0., 0., 0. );
  fourMomentum.SetPx( momVect.X() );
  fourMomentum.SetPy( momVect.Y() );
  fourMomentum.SetPz( momVect.Z() );
  fourMomentum.SetE( std::sqrt( m_mass * m_mass + fourMomentum.P2() ) );
  pdgId = m_pdgCode.value();
}
