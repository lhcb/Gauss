/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichPatternGun.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $

// local
#include "RichPatternGun.h"

// Event model classes

#include "HepMC/GenEvent.h"

// Declaration of the Algorithm Factory

DECLARE_COMPONENT( RichPatternGun )

//===========================================================================
// Initialization
//===========================================================================
StatusCode RichPatternGun::initialize() {

  // Initialize the base class
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  info() << "Generate event of type " << m_eventType.value() << endmsg;

  sc = m_particleGenerator.initialize( randSvc(), Rndm::Flat( 0., 1. ) );
  if ( !sc.isSuccess() ) return Error( "Cannot initialize flat generator" );

  return sc;
}

//===========================================================================
// Execute method
//===========================================================================
StatusCode RichPatternGun::execute() {

  debug() << "Processing event type " << m_eventType.value() << endmsg;

  LHCb::HepMCEvents* anhepMCVector = new LHCb::HepMCEvents();
  put( anhepMCVector, m_eventLoc.value() );

  LHCb::GenCollisions* collVector = new LHCb::GenCollisions();
  put( collVector, m_collLoc.value() );

  double weight = 0;

  // Update the GenHeader
  LHCb::GenHeader* genHead = get<LHCb::GenHeader>( m_headerLoc.value() );
  genHead->setEvType( m_eventType.value() );

  for ( m_curx = m_minx; m_curx < m_maxx; m_curx++ ) {
    for ( m_cury = m_miny; m_cury < m_maxy; m_cury++ ) {
      if ( m_curx == m_maxx || m_cury == m_maxy || m_curx == m_minx || m_cury == m_miny || ( m_curx % 24 ) == 0 ||
           ( m_cury % 24 ) == 0 ||
           ( ( ( m_curx % 4 ) == 0 && ( m_curx % 24 ) != 0 ) && ( ( m_cury % 4 ) == 0 && ( m_cury % 24 ) != 0 ) ) ||
           ( ( ( ( m_curx - 2 ) % 4 ) == 0 && ( ( m_curx - 2 ) % 24 ) != 0 && ( ( m_curx - 6 ) % 24 ) ) &&
             ( ( ( m_cury - 2 ) % 4 ) == 0 && ( ( m_cury - 2 ) % 24 ) != 0 && ( ( m_cury - 6 ) % 24 ) ) ) ) {
        weight = ( exp( -( ( m_curx - m_mean.value() ) * ( m_curx - m_mean.value() ) ) /
                        ( 2 * ( m_xsigma.value() ) * ( m_xsigma.value() ) ) ) ) *
                 ( exp( -( ( m_cury - m_mean.value() ) * ( m_cury - m_mean.value() ) ) /
                        ( 2 * ( m_ysigma.value() ) * ( m_ysigma.value() ) ) ) );
        weight = weight * m_peak.value();

        int particles = (int)( weight * m_particleGenerator() );

        for ( int ip = 0; ip < particles; ip++ ) {
          LHCb::HepMCEvent* mcevt = new LHCb::HepMCEvent();
          mcevt->setGeneratorName( name() );
          HepMC::GenEvent* evt = mcevt->pGenEvt();
          // Generate an event
          StatusCode sc = callParticleGun( evt );
          if ( !sc.isSuccess() ) return Error( "Failed to generate event", sc );

          anhepMCVector->insert( mcevt );

          LHCb::GenCollision* coll = new LHCb::GenCollision();
          coll->setIsSignal( false );
          coll->setProcessType( evt->signal_process_id() );
          coll->setEvent( mcevt );
          collVector->insert( coll );

          genHead->addToCollisions( coll );
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=========================================================================
// Generate one particle
//=========================================================================
StatusCode RichPatternGun::callParticleGun( HepMC::GenEvent* evt ) {
  unsigned int iPart;
  for ( iPart = 0; iPart < m_nParticles.value(); ++iPart ) {
    double px = 0.;
    double py = 0.;
    double pz = 4. * Gaudi::Units::eV;

    double            energy = sqrt( px * px + py * py + pz * pz );
    HepMC::FourVector fourMom( px, py, pz, energy );

    const HepMC::FourVector vtx( m_curx + m_xvtx.value(), m_cury + m_yvtx.value(), m_zvtx.value(), 0. );

    HepMC::GenVertex* v1 = new HepMC::GenVertex( vtx );
    evt->add_vertex( v1 );
    v1->add_particle_out( new HepMC::GenParticle( fourMom, 10000022, LHCb::HepMCEvent::StableInProdGen ) );
  }

  evt->set_signal_process_id( m_nParticles.value() );

  return StatusCode::SUCCESS;
}
