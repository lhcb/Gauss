/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MomentumRange.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $

// This class
#include "MomentumRange.h"

// From STL
#include <cmath>

// FromGaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/IRndmGenSvc.h"

//===========================================================================
// Implementation file for class: MomentumRange
//
// 2008-05-18: Patrick Robbe adaptation to tool structure
//===========================================================================

DECLARE_COMPONENT( MomentumRange )

//===========================================================================
// Initialize Particle Gun parameters
//===========================================================================
StatusCode MomentumRange::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( !sc.isSuccess() ) return sc;

  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  sc                   = m_flatGenerator.initialize( randSvc, Rndm::Flat( 0., 1. ) );
  if ( !sc.isSuccess() ) return Error( "Cannot initialize flat generator" );

  // Get the mass of the particle to be generated
  //
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  // check momentum and angles
  if ( ( m_minMom.value() > m_maxMom.value() ) || ( m_minTheta.value() > m_maxTheta.value() ) ||
       ( m_minPhi.value() > m_maxPhi.value() ) )
    return Error( "Incorrect values for momentum, theta or phi!" );

  // setup particle information
  m_masses.clear();

  info() << "Particle type chosen randomly from :";
  PIDs::iterator icode;
  for ( icode = m_pdgCodes.value().begin(); icode != m_pdgCodes.value().end(); ++icode ) {
    const LHCb::ParticleProperty* particle = ppSvc->find( LHCb::ParticleID( *icode ) );
    m_masses.push_back( ( particle->mass() ) );
    m_names.push_back( particle->particle() );
    info() << " " << particle->particle();
  }

  info() << endmsg;

  info() << "Momentum range: " << m_minMom.value() / Gaudi::Units::GeV << " GeV <-> "
         << m_maxMom.value() / Gaudi::Units::GeV << " GeV" << endmsg;
  info() << "Theta range: " << m_minTheta.value() / Gaudi::Units::rad << " rad <-> "
         << m_maxTheta.value() / Gaudi::Units::rad << " rad" << endmsg;
  info() << "Phi range: " << m_minPhi.value() / Gaudi::Units::rad << " rad <-> " << m_maxPhi.value() / Gaudi::Units::rad
         << " rad" << endmsg;

  release( ppSvc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return sc;
}

//===========================================================================
// Generate the particles
//===========================================================================
void MomentumRange::generateParticle( Gaudi::LorentzVector& momentum, Gaudi::LorentzVector& origin, int& pdgId ) {

  origin.SetCoordinates( 0., 0., 0., 0. );
  double px( 0. ), py( 0. ), pz( 0. );

  // Generate values for energy, theta and phi
  double p     = m_minMom.value() + m_flatGenerator() * ( m_maxMom.value() - m_minMom.value() );
  double theta = m_minTheta.value() + m_flatGenerator() * ( m_maxTheta.value() - m_minTheta.value() );
  double phi   = m_minPhi.value() + m_flatGenerator() * ( m_maxPhi.value() - m_minPhi.value() );

  // Transform to x,y,z coordinates
  double pt = p * sin( theta );
  px        = pt * cos( phi );
  py        = pt * sin( phi );
  pz        = p * cos( theta );

  // randomly choose a particle type
  unsigned int currentType = (unsigned int)( m_pdgCodes.value().size() * m_flatGenerator() );
  // protect against funnies
  if ( currentType >= m_pdgCodes.value().size() ) currentType = 0;

  momentum.SetPx( px );
  momentum.SetPy( py );
  momentum.SetPz( pz );
  momentum.SetE( std::sqrt( m_masses[currentType] * m_masses[currentType] + momentum.P2() ) );

  pdgId = m_pdgCodes.value()[currentType];

  debug() << " -> " << m_names[currentType] << endmsg << "   P   = " << momentum << endmsg;
}
