/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GaussianTheta.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_GAUSSIANTHETA_H
#define PARTICLEGUNS_GAUSSIANTHETA_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"

// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

/** @class GaussianTheta GaussianTheta.h "GaussianTheta.h"
 *
 *  This code is used to generate particles with Gaussian theta distribution,
 *  and flat phi distribution, and flat momentum distribution
 *
 *  @author P. Robbe (adaptation to new structure)
 *  @date 2008-06-09
 */
class GaussianTheta : public extends<GaudiTool, IParticleGunTool> {
public:
  /// Constructor
  using extends::extends;

  /// Initialize method
  StatusCode initialize() override;

  /// Generate the particle
  void generateParticle( Gaudi::LorentzVector& fourMomentum, Gaudi::LorentzVector& origin, int& pdgId ) override;

  /// Print counters
  void printCounters() override { ; };

private:
  double                  m_mass;
  Gaudi::Property<int>    m_pdgCode{ this, "PdgCode", 211, "Pdg Code" };
  Gaudi::Property<double> m_minMom{ this, "MomentumMin", 100.0 * Gaudi::Units::GeV, "Min momentum" };
  Gaudi::Property<double> m_maxMom{ this, "MomentumMax", 100.0 * Gaudi::Units::GeV,
                                    "Max momentum" }; ///< Max and min momentum
  Gaudi::Property<double> m_meanTheta{ this, "MeanTheta", 0. * Gaudi::Units::rad,
                                       "Mean value of theta" }; ///< Mean value of theta
  Gaudi::Property<double> m_sigmaTheta{ this, "SigmaTheta", 1. * Gaudi::Units::rad,
                                        "Sigma of Theta Gaussian" }; ///< Sigma of Theta Gaussian
  Gaudi::Property<double> m_x_axis{ this, "XAxis", 0., "XAxis" };
  Gaudi::Property<double> m_y_axis{ this, "YAxis", 0., "YAxis" };

  // Random generators:
  Rndm::Numbers m_flatGenerator;
  Rndm::Numbers m_gaussGenerator;
};

#endif // PARTICLEGUNS_GENERICGUN_H
