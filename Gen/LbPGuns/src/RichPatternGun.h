/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichPatternGun.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $

#ifndef PARTICLEGUNS_RICHPATTERNGUN_H
#define PARTICLEGUNS_RICHPATTERNGUN_H 1

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/RndmGenerators.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Event model classes
#include "Event/GenCollision.h"
#include "Event/GenHeader.h"
#include "Event/HepMCEvent.h"

namespace HepMC {
  class GenEvent;
}

/** @class RichPatternGun RichPatternGun.h
 *
 *  Algorithm to generate RICH optical photon gun
 *
 *  @author
 *  @date   2008-06-09
 */
class RichPatternGun : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;
  /** Initialize method (implemented in ParticleGunBaseAlg)
   *  @return Status Code
   */
  StatusCode initialize() override;

  /** Execute method (implemented in ParticleGunBaseAlg)
   *  @return Status Code
   */
  StatusCode execute() override;

protected:
private:
  /// generate one particle
  StatusCode callParticleGun( HepMC::GenEvent* evt );

  int m_curx{ 0 };
  int m_cury{ 0 };

  int m_minx{ -400 };
  int m_miny{ -300 };
  int m_maxx{ 400 };
  int m_maxy{ 300 };

  /// Event Type
  Gaudi::Property<int> m_eventType{ this, "EventType", 0, "EventType" };

  Gaudi::Property<double> m_mean{ this, "Mean", 0, "Mean" };
  Gaudi::Property<double> m_xsigma{ this, "XSigma", 5, "XSigma" };
  Gaudi::Property<double> m_ysigma{ this, "YSigma", 5, "YSigma" };
  Gaudi::Property<double> m_peak{ this, "Peak", 1, "Peak" };

  Gaudi::Property<std::string> m_eventLoc{ this, "HepMCEvents", LHCb::HepMCEventLocation::Default, "HepMCEvents" };
  Gaudi::Property<std::string> m_headerLoc{ this, "GenHeader", LHCb::GenHeaderLocation::Default, "GenHeader" };
  Gaudi::Property<std::string> m_collLoc{ this, "GenCollisions", LHCb::GenCollisionLocation::Default, "GenCollisions" };

  Gaudi::Property<unsigned int> m_nParticles{ this, "NParticles", 1, "NParticles" };
  Gaudi::Property<double>       m_xvtx{ this, "xPatternCentre", 0. * Gaudi::Units::mm, "xPatternCentre" };
  Gaudi::Property<double>       m_yvtx{ this, "yPatternCentre", 0. * Gaudi::Units::mm, "yPatternCentre" };
  Gaudi::Property<double>       m_zvtx{ this, "zPatternCentre", 5000. * Gaudi::Units::mm, "zPatternCentre" };

  Rndm::Numbers m_particleGenerator;
};

#endif // PARTICLEGUNS_RICHPATTERNGUN_H
