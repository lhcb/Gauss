/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ParticleGun.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_PARTICLEGUN_H
#define PARTICLEGUNS_PARTICLEGUN_H 1

#include "Event/GenCollision.h"
#include "Event/GenHeader.h"
#include "GaudiAlg/GaudiAlgorithm.h"

#include "GenEvent/HepMCUtils.h"

// Forward declarations
class IParticleGunTool;
class IPileUpTool;
class IDecayTool;
class ISampleGenerationTool;
class IVertexSmearingTool;
class IFullGenEventCutTool;
class IGenCutTool;

namespace HepMC {
  class GenEvent;
}

/** @class ParticleGun ParticleGun.h "ParticleGun.h"
 *
 *  Main algorithm to generate particle gun events.
 *
 *  @author Patrick Robbe
 *  @date   2008-05-18
 */
class ParticleGun : public GaudiAlgorithm {
public:
  typedef std::vector<HepMC::GenParticle*> ParticleVector;
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  /** Algorithm initialization.
   *  -# Initializes the common Gaudi random number generator used in all
   *     generators,
   *  -# Retrieve particle gun tool, decay tool, vertex smearing tool and
   *     full event cut tool used in the generation of events.
   */
  StatusCode initialize() override;

  /** Algorithm execution.
   *  Repeat the following sequence until a good set of interactions is
   *  generated.
   *  -#
   */
  StatusCode execute() override;

  /** Algorithm finalization.
   *  Print generation counters.
   */
  StatusCode finalize() override;

protected:
  /// Decay the event with the IDecayTool.
  HepMC::GenParticle* decayEvent( LHCb::HepMCEvent* theEvent, ParticleVector& particleList, StatusCode& sc );

  /// Perpare the particle containers
  void prepareInteraction( LHCb::HepMCEvents* theEvents, LHCb::GenCollisions* theCollisions,
                           HepMC::GenEvent*& theGenEvent, LHCb::GenCollision*& theGenCollision ) const;

private:
  Gaudi::Property<int> m_eventType{ this, "EventType", 50000000, "Event type" }; ///< Event type (set by options)

  /// Location where to store generator events (set by options)
  Gaudi::Property<std::string> m_hepMCEventLocation{ this, "HepMCEventLocation", LHCb::HepMCEventLocation::Default,
                                                     "Location where to store generator events " };

  /// Location where to store the Header of the events (set by options)
  Gaudi::Property<std::string> m_genHeaderLocation{ this, "GenHeaderLocation", LHCb::GenHeaderLocation::Default,
                                                    "Location where to store the Header of the events" };

  /// Location where to store HardInfo (set by options)
  Gaudi::Property<std::string> m_genCollisionLocation{
      this, "GenCollisionLocation", LHCb::GenCollisionLocation::Default, "Location where to store HardInfo" };

  IParticleGunTool* m_particleGunTool{ nullptr }; ///< Particle gun tool

  IPileUpTool* m_numberOfParticlesTool{ nullptr }; ///< Number of particles tool

  IDecayTool* m_decayTool{ nullptr }; ///< Decay tool

  ISampleGenerationTool* m_sampleGenerationTool{ nullptr }; ///< Sample tool

  IVertexSmearingTool* m_vertexSmearingTool{ nullptr }; ///< Smearing tool

  IFullGenEventCutTool* m_fullGenEventCutTool{ nullptr }; ///< Cut tool

  IGenCutTool* m_genCutTool{ nullptr }; ///< Cut tool

  /// Name of the IParticleGunTool (set by options)
  Gaudi::Property<std::string> m_particleGunToolName{ this, "ParticleGunTool", "GenericGun",
                                                      "Name of the IParticleGunTool" };

  /// Name of the tool to set number of particles per event (set by options)
  Gaudi::Property<std::string> m_numberOfParticlesToolName{ this, "NumberOfParticlesTool", "FixedNInteractions",
                                                            "Name of the tool to set number of particles per event" };

  /// Name of the IDecayTool (set by options)
  Gaudi::Property<std::string> m_decayToolName{ this, "DecayTool", "", "Name of the IDecayTool" };

  /// Name of the IVertexSmearingTool (set by options)
  Gaudi::Property<std::string> m_vertexSmearingToolName{ this, "VertexSmearingTool", "",
                                                         "Name of the IVertexSmearingTool" };

  /// Name of the IFullGenEventCutTool (set by options)
  Gaudi::Property<std::string> m_fullGenEventCutToolName{ this, "FullGenEventCutTool", "",
                                                          "Name of the IFullGenEventCutTool" };

  /// Name of the IGenCutTool (set by options)
  Gaudi::Property<std::string> m_genCutToolName{ this, "GenCutTool", "", "Name of the IGenCutTool" };

  /// Name to put in the event
  std::string m_particleGunName;

  unsigned int m_nEvents{ 0 }; ///< Number of generated events

  unsigned int m_nAcceptedEvents{ 0 }; ///< Number of accepted events

  unsigned int m_nParticles{ 0 }; ///< Number of generated particles

  /// Number of particles in accepted events
  unsigned int m_nAcceptedParticles{ 0 };

  /// Counter of events before the full event generator level cut
  unsigned int m_nBeforeFullEvent{ 0 };

  /// Counter of events after the full event generator level cut
  unsigned int m_nAfterFullEvent{ 0 };

  /// Counter of events before the generator level cut
  unsigned int m_nBeforeCut{ 0 };

  /// Counter of events after the generator level cut
  unsigned int m_nAfterCut{ 0 };

  /// PDG id of signal particle.
  Gaudi::Property<int> m_sigPdgCode{ this, "SignalPdgCode", 0, "PDG id of signal particle." };
};
#endif // PARTICLEGUNS_PARTICLEGUN_H
