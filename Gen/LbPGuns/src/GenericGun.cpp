/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GenericGun.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
// Include files

// This class
#include "GenericGun.h"

// From STL
#include <cmath>

// From Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/IRndmGenSvc.h"

//==========================================================================
// Implementation file for class: GenericGun
//
// 2008-05-18: Patrick Robbe, rewrite in tool format particle gun algorithm
//==========================================================================

DECLARE_COMPONENT( GenericGun )

//==========================================================================
// Initialize Generator
//==========================================================================
StatusCode GenericGun::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( !sc.isSuccess() ) return sc;

  // Create the flat and gaussian generators
  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  sc                   = m_flatGenerator.initialize( randSvc, Rndm::Flat( 0., 1. ) );
  if ( !sc.isSuccess() ) return Error( "Cannot initialize flat generator" );

  sc = m_gaussGenerator.initialize( randSvc, Rndm::Gauss( 0., 1. ) );
  if ( !sc.isSuccess() ) return Error( "Cannot initialize Gaussian generator" );

  // Get the mass of the particle to be generated
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  const LHCb::ParticleProperty* particle = ppSvc->find( LHCb::ParticleID( m_pdgCode.value() ) );
  m_mass                                 = particle->mass();

  //
  // Make sure the parameters are in a sensible range...
  //
  if ( SPGGenMode::FixedMode != m_PtGenMode.value() ) {
    if ( ( m_minPt.value() > m_requestedPt.value() ) || ( m_maxPt.value() < m_requestedPt.value() ) ||
         ( m_maxPt.value() < m_minPt.value() ) ) {
      error() << " Pt min and max out of range." << endmsg << " Will set Pt mode to Fixed!!!" << endmsg;
      m_PtGenMode.value() = SPGGenMode::FixedMode;
    }
  }

  if ( SPGGenMode::FixedMode != m_EtaGenMode.value() ) {
    if ( ( m_minEta.value() > m_requestedEta.value() ) || ( m_maxEta.value() < m_requestedEta.value() ) ||
         ( m_maxEta.value() < m_minEta.value() ) ) {
      error() << " Eta min and max out of range." << endmsg << " Will set Eta mode to Fixed!!!" << endmsg;
      m_EtaGenMode.value() = SPGGenMode::FixedMode;
    }
  }

  if ( SPGGenMode::FixedMode != m_PhiGenMode.value() ) {
    if ( ( m_minPhi.value() > m_requestedPhi.value() ) || ( m_maxPhi.value() < m_requestedPhi.value() ) ||
         ( m_maxPhi.value() < m_minPhi.value() ) ) {
      error() << " Phi min and max out of range.  \n"
              << " Will set Phi mode to Fixed!!!" << endmsg;
      m_PhiGenMode.value() = SPGGenMode::FixedMode;
    }
  }

  release( ppSvc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return sc;
}

//===========================================================================
// Generate Particle
//===========================================================================
void GenericGun::generateParticle( Gaudi::LorentzVector& fourMomentum, Gaudi::LorentzVector& origin, int& pdgId ) {
  // Generate values for pt, eta and phi
  //
  double pt =
      generateValue( m_PtGenMode.value(), m_requestedPt.value(), m_sigmaPt.value(), m_minPt.value(), m_maxPt.value() );
  double eta = generateValue( m_EtaGenMode.value(), m_requestedEta.value(), m_sigmaEta.value(), m_minEta.value(),
                              m_maxEta.value() );
  double phi = generateValue( m_PhiGenMode.value(), m_requestedPhi.value(), m_sigmaPhi.value(), m_minPhi.value(),
                              m_maxPhi.value() );

  // Transform to x,y,z coordinates
  //
  double theta = 2. * atan( exp( -eta ) );
  double px    = pt * cos( phi );
  double py    = pt * sin( phi );
  double pz    = pt / tan( theta );

  origin.SetCoordinates( 0., 0., 0., 0. );
  fourMomentum.SetPx( px );
  fourMomentum.SetPy( py );
  fourMomentum.SetPz( pz );
  fourMomentum.SetE( std::sqrt( m_mass * m_mass + fourMomentum.P2() ) );
  pdgId = m_pdgCode.value();
}

//============================================================================
// Generate value
//============================================================================
double GenericGun::generateValue( const int mode, const double val, const double sigma, const double min,
                                  const double max ) {
  double    tmp;
  int       i        = 0;
  const int maxtries = 100;

  switch ( mode ) {
  case SPGGenMode::FixedMode:
    return val;
  case SPGGenMode::GaussMode:
    tmp = max + 1.0;
    i   = 0;
    do {
      tmp = m_gaussGenerator() * sigma + val;
      i++;
    } while ( ( ( tmp < min ) || ( tmp > max ) ) && ( i < maxtries ) );
    if ( i > maxtries ) {
      error() << "Cant generate value in range (min, max) " << val << "\t" << min << "\t" << max << endmsg;
    }
    return tmp;
  case SPGGenMode::FlatMode:
    tmp = m_flatGenerator() * ( max - min ) + min;
    return tmp;
  default:
    error() << "Unknown Generation Mode" << endmsg;
    return 0.;
  }
}
