/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForDFromB.h"

// from Kernel
#include "Kernel/ParticleID.h"

// from Generators
#include "GenEvent/HepMCUtils.h"
#include "MCInterfaces/IDecayTool.h"

// from STL
#include <algorithm>

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDFromB
//
// 2019-03-27 Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDFromB )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DaughtersInLHCbAndCutsForDFromB::DaughtersInLHCbAndCutsForDFromB( const std::string& type, const std::string& name,
                                                                  const IInterface* parent )
    : DaughtersInLHCbAndCutsForD( type, name, parent ), m_fromBcuts( NULL ) {
  declareInterface<IGenCutTool>( this );
}

//=============================================================================
// Destructor
//=============================================================================
DaughtersInLHCbAndCutsForDFromB::~DaughtersInLHCbAndCutsForDFromB() { ; }

//=============================================================================
// initialize
//=============================================================================
StatusCode DaughtersInLHCbAndCutsForDFromB::initialize() {
  const StatusCode sc = DaughtersInLHCb::initialize();
  if ( sc.isFailure() ) return sc;

  // load the tool to check if the signal is ultimately from a b
  m_fromBcuts = tool<IGenCutTool>( "SignalIsFromBDecay", this );

  return sc;
}

//=============================================================================
// Acceptance function
//=============================================================================
bool DaughtersInLHCbAndCutsForDFromB::applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                                                const LHCb::GenCollision* theHardInfo ) const {
  return ( m_fromBcuts->applyCut( theParticleVector, theEvent, theHardInfo ) &&
           DaughtersInLHCbAndCutsForD::applyCut( theParticleVector, theEvent, theHardInfo ) );
}
