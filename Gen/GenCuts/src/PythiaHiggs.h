/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PythiaHiggs.h,v 1.5 2009-10-05 06:18:58 ngueissa Exp $
#ifndef GENCUTS_PYTHIAHIGGS_H
#define GENCUTS_PYTHIAHIGGS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IGenCutTool.h"

/** @class PythiaHiggs PythiaHiggs.h component/PythiaHiggs.h
 *
 *  Tool to select events with 2 b quarks from Higgs in acceptance
 *
 *  @author Patrick Robbe
 *  @date   2005-11-21
 */
class PythiaHiggs : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  bool applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  Gaudi::Property<double> m_thetaMax{ this, "ThetaMax", 400 * Gaudi::Units::mrad, "ThetaMax" };
};
#endif // GENCUTS_PYTHIAHIGGS_H
