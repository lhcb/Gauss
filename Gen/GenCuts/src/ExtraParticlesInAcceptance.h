/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_ExtraParticlesInAcceptance_H
#define GENCUTS_ExtraParticlesInAcceptance_H 1

// Include files
// parent class

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IFullGenEventCutTool.h"

// from HepMC
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"

#include <map>

/** @class ExtraParticlesInAcceptance ExtraParticlesInAcceptance.h
 *  Require a certain number of certain particles in the full event(s).
 *  Particles in the 'Wanted' list must pass pT, eta and vertex-z cuts, and
 *  optionally must come from one of the particles in RequiredAncestors.
 *  Particles in the 'Extra' list have no cuts applied.
 *  e.g. I want exactly 5 pi+/- in the full event in acceptance with at least
 *  one D0 or D+/- somewhere else in the event. I also want the pions to have a
 *  B0 in their ancestors
 *  @code{.py}
 *  WantedIDs = [211, -211]
 *  NumWanted = 5
 *  AtLeast = False
 *  ExcludeSignalDaughters = False
 *  RequiredAncestors = [511, -511]
 *  ExtraIDs = [411, -411, 421, -421]
 *  NumExtra = 1
 *  AtLeastExtra = True
 *  @endcode
 *  @author Adam Morris
 *  @date   2018-04-05
 **/
class ExtraParticlesInAcceptance : public extends<GaudiTool, IFullGenEventCutTool> {
public:
  typedef HepMC::GenParticle*              Particle;
  typedef std::vector<HepMC::GenParticle*> Particles;
  typedef HepMC::GenEvent*                 Event;
  typedef std::set<int>                    PIDs;
  /// Standard constructor
  using extends::extends;
  StatusCode initialize() override; ///< Initialize method
  /** Look at the whole event and check all requirements.
   *  @param theEvents All events in the collision
   *  @param theCollisions Unused
   *  @return true if all conditions are met
   **/
  bool studyFullEvent( LHCb::HepMCEvents* theEvents, LHCb::GenCollisions* theCollisions ) const override;

protected:
  /** Is this particle in the list of wanted PIDs?
   *  @param p Particle in question
   *  @param list List of wanted PIDs
   *  @return true if the particle has a PID in the list of wanted PIDs
   **/
  bool hasPIDInList( const Particle p, const PIDs& list ) const;
  /** Does this particle pass the acceptance cuts?
   *  @param p Particle in question
   *  @return true if the particle passes the theta, pT and production-vertex z cuts
   **/
  bool passesCuts( const Particle p ) const;
  /** Does this particle have an ancestor in the list of required ancestors?
   *  @param p Particle in question
   *  @return true if RequiredAncestors is zero or one of the ancestors is in the list
   **/
  bool hasAncestor( const Particle p ) const;
  /** Does this particle have a specific vertex in its ancestry?
   *  @param p Particle in question
   *  @param signal_vertex The vertex to look for
   *  @return true if the barcode of signal_vertex matches that of any end vertex of any ancestor of p
   **/
  bool hasVertexInAncestors( const Particle p, const HepMC::GenVertex* signal_vertex ) const;
  /** Find the most recent b-hadron ancestor of a given vertex
   * @param vtx Vertex in question
   * @return Vertex of most recent b-hadron ancestor if found, otherwise nullptr
   **/
  HepMC::GenVertex* mostRecentBAncestor( HepMC::GenVertex* vtx ) const;
  /** How many particles have PIDs in a given list pass cuts and meet ancestor requirements?
   *  @param parts Particles in question
   *  @return Number of particles with PIDs in the list that meet requirements
   **/
  int countInAcceptance( const Particles& parts ) const;
  /** Get all particles with PIDs in a set from either every event or just the signal
   *  @param theEvents All events in this bunch crossing
   *  @param list Set of PIDs to return
   *  @return A list of particles to check
   **/
  Particles getAllParticles( const LHCb::HepMCEvents* theEvents, const PIDs& list ) const;
  /** Check an integer against a target using either >= or ==
   *  @param count The number to check
   *  @param target The target to check against
   *  @param atleast If true use >=, else ==
   *  @return true if the requirement is met
   **/
  bool checkCount( int count, int target, bool atleast ) const;

private:
  /// Do we only consider the signal event?
  Gaudi::Property<bool> m_SignalEventOnly{ this, "SignalEventOnly", true,
                                           "Only consider the signal event? Default true" };
  /// Only consider particles outside of the signal decay chain?
  Gaudi::Property<bool> m_ExcludeSignalDaughters{
      this, "ExcludeSignalDaughters", true, "Only consider particles outside of the signal decay chain? Default true" };
  /// List of desired PIDs, subject to acceptance cuts.
  PIDs                              m_WantedIDs;
  Gaudi::Property<std::vector<int>> m_tmp_WantedIDs{
      this, "WantedIDs", {}, "List of desired PIDs, subject to acceptance cuts" };
  /// Required number of particles with a PID in WantedIDs and pass acceptance cuts.
  Gaudi::Property<int> m_NumWanted{
      this, "NumWanted", 0, "Desired number of particles with a PID in WantedIDs and pass acceptance cuts. Default 0" };
  /// At least or exactly NumWanted?
  Gaudi::Property<bool> m_AtLeast{ this, "AtLeast", true, "At least (opposed to exactly) NumWanted? Default true" };
  /// List of PIDs. Each Wanted particle must have one of these in its ancestors.
  PIDs                              m_RequiredAncestors;
  Gaudi::Property<std::vector<int>> m_tmp_RequiredAncestors{
      this,
      "RequiredAncestors",
      {},
      "Optional) List of PIDs. Each Wanted particle must have one of these in its ancestors" };
  /// List of PIDs that must appear somewhere in the event without acceptance cuts.
  PIDs                              m_ExtraIDs;
  Gaudi::Property<std::vector<int>> m_tmp_ExtraIDs{
      this,
      "ExtraIDs",
      {},
      "Optional) List of PIDs that must appear somewhere in the event without acceptance cuts. Default 0" };
  /// Number of extra particles required.
  Gaudi::Property<int> m_NumExtra{ this, "NumExtra", 0, "Number of extra particles required" };
  /// At least or exactly NumExtra?
  Gaudi::Property<bool> m_AtLeastExtra{ this, "AtLeastExtra", true, "At least or exactly NumExtra" };
  /// Minimum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMin{ this, "ChargedThetaMin", 10 * Gaudi::Units::mrad,
                                             "Minimum value of angle around z-axis for charged daughters" };
  /// Maximum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMax{ this, "ChargedThetaMax", 400 * Gaudi::Units::mrad,
                                             "Maximum value of angle around z-axis for charged daughters" };
  /// Minimum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMin{ this, "NeutralThetaMin", 5 * Gaudi::Units::mrad,
                                             "Minimum value of angle around z-axis for neutral daughters" };
  /// Maximum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMax{ this, "NeutralThetaMax", 400 * Gaudi::Units::mrad,
                                             "Maximum value of angle around z-axis for neutral daughters" };
  /// Minimum value of pT to pass acceptance cuts
  Gaudi::Property<double> m_PtMin{ this, "PtMin", 0, "Minimum value of pT to pass acceptance cuts" };
  /// Maximum value of pT to pass acceptance cuts
  Gaudi::Property<double> m_PtMax{ this, "PtMax", 7 * Gaudi::Units::TeV,
                                   "Maximum value of pT to pass acceptance cuts" };
  /// Minimum value of production-vertex z to pass acceptance cuts
  Gaudi::Property<double> m_ZPosMin{ this, "ZPosMin", -500.0 * Gaudi::Units::mm,
                                     "Minimum value of production-vertex z to pass acceptance cuts" };
  /// Maximum value of production-vertex z to pass acceptance cuts
  Gaudi::Property<double> m_ZPosMax{ this, "ZPosMax", 1.0 * Gaudi::Units::km,
                                     "Maximum value of production-vertex z to pass acceptance cuts" };
  /// Do we consider only particles that come from the same most-recent b-hadron as the signal?
  Gaudi::Property<bool> m_AllFromSameB{
      this, "AllFromSameB", false,
      "Only consider particles that come from the most-recent b-hadron ancestor of the signal? Default false" };
  /// Debug function
  void printChildren( HepMC::GenParticle* part, int level = 0, std::set<HepMC::GenVertex*>* done = nullptr,
                      std::map<int, int>* remaining_at_level = nullptr ) const;
};
#endif // GENCUTS_ExtraParticlesInAcceptance_H
