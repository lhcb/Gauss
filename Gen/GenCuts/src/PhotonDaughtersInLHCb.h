/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATORS_PHOTONDAUGHTERSINLHCB_H
#define GENERATORS_PHOTONDAUGHTERSINLHCB_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Transform4DTypes.h"

#include "MCInterfaces/IGenCutTool.h"

// Forward declaration
class IDecayTool;

/** @class PhotonDaughtersInLHCb PhotonDaughtersInLHCb.h
 *
 *  Tool to keep events with photon daughters
 *  in the LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Sean Benson
 *  @date   2014-10-16
 */

class PhotonDaughtersInLHCb : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override; ///< Initialize method

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

  StatusCode finalize() override; ///< Finalize method

protected:
private:
  /// Decay tool
  IDecayTool* m_decayTool{ nullptr };

  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle* theSignal ) const;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{ this, "ChargedThetaMin", 10 * Gaudi::Units::mrad, "ChargedThetaMin" };

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{ this, "ChargedThetaMax", 400 * Gaudi::Units::mrad, "ChargedThetaMax" };

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{ this, "NeutralThetaMin", 5 * Gaudi::Units::mrad, "NeutralThetaMin" };

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{ this, "NeutralThetaMax", 400 * Gaudi::Units::mrad, "NeutralThetaMax" };

  /// Name of the decay tool to use
  Gaudi::Property<std::string> m_decayToolName{ this, "DecayTool", "EvtGenDecay", "DecayTool" };

  Gaudi::Property<int> m_BsPID{ this, "SignalPID", 531,
                                "SignalPID" }; ///< PDG Id of the Bs to consider (set by options)
};
#endif // GENERATORS_PHOTONDAUGHTERSINLHCB_H
