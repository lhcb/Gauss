/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_BEAUTYTOMUCHARMTO3H_H
#define GENCUTS_BEAUTYTOMUCHARMTO3H_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "GaudiKernel/Transform4DTypes.h"
#include "MCInterfaces/IFullGenEventCutTool.h"
#include "MCInterfaces/IGenCutTool.h"

/** @class BeautyTomuCharmTo3h BeautyTomuCharmTo3h
 *
 *  Tool to filter SL decays of H_b->H_c(*)(3h)munu and H_b->H_c(*)(3h)tau(mununu)nu decays
 *  while only enforcing that the 3h from the H_c decay and the mu from the H_b or tau
 *  are in the acceptance.
 *  Allows for the use of kinematic requirements (p/pT) on the muon and hadrons.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Stephen Ogilvy
 *  @date   2017-03-17
 */

class BeautyTomuCharmTo3h : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  bool applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  // Function to check the muon passes the acceptance cuts.
  void passMuonKinematics( const HepMC::GenParticle* theMuon, bool& hasMuon ) const;

  // Function to check the muon passes the acceptance cuts.
  void passMuonFromTauonKinematics( const HepMC::GenParticle* theTauon, bool& hasMuon ) const;

  // Function to check a Ds has all daughters accepted.
  void passCharmTo3hKinematics( const HepMC::GenParticle* theDs, bool& hasHadrons ) const;

  Gaudi::Property<double> m_chargedThetaMin{ this, "ChargedThetaMin", 10 * Gaudi::Units::mrad, "ChargedThetaMin" };
  Gaudi::Property<double> m_chargedThetaMax{ this, "ChargedThetaMax", 400 * Gaudi::Units::mrad, "ChargedThetaMax" };
  Gaudi::Property<double> m_muonptmin{ this, "MuonPtMin", 0.0 * Gaudi::Units::GeV, "MuonPtMin" };
  Gaudi::Property<double> m_muonpmin{ this, "MuonPMin", 0.95 * Gaudi::Units::GeV, "MuonPMin" };
  Gaudi::Property<double> m_hadronptmin{ this, "HadronPtMin", 0.2375 * Gaudi::Units::GeV, "HadronPtMin" };
  Gaudi::Property<double> m_hadronpmin{ this, "HadronPMin", 1.9 * Gaudi::Units::GeV, "HadronPMin" };
};
#endif // GENCUTS_BEAUTYTOMUCHARMTO3H_H
