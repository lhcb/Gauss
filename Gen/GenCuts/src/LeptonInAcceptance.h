/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: LeptonInAcceptance.h,v 1.3 2005-12-31 17:33:12 robbep Exp $
#ifndef GENCUTS_LEPTONINACCEPTANCE_H
#define GENCUTS_LEPTONINACCEPTANCE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IFullGenEventCutTool.h"

/** @class LeptonInAcceptance LeptonInAcceptance.h "LeptonInAcceptance.h"
 *  component/LeptonInAcceptance.h
 *
 *  Cut on events with one lepton in LHCb acceptance + minimum pT.
 *  Concrete implementation of IFullGenEventCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-11-21
 */
class LeptonInAcceptance : public extends<GaudiTool, IFullGenEventCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Apply cut on full event.
   *  Keep events with at least one lepton in angular region around
   *  z axis (forward) and with a minimum pT.
   *  Implements IFullGenEventCutTool::studyFullEvent.
   */
  bool studyFullEvent( LHCb::HepMCEvents* theEvents, LHCb::GenCollisions* theCollisions ) const override;

private:
  ///< Maximum value for theta angle of lepton (set by options)
  Gaudi::Property<double> m_thetaMax{ this, "ThetaMax", 400 * Gaudi::Units::mrad, "ThetaMax" };

  Gaudi::Property<double> m_ptMin{ this, "PtMin", 4. * Gaudi::Units::GeV,
                                   "PtMin" }; ///< Minimum pT of lepton (set by options)
};
#endif // GENCUTS_LEPTONINACCEPTANCE_H
