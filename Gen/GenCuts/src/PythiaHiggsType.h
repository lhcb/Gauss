/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_PythiaHiggsType_H
#define GENCUTS_PythiaHiggsType_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"

// from Generators
#include "MCInterfaces/IGenCutTool.h"

/** @class PythiaHiggsType PythiaHiggsType.h component/PythiaHiggsType.h
 *
 *  Tool to select events with, 1 or 2 b quarks in the acceptance coming
 *  from a given mother, and/or 1 or 2 leptons in the acceptance with given
 *  minimal pt and comming (or not) from
 *  a W or Z boson
 *
 *  @author Victor Coco, Neal Gauvin
 *  @date   2007-01-21
 */
class PythiaHiggsType : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;
  /// initialize function
  StatusCode initialize() override;

  /* Accept events following criteria given in options.
   * Implements IGenCutTool::applyCut
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  /// Return true if the particle is a b quark
  bool Isb( const HepMC::GenParticle* p ) const;

  /// Return true if the particle is a lepton
  bool IsLepton( const HepMC::GenParticle* p ) const;

  /// Max theta angle to define the LHCb acceptance
  Gaudi::Property<double> m_thetaMax{ this, "ThetaMax", 400 * Gaudi::Units::mrad,
                                      "Rough definition of the acceptance" };

  /// Number of leptons required in the acceptance
  Gaudi::Property<int> m_nbLepton{ this, "NumberOfLepton", 1, "Number of leptons requiered to be in the acceptance" };

  /// Type of lepton requiered in the acceptance
  Gaudi::Property<std::vector<std::string>> m_TypeLepton{
      this, "TypeOfLepton", { "e+", "mu+" }, "Type of leptons requiered to be in the acceptance" };

  /// Minimum pT of the lepton
  Gaudi::Property<double> m_ptMin{ this, "LeptonPtMin", 10 * Gaudi::Units::GeV,
                                   "LeptonPtMin is the minimum value the pt of lepton can have" };

  /// If true, the lepton is required to come from a given mother
  Gaudi::Property<bool> m_leptonFromMother{
      this, "LeptonIsFromMother", true,
      "LeptonIsFromMother enabled will requiered that the lepton comes from a specific mother" };

  /// List of mother of the lepton
  Gaudi::Property<std::vector<std::string>> m_motheroflepton{
      this, "MotherOfLepton", { "W+", "Z0" }, "MotherOfLepton is the list of particles from which the lepton comes" };

  /// Minimum mass of the Mother of the lepton
  Gaudi::Property<double> m_MinMass{ this, "MotherOfLeptonMinMass", -1., "MotherOfLeptonMinMass" };

  /// Number of quarks required to be in the acceptance
  Gaudi::Property<int> m_nbbquarks{
      this, "NumberOfbquarks", 1,
      "max 2 b quarks -- 0(only with pzb>0), 1, 2  and -1(deactivated) default=1. BECAREFULL NumberOfbquarks=0 will "
      "search b comming from mother with pz>0 if you don't want b (ex: Z->mumu) put -1." };

  /// PDG id of the mother of the b quarks
  Gaudi::Property<std::string> m_motherofb_id{ this, "MotherOfThebquarks", "H_10",
                                               "MotherOfThebquarks define the mother of the b quarks" };

  /// Substitute of the b quark, if any reqested
  Gaudi::Property<std::string> m_subb{
      this, "SubstitutebBy", "No",
      "You should be able to substitute the b by another particle in this program. For example a mu" };

  int m_motherofb_pid{ 0 };
  int m_b_pid{ 0 };

  LHCb::IParticlePropertySvc* m_ppSvc{ nullptr };
};
#endif // GENCUTS_PythiaHiggsType_H
