/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_PARTICLESINACCEPTANCE_H
#define GENCUTS_PARTICLESINACCEPTANCE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IFullGenEventCutTool.h"

// from HepMC
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"

/** @class ParticlesInAcceptance ParticlesInAcceptance.h component/ParticlesInAcceptance.h
 *
 * FullGenEventCutTool to select events with particles satisfying certain cuts.
 * Usage :
 * Generation.FullGenEventCutTool="ParticlesInAcceptance";
 * Condition to apply :
 * LSPCond = 1 : LSP in acceptance,
 *         = 2 : all daughters in DgtsInAcc in acceptance
 *         = 3 : all daughters in acceptance
 * Daughters required to be in acceptance :
 * DgtsInAcc = {3,4,5,23,24}, D = {}
 * Generation.ParticlesInAcceptance.PartCond = 1 ; // =d
 * Number of desired Part :
 * Generation.ParticlesInAcceptance.NbPart = 1 ; //=d
 * Set if at least NbPart should respect conditions or just NbPart
 * Generation.ParticlesInAcceptance.PartID = { 23, 24, 3, 4, 5 };
 * Availabe cuts :
 * Generation.ParticlesInAcceptance.DistToPVMin (Max) = x*mm ; // d=0,infty
 * Generation.ParticlesInAcceptance.ZPosMin (Max) = y*mm ;// d=-infty,infty
 * Generation.ParticlesInAcceptance.EtaMax = 1.8 ; // =d
 * Generation.ParticlesInAcceptance.EtaMin = 4.9 ; // =d
 *
 *  @author Neal Gauvin (Gueissaz)
 *  @date   2009-August-3
 **/
using namespace Gaudi::Units;
class ParticlesInAcceptance : public extends<GaudiTool, IFullGenEventCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  bool studyFullEvent( LHCb::HepMCEvents* theEvents, LHCb::GenCollisions* theCollisions ) const override;

protected:
private:
  // void ResetParticles( HepMC::GenParticle *,CLHEP::HepLorentzVector &) const;
  bool        IsQuark( HepMC::GenParticle* ) const;
  bool        IsLepton( HepMC::GenParticle* ) const;
  bool        IsPart( HepMC::GenParticle* ) const;
  bool        IsDgts( HepMC::GenParticle* ) const;
  double      Dist( HepMC::GenVertex*, HepMC::GenVertex* ) const;
  std::string Print( HepMC::ThreeVector ) const;
  std::string Print( HepMC::FourVector ) const;

  Gaudi::Property<std::vector<int>> m_PartID{ this, "PartID", { 5 }, "PartID" };
  Gaudi::Property<std::vector<int>> m_Dgts{ this, "DgtsInAcc", {}, "DgtsInAcc" };
  Gaudi::Property<int>              m_NbPart{ this, "NbPart", 1, "NbPart" };
  Gaudi::Property<int>              m_PartCond{ this, "PartCond", 1, "PartCond" };
  Gaudi::Property<bool>             m_AtLeast{ this, "AtLeast", true, "AtLeast" };
  Gaudi::Property<double>           m_EtaMin{ this, "EtaMin", 1.8, "EtaMin" };
  Gaudi::Property<double>           m_EtaMax{ this, "EtaMax", 4.9, "EtaMax" };
  Gaudi::Property<double>           m_DistToPVMin{ this, "DistToPVMin", -1., "DistToPVMin" };
  Gaudi::Property<double>           m_DistToPVMax{ this, "DistToPVMax", -1.0, "DistToPVMax" };
  Gaudi::Property<double>           m_ZPosMin{ this, "ZPosMin", -500.0 * mm, "ZPosMin" };
  Gaudi::Property<double>           m_ZPosMax{ this, "ZPosMax", 1.0 * km, "ZPosMax" };
};
#endif // GENCUTS_PARTICLESINACCEPTANCE_H
