/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SignalIsFromBDecay.cpp,v 1.2 2007-11-26 13:51:23 jonrob Exp $
// Include files

// local
#include "SignalIsFromBDecay.h"

// from Gaudi

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// Kernel
#include "Kernel/ParticleID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SignalIsFromBDecay
//
// 22/11/2007 : Chris Jones
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( SignalIsFromBDecay )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SignalIsFromBDecay::SignalIsFromBDecay( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  // interface
  declareInterface<IGenCutTool>( this );
}

//=============================================================================
// Destructor
//=============================================================================
SignalIsFromBDecay::~SignalIsFromBDecay() {}

//=============================================================================
// Acceptance function
//=============================================================================
bool SignalIsFromBDecay::applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                                   const LHCb::GenCollision* /* theHardInfo */ ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Passed " << theParticleVector.size() << " Particles" << endmsg;

  // Apply from B cuts
  bool fromB = false;
  for ( ParticleVector::iterator it = theParticleVector.begin(); it != theParticleVector.end(); ++it ) {
    if ( msgLevel( MSG::DEBUG ) ) { debug() << " -> Particle PDG Code = " << ( *it )->pdg_id() << endmsg; }
    fromB = isFromB( *it );
    if ( fromB ) break;
  }
  if ( msgLevel( MSG::DEBUG ) ) { debug() << " -> Particle is from B = " << fromB << endmsg; }

  // print the event record in verbose mode
  if ( msgLevel( MSG::VERBOSE ) ) { theEvent->print(); }

  return fromB;
}

//=============================================================================
// method to determine if a given particle came from a b at some point
//=============================================================================
bool SignalIsFromBDecay::isFromB( const HepMC::GenParticle* part, unsigned int tree_level ) const {
  if ( !part ) return false;
  if ( tree_level > 999 ) {
    Warning( "Recursion Limit Reached !" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false;
  }

  // Is this particle itself a b hadron ?
  if ( LHCb::ParticleID( part->pdg_id() ).hasBottom() ) return true;

  // production vertex
  const HepMC::GenVertex* vert = part->production_vertex();
  if ( !vert ) return false;

  // loop over parents
  for ( HepMC::GenVertex::particles_in_const_iterator it = vert->particles_in_const_begin();
        it != vert->particles_in_const_end(); ++it ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << std::string( tree_level, ' ' ) << " -> Particle PDG Code = " << ( *it )->pdg_id() << endmsg;
    }
    if ( isFromB( *it, tree_level + 1 ) ) return true;
  }

  // if get here, not from a b
  return false;
}
