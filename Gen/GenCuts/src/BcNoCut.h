/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BcNoCut.h,v 1.3 2008-09-03 09:04:49 gcorti Exp $
#ifndef GENERATORS_BCNOCUT_H
#define GENERATORS_BCNOCUT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Transform4DTypes.h"

#include "MCInterfaces/IGenCutTool.h"

// Forward declaration
class IDecayTool;

/** @class BcNoCut BcNoCut.h
 *
 *  Tool to generate Bc in full phase space
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Jibo He
 *  @author Patrick Robbe
 *  @date   2013-06-11
 */

class BcNoCut : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override; ///< Initialize method

  /**
   *  Decay Bc
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

  StatusCode finalize() override; ///< Finalize method

protected:
private:
  /// Decay tool
  IDecayTool* m_decayTool{ nullptr };

  /// Name of the decay tool to use
  Gaudi::Property<std::string> m_decayToolName{ this, "DecayTool", "EvtGenDecay", "Name of the decay tool to use" };

  int m_sigBcPID{ 541 }; ///< PDG Id of the B_c
};
#endif // GENERATORS_BCDAUGHTERSINLHCB_H
