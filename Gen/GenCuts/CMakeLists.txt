###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/GenCuts
-----------
#]=======================================================================]

gaudi_add_module(GenCuts
    SOURCES
        src_future/DaughtersInLHCb.cpp
        src_future/DaughtersInLHCbAndCutsForDACP.cpp
        src_future/DaughtersInLHCbAndCutsForDinSLBdecays.cpp
        src_future/DaughtersInLHCbAndCutsForDstar.cpp
        src_future/DaughtersInLHCbAndCutsForDstarFromB.cpp
        src_future/DaughtersInLHCbAndCutsForLambdacD.cpp
        src_future/DaughtersInLHCbAndCutsForLc3pi.cpp
        src_future/DaughtersInLHCbAndFromB.cpp
        src_future/DaughtersInLHCbAndMassCut.cpp
        src_future/DaughtersInLHCbAndWithDaughAndBCuts.cpp
        src_future/DaughtersInLHCbAndWithMinP.cpp
        src_future/GenericFullEventCutTool.cpp
        src_future/GenericGenCutTool.cpp
        src_future/GenericGenCutToolWithDecay.cpp
        # src/BcChargedNumInLHCb.cpp
        # src/BcDaughtersInLHCb.cpp
        # src/BcDaughtersInLHCbAndMassCut.cpp
        # src/BcNoCut.cpp
        # src/BeautyTo2CharmTomu3h.cpp
        # src/BeautyTomuCharmTo3h.cpp
        # src/BiasedBB.cpp
        # src/DMuCascadeInAcc.cpp
        # src/DaughtersInLHCb.cpp
        # src/DaughtersInLHCbAndCutsForD.cpp
        # src/DaughtersInLHCbAndCutsForDACP.cpp
        # src/DaughtersInLHCbAndCutsForDFromB.cpp
        # src/DaughtersInLHCbAndCutsForDinSLBdecays.cpp
        # src/DaughtersInLHCbAndCutsForDstar.cpp
        # src/DaughtersInLHCbAndCutsForDstarFromB.cpp
        # src/DaughtersInLHCbAndCutsForLambdacD.cpp
        # src/DaughtersInLHCbAndCutsForLc3pi.cpp
        # src/DaughtersInLHCbAndFromB.cpp
        # src/DaughtersInLHCbAndMassCut.cpp
        # src/DaughtersInLHCbAndWithDaughAndBCuts.cpp
        # src/DaughtersInLHCbAndWithMinP.cpp
        # src/DaughtersInLHCbKeepOnlySignal.cpp
        # src/DiBosonType.cpp
        # src/DiLeptonInAcceptance.cpp
        # src/ExtraParticlesInAcceptance.cpp
        # src/FullEventHiggsType.cpp
        # src/GenericFullEventCutTool.cpp
        # src/GenericGenCutTool.cpp
        # src/GenericGenCutToolWithDecay.cpp
        # src/InvariantMassQQMCJets.cpp
        # src/JpsiLeptonInAcceptance.cpp
        src_future/LHCbAcceptance.cpp
        # src/LHCbAcceptanceAndFromB.cpp
        # src/LeptonInAcceptance.cpp
        # src/ListOfDaughtersInLHCb.cpp
        # src/ParticlesInAcceptance.cpp
        # src/PhotonDaughtersInLHCb.cpp
        # src/PythiaHiggs.cpp
        # src/PythiaHiggsType.cpp
        # src/PythiaLSP.cpp
        # src/SelectedDaughterInLHCb.cpp
        # src/SemilepCutForADS.cpp
        # src/SignalIsFromBDecay.cpp
        # src/UpsilonDaughtersInLHCb.cpp
        # src/XiccDaughtersInLHCb.cpp
        # src/XiccDaughtersInLHCbAndWithMinPT.cpp
    LINK
        Gaudi::GaudiAlgLib
	    Gaussino::GeneratorsLib
        Gauss::LoKiGenLib
	# FastJet::FastJet
)
