/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/Transform4DTypes.h"
#include "GenInterfaces/IGenCutTool.h"
#include "HepMC3/GenParticle.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

/** @class DaughtersInLHCbAndWithDaughAndBCuts DaughtersInLHCbAndWithDaughAndBCuts.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb AndWithMinP.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Alex Shires
 *  @date   2011-03-02
 */
class DaughtersInLHCbAndWithDaughAndBCuts : public extends<GaudiTool, IGenCutTool> {
public:
  using extends::extends;
  /** Accept events with daughters in LHCb AndWithMinP (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC3::GenParticlePtr theSignal ) const;

  /** Flight Distance Cut function
   *
   */
  bool flightCut( const HepMC3::GenParticlePtr, double ) const;

  /** Momentum Cut function
   *
   */
  bool momentumCut( const HepMC3::GenParticlePtr p, const double pMin, double& sumP ) const;

  /** Transverse Momentum Cut function
   *
   */
  bool transverseMomentumCut( const HepMC3::GenParticlePtr p, const double pTMin, double& sumPt ) const;

  Gaudi::Property<double> m_chargedThetaMin{ this, "ChargedThetaMin", 10 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_chargedThetaMax{ this, "ChargedThetaMax", 400 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_neutralThetaMin{ this, "NeutralThetaMin", 5 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_neutralThetaMax{ this, "NeutralThetaMax", 400 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_llThetaMin{ this, "LongLivedThetaMin", 0 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_llThetaMax{ this, "LongLivedThetaMax", 400 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_minMuonP{ this, "MinMuonP", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minTrackP{ this, "MinTrackP", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minLongLivedP{ this, "MinLongLivedP", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minLongLivedDaughP{ this, "MinLongLivedDaughP", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minSumP{ this, "MinSumP", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minBP{ this, "MinBP", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minMuonPT{ this, "MinMuonPT", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minTrackPT{ this, "MinTrackPT", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minLongLivedPT{ this, "MinLongLivedPT", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minLongLivedDaughPT{ this, "MinLongLivedDaughPT", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minBPT{ this, "MinBPT", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minSumPT{ this, "MinSumPT", 0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_minBFD{ this, "MinBFD", 0 * Gaudi::Units::mm };
};
