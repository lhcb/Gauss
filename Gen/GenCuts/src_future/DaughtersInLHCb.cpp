/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaughtersInLHCb.cpp,v 1.7 2008-07-09 14:33:47 robbep Exp $
// Include files

// local
#include "DaughtersInLHCb.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from Kernel
#include "GaudiKernel/Vector4DTypes.h"
#include "HepMC3/Relatives.h"
#include "Kernel/ParticleID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCb
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCb )

//=============================================================================
// Acceptance function
//=============================================================================
bool DaughtersInLHCb::applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent*,
                                const LHCb::GenCollision* ) const {
  ParticleVector tmp;
  for ( auto p : theParticleVector ) {

    if ( passCuts( p ) ) { tmp.push_back( p ); }
  }
  theParticleVector = tmp;

  return ( !theParticleVector.empty() );
}

//=============================================================================
// Functions to test if all daughters are in acceptance
//=============================================================================
bool DaughtersInLHCb::passCuts( const HepMC3::GenParticlePtr theSignal ) const {
  auto EV = theSignal->end_vertex();
  if ( 0 == EV ) return true;

  typedef std::vector<HepMC3::GenParticlePtr> Particles;
  Particles                                   stables;

  for ( auto desc : HepMC3::Relatives::DESCENDANTS( theSignal ) ) {
    if ( !desc->end_vertex() ) { stables.push_back( desc ); }
  }

  if ( stables.empty() ) Exception( "Signal has no stable daughters !" );

  double angle( 0. );
  double firstpz = stables.front()->momentum().pz();

  for ( auto stable : stables ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Check particle " << stable->pdg_id() << " with angle "
              << stable->momentum().theta() / Gaudi::Units::mrad << " mrad." << endmsg;
    }

    // Remove neutrinos
    if ( ( 12 == abs( stable->pdg_id() ) ) || ( 14 == abs( stable->pdg_id() ) ) || ( 16 == abs( stable->pdg_id() ) ) )
      continue;

    // Don't use daughters of Lambda and KS:
    auto theParent = *std::begin( stable->parents() );
    if ( 3122 == abs( theParent->pdg_id() ) ) continue;
    if ( 310 == theParent->pdg_id() ) continue;

    // Consider only gammas from pi0 and eta
    if ( 22 == stable->pdg_id() ) {
      if ( ( 111 != theParent->pdg_id() ) && ( 221 != theParent->pdg_id() ) ) continue;
    }

    // All particles in same direction
    if ( 0 > ( firstpz * ( stable->momentum().pz() ) ) ) return false;

    angle = stable->momentum().theta();

    LHCb::ParticleID pid( stable->pdg_id() );
    if ( 0 == pid.threeCharge() ) {
      if ( fabs( sin( angle ) ) > fabs( sin( m_neutralThetaMax ) ) ) return false;
      if ( fabs( sin( angle ) ) < fabs( sin( m_neutralThetaMin ) ) ) return false;
    } else {
      if ( fabs( sin( angle ) ) > fabs( sin( m_chargedThetaMax ) ) ) return false;
      if ( fabs( sin( angle ) ) < fabs( sin( m_chargedThetaMin ) ) ) return false;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Event passed !" << endmsg; }

  return true;
}
