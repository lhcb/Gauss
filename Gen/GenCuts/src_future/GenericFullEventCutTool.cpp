/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// =============================================================================
// Include files
// =============================================================================
// STD & STL
// =============================================================================
#include <cmath>
// =============================================================================
// GaudiKernel
// =============================================================================
// =============================================================================
// GenEvent
// =============================================================================
#include "HepMC3/GenEvent.h"
// =============================================================================
// Generators
// =============================================================================
#include "GenInterfaces/IFullGenEventCutTool.h"
// =============================================================================
// LoKi
// =============================================================================
#include "LoKi/FilterTool.h"
#include "LoKi/GenTypes.h"
#include "LoKi/IGenDecay.h"
#include "LoKi/IGenHybridFactory.h"
// =============================================================================
namespace LoKi {
  // ===========================================================================
  /** @class   FullGenEventCut
   *
   *  Simple generic implementation of IFullGenEventCutTool interface
   *  to check if event is "good"
   *
   *  Select event with at leats two J/psi:
   *  @code
   *
   *    tool.Code = " count ( 'J/psi(1S)' == GID )  > 1 "
   *
   *  @endcode
   *
   *  @see IFullGenEventCutTool
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   */
  class FullGenEventCut
      // : public extends1<LoKi::FilterTool,IFullGenEventCutTool>
      : public LoKi::FilterTool,
        public virtual IFullGenEventCutTool {
  public:
    // =========================================================================
    /// Initialization
    StatusCode initialize() override { return LoKi::FilterTool::initialize(); }
    /// Finalization
    StatusCode finalize() override;
    // =========================================================================
    /** Apply the cut on a event.
     *  @param theEvents      Container of all interactions in the event.
     *  @param theCollisions  Container of hard process informations of each
     *                            pile-up interactions of the event.
     *  @return    true  if the full event passes the cut.
     *  @see IFullGEnEventrCutTool
     */
    bool studyFullEvent( const HepMC3::GenEventPtrs& theEvents,
                         const LHCb::GenCollisions& /* theCollisions */ ) const override;
    // =========================================================================
  public:
    // =========================================================================
    /// perform the actual decoding of the functor
    StatusCode decode() override; // perform the actual decoding of the functor
    // =========================================================================
    /** standard constructor
     *  @param type the tool type (???)
     *  @param name the actual instance name
     *  @param parent the tool parent
     */
    FullGenEventCut( const std::string& type,    //    the tool type (???)
                     const std::string& name,    // the tool isntance name
                     const IInterface*  parent ); //        the tool parent

    virtual ~FullGenEventCut();
    // =========================================================================
  private:
    // =========================================================================
    /// the actual code
    LoKi::Types::GCutVal m_cutval; // the actual code
    // =========================================================================
  };
  // ===========================================================================
} //                                                       end of namespace LoKi
// =============================================================================
namespace {
  // ===========================================================================
  typedef LoKi::BasicFunctors<LoKi::GenTypes::GenContainer>::BooleanConstant _BOOLC;
  // ===========================================================================
} // namespace
// =============================================================================
/*  standard constructor
 *  @param type the tool type (???)
 *  @param name the actual instance name
 *  @param parent the tool parent
 */
// =============================================================================
LoKi::FullGenEventCut::FullGenEventCut( const std::string& type,   //    the tool type (???)
                                        const std::string& name,   // the tool isntance name
                                        const IInterface*  parent ) //        the tool parent
                                                                   // : base_class ( type , name , parent )
    : LoKi::FilterTool( type, name, parent )
    //
    , m_cutval( _BOOLC( false ) )
///
{
  //
  declareInterface<IFullGenEventCutTool>( this );
  //
  addToPreambulo( "from LoKiGen.decorators     import *" );
  addToPreambulo( "from LoKiNumbers.decorators import *" );
  addToPreambulo( "from LoKiCore.functions     import *" );
  addToPreambulo( "from LoKiCore.math          import *" );
  //
  StatusCode sc = setProperty( "Factory", "LoKi::Hybrid::GenTool/GenFactory" );
  Assert( sc.isSuccess(), "Unable to reset 'Factory' property", sc );
}
// ==============================================================================
// destructor
// ==============================================================================
LoKi::FullGenEventCut::~FullGenEventCut() {}
// ==============================================================================
// Finalization
// ==============================================================================
StatusCode LoKi::FullGenEventCut::finalize() {
  // reset the functor
  m_cutval = _BOOLC( false );
  // finalize the base class
  return LoKi::FilterTool::finalize();
}
// ============================================================================
// perform the actual decoding of the functor
// ============================================================================
StatusCode LoKi::FullGenEventCut::decode() {
  //
  typedef LoKi::IGenHybridFactory FACTORY;
  //
  m_cutval = _BOOLC( false );
  return i_decode<FACTORY>( m_cutval );
}
// ============================================================================
/*  Apply the cut on a event.
 *  @param theEvents      Container of all interactions in the event.
 *  @param theCollisions  Container of hard process informations of each
 *                            pile-up interactions of the event.
 *  @return    true  if the full event passes the cut.
 *  @see IFullGEnEventrCutTool
 */
// ============================================================================
bool LoKi::FullGenEventCut::studyFullEvent( const HepMC3::GenEventPtrs& theEvents,
                                            const LHCb::GenCollisions& /* theCollisions */ ) const {
  //
  Assert( !updateRequired(), "Update is required!" );
  //
  // copy all particles into one constainer
  LoKi::GenTypes::GenContainer particles;
  //
  for ( auto& evt : theEvents ) {
    particles.insert( particles.end(), std::begin( evt->particles() ), std::end( evt->particles() ) );
  }
  //
  // evaluate the functor:
  const bool result = m_cutval( particles );
  //
  counter( "#accept" ) += result;
  //
  return result;
}
// ============================================================================
// factory
// ============================================================================
DECLARE_COMPONENT( LoKi::FullGenEventCut )
// ============================================================================
// The END
// ============================================================================
