/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "DaughtersInLHCbAndCutsForDinSLBdecays.h"
#include <iostream>

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Transform4DTypes.h"

// from Kernel
#include "GaudiKernel/Vector4DTypes.h"
#include "Kernel/ParticleID.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// from Generators
#include "GenEvent/HepMCUtils.h"
#include "HepMC3/Relatives.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDinSLBdecays
//
// 2013-08-28 : Maurizio Martinelli
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDinSLBdecays )

//=============================================================================
// Cut function
//=============================================================================
bool DaughtersInLHCbAndCutsForDinSLBdecays::applyCut( ParticleVector& theParticleVector,
                                                      const HepMC3::GenEvent* /* theEvent */,
                                                      const LHCb::GenCollision* /* theHardInfo */ ) const {
  ParticleVector::iterator it;

  for ( it = theParticleVector.begin(); it != theParticleVector.end(); ) {
    // Check it is a B+/-
    if ( abs( ( *it )->pdg_id() ) != 521 ) {
      debug() << "Particle ID: " << ( *it )->pdg_id() << endmsg;
      Exception( "The signal is not a B+ or B-" );
    }

    if ( !passCuts( *it ) ) {
      it = theParticleVector.erase( it );
    } else
      ++it;
  }

  return ( !theParticleVector.empty() );
}

//=============================================================================
// Apply cuts Function
//=============================================================================
bool DaughtersInLHCbAndCutsForDinSLBdecays::passCuts( const HepMC3::GenParticlePtr theSignal ) const {

  auto EV = theSignal->end_vertex();
  if ( 0 == EV ) return true;

  typedef std::vector<HepMC3::GenParticlePtr> Particles;
  Particles                                   stables;
  HepMC3::GenParticlePtr                      theMuon{ nullptr };
  HepMC3::GenParticlePtr                      theD0{ nullptr };
  HepMC3::GenParticlePtr                      theNeutrino{ nullptr };
  Particles                                   d0daughters;

  for ( auto desc : HepMC3::Relatives::DESCENDANTS( theSignal ) ) {
    // Fill all daughters
    if ( !desc->end_vertex() ) stables.push_back( desc );

    // The D0
    if ( abs( desc->pdg_id() ) == 421 ) theD0 = desc;

    // The muon
    if ( abs( desc->pdg_id() ) == 13 ) theMuon = desc;

    // The neutrino
    if ( abs( desc->pdg_id() ) == 14 ) theNeutrino = desc;
  }

  // check D0, mu, and nu are found
  if ( !theD0 ) Exception( "No D0 in the signal decay chain !" );

  if ( stables.empty() ) Exception( "Signal has no stable daughters !" );

  if ( !theMuon ) Exception( "No muon in the decay chain !" );

  if ( !theNeutrino ) Exception( "No neutrino in the decay chain !" );
  if ( !( theD0 && theMuon && theNeutrino ) ) return false;

  // daughters of D0
  EV = theD0->end_vertex();
  if ( !EV ) Exception( "The D0 has no daughters" );

  for ( auto desc : HepMC3::Relatives::DESCENDANTS( EV ) ) {
    // Fill all daughters but exclude photons (can be radiative photons)
    if ( !desc->end_vertex() )
      if ( 22 != desc->pdg_id() ) d0daughters.push_back( desc );
  }

  if ( d0daughters.empty() ) Exception( "No D0 daughters in signal chain !" );

  debug() << "New event" << endmsg;

  // ---------------------------------------------------------
  // Check all stable particles from decay chain in Acceptance
  //
  double firstpz = stables.front()->momentum().pz();
  for ( Particles::const_iterator it = stables.begin(); it != stables.end(); ++it )
    if ( !InLHCbAcceptance( *it, firstpz ) ) return false;

  // ---------------------------------------------------------
  // Check (D0mu) invariant mass
  //
  debug() << "M(D0mu) = "
          << ( ( Gaudi::LorentzVector )( theD0->momentum() ) + ( Gaudi::LorentzVector )( theMuon->momentum() ) ).mass()
          << endmsg;
  double mD0mu =
      ( ( Gaudi::LorentzVector )( theD0->momentum() ) + ( Gaudi::LorentzVector )( theMuon->momentum() ) ).mass();
  if ( mD0mu < m_md0muMin.value() ) return false;

  // ---------------------------------------------------------
  // check D0 transverse momentum
  //
  debug() << "D0 pT = " << theD0->momentum().perp() << endmsg;
  if ( theD0->momentum().perp() < m_d0PtMin.value() ) return false;

  // ---------------------------------------------------------
  // check D0 proper time
  //
  if ( m_d0ctauMin.value() > 0. ) {
    // Apply ctau cut
    double ctau = ProperTime( theD0 );
    if ( ctau < m_d0ctauMin.value() ) return false;
  }

  // ---------------------------------------------------------
  // check muon momentum
  //
  debug() << "Muon p = " << theMuon->momentum().p3mod() << endmsg;
  if ( theMuon->momentum().p3mod() < m_muonPMin.value() ) return false;

  // ---------------------------------------------------------
  // check muon transverse momentum
  //
  debug() << "Muon pT = " << theMuon->momentum().perp() << endmsg;
  if ( theMuon->momentum().perp() < m_muonPtMin.value() ) return false;

  // ---------------------------------------------------------
  // check D0 daughters transverse momentum and momentum
  //
  for ( Particles::const_iterator it = d0daughters.begin(); it != d0daughters.end(); ++it ) {
    debug() << "Daughter pT = " << ( *it )->momentum().perp() << " p = " << ( *it )->momentum().p3mod() << endmsg;
    if ( ( *it )->momentum().perp() < m_daughtersPtMin.value() ) return false;
    if ( ( *it )->momentum().p3mod() < m_daughtersPMin.value() ) return false;
  }

  return true;
}

bool DaughtersInLHCbAndCutsForDinSLBdecays::InLHCbAcceptance( const HepMC3::GenParticlePtr stable,
                                                              const double                 firstpz ) const {

  debug() << "Check particle " << stable->pdg_id() << " with angle " << stable->momentum().theta() / Gaudi::Units::mrad
          << " mrad." << endmsg;

  // Don't care about neutrinos
  if ( ( 12 == abs( stable->pdg_id() ) ) || ( 14 == abs( stable->pdg_id() ) ) || ( 16 == abs( stable->pdg_id() ) ) )
    return true;

  // Don't use daughters of Lambda and KS:
  auto theParent = *std::cbegin( stable->production_vertex()->particles_in() );
  if ( 3122 == abs( theParent->pdg_id() ) || 310 == theParent->pdg_id() ) return true;

  // Consider only gammas from pi0 and eta
  if ( 22 == stable->pdg_id() ) {
    if ( ( 111 != theParent->pdg_id() ) && ( 221 != theParent->pdg_id() ) ) return true;
  }

  // All particles in same direction
  if ( 0 > ( firstpz * ( stable->momentum().pz() ) ) ) return false;

  // Check angle
  double           angle = stable->momentum().theta();
  LHCb::ParticleID pid( stable->pdg_id() );
  if ( 0 == pid.threeCharge() ) {
    if ( fabs( sin( angle ) ) > fabs( sin( m_neutralThetaMax.value() ) ) ) return false;
    if ( fabs( sin( angle ) ) < fabs( sin( m_neutralThetaMin.value() ) ) ) return false;
  } else {
    if ( fabs( sin( angle ) ) > fabs( sin( m_chargedThetaMax.value() ) ) ) return false;
    if ( fabs( sin( angle ) ) < fabs( sin( m_chargedThetaMin.value() ) ) ) return false;
  }

  // If ok
  return true;
}

double DaughtersInLHCbAndCutsForDinSLBdecays::ProperTime( const HepMC3::GenParticlePtr resonance ) const {
  Gaudi::LorentzVector pEnd, pBegin;
  pEnd.SetXYZT( resonance->end_vertex()->position().x(), resonance->end_vertex()->position().y(),
                resonance->end_vertex()->position().z(),
                resonance->end_vertex()->position().t() * Gaudi::Units::c_light );

  pBegin.SetXYZT( resonance->production_vertex()->position().x(), resonance->production_vertex()->position().y(),
                  resonance->production_vertex()->position().z(),
                  resonance->production_vertex()->position().t() * Gaudi::Units::c_light );

  Gaudi::LorentzVector disp = pEnd - pBegin;

  // Boost transformation
  Gaudi::LorentzVector fourM( resonance->momentum() );
  ROOT::Math::Boost    theBoost( fourM.BoostToCM() );
  Gaudi::LorentzVector restDisp = theBoost( disp );
  double               ctau     = restDisp.t();

  debug() << "D0 ctau = " << ctau << endmsg;
  return ctau;
}
