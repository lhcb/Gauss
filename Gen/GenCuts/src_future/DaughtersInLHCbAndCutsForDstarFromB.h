/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "DaughtersInLHCbAndCutsForDstar.h"
#include "GaudiKernel/ToolHandle.h"

/** @class DaughtersInLHCbAndCutsForDstarFromB DaughtersInLHCbAndCutsForDstarFromB.h
 *
 *  Tool to select D* particles from a b-hadron with pT cuts
 *
 *  @author Adam Morris
 *  @date   2018-03-30
 */
class DaughtersInLHCbAndCutsForDstarFromB : public extends<DaughtersInLHCbAndCutsForDstar, IGenCutTool> {
public:
  using extends::extends;

  /** Check that the signal D* satisfies the cuts in
   *  SignalIsFromBDecay and DaughtersInLHCbAndCutsForDstar.
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  /// From a b cut tool
  ToolHandle<IGenCutTool> m_fromBcuts{ "SignalIsFromBDecay", this };
};
