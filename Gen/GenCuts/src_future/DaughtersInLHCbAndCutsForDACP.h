/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/Transform4DTypes.h"
#include "GenInterfaces/IGenCutTool.h"
#include "HepMC3/GenParticle.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

/** @class DaughtersInLHCbAndCutsForDACP DaughtersInLHCbAndCutsForDACP.h
 *
 *  Tool to keep events with daughters from signal particles in LHCb
 *  and with cuts on:
 *     (Dst p, D0 pT) plane
 *     (Dst p - D0 p, Dst pT - D0 pT) plane
 *     D0 tau
 *     D0 flight distance from origin vertex
 *     D0 daughter p and pT
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Chris Thomas, based on DaughtersInLHCbAndCutsForDstar
 *  @date   2012-05-14
 */
class DaughtersInLHCbAndCutsForDACP : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Accept events with daughters in LHCb and cuts outlined above
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndCutsForDACP
   */
  bool passCuts( const HepMC3::GenParticlePtr theSignal ) const;

  /** Momentum Cut function
   *
   */
  bool momentumCut( const HepMC3::GenParticlePtr, double ) const;

  Gaudi::Property<double> m_chargedThetaMin{ this, "ChargedThetaMin", 10 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_chargedThetaMax{ this, "ChargedThetaMax", 400 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_neutralThetaMin{ this, "NeutralThetaMin", 5 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_neutralThetaMax{ this, "NeutralThetaMax", 400 * Gaudi::Units::mrad };
  Gaudi::Property<double> m_scaleDst{ this, "ScaleDst", 0.1 };
  Gaudi::Property<double> m_uDst{ this, "uDst", 6000.0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_vDst{ this, "vDst", 60000.0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_costhetaDst{ this, "costhetaDst", 0.798636 };
  Gaudi::Property<double> m_sinthetaDst{ this, "sinthetaDst", 0.601815 };
  Gaudi::Property<double> m_kDst{ this, "kDst", 0.00009 };
  Gaudi::Property<double> m_scaleDiff{ this, "ScaleDiff", 0.1 };
  Gaudi::Property<double> m_uDiff{ this, "uDiff", 400.0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_vDiff{ this, "vDiff", 4500.0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_costhetaDiff{ this, "costhetaDiff", 0.788011 };
  Gaudi::Property<double> m_sinthetaDiff{ this, "sinthetaDiff", 0.615661 };
  Gaudi::Property<double> m_kDiff{ this, "kDiff", 0.0014 };
  Gaudi::Property<double> m_D0_TAU{ this, "D0_TAU", 0.0005 * Gaudi::Units::ns };
  Gaudi::Property<double> m_D0_FD_ORIVX{ this, "D0_FD_ORIVX", 5.0 * Gaudi::Units::mm };
  Gaudi::Property<double> m_D0_daugHiPT{ this, "D0_daug_HiPT", 3000.0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_D0_daugLoPT{ this, "D0_daug_LoPT", 1000.0 * Gaudi::Units::MeV };
  Gaudi::Property<double> m_D0_daugLoP{ this, "D0_daug_LoP", 5000.0 * Gaudi::Units::MeV };
};
