/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "DaughtersInLHCb.h"

/** @class DaughtersInLHCbAndFromB DaughtersInLHCbAndFromB.h
 *
 *  Inherits from DaughtersInLHCb adding the additional requirement that
 *  the signal be from a b decay.
 *
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Chris Jones
 *  @date   22/11/2007
 */

class DaughtersInLHCbAndFromB : public extends<DaughtersInLHCb, IGenCutTool> {

public:
  using extends::extends;

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private: // data
  /// From a b cut tool
  ToolHandle<IGenCutTool> m_fromBcuts{ "SignalIsFromBDecay", this };
};
