/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaughtersInLHCbAndFromB.cpp,v 1.1 2007-11-26 13:44:34 jonrob Exp $
// Include files

// local
#include "DaughtersInLHCbAndFromB.h"

// from Gaudi

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// Kernel
#include "Kernel/ParticleID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndFromB
//
// 22/11/2007 : Chris Jones
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( DaughtersInLHCbAndFromB )

//=============================================================================
// Acceptance function
//=============================================================================
bool DaughtersInLHCbAndFromB::applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                                        const LHCb::GenCollision* theHardInfo ) const {
  return ( m_fromBcuts->applyCut( theParticleVector, theEvent, theHardInfo ) &&
           DaughtersInLHCb::applyCut( theParticleVector, theEvent, theHardInfo ) );
}
