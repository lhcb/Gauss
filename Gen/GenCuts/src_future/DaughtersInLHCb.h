/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "GaudiKernel/Transform4DTypes.h"
#include "GenInterfaces/IGenCutTool.h"
#include "HepMC3/GenParticle.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

/** @class DaughtersInLHCb DaughtersInLHCb.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class DaughtersInLHCb : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;
  typedef std::vector<HepMC3::GenParticlePtr> ParticleVector;

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC3::GenParticlePtr theSignal ) const;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{ this, "ChargedThetaMin", 10 * Gaudi::Units::mrad,
                                             "Minimum value of angle around z-axis for charged daughters" };

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{ this, "ChargedThetaMax", 400 * Gaudi::Units::mrad,
                                             "Maximum value of angle around z-axis for charged daughters" };

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{ this, "NeutralThetaMin", 5 * Gaudi::Units::mrad,
                                             "Minimum value of angle around z-axis for neutral daughters" };

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{ this, "NeutralThetaMax", 400 * Gaudi::Units::mrad,
                                             "Maximum value of angle around z-axis for neutral daughters" };
};
