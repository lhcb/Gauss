/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForLc3pi.h"

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Transform4DTypes.h"

// from Kernel
#include "GaudiKernel/Vector4DTypes.h"
#include "Kernel/ParticleID.h"

// from HepMC
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"

// from Generators
#include "HepMC3/Relatives.h"
#include "HepMCUtils/HepMCUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForLc3pi
//
// 2016-01-05 : Victor Renaudin
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForLc3pi )

//=============================================================================
// AndWithMinP function
//=============================================================================
bool DaughtersInLHCbAndCutsForLc3pi::applyCut( ParticleVector& theParticleVector,
                                               const HepMC3::GenEvent* /* theEvent */,
                                               const LHCb::GenCollision* /* theHardInfo */ ) const {
  ParticleVector::iterator it;

  for ( it = theParticleVector.begin(); it != theParticleVector.end(); ) {
    // Check that the signal is the Lc sig
    if ( abs( ( *it )->pdg_id() ) != 4122 ) Exception( "The signal is not our dear Lc" );

    if ( !passCuts( *it ) ) {
      it = theParticleVector.erase( it );
    } else
      ++it;
  }

  return ( !theParticleVector.empty() );
}

//=============================================================================
// Functions to test if the Lc and 3 pi from the daughters are in acceptance
//=============================================================================
bool DaughtersInLHCbAndCutsForLc3pi::passCuts( const HepMC3::GenParticlePtr theSignal ) const {
  auto EV = theSignal->end_vertex();
  if ( 0 == EV ) return true;

  typedef std::vector<HepMC3::GenParticlePtr> Particles;
  Particles                                   stables;
  Particles                                   Lc_parents;
  Particles                                   lcdaughters;

  // first check on the Lc origin : it should come from a B decay
  int bOK = 0;
  debug() << "Check the Lc ancestor " << endmsg;
  double zB, goodzB;
  zB          = -999.;
  goodzB      = -999.;
  bool firstB = true;

  for ( auto anc : HepMC3::Relatives::ANCESTORS( EV ) ) {

    Lc_parents.push_back( anc );
    // is there a B
    if ( int( abs( ( anc )->pdg_id() ) ) % 10000 > 500 && int( abs( ( anc )->pdg_id() ) ) % 10000 < 600 ) bOK++;
    if ( int( abs( ( anc )->pdg_id() ) ) % 10000 > 5000 && int( abs( ( anc )->pdg_id() ) ) % 10000 < 6000 ) bOK++;

    // will be used to be sure that the B of a relative is the same B as the Lc one
    if ( ( int( abs( ( anc )->pdg_id() ) ) % 10000 > 500 && int( abs( ( anc )->pdg_id() ) ) % 10000 < 600 &&
           firstB == true ) ||
         ( int( abs( ( anc )->pdg_id() ) ) % 10000 > 5000 && int( abs( ( anc )->pdg_id() ) ) % 10000 < 6000 &&
           firstB == true ) ) {
      goodzB = ( anc )->end_vertex()->position().z();
      firstB = false;
    }
  }
  debug() << "start of Lc parent loop " << bOK << endmsg;
  if ( bOK == 0 ) return false;

  for ( Particles::const_iterator it = Lc_parents.begin(); it != Lc_parents.end(); ++it ) {
    debug() << " particle type" << ( *it )->pdg_id() << "vtx " << ( *it )->end_vertex() << endmsg;
    if ( ( *it )->end_vertex() == 0 ) continue;
    debug() << "vtx " << ( *it )->end_vertex()->position().z() << endmsg;
    if ( abs( ( *it )->pdg_id() ) != 511 && abs( ( *it )->pdg_id() ) != 521 && abs( ( *it )->pdg_id() ) != 531 &&
         abs( ( *it )->pdg_id() ) != 5122 )
      continue;
    zB = ( *it )->end_vertex()->position().z();
    break;
  }

  debug() << "N beauty in Lc ancestors " << bOK << " vertex position " << zB << endmsg;

  // ask all Lc daughters in acceptance
  for ( auto desc : HepMC3::Relatives::DESCENDANTS( EV ) ) {
    // Fill all daughters
    if ( 0 == ( desc )->end_vertex() ) stables.push_back( desc );
  }

  if ( stables.empty() ) Exception( "Signal has no stable daughters !" );

  int nKLc  = 0;
  int npiLc = 0;
  int npLc  = 0;

  for ( auto desc : HepMC3::Relatives::DESCENDANTS( EV ) ) {
    // Fill all daughters but exclude photons (can be radiative photons)
    if ( 0 == ( desc )->end_vertex() )
      if ( 22 != ( desc )->pdg_id() ) lcdaughters.push_back( desc );
    if ( 211 == abs( int( ( desc )->pdg_id() ) ) ) npiLc++;
    if ( 321 == abs( int( ( desc )->pdg_id() ) ) ) nKLc++;
    if ( 2212 == abs( int( ( desc )->pdg_id() ) ) ) npLc++;
  }

  if ( lcdaughters.empty() ) Exception( "No Lc daughters in signal chain !" );
  // check that the Lc goes into p K pi
  debug() << "Check Lc daughters nK " << nKLc << " Npi " << npiLc << " Np " << npLc << endmsg;
  if ( nKLc * npiLc * npLc != 1 ) return false;
  double angle( 0. );
  double firstpz = stables.front()->momentum().pz();

  debug() << "New event" << endmsg;

  for ( Particles::const_iterator it = stables.begin(); it != stables.end(); ++it ) {

    debug() << "Check particle " << ( *it )->pdg_id() << " with angle "
            << ( *it )->momentum().theta() / Gaudi::Units::mrad << " mrad." << endmsg;
    // All particles in same direction
    if ( 0 > ( firstpz * ( ( *it )->momentum().pz() ) ) ) return false;

    angle = ( *it )->momentum().theta();

    LHCb::ParticleID pid( ( *it )->pdg_id() );
    // only charged tracks to be considered
    if ( 0 != pid.threeCharge() ) {
      if ( fabs( sin( angle ) ) > fabs( sin( m_chargedThetaMax.value() ) ) ) return false;
      if ( fabs( sin( angle ) ) < fabs( sin( m_chargedThetaMin.value() ) ) ) return false;
    }
  }

  // Now check other cuts
  debug() << "Check other cuts" << endmsg;

  debug() << "Lc pT = " << theSignal->momentum().perp() << endmsg;
  if ( theSignal->momentum().perp() < m_lcptCut.value() ) return false;

  double minpt = 14. * Gaudi::Units::TeV;
  double maxpt = 0.;
  double minp  = 14. * Gaudi::Units::TeV;

  for ( Particles::const_iterator it = lcdaughters.begin(); it != lcdaughters.end(); ++it ) {
    debug() << "Daughter pT = " << ( *it )->momentum().perp() << " p = " << ( *it )->momentum().p3mod() << endmsg;

    if ( ( *it )->momentum().perp() > maxpt ) maxpt = ( *it )->momentum().perp();
    if ( ( *it )->momentum().perp() < minpt ) minpt = ( *it )->momentum().perp();
    if ( ( *it )->momentum().p3mod() < minp ) minp = ( *it )->momentum().p3mod();
  }

  debug() << "Min Pt = " << minpt << " Max Pt = " << maxpt << " Min P = " << minp << endmsg;

  if ( minpt < m_daughtersptminCut.value() ) return false;
  if ( maxpt < m_daughtersptmaxCut.value() ) return false;
  if ( minp < m_daughterspminCut.value() ) return false;
  // now look for 3 pions in the Lc family
  debug() << "Check for 3 pions in addition to Lc " << zB << endmsg;
  int Npions_inacc = 0;

  for ( auto rel : theSignal->parent_event()->particles() ) {
    if ( 0 == ( rel )->end_vertex() ) stables.push_back( rel );
  }

  // now loop on the stable particles
  // we  look at all the children of the B ancestor by requiring z>ZB
  // this will include the 3 Lc daughters so we ask 4 pions in the acceptance
  for ( Particles::const_iterator it = stables.begin(); it != stables.end(); ++it ) {

    double zB_B_parent_stables;
    zB_B_parent_stables = -999.;
    // bool firstB_stables = true;
    auto EV_parent_stables = ( *it )->production_vertex();

    auto parent = std::begin( HepMC3::Relatives::ANCESTORS( EV_parent_stables ) );

    // We want to be sure that the stables come from the same B as the Lc
    while (
        ( ( int( abs( ( *parent )->pdg_id() ) ) % 10000 > 500 && int( abs( ( *parent )->pdg_id() ) ) % 10000 < 600 ) ||
          ( int( abs( ( *parent )->pdg_id() ) ) % 10000 > 5000 &&
            int( abs( ( *parent )->pdg_id() ) ) % 10000 < 6000 ) ) &&
        ( ( *parent )->status() != 4 || ( *parent )->status() != 1 ) ) {
      parent = std::begin( HepMC3::Relatives::ANCESTORS( ( *parent )->production_vertex() ) );
    }

    if ( ( int( abs( ( *parent )->pdg_id() ) ) % 10000 > 500 && int( abs( ( *parent )->pdg_id() ) ) % 10000 < 600 ) ||
         ( int( abs( ( *parent )->pdg_id() ) ) % 10000 > 5000 &&
           int( abs( ( *parent )->pdg_id() ) ) % 10000 < 6000 ) ) {
      zB_B_parent_stables = ( *parent )->end_vertex()->position().z();
    }

    if ( ( *it )->production_vertex() == 0 ) continue;

    debug() << "particle relative" << ( *it )->pdg_id() << " z " << ( *it )->production_vertex()->position().z()
            << endmsg;

    if ( zB_B_parent_stables == goodzB ) continue;

    if ( ( *it )->production_vertex()->position().z() < zB ) continue;

    if ( ( *it )->production_vertex()->position().z() > 200. ) continue;

    if ( 211 != abs( ( *it )->pdg_id() ) ) continue;

    angle = ( *it )->momentum().theta();

    if ( fabs( sin( angle ) ) < fabs( sin( m_chargedThetaMin.value() ) ) ) continue;
    if ( fabs( sin( angle ) ) > fabs( sin( m_chargedThetaMax.value() ) ) ) continue;
    Npions_inacc++;
  }
  debug() << "Number of  pions in the Lc family that passed " << Npions_inacc << endmsg;

  if ( Npions_inacc < 4 ) return false;
  debug() << "Event passed !" << endmsg;

  return true;
}
