/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForDstarFromB.h"

// from Kernel
#include "Kernel/ParticleID.h"

// from Generators
#include "GenEvent/HepMCUtils.h"

// from STL
#include <algorithm>

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDstarFromB
//
// 2018-03-30 Adam Morris
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDstarFromB )

//=============================================================================
// Acceptance function
//=============================================================================
bool DaughtersInLHCbAndCutsForDstarFromB::applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                                                    const LHCb::GenCollision* theHardInfo ) const {
  return ( m_fromBcuts->applyCut( theParticleVector, theEvent, theHardInfo ) &&
           DaughtersInLHCbAndCutsForDstar::applyCut( theParticleVector, theEvent, theHardInfo ) );
}
