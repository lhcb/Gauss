/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// =============================================================================
// Include files
// =============================================================================
// STD & STL
// =============================================================================
#include <cmath>
// =============================================================================
// GaudiKernel
// =============================================================================
#include "GaudiKernel/ToStream.h"
// =============================================================================
// GaudiAlg
// =============================================================================
#include "GaudiAlg/GaudiHistoTool.h"
// =============================================================================
// GaudiUtils
// =============================================================================
#include "GaudiUtils/Aida2ROOT.h"
// =============================================================================
// AIDA
// =============================================================================
#include "AIDA/IHistogram2D.h"
// =============================================================================
// PartProp
// =============================================================================
#include "Kernel/iNode.h"
// =============================================================================
// LHCbMath
// =============================================================================
#include "LHCbMath/LHCbMath.h"
#include "LHCbMath/ValueWithError.h"
// =============================================================================
// LoKi
// =============================================================================
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/Relatives.h"
#include "LoKi/GenDecayChain.h"
#include "LoKi/GenOscillated.h"
#include "LoKi/GenParticles.h"
#include "LoKi/GenTypes.h"
#include "LoKi/IGenHybridFactory.h"
#include "LoKi/Objects.h"
#include "LoKi/ParticleProperties.h"
#include "LoKi/Primitives.h"
#include "LoKi/PrintHepMCDecay.h"
// =============================================================================
// ROOT
// =============================================================================
#include "TAxis.h"
#include "TH2D.h"
// =============================================================================
// Boost
// =============================================================================
#include "boost/algorithm/string/join.hpp"
// =============================================================================
// Local
// =============================================================================
#include "GenericGenCutTool.h"
// =============================================================================
// =============================================================================
// constructor
// =============================================================================
LoKi::GenCutTool::TwoCuts::TwoCuts( const Decays::iNode& c1, const LoKi::GenTypes::GCuts& c2 )
    : first( c1 ), second( c2 ), counter( 0 ) {}
// =============================================================================
// constructor
// =============================================================================
LoKi::GenCutTool::TwoCuts::TwoCuts() : first( s_NODE ), second( s_PRED ), counter( 0 ) {}

// =============================================================================
// update-handler for the property "DecayDescriptor"
// =============================================================================
void LoKi::GenCutTool::updateDescriptor( Gaudi::Details::PropertyBase& /* p */ ) {
  // no action if not yet initialized
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  m_update_decay = true;
}
// =============================================================================
// update-handler for the properties "Cuts","Preambulo","Factory"
// =============================================================================
void LoKi::GenCutTool::updateCuts( Gaudi::Details::PropertyBase& /* p */ ) {
  // no action if not yet initialized
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  m_update_cuts   = true;
  m_update_histos = true;
}
// =============================================================================
// update-handler for the properties "XAxis","YAxis"
// =============================================================================
void LoKi::GenCutTool::updateHistos( Gaudi::Details::PropertyBase& /* p */ ) {
  // no action if not yet initialized
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  m_update_histos = true;
}
// =============================================================================
// decode the decay descriptor
// =============================================================================
StatusCode LoKi::GenCutTool::decodeDescriptor() const {
  //
  m_update_decay = true;
  // get the factory:
  Decays::IGenDecay* factory = tool<Decays::IGenDecay>( "LoKi::GenDecay", this );
  // use the factory:
  Decays::IGenDecay::Tree tree = factory->tree( m_descriptor.value() );
  if ( !tree ) { return Error( "Unable to decode the descriptor '" + m_descriptor.value() + "'" ); }
  //
  m_finder = Decays::IGenDecay::Finder( tree );
  if ( !m_finder ) { return Error( "Unable to create the finder '" + m_descriptor.value() + "'" ); }
  //
  m_update_decay = false;
  //
  release( factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  return StatusCode::SUCCESS;
}
// =============================================================================
// decode cuts
// =============================================================================
StatusCode LoKi::GenCutTool::decodeCuts() const {
  //
  m_update_cuts = true;
  //
  m_criteria.clear();
  //
  // get the factory:
  LoKi::IGenHybridFactory* factory = tool<LoKi::IGenHybridFactory>( m_factory.value(), this );
  // get the factory:
  Decays::IGenDecay* nodes = tool<Decays::IGenDecay>( "LoKi::GenDecay", this );
  //
  // decode cuts :
  for ( const auto& entry : m_cuts.value() ) {
    TwoCuts item;
    // use node-factory
    item.first = nodes->node( entry.first );
    if ( !item.first ) { return Error( "Unable to decode the node '" + entry.first + "'" ); }
    // use the cut-factory:
    StatusCode sc = factory->get( entry.second, item.second, preambulo() );
    if ( sc.isFailure() ) { return Error( "Unable to decode the cut '" + entry.second + "'", sc ); }
    //
    m_criteria.push_back( item );
  }
  //
  m_update_cuts = false;
  //
  release( nodes ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  release( factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  return StatusCode::SUCCESS;
}
// =============================================================================
// decode Histos
// =============================================================================
StatusCode LoKi::GenCutTool::decodeHistos() const {
  m_update_histos = true;
  // reset histos
  if ( 0 != m_histo1 ) {
    m_histo1->reset();
    m_histo1 = 0;
  }
  if ( 0 != m_histo2 ) {
    m_histo2->reset();
    m_histo2 = 0;
  }
  if ( 0 != m_histo3 ) {
    m_histo3->reset();
    m_histo3 = 0;
  }
  // reset functions
  m_x = s_FUNC;
  m_y = s_FUNC;
  //
  // aquire the factory:
  LoKi::IGenHybridFactory* factory = tool<LoKi::IGenHybridFactory>( m_factory.value(), this );
  //
  StatusCode sc = factory->get( m_xaxis.value().title(), m_x, preambulo() );

  if ( sc.isFailure() ) { return Error( "Unable to decode the X-axis '" + m_xaxis.value().title() + "'", sc ); }
  sc = factory->get( m_yaxis.value().title(), m_y, preambulo() );
  if ( sc.isFailure() ) { return Error( "Unable to decode the Y-axis '" + m_xaxis.value().title() + "'", sc ); }

  if ( produceHistos() ) {
    m_histo1 = book2D( "All", m_descriptor.value(),
                       //
                       m_xaxis.value().lowEdge(), m_xaxis.value().highEdge(), m_xaxis.value().bins(),
                       //
                       m_yaxis.value().lowEdge(), m_yaxis.value().highEdge(), m_yaxis.value().bins() );
    //
    m_histo2 = book2D( "Accepted", m_descriptor.value(),
                       //
                       m_xaxis.value().lowEdge(), m_xaxis.value().highEdge(), m_xaxis.value().bins(),
                       //
                       m_yaxis.value().lowEdge(), m_yaxis.value().highEdge(), m_yaxis.value().bins() );
    //
    m_histo3 = book2D( "Efficiency", m_descriptor.value(),
                       //
                       m_xaxis.value().lowEdge(), m_xaxis.value().highEdge(), m_xaxis.value().bins(),
                       //
                       m_yaxis.value().lowEdge(), m_yaxis.value().highEdge(), m_yaxis.value().bins() );
  }
  // ==========================================================================
  m_update_histos = false;
  release( factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  return StatusCode::SUCCESS;
}
// =============================================================================
// intialize the tool
// =============================================================================
StatusCode LoKi::GenCutTool::initialize() {
  // initialize the base
  StatusCode sc = GaudiHistoTool::initialize();
  if ( sc.isFailure() ) { return sc; }
  //
  // decode the decay descriptor
  sc = decodeDescriptor();
  if ( sc.isFailure() ) { return Error( "Unable to decode the descriptor '" + m_descriptor.value() + "'", sc ); }
  //
  // decode the cuts
  sc = decodeCuts();
  if ( sc.isFailure() ) { return Error( "Unable to decode cuts", sc ); }
  //
  // decode the histos
  sc = decodeHistos();
  if ( sc.isFailure() ) { return Error( "Unable to decode histos", sc ); }
  //
  if ( msgLevel( MSG::DEBUG ) && !m_fill_counters.value() ) { m_fill_counters.value() = true; }
  //
  if ( m_fill_counters.value() ) {
    for ( auto& cut : m_criteria ) {
      StatEntity& cnt = counter( "Efficiency for " + cut.first.toString() );
      cut.counter     = &cnt;
    }
  }
  //
  return StatusCode::SUCCESS;
}
// =============================================================================
// finalize the tool
// =============================================================================
StatusCode LoKi::GenCutTool::finalize() {
  /// calculate the efficiency
  getEfficiency().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  /// check errors/problems
  if ( m_fill_counters.value() ) {
    for ( const auto& cut : m_criteria ) {
      if ( !cut.counter ) { continue; }
      if ( 0 == cut.counter->nEntries() ) {
        Error( "Cut for " + cut.first.toString() + " is defined, but not used" ).ignore();
      } else if ( 0 == cut.counter->sum() ) {
        Warning( "No passed events for " + cut.first.toString() + " cut" ).ignore();
      }
    }
  }
  /// finalize the base class
  return GaudiHistoTool::finalize();
}
// =============================================================================
// accept the particle
// =============================================================================
bool LoKi::GenCutTool::accept( const HepMC3::ConstGenParticlePtr& particle ) const {
  /// check the argument
  if ( 0 == particle ) { return false; } // RETURN
  //
  if ( m_filter.value() ) {
    const bool matched = m_finder.tree()( particle );
    counter( "#accept_decay" ) += matched;
    if ( !matched ) {
      Warning( "The decay is not matched", StatusCode::FAILURE, 10 ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "the  decay is not-matched:" << LoKi::Print::printDecay( particle ) << endmsg;
      }
      return false;
    }
  }
  //
  bool                         accepted = true;
  LoKi::GenTypes::GenContainer particles;
  m_finder.findDecay( LoKi::GenTypes::GenContainer( 1, particle ), particles );
  std::reverse( particles.begin(), particles.end() );
  if ( msgLevel( MSG::DEBUG ) ) {
    MsgStream& stream = debug();
    stream << "Selected marked particles (" << particles.size() << ") : ";
    LoKi::GenDecayChain printer{};
    printer.print( particles.begin(), particles.end(), stream, endmsg,
                   LoKi::Objects::_VALID_, // show
                   LoKi::Objects::_NONE_,  // mark
                   " ", 0 )
        << endmsg;
  }
  if ( particles.empty() ) {
    const std::string decay = LoKi::Print::printDecay( particle );
    Error( "No particles are selected from: " + decay, StatusCode::FAILURE, 20 ).ignore();
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "No particles are selected from the decay:";
      LoKi::GenDecayChain printer{};
      printer.print( particle, warning(), endmsg,
                     LoKi::Objects::_VALID_,                         // show
                     LoKi::GenParticles::DecTree{ m_finder.tree() }, // mark
                     "  ", 0 )
          << endmsg;
    }
    return false; // RETURN
  }
  // count them
  counter( "selected marked" ) += particles.size();
  // ===========================================================================
  // loop over the selected particles
  // ===========================================================================
  for ( auto p : particles ) {
    if ( 0 == p ) { continue; } // CONTINUE
    if ( !accepted ) { break; } // BREAK
    //
    const LHCb::ParticleID pid( p->pdg_id() );
    //
    bool found = false;
    for ( const auto& cut : m_criteria ) {
      if ( !accepted ) { break; }            // BREAK
      if ( !cut.first( pid ) ) { continue; } // CONTINUE
      //
      found    = true;
      accepted = cut.second( p );
      if ( m_fill_counters.value() && cut.counter ) {
        StatEntity& cnt = *( cut.counter );
        cnt += accepted;
      }
    }
    if ( !found ) {
      const std::string pname = LoKi::Particles::nameFromPID( pid );
      ++counter( "no cuts found for " + pname );
      Error( "No cuts are specified for selected particle " + pname, StatusCode::FAILURE, 20 ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "No cuts are specified for the selected particle " << pname << " :";
        LoKi::GenDecayChain printer{};
        printer.print( particle, error(), endmsg,
                       LoKi::Objects::_VALID_,                          // show
                       LoKi::TheSame<HepMC3::ConstGenParticlePtr>{ p }, // mark
                       " ", 0 )
            << endmsg;
      }
    }
  }
  //
  // fill the histogram
  //
  if ( 0 != m_histo1 && 0 != m_histo2 ) {
    const double x = m_x( particle );
    const double y = m_y( particle );
    m_histo1->fill( x, y, 1 );
    m_histo2->fill( x, y, accepted );
  }
  //
  return accepted;
}
// ============================================================================
// construct preambulo string
// ============================================================================
std::string LoKi::GenCutTool::preambulo() const { return boost::algorithm::join( m_preambulo.value(), "\n" ); }
// ============================================================================
namespace {
  // ==========================================================================
  /// integer number ?
  inline bool non_integer( const double value ) { return !LHCb::Math::equal_to_double( value, std::floor( value ) ); }
  // ==========================================================================
  /// evaluate the binomial effciency
  inline Gaudi::Math::ValueWithError efficiency( const double n, const double N ) {
    if ( 0 >= N ) { Gaudi::Math::ValueWithError( 0, 1 ); }
    //
    const double n1 = std::max( n, 1.0 );
    const double n2 = std::max( N - n, 1.0 );
    //
    return Gaudi::Math::ValueWithError( n / N, n1 * n2 / ( N * N * N ) );
  }
  // ==========================================================================
} // namespace
// ============================================================================
// calculate the efficiency histo
// ============================================================================
StatusCode LoKi::GenCutTool::getEfficiency() {
  //
  if ( 0 == m_histo1 || 0 == m_histo2 || 0 == m_histo3 ) { return Error( "Unable to calculate the efficiency" ); }
  //
  // reset the efficiency histo
  m_histo3->reset();
  //
  TH2D* h1 = Gaudi::Utils::Aida2ROOT::aida2root( m_histo1 );
  TH2D* h2 = Gaudi::Utils::Aida2ROOT::aida2root( m_histo2 );
  TH2D* h3 = Gaudi::Utils::Aida2ROOT::aida2root( m_histo3 );
  //
  if ( 0 == h1 || 0 == h2 || 0 == h3 ) { return Error( "Unable to access ROOT historgams" ); }
  //
  TAxis* xaxis = h1->GetXaxis();
  TAxis* yaxis = h1->GetYaxis();
  //
  unsigned int bad_bins  = 0;
  unsigned int null_bins = 0;
  //
  for ( int ix = 1; ix <= xaxis->GetNbins(); ++ix ) {
    for ( int iy = 1; iy <= yaxis->GetNbins(); ++iy ) {
      const double N = h1->GetBinContent( ix, iy );
      //
      if ( N < 0 ) // || !non_integer ( N )  )
      {
        Warning( "Can't calculate the efficiency: illegal content N" ).ignore();
        ++bad_bins;
        continue;
      }
      //
      const double n = h2->GetBinContent( ix, iy );
      //
      if ( n < 0 || N < n ) //  || !non_integer ( N )  )
      {
        Warning( "Can't calculate the efficicency: illegal content n" ).ignore();
        ++bad_bins;
        continue;
      }
      //
      if ( 0 == N ) { ++null_bins; }
      //
      Gaudi::Math::ValueWithError eff = efficiency( n, N );
      if ( eff.cov2() < 0 ) {
        Warning( "Can't calculate the binomial efficiency" ).ignore();
        ++bad_bins;
        continue;
      }
      //
      h3->SetBinContent( ix, iy, eff.value() );
      h3->SetBinError( ix, iy, eff.error() );
    }
  }
  //
  if ( 0 < bad_bins || 0 < null_bins ) {
    Warning( "# of bad/null bins for efficiency histogram: " + Gaudi::Utils::toString( bad_bins ) + "/" +
                 Gaudi::Utils::toString( null_bins ),
             StatusCode::FAILURE, 0 )
        .ignore();
  }
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// Accept events with 'good' particles
// ============================================================================
bool LoKi::GenCutTool::applyCut( ParticleVector& particles, const HepMC3::GenEvent* /* theEvent     */,
                                 const LHCb::GenCollision* /* theCollision */ ) const {
  //
  StatEntity& cnt1 = counter( "accept_particles" );
  StatEntity& cnt2 = counter( "accept_events" );
  //
  ParticleVector good;
  good.reserve( particles.size() );
  //
  for ( auto& p : particles ) {
    if ( 0 == p ) { continue; }
    //
    const bool accepted = accept( p );
    if ( accepted ) { good.push_back( p ); }
    cnt1 += accepted;
  }
  //
  particles.clear();
  particles.insert( particles.end(), good.begin(), good.end() );
  //
  const bool accepted = !particles.empty();
  //
  cnt2 += accepted;
  //
  return accepted;
}
// ============================================================================
// factory
// ============================================================================
DECLARE_COMPONENT( LoKi::GenCutTool )
// ============================================================================
// The END
// ============================================================================
