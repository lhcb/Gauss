/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Transform4DTypes.h"

#include "GenInterfaces/IGenCutTool.h"
#include "HepMC3/GenParticle.h"

/** @class LHCbAcceptance LHCbAcceptance.h
 *
 *  Tool to keep events with particles only in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class LHCbAcceptance : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Accept events in LHCb acceptance (defined by angle)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* thGeneEvent,
                 const LHCb::GenCollision* theCollision ) const override;

private:
  /// Maximum value of angle around z-axis (set by options)
  Gaudi::Property<double> m_thetaMax{ this, "ThetaMax", 400 * Gaudi::Units::mrad, "ThetaMax" };
};
