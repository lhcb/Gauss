/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForDstar.h"

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Transform4DTypes.h"

// from Kernel
#include "GaudiKernel/Vector4DTypes.h"
#include "Kernel/ParticleID.h"

// from HepMC
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/Relatives.h"

// from Generators
#include "GenEvent/HepMCUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDstar
//
// 2012-02-07 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDstar )

//=============================================================================
// AndWithMinP function
//=============================================================================
bool DaughtersInLHCbAndCutsForDstar::applyCut( ParticleVector& theParticleVector, const HepMC3::GenEvent* theEvent,
                                               const LHCb::GenCollision* theHardInfo ) const {
  if ( !DaughtersInLHCb::applyCut( theParticleVector, theEvent, theHardInfo ) ) { return false; }
  ParticleVector::iterator it;

  for ( it = theParticleVector.begin(); it != theParticleVector.end(); ) {
    // Check it is a D*+/-
    if ( abs( ( *it )->pdg_id() ) != 413 ) Exception( "The signal is not a D*+ or D*-" );

    if ( !passCuts( *it ) ) {
      it = theParticleVector.erase( it );
    } else
      ++it;
  }

  return ( !theParticleVector.empty() );
}

//=============================================================================
// Functions to test if all daughters are in acceptance
//=============================================================================
bool DaughtersInLHCbAndCutsForDstar::passCuts( const HepMC3::GenParticlePtr theSignal ) const {
  auto EV = theSignal->end_vertex();
  if ( 0 == EV ) return true;

  typedef std::vector<HepMC3::GenParticlePtr> Particles;
  HepMC3::GenParticlePtr                      theSoftPion{ nullptr };
  HepMC3::GenParticlePtr                      theD0{ nullptr };
  HepMC3::GenParticlePtr                      theParent{ nullptr };
  Particles                                   d0daughters;

  for ( auto desc : HepMC3::Relatives::DESCENDANTS( theSignal ) ) {

    // The D0
    if ( abs( desc->pdg_id() ) == 421 ) theD0 = desc;

    // The soft pion
    theParent = *std::cbegin( ( desc->production_vertex()->particles_in() ) );
    if ( ( 413 == abs( theParent->pdg_id() ) ) && ( 211 == abs( desc->pdg_id() ) ) ) theSoftPion = desc;
  }

  if ( 0 == theD0 ) Exception( "No D0 in the signal decay chain !" );

  if ( 0 == theSoftPion ) Exception( "No soft pion in the decay chain !" );

  // daughters of D0
  EV = theD0->end_vertex();
  if ( 0 == EV ) Exception( "The D0 has no daughters" );

  for ( auto desc : HepMC3::Relatives::DESCENDANTS( EV ) ) {
    // Fill all daughters but exclude photons (can be radiative photons)
    if ( 0 == desc->end_vertex() )
      if ( 22 != desc->pdg_id() ) d0daughters.push_back( desc );
  }

  if ( d0daughters.empty() ) Exception( "No D0 daughters in signal chain !" );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "New event" << endmsg;

  // Now check other cuts
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Check other cuts" << endmsg;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "D0 pT = " << theD0->momentum().perp() << endmsg;
  if ( theD0->momentum().perp() < m_d0ptCut.value() ) return false;

  if ( m_d0ctauCut.value() > 0. ) {
    // Apply ctau cut
    const HepMC3::FourVector& D0fourmomentum = theD0->momentum();
    double                    gamma          = D0fourmomentum.e() / D0fourmomentum.m();
    double delta_t = theD0->end_vertex()->position().t() - theD0->production_vertex()->position().t();
    double ctau    = Gaudi::Units::c_light * delta_t / gamma;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "D0 ctau = " << ctau << endmsg;
    if ( ctau < m_d0ctauCut.value() ) return false;
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Soft pion pT = " << theSoftPion->momentum().perp() << endmsg;
  if ( theSoftPion->momentum().perp() < m_softpiptCut.value() ) return false;

  double minpt = 14. * Gaudi::Units::TeV;
  double maxpt = 0.;
  double minp  = 14. * Gaudi::Units::TeV;

  for ( auto& d0daug : d0daughters ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Daughter pT = " << d0daug->momentum().perp() << " p = " << d0daug->momentum().p3mod() << endmsg;
    }
    if ( d0daug->momentum().perp() > maxpt ) maxpt = d0daug->momentum().perp();
    if ( d0daug->momentum().perp() < minpt ) minpt = d0daug->momentum().perp();
    if ( d0daug->momentum().p3mod() < minp ) minp = d0daug->momentum().p3mod();
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Min Pt = " << minpt << " Max Pt = " << maxpt << " Min P = " << minp << endmsg;
  }

  if ( minpt < m_daughtersptminCut.value() ) return false;
  if ( maxpt < m_daughtersptmaxCut.value() ) return false;
  if ( minp < m_daughterspminCut.value() ) return false;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Event passed !" << endmsg;

  return true;
}
