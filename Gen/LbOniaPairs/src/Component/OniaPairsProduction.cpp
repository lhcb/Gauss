/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// ============================================================================
// local LbOniaPairs
#include "LbOniaPairs/OniaPairs.h"

// from GaudiKernel

// from Event
#include "Event/GenCollision.h"

// Generators
// #include "Generators/IBeamTool.h"

// LbPythia
#include "LbPythia/Pythia.h"

// local
#include "OniaPairsProduction.h"

// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
OniaPairsProduction::OniaPairsProduction( const std::string& type, const std::string& name, const IInterface* parent )
    : PythiaProduction( type, name, parent ) {

  declareInterface<IProductionTool>( this );

  // Use raw Pythia, except ...
  m_PyDefComm.clear();
  // PDFs - select CTEQ6l1
  // m_PyDefComm.push_back( "pypars mstp 52 2" ) ;
  // m_PyDefComm.push_back( "pypars mstp 51 10042" ) ;
  // Tune - Perugia 2012 - already with CTEQ6L1
  m_PyDefComm.push_back( "pypars mstp 5 370" );
  // event record - store decay products in main section
  m_PyDefComm.push_back( "pypars mstp 128 2" );
}
// =============================================================================
// initialize the production tool
// =============================================================================
StatusCode OniaPairsProduction::initialize() {

  OniaPairs::SetPar( "ECM", m_ecm.value() );

  OniaPairs::SetPar( "PSI1S1S", m_psi1S1S.value() );
  OniaPairs::SetPar( "PSI1S2S", m_psi1S2S.value() );
  OniaPairs::SetPar( "PSI2S2S", m_psi2S2S.value() );

  OniaPairs::SetPar( "UPS1S1S", m_ups1S1S.value() );
  OniaPairs::SetPar( "UPS1S2S", m_ups1S2S.value() );
  OniaPairs::SetPar( "UPS1S3S", m_ups1S3S.value() );
  OniaPairs::SetPar( "UPS2S2S", m_ups2S2S.value() );
  OniaPairs::SetPar( "UPS2S3S", m_ups2S3S.value() );
  OniaPairs::SetPar( "UPS3S3S", m_ups3S3S.value() );

  OniaPairs::SetPar( "ScfAlpS", m_ScfAlpS.value() );
  OniaPairs::SetPar( "ScfPDF", m_ScfPDF.value() );
  OniaPairs::SetPar( "ScfShwr", m_ScfShwr.value() );

  OniaPairs::SetPar( "MaxWghtMult", m_MaxWghtMult.value() );

  // my user process number
  m_userProcess = 6;

  // set user process in pythia
  m_frame  = "USER";
  m_beam   = "p";
  m_target = "p";
  m_win    = m_ecm.value();

  // Discard default settings from LbPythia
  m_defaultSettings.clear();
  // Do not configure Pythia from behind
  m_commandVector.clear();

  // Configure Pythia from this tool only
  m_defaultSettings = m_PyDefComm;
  m_commandVector   = m_PyCommVec.value();

  StatusCode sc = PythiaProduction::initialize();
  if ( sc.isFailure() ) return sc;

  return StatusCode::SUCCESS;
}
// =============================================================================
// finalize
// =============================================================================
StatusCode OniaPairsProduction::finalize() {

  OniaPairs::PrintCSTable();

  // Finalize Pythia Production
  StatusCode sc = PythiaProduction::finalize();
  if ( sc.isFailure() ) return sc;

  return StatusCode::SUCCESS;
}
// =============================================================================
//   Function called to generate one event with Pythia
// =============================================================================
StatusCode OniaPairsProduction::generateEvent( HepMC::GenEvent* theEvent, LHCb::GenCollision* theCollision ) {

  // Generate event
  StatusCode sc = PythiaProduction::generateEvent( theEvent, theCollision );
  if ( sc.isFailure() ) return sc;

  return StatusCode::SUCCESS;
}
// =============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT( OniaPairsProduction )
// =============================================================================
// The END
// =============================================================================
