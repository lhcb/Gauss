/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local.
#include "BoundProduction.h"

//-----------------------------------------------------------------------------
// Implementation file for class: BoundProduction
//
// 2016-03-18 : Philip Ilten
//-----------------------------------------------------------------------------

// Declare the BoundProduction tool.
// DECLARE_COMPONENT( BoundProduction )  // Removed by Phil...

//=============================================================================
// Initialize the tool.
//=============================================================================
StatusCode BoundProduction::initialize() {

  // Print the initialization banner.
  always() << "============================================================="
           << "=====" << endmsg;
  always() << "Using as production engine " << this->type() << endmsg;
  always() << "============================================================="
           << "=====" << endmsg;

  // Initialize the Gaudi tool.
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { Exception( "Failed to initialize the Gaudi tool." ); }

  // Initialize the production tool.
  if ( !m_prod ) {
    if ( m_prodToolName.value() == this->type() ) {
      Exception( "This tool cannot be used as its own production tool." );
    }
    m_prod = tool<IProductionTool>( m_prodToolName.value(), this );
    if ( !m_prod ) Exception( "Failed to initialize the production tool." );
    if ( ( dynamic_cast<GaudiTool*>( m_prod ) )->hasProperty( "BeamToolName" ) ) {
      ( dynamic_cast<GaudiTool*>( m_prod ) )
          ->setProperty( "BeamToolName", m_beamToolName.value() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    } else {
      Exception( "Production tool has no BeamToolName property." );
    }
  }

  // Initialize the bound process tool.

  if ( sc ) sc = boundInitialize();
  return sc;
}

//=============================================================================
// Initialize the generator.
//=============================================================================
StatusCode BoundProduction::initializeGenerator() {
  if ( !m_prod ) return StatusCode::FAILURE;
  StatusCode sc = m_prod->initializeGenerator();
  if ( sc ) sc = boundInitializeGenerator();
  return sc;
}

//=============================================================================
// Finalize the tool.
//=============================================================================
StatusCode BoundProduction::finalize() {
  if ( m_prod ) release( m_prod ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  StatusCode sc = boundFinalize();
  if ( sc ) sc = GaudiTool::finalize();
  return sc;
}

//=============================================================================
// Generate an event.
//=============================================================================
StatusCode BoundProduction::generateEvent( HepMC::GenEvent* theEvent, LHCb::GenCollision* theCollision ) {
  if ( !m_prod ) return StatusCode::FAILURE;
  ++m_nEvents;
  StatusCode sc = m_prod->generateEvent( theEvent, theCollision );
  if ( sc ) sc = bindStates( theEvent );
  return sc;
}

//=============================================================================
// Set a particle stable.
//=============================================================================
void BoundProduction::setStable( const LHCb::ParticleProperty* thePP ) {
  if ( m_prod ) m_prod->setStable( thePP );
}

//=============================================================================
// Update a particle.
//=============================================================================
void BoundProduction::updateParticleProperties( const LHCb::ParticleProperty* thePP ) {
  if ( m_prod ) m_prod->updateParticleProperties( thePP );
}

//=============================================================================
// Turn on and off fragmentation.
//=============================================================================
void BoundProduction::turnOnFragmentation() {
  if ( m_prod ) m_prod->turnOnFragmentation();
}

void BoundProduction::turnOffFragmentation() {
  if ( m_prod ) m_prod->turnOffFragmentation();
}

//=============================================================================
// Hadronize an event.
//=============================================================================
StatusCode BoundProduction::hadronize( HepMC::GenEvent* theEvent, LHCb::GenCollision* theCollision ) {
  if ( !m_prod ) return StatusCode::FAILURE;
  StatusCode sc = m_prod->hadronize( theEvent, theCollision );
  if ( sc ) sc = bindStates( theEvent );
  return sc;
}

//=============================================================================
// Save the event record.
//=============================================================================
void BoundProduction::savePartonEvent( HepMC::GenEvent* theEvent ) {
  if ( m_prod ) m_prod->savePartonEvent( theEvent );
}

//=============================================================================
// Retrieve the event record.
//=============================================================================
void BoundProduction::retrievePartonEvent( HepMC::GenEvent* theEvent ) {
  if ( m_prod ) m_prod->retrievePartonEvent( theEvent );
}

//=============================================================================
// Print the running conditions.
//=============================================================================
void BoundProduction::printRunningConditions() {
  if ( m_prod ) m_prod->printRunningConditions();
}

//=============================================================================
// Returns whether a particle has special status.
//=============================================================================
bool BoundProduction::isSpecialParticle( const LHCb::ParticleProperty* thePP ) const {
  if ( !m_prod )
    return false;
  else
    return m_prod->isSpecialParticle( thePP );
}

//=============================================================================
// Setup forced fragmentation.
//=============================================================================
StatusCode BoundProduction::setupForcedFragmentation( const int thePdgId ) {
  if ( !m_prod ) return StatusCode::FAILURE;
  return m_prod->setupForcedFragmentation( thePdgId );
}

//=============================================================================
// The bound process methods.
//=============================================================================
StatusCode BoundProduction::boundInitialize() { return StatusCode::SUCCESS; }

StatusCode BoundProduction::boundInitializeGenerator() { return StatusCode::SUCCESS; }

StatusCode BoundProduction::boundFinalize() { return StatusCode::SUCCESS; }

//=============================================================================
// The END.
//=============================================================================
