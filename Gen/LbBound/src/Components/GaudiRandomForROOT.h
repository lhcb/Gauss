/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBROOT_GAUDIRANDOMFORROOT_H
#define LBROOT_GAUDIRANDOMFORROOT_H 1

// Gaudi.
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

// ROOT.
#include "TRandom.h"

/**
 * Replaces the global ROOT random number generator, gRandom, with a
 * wrapped version of the Gaudi random number generator. Only the Rndm
 * and RndmArray methods are implemented, as all other functionality
 * uses these flat random numbers.
 *
 * @class  GaudiRandomForROOT
 * @file   GaudiRandomForROOT.h
 * @author Philip Ilten
 * @date   2018-07-23
 */
class GaudiRandomForROOT : public TRandom {
public:
  /// Constructor.
  GaudiRandomForROOT( IRndmGenSvc* rs, StatusCode& sc );

  /// Destructor.
  ~GaudiRandomForROOT();

  /// Make sure we do not hide functions we do not overload
  using TRandom::Rndm;

  /// Throw a flat random number.
  Double_t Rndm() override;

  /// Throw an array of flat random numbers.
  void RndmArray( Int_t n, Double_t* array ) override;

  /// Throw an array of floats.
  void RndmArray( Int_t n, Float_t* array ) override;

private:
  // Members.
  Rndm::Numbers m_gaudiGenerator; ///< Internal gaudi random generator.
  TRandom*      m_rootGenerator;  ///< Saved gRandom generator.
};

#endif // LBROOT_GAUDIRANDOMFORROOT_H
