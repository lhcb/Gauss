/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local.
#include "GaudiRandomForROOT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GaudiRandomForROOT
//
// 2018-07-23 : Philip Ilten
//-----------------------------------------------------------------------------

//=============================================================================
// Constructor.
//=============================================================================
GaudiRandomForROOT::GaudiRandomForROOT( IRndmGenSvc* rs, StatusCode& sc ) {
  SetName( "GaudiRandomForROOT" );
  sc              = m_gaudiGenerator.initialize( rs, Rndm::Flat( 0, 1 ) );
  m_rootGenerator = gRandom;
  gRandom         = this;
}

//=============================================================================
// Throw a flat random numbers.
//=============================================================================
Double_t GaudiRandomForROOT::Rndm() { return m_gaudiGenerator(); }

void GaudiRandomForROOT::RndmArray( Int_t n, Double_t* array ) {
  for ( Int_t i = 0; i < n; ++i ) array[i] = Rndm();
}

void GaudiRandomForROOT::RndmArray( Int_t n, Float_t* array ) {
  for ( Int_t i = 0; i < n; ++i ) array[i] = (Float_t)Rndm();
}

//=============================================================================
// Destructor.
//=============================================================================
GaudiRandomForROOT::~GaudiRandomForROOT() {
  m_gaudiGenerator.finalize().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  gRandom = m_rootGenerator;
}

//=============================================================================
// The END.
//=============================================================================
