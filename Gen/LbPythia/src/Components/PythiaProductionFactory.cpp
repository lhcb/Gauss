/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "LbPythia/PythiaProduction.h"
#include "LbPythia/ReadLHE.h"

// It must be present otherwise the class cannot be instanciated, since the
// Factory is in the linker library since it can (and is) specialized by
// other production tools based on Pythia
DECLARE_COMPONENT( PythiaProduction )
DECLARE_COMPONENT( LbPythia::ReadLHE )
