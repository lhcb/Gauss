/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Rconst.cpp,v 1.1.1.1 2006-04-24 21:45:50 robbep Exp $
// access BcGen common Rconst
#include "LbBcVegPy/Rconst.h"

// set pointer to zero at start
Rconst::RCONST* Rconst::s_rconst = 0;

// Constructor
Rconst::Rconst() : m_dummy( 0 ), m_realdummy( 0. ) {}

// Destructor
Rconst::~Rconst() {}

// access pi in common
double& Rconst::pi() {
  init(); // check COMMON is initialized
  return s_rconst->pi;
}
