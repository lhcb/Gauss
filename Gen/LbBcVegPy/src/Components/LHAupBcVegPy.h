/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBBCVEGPY_LHAUPBCVEGPY_H
#define LBBCVEGPY_LHAUPBCVEGPY_H 1

// Parton shower packages.
#include "LbHard/HardProduction.h"
#include "Pythia8Plugins/LHAFortran.h"

/**
 * Pythia 8 Les Houches Accord (LHA) user process tool for BcVegPy.
 *
 * Generates events via Pythia with the PYUPEV subroutine and then
 * passes these to Pythia 8 from the HEPEUP and HEPRUP common
 * blocks. Fixes the IDPRUP and ISTUP for the process.
 *
 * @class  LHAupBcVegPy
 * @file   LHAupBcVegPy.h
 * @author Philip Ilten
 * @date   2015-05-01
 */
namespace Pythia8 {
  class LHAupBcVegPy : public LHAupFortran {
  public:
    /// Standard constructor.
    LHAupBcVegPy( HardProduction* hard );

    /// Fill the HEPRUP common block.
    bool fillHepRup() override;

    /**
     * Fill the HEPEUP common block.
     *
     * From the derived LHAupFortran class the default idProcIn for
     * setEvent is 0 to which IDPRUP is then set. However, for BcVegPy
     * IDPRUP must be set to 1001. Additionally, the ISTUP (particle
     * status) is not always correctly set for the incoming gluons,
     * and so this is also changed manually. See arXiv:hep-ph/0109068
     * for the HEPEUP and HEPRUP standards.
     */
    bool fillHepEup() override;

    // Members.
    HardProduction* m_hard; ///< The hard production tool.
  };
} // namespace Pythia8

#endif // LBBCVEGPY_LHAUPBCVEGPY_H
