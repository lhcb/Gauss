###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/LbBcVegPy
-------------
#]=======================================================================]

string(APPEND CMAKE_Fortran_FLAGS " -fsecond-underscore")

gaudi_add_library(LbBcVegPyLib
    SOURCES
        src/Lib/BcVegPy.cpp
        src/Lib/Colflow.cpp
        src/Lib/Coloct.cpp
        src/Lib/Confine.cpp
        src/Lib/Counter.cpp
        src/Lib/Funtrans.cpp
        src/Lib/Genefull.cpp
        src/Lib/Grade.cpp
        src/Lib/Hepeup.cpp
        src/Lib/Intinif.cpp
        src/Lib/Intinip.cpp
        src/Lib/Loggrade.cpp
        src/Lib/Mixevnt.cpp
        src/Lib/Octmatrix.cpp
        src/Lib/Outpdf.cpp
        src/Lib/Qqbar.cpp
        src/Lib/Rconst.cpp
        src/Lib/Subopen.cpp
        src/Lib/Totcross.cpp
        src/Lib/Upcom.cpp
        src/Lib/Usertran.cpp
        src/Lib/Vegasinf.cpp
        src/Lib/Vegcross.cpp
        src/Lib/Wavezero.cpp
        src/Lib/gcolflow.F
        src/Lib/gcoloct.F
        src/Lib/gconfine.F
        src/Lib/gcounter.F
        src/Lib/gfuntrans.F
        src/Lib/ggenefull.F
        src/Lib/ggrade.F
        src/Lib/ghepeup.F
        src/Lib/gintinif.F
        src/Lib/gintinip.F
        src/Lib/gloggrade.F
        src/Lib/gmixevnt.F
        src/Lib/goctmatrix.F
        src/Lib/goutpdf.F
        src/Lib/gqqbar.F
        src/Lib/grconst.F
        src/Lib/gsubopen.F
        src/Lib/gtotcross.F
        src/Lib/gupcom.F
        src/Lib/gusertran.F
        src/Lib/gvegasinf.F
        src/Lib/gvegcross.F
        src/Lib/gwavezero.F
    LINK
        PUBLIC
            Gauss::BcVegPy
            Gauss::LbPythiaLib
)

gaudi_add_module(LbBcVegPy
    SOURCES
        src/Components/BcVegPyProduction.cpp
        src/Components/LHAupBcVegPy.cpp
    LINK
        Gauss::LbHardLib
        Gauss::LbBcVegPyLib
)
