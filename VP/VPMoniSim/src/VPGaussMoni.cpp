/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiKernel/Vector4DTypes.h"

// LHCb
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
// LHCb
#include "Event/MCHit.h"

// from AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

/** @class VPGaussMoni VPGaussMoni.h
 *  Algorithm run in Gauss for monitoring VP MCHits.
 *
 *  @author Victor Coco based on VeloGaussMoni
 *  @date   2009-06-05
 */

class VPGaussMoni : public Gaudi::Functional::Consumer<void( const LHCb::MCHits& ),
                                                       Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
  // public GaudiTupleAlg {
public:
  /// Standard constructor
  VPGaussMoni( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, { KeyValue{ "VeloMCHits", LHCb::MCHitLocation::VP } } ){};

  virtual ~VPGaussMoni() = default; ///< Destructor
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode finalize() override;   ///< Algorithm finalization
  void       operator()( const LHCb::MCHits& ) const override;

private:
  Gaudi::Property<bool>        m_printInfo{ this, "PrintInfo", false };
  Gaudi::Property<bool>        m_detailedMonitor{ this, "DetailedMonitor", true };
  Gaudi::Property<std::string> m_histTopDirName{ this, "HistTopDirName", "VP/" };

  /// Create monitoring plots
  void monitor( const LHCb::MCHits& ) const;
  void bookHistograms();

  mutable std::atomic_ulong m_nMCH{ 0 };
  mutable std::atomic_ulong m_nMCH2{ 0 };
  mutable std::atomic_uint  m_nEvent{ 0 };

  mutable std::mutex m_lazy_lock;

  AIDA::IHistogram1D* m_hist_nMCHits{ nullptr };
  AIDA::IHistogram1D* m_hist_eDepSi{ nullptr };
  AIDA::IHistogram1D* m_hist_TOF{ nullptr };
  AIDA::IHistogram1D* m_hist_dist{ nullptr };

  AIDA::IHistogram2D* m_hist_entryZX{ nullptr };
  AIDA::IHistogram2D* m_hist_entryXY{ nullptr };
  AIDA::IHistogram2D* m_hist_exitZX{ nullptr };
  AIDA::IHistogram2D* m_hist_exitXY{ nullptr };

  AIDA::IHistogram2D* m_hist_vertexXY{ nullptr };
  AIDA::IHistogram2D* m_hist_vertexZX{ nullptr };
  AIDA::IHistogram2D* m_hist_vertexZY{ nullptr };

  AIDA::IHistogram1D* m_hist_eMCPart{ nullptr };
};

DECLARE_COMPONENT( VPGaussMoni )

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPGaussMoni::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;
  setHistoTopDir( m_histTopDirName );
  bookHistograms();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void VPGaussMoni::operator()( const LHCb::MCHits& hits ) const {
  ++m_nEvent;
  std::lock_guard<std::mutex> guard_lock( m_lazy_lock );
  // Get data.
  if ( m_printInfo ) {
    info() << "--------------------------------------------------" << endmsg;
    info() << " ==> Found " << hits.size() << " VP MCHits" << endmsg;
    info() << "--------------------------------------------------" << endmsg;
  }
  monitor( hits );
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VPGaussMoni::finalize() {
  double ave_nMCH  = (double)m_nMCH / m_nEvent;
  double ave_nMCH2 = (double)m_nMCH2 / m_nEvent;
  double err       = sqrt( ( ave_nMCH2 - ( ave_nMCH * ave_nMCH ) ) / m_nEvent );

  info() << "------------------------------------------------------" << endmsg;
  info() << "                - VPGaussMoni table -                 " << endmsg;
  info() << "------------------------------------------------------" << endmsg;
  if ( m_nMCH > 0 ) {
    info() << " Number of MCHits/Event: " << m_nMCH << "+/-" << err << endmsg;
  } else {
    info() << " ==> No MCHits found! " << endmsg;
  }
  info() << "------------------------------------------------------" << endmsg;
  return GaudiAlgorithm::finalize(); // must be called after all other actions
}

//=============================================================================
// Create monitoring plots
//=============================================================================
void VPGaussMoni::monitor( const LHCb::MCHits& hits ) const {
  const double size = hits.size();
  m_nMCH += size;
  m_nMCH2 += size * size;

  m_hist_nMCHits->fill( size );
  if ( !m_detailedMonitor ) return;
  // Loop over all MCHits in the container.
  for ( auto& hit : hits ) {
    if ( m_printInfo ) {
      info() << " ==> Test MCHit:\n"
             << " sensor: " << ( hit->sensDetID() ) << "\nentry: x= " << ( hit->entry() ).x() / Gaudi::Units::mm
             << " mm"
             << "\nentry: y= " << ( hit->entry() ).y() / Gaudi::Units::mm << " mm"
             << "\nentry: z= " << ( hit->entry() ).z() / Gaudi::Units::mm << " mm"
             << "\nexit: x= " << ( hit->exit() ).x() / Gaudi::Units::mm << " mm"
             << "\nexit: y= " << ( hit->exit() ).y() / Gaudi::Units::mm << " mm"
             << "\nexit: z= " << ( hit->exit() ).z() / Gaudi::Units::mm << " mm"
             << "energy: " << ( hit->energy() ) / Gaudi::Units::eV << " eV"
             << "TOF: " << ( hit->time() ) / Gaudi::Units::ns << " ns" << endmsg;
    }
    m_hist_eDepSi->fill( hit->energy() / Gaudi::Units::eV );
    double x = hit->entry().x() / Gaudi::Units::cm;
    double y = hit->entry().y() / Gaudi::Units::cm;
    double z = hit->entry().z() / Gaudi::Units::cm;
    m_hist_entryXY->fill( x, y );
    m_hist_entryZX->fill( z, x );

    x = hit->exit().x() / Gaudi::Units::cm;
    y = hit->exit().y() / Gaudi::Units::cm;
    z = hit->exit().z() / Gaudi::Units::cm;

    m_hist_exitZX->fill( z, x );
    m_hist_exitXY->fill( x, y );
    m_hist_TOF->fill( hit->time() / Gaudi::Units::ns );
    // Get access to the MCParticle which made the hit, and write out 4-mom
    const LHCb::MCParticle* particle = hit->mcParticle();
    if ( !particle ) continue;
    Gaudi::LorentzVector fMom = particle->momentum();
    m_hist_eMCPart->fill( fMom.e() / Gaudi::Units::GeV );
    // Get the production vertex position
    const LHCb::MCVertex* vert = particle->originVertex();
    x                          = vert->position().x() / Gaudi::Units::cm;
    y                          = vert->position().y() / Gaudi::Units::cm;
    z                          = vert->position().z() / Gaudi::Units::cm;
    m_hist_vertexXY->fill( x, y );
    m_hist_vertexZX->fill( z, x );
    m_hist_vertexZY->fill( z, y );

    // Get the distance traversed through silicon for each particle
    double xdist2 = pow( hit->entry().x() - hit->exit().x(), 2 );
    double ydist2 = pow( hit->entry().y() - hit->exit().y(), 2 );
    double zdist2 = pow( hit->entry().z() - hit->exit().z(), 2 );
    double dist   = sqrt( xdist2 + ydist2 + zdist2 ) / Gaudi::Units::cm;
    plot( dist, "SiDist", "Distance traversed through silicon by particle [cm]", 0., .05, 500. );

    if ( m_printInfo ) {
      info() << " ==> MCHit - MCParticle: "
             << "\np_x = " << fMom.px() / Gaudi::Units::GeV << "\np_y = " << fMom.py() / Gaudi::Units::GeV
             << "\np_z = " << fMom.pz() / Gaudi::Units::GeV << endmsg;
    }
  }
}

void VPGaussMoni::bookHistograms() {
  m_hist_nMCHits = book1D( "nMCHits", "Number of hits in Velo per event", 0., 10000., 100 );
  if ( !m_detailedMonitor ) return;
  m_hist_eDepSi  = book( "eDepSi", "Energy deposited in Si [eV]", 0., 300000., 100 );
  m_hist_TOF     = book( "TOF", "Time Of Flight [ns]", 0., 5., 100 );
  m_hist_entryZX = book2D( "entryZX", "Particle entry point in Si [cm] - ZX plane", -30., 80., 1100, -5., 5., 50 );
  m_hist_entryXY = book2D( "entryXY", "Particle entry point in Si [cm] - XY plane", -5., 5., 50, -5., 5., 50 );
  m_hist_exitZX  = book2D( "exitZX", "Particle exit point in Si [cm] - ZX plane", -30., 80., 1100, -5., 5., 50 );
  m_hist_exitXY  = book2D( "exitXY", "Particle exit point in Si [cm] - XY plane", -5., 5., 50, -5., 5., 50 );

  m_hist_eMCPart = book( "eMCPart", "Particle energy [GeV]", 0., 50., 100 );

  m_hist_vertexXY = book2D( "MCVertexPosXY", "Position of production MC Vertex of MCParticles giving hits - XY [cm]",
                            -5., 5., 500, -5., 5., 500 );
  m_hist_vertexZX = book2D( "MCVertexPosZX", "Position of production MC Vertex of MCParticles giving hits - ZX [cm]",
                            -5., 5., 500, -5., 5., 500 );
  m_hist_vertexZY = book2D( "MCVertexPosZY", "Position of production MC Vertex of MCParticles giving hits - ZY [cm]",
                            -5., 5., 500, -5., 5., 500 );

  m_hist_dist = book1D( "SiDist", "Distance traversed through silicon by particle [cm]", 0.015, .03, 200. );
}
