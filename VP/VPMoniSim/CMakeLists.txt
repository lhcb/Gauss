###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
VP/VPMoniSim
------------
#]=======================================================================]

if (USE_DD4HEP)
    set(DetDescLocation Run2Support)
else()
    set(DetDescLocation LHCb)
endif()

gaudi_add_module(VPMoniSim
    SOURCES
        src/VPGaussMoni.cpp
    LINK
        ${DetDescLocation}::VPDetLib
        LHCb::MCEvent
        Gaudi::GaudiAlgLib
)
