/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * LumiHit.cpp
 *
 *  Created on: Oct 3, 2018
 *      Author: vsevolod
 */

#include "LumiHit.h"

#include "G4Circle.hh"
#include "G4Color.hh"
#include "G4Square.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<LumiHit>* aLumiHitAllocator = nullptr;

LumiHit::LumiHit() {
  for ( G4int i = 0; i < LConst::nSecOut; ++i ) { _nPhot[i] = 0; }
}

LumiHit::LumiHit( const LumiHit& right ) {
  for ( G4int i = 0; i < LConst::nSecOut; ++i ) { _nPhot[i] = right._nPhot[i]; }
}

const LumiHit& LumiHit::operator=( const LumiHit& right ) {
  for ( G4int i = 0; i < LConst::nSecOut; ++i ) { _nPhot[i] = right._nPhot[i]; }
  return *this;
}

G4int LumiHit::operator==( const LumiHit& right ) const { return ( this == &right ) ? 1 : 0; }
