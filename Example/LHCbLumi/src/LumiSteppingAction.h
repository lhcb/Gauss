/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * LumiSteppingAction.h
 *
 *  Created on: Oct 2, 2018
 *      Author: vsevolod
 */

#pragma once

#include "G4UserSteppingAction.hh"
#include "globals.hh"

class LumiSteppingAction : public G4UserSteppingAction {
public:
  LumiSteppingAction()          = default;
  virtual ~LumiSteppingAction() = default;
  void UserSteppingAction( const G4Step* ) override;

private:
  void InternalReflectionProbability( G4double energy, G4double& probability );
};
