/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * LumiHit.h
 *
 *  Created on: Oct 3, 2018
 *      Author: vsevolod
 */

#pragma once

#include "G4VHit.hh"
// G4
#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "LConst.hh"

// my
#include "HitDataStructure.hh"

class LumiHit : public G4VHit {
public:
  LumiHit();

  LumiHit( const LumiHit& );
  const LumiHit& operator=( const LumiHit& );
  G4int          operator==( const LumiHit& ) const;

  inline void* operator new( size_t );
  inline void  operator delete( void* );
  inline void  InsertPhoton( G4int secID ) { _nPhot[secID - 1]++; }
  inline G4int GetNPhotons( G4int secID ) const { return _nPhot[secID - 1]; }

private:
  G4int _nPhot[LConst::nSecOut];
};

typedef G4THitsCollection<LumiHit> LumiHitsCollection;

extern G4ThreadLocal G4Allocator<LumiHit>* aLumiHitAllocator;

inline void* LumiHit::operator new( size_t ) {
  if ( !aLumiHitAllocator ) { aLumiHitAllocator = new G4Allocator<LumiHit>; }
  return (void*)aLumiHitAllocator->MallocSingle();
}

inline void LumiHit::operator delete( void* aHit ) { aLumiHitAllocator->FreeSingle( (LumiHit*)aHit ); }
