/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HitDataStructure_h
#define HitDataStructure_h 1

#include "globals.hh"

struct HitData {
  G4int    TrackID;
  G4int    ParentID;
  G4int    PdgID;
  G4int    StationID;
  G4double Energy;
  G4double Time;
  G4double X;
  G4double Y;
  G4double Z;
  G4double Px;
  G4double Py;
  G4double Pz;
  G4double Momentum;
};

#endif
