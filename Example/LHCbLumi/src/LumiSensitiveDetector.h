/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "G4VSensitiveDetector.hh"
#include "GiGaMTCoreMessage/IGiGaMessage.h"
#include "HitDataStructure.hh"
#include "LumiHit.h"

class G4HCofThisEvent;
class G4Step;
class G4TouchableHistory;

class LumiSensitiveDetector : public G4VSensitiveDetector, public virtual GiGaMessage {
public:
  LumiSensitiveDetector( G4String name );
  virtual ~LumiSensitiveDetector();

  void Initialize( G4HCofThisEvent* ) override;

  G4bool ProcessHits( G4Step*, G4TouchableHistory* ) override;

  G4bool ProcessHitsL( G4Step*, G4TouchableHistory* );

private:
  LumiHitsCollection* _Collection{ nullptr };
  LumiHit*            _theHit{ nullptr };
};
