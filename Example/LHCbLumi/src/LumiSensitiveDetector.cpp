/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * LumiSensitiveDetector.cpp
 *
 *  Created on: Oct 3, 2018
 *      Author: vsevolod
 */

#include "LumiSensitiveDetector.h"

#include "G4HCofThisEvent.hh"
#include "G4PhysicalConstants.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"

#include <iterator>

LumiSensitiveDetector::LumiSensitiveDetector( G4String name )
    : // @suppress("Class members should be properly initialized")
    G4VSensitiveDetector( name ) {
  //  G4RunManager* runManager = G4RunManager::GetRunManager();
  collectionName.insert( "Hits" );
}

LumiSensitiveDetector::~LumiSensitiveDetector() {}

void LumiSensitiveDetector::Initialize( G4HCofThisEvent* HCE ) {
  _Collection = new LumiHitsCollection( SensitiveDetectorName, collectionName[0] );

  std::string hit_location = SensitiveDetectorName + "/" + collectionName[0];

  debug( "Registering location at " + hit_location );
  int HCID = G4SDManager::GetSDMpointer()->GetCollectionID( hit_location );

  HCE->AddHitsCollection( HCID, _Collection );

  _theHit = new LumiHit();
  _Collection->insert( _theHit );
}

G4bool LumiSensitiveDetector::ProcessHitsL( G4Step* aStep, G4TouchableHistory* hist ) {
  return ProcessHits( aStep, hist );
}

G4bool LumiSensitiveDetector::ProcessHits( G4Step* aStep, G4TouchableHistory* ) {
  G4Track*      aTrack         = aStep->GetTrack();
  G4ThreeVector globalPosition = aStep->GetPostStepPoint()->GetPosition();
  //	newHit->myData;

  G4StepPoint* aPostPoint = aStep->GetPostStepPoint();
  G4StepPoint* aPrevPoint = aStep->GetPreStepPoint();
  if ( !aPostPoint->GetPhysicalVolume() ) return false;

  G4LogicalVolume* PostVolume = aPostPoint->GetPhysicalVolume()->GetLogicalVolume();
  G4LogicalVolume* PrevVolume = aPrevPoint->GetPhysicalVolume()->GetLogicalVolume();

  G4String PreName  = PrevVolume->GetName();
  G4String PostName = PostVolume->GetName();

  const G4DynamicParticle* aParticle = aTrack->GetDynamicParticle();
  if ( printDebug() ) {
    std::stringstream sstr;
    sstr << "SD hit by " << aParticle->GetDefinition()->GetParticleName();
    debug( sstr.str() );
  }

  // Handling only optical photons
  if ( aParticle->GetDefinition()->GetParticleName() != "opticalphoton" ) return false;

  // Vectors of sector's and detector's names splitted into words
  std::vector<G4String> sectorWords;
  std::vector<G4String> detectorWords;

  auto splitName = []( auto& str, auto& cont ) {
    std::istringstream iss( str );
    std::copy( std::istream_iterator<std::string>( iss ), std::istream_iterator<std::string>(),
               std::back_inserter( cont ) );
  };

  // Splitting a string into words
  splitName( PreName, sectorWords );
  splitName( PostName, detectorWords );
  if ( printDebug() ) {
    std::stringstream sstr;
    sstr << "sectorWords " << sectorWords[0];
    debug( sstr.str() );
    sstr.clear();
    sstr << "detectorWords " << detectorWords[0] << " -> " << detectorWords[0];
    debug( sstr.str() );
  }

  // Sector ID discrimination for the hit
  if ( sectorWords[0] == "sector" && detectorWords[0] == "detector" ) {
    G4int stationID = atoi( detectorWords[2] );
    _theHit->InsertPhoton( stationID );
  } else
    return false;

  return true;
}

#include "GiGaMTDetFactories/GiGaMTG4SensDetFactory.h"

// As long we do not have any additional
typedef GiGaMTG4SensDetFactory<LumiSensitiveDetector> LumiSensitiveDetectorFAC;

DECLARE_COMPONENT_WITH_ID( LumiSensitiveDetectorFAC, "LumiSensitiveDetector" )
