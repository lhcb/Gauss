/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "G4AssemblyVolume.hh"
#include "G4Box.hh"
#include "G4Color.hh"
#include "G4ExtrudedSolid.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4MultiUnion.hh"
#include "G4NistManager.hh"
#include "G4OpticalSurface.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4Trap.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4TwoVector.hh"
#include "G4UnionSolid.hh"
#include "G4VSolid.hh"
#include "G4VisAttributes.hh"
#include "GaudiAlg/GaudiTool.h"
#include "SimInterfaces/IGaussinoTool.h"
#include "globals.hh"

#include "LConst.hh"

#include "LumiVolumeStructures.h"

class AddLumiDetectorTool : public extends<GaudiTool, IGaussinoTool> {
public:
  using extends::extends;

public:
  StatusCode initialize() override;
  StatusCode process( const std::string& parameter = "" ) const override;

private:
  Gaudi::Property<std::string> m_worldName{ this, "WorldName", "World" };
  G4Material*                  worldMaterial;
  G4Material*                  ScintMaterial;
  G4Material*                  Vacuum;
  G4Material*                  BPMaterial;
  G4Material*                  INOX;
  G4Material*                  SiO2;
  G4Material*                  Copper;
  G4Material*                  Beryllium;

  G4Material* Air;

  TrapezeSectorStructIn  sectorIn;
  TrapezeSectorStructOut sectorOut;
  TrapezeAbsStructIn     absorberIn;
  TrapezeAbsStructOut    absorberOut;
};
