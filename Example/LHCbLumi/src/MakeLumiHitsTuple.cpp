/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from Geant4
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"

// local
#include "LumiHit.h"
#include "MakeLumiHitsTuple.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MakeLumiHitsTuple
//
// 2018-04-05 : Dominik Muller
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MakeLumiHitsTuple )

//=============================================================================
// Initialization
//=============================================================================
StatusCode MakeLumiHitsTuple::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  if ( "" == m_colName ) {
    fatal() << "Property CollectionName need to be set! " << endmsg;
    return StatusCode::FAILURE;
  }
  m_file = new TFile( "LumiHits.root", "RECREATE" );
  m_tree = new TTree( "Tree", "Tree" );
  _nSec  = LConst::nSecOut;
  m_tree->Branch( "nSec", &_nSec, "nSec/I" );
  m_tree->Branch( "nPhot", _nPhot, "nPhot[nSec]/I" );
  m_tree->SetDirectory( m_file );
  m_tree->Write();

  return StatusCode::SUCCESS;
}

StatusCode MakeLumiHitsTuple::finalize() {
  m_file->cd();
  m_tree->SetDirectory( m_file );
  m_tree->Write();
  m_file->Close();

  return Consumer::finalize(); // must be executed first
}

//=============================================================================
// Main execution
//=============================================================================
void MakeLumiHitsTuple::operator()( const G4EventProxies& eventproxies, const LinkedParticleMCParticleLinks& ) const {
  debug() << "==> Execute" << endmsg;
  std::lock_guard locked{ m_lazy_lock };
  m_tree->SetDirectory( m_file );

  for ( int idx = 0; idx < LConst::nSecOut; idx++ ) {
    // FIXME: + 1 here necessary?
    _nPhot[idx] = 0;
  }
  for ( auto& ep : eventproxies ) {
    auto hitCollection = ep->GetHitCollection<LumiHitsCollection>( m_colName );
    if ( !hitCollection ) {
      warning() << "The hit collection='" + m_colName + "' is not found!" << endmsg;
      continue;
    }
    // reserve elements on output container
    int numOfHits = hitCollection->entries();
    if ( numOfHits != 1 ) {
      error() << "Number of LumiHits for G4Event is not exactly one!" << endmsg;
      continue;
    }
    auto hit = ( *hitCollection )[0];
    for ( int idx = 0; idx < LConst::nSecOut; idx++ ) {
      // FIXME: + 1 here necessary?
      _nPhot[idx] += hit->GetNPhotons( idx + 1 );
    }
  }
  m_file->cd();
  m_tree->SetDirectory( m_file );
  m_tree->Fill();
}
