/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <vector>

// Include files
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GiGaMTCoreRun/G4EventProxy.h"
#include "GiGaMTCoreRun/MCTruthConverter.h"
#include "MCTruthToEDM/LinkedParticleMCParticleLink.h"

#include "Defaults/Locations.h"

#include "LConst.hh"
#include "TFile.h"
#include "TTree.h"

// Forward declarations
class IGiGaSvc;
class IGiGaKineCnvSvc;
class DetectorElement;
class TrackerHit;

/** @class MakeLumiHitsTuple MakeLumiHitsTuple.h
 *
 *
 *  @author Gloria CORTI
 *  @date   2005-10-02
 */
class MakeLumiHitsTuple
    : public Gaudi::Functional::Consumer<void( const G4EventProxies&, const LinkedParticleMCParticleLinks& )> {
public:
  /// Standard constructor
  MakeLumiHitsTuple( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "Input", Gaussino::G4EventsLocation::Default },
                    KeyValue{ "LinkedParticleMCParticleLinks",
                              Gaussino::LinkedParticleMCParticleLinksLocation::Default } } ) {}
  virtual ~MakeLumiHitsTuple() = default;

  virtual void operator()( const G4EventProxies&, const LinkedParticleMCParticleLinks& ) const override;

  virtual StatusCode initialize() override; ///< Algorithm initialization
  virtual StatusCode finalize() override;   ///< Algorithm initialization

private:
  Gaudi::Property<std::string>              m_colName{ this, "CollectionName", "",
                                          "Name of Geant4 collection where to retrieve hits" };
  Gaudi::Property<std::vector<std::string>> m_detName{
      this,
      "Detectors",
      {},
      "List of detector paths in TDS for which to retrieve the hits (most of "
      "the time one" };
  mutable std::mutex m_lazy_lock;
  G4int              _nSec;
  mutable G4int      _nPhot[LConst::nSecOut];
  mutable TTree*     m_tree;
  mutable TFile*     m_file;
};
