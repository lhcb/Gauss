###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import CondDB, Gauss, LHCbApp
from Gaudi.Configuration import importOptions
from Gaussino.Generation import GenPhase

importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py")

CondDB().Upgrade = True
LHCbApp().DDDBtag = "dddb-20171010"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"

CondDB().Upgrade = True
Gauss().DataType = "Run3"

from Gauss.Geometry import LHCbGeo

LHCbGeo().DetectorGeo = {"Detectors": ["VP", "Magnet"]}
LHCbGeo().DetectorSim = {"Detectors": []}
LHCbGeo().DetectorMoni = {"Detectors": []}

LHCbGeo().ExtraGeoTools = ["AddLumiDetectorTool"]
LHCbGeo().SensDetMap = {
    "LumiSensitiveDetector/LSD": ["sector out {}".format(j + 1) for j in range(100)]
}

from Configurables import GiGaMT, GiGaMTDetectorConstructionFAC

giga = GiGaMT()
dettool = giga.addTool(GiGaMTDetectorConstructionFAC, "GiGaMTDetectorConstructionFAC")
dettool.Output = "Dump.gdml"

from Configurables import LumiSensitiveDetector

lsd = dettool.addTool(LumiSensitiveDetector, "LSD")
# giga.OutputLevel = 1

from Configurables import ApplicationMgr, MakeLumiHitsTuple

# ApplicationMgr().ExtSvc += ["NTupleSvc"];
# from Gaudi.Configuration import NTupleSvc
# NTupleSvc().Output = ["FILE1 DATAFILE='LumiHits.root' OPT='NEW'"];
ApplicationMgr().TopAlg += [MakeLumiHitsTuple(CollectionName="LSD/Hits")]

from Gauss import G4Physics

G4Physics().AddPhysConstr = ["GiGaMT_G4OpticalPhysics"]

from Configurables import Generation

Generation().EventType = 30000000

GenPhase().ProductionTool = "Pythia8Production"
GenPhase().SampleGenerationTool = "MinimumBias"
GenPhase().DecayTool = "EvtGenDecay"
GenPhase().CutTool = ""

from Configurables import EvtGenDecay, ToolSvc

ToolSvc().addTool(EvtGenDecay)
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/minbias.dec"


from Configurables import Gauss

nthreads = 3

# GenPhase().Production = 'P8MB'
GenPhase().GenMonitor = True
Gauss().EvtMax = 100
Gauss().EnableHive = True

Gauss().ThreadPoolSize = nthreads
Gauss().EventSlots = nthreads
Gauss().ConvertEDM = True

from Configurables import GenRndInit

GenRndInit().FirstEventNumber = 10042
GenRndInit().TimingSkipAtStart = 10

from Configurables import GiGaMT

giga = GiGaMT()
giga.NumberOfWorkerThreads = nthreads

from Gaudi.Configuration import appendPostConfigAction


def force_engine():
    from Configurables import GiGaMT
    from Gaussino.Utilities import get_set_configurable

    giga = GiGaMT()
    actioninit = get_set_configurable(giga, "ActionInitializer")
    actioninit.TrackingActions += ["DebugTrackAction"]


# appendPostConfigAction(force_engine)
