/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gauss
#include "CaloMonitor.h"

// Run2Support
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/ConditionAccessorHolder.h"

namespace Gauss::Calo {
  using DetDescBase = LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeCalorimeter>;

  namespace Ecal::DetDesc {
    using MonitorBase =
        Calo::Monitor<DetDescBase, DeCalorimeter, LHCb::MCCaloHitLocation::Ecal, DeCalorimeterLocation::Ecal>;
    struct Monitor : public MonitorBase {
      using MonitorBase::Monitor;
      virtual std::string detectorName() const override { return "ECAL"; }
    };
    DECLARE_COMPONENT_WITH_ID( Monitor, "EcalMonitorDetDesc" )
  } // namespace Ecal::DetDesc

  namespace Hcal::DetDesc {
    using MonitorBase =
        Calo::Monitor<DetDescBase, DeCalorimeter, LHCb::MCCaloHitLocation::Hcal, DeCalorimeterLocation::Hcal>;
    struct Monitor : public MonitorBase {
      using MonitorBase::Monitor;
      virtual std::string detectorName() const override { return "HCAL"; }
    };
    DECLARE_COMPONENT_WITH_ID( Monitor, "HcalMonitorDetDesc" )
  } // namespace Hcal::DetDesc

  namespace Spd::DetDesc {
    using MonitorBase =
        Calo::Monitor<DetDescBase, DeCalorimeter, LHCb::MCCaloHitLocation::Spd, DeCalorimeterLocation::Spd>;
    struct Monitor : public MonitorBase {
      using MonitorBase::Monitor;
      virtual std::string detectorName() const override { return "SPD"; }
    };
    DECLARE_COMPONENT_WITH_ID( Monitor, "SpdMonitorDetDesc" )
  } // namespace Spd::DetDesc

  namespace Prs::DetDesc {
    using MonitorBase =
        Calo::Monitor<DetDescBase, DeCalorimeter, LHCb::MCCaloHitLocation::Prs, DeCalorimeterLocation::Prs>;
    struct Monitor : public MonitorBase {
      using MonitorBase::Monitor;
      virtual std::string detectorName() const override { return "PRS"; }
    };
    DECLARE_COMPONENT_WITH_ID( Monitor, "PrsMonitorDetDesc" )
  } // namespace Prs::DetDesc

} // namespace Gauss::Calo
