/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifdef USE_DD4HEP
// Gauss
#  include "CaloMonitor.h"

// Detector
#  include "Detector/Calo/DeCalorimeter.h"
#  include "LbDD4hep/ConditionAccessorHolder.h"

namespace Gauss::Calo {
  using DD4hepBase = LHCb::Det::LbDD4hep::usesBaseAndConditions<GaudiHistoAlg, LHCb::Detector::Calo::DeCalorimeter>;

  namespace Ecal::DD4hep {
    using MonitorBase = Calo::Monitor<DD4hepBase, LHCb::Detector::Calo::DeCalorimeter, LHCb::MCCaloHitLocation::Ecal,
                                      LHCb::Detector::Calo::DeCalorimeterLocation::Ecal>;
    struct Monitor : public MonitorBase {
      using MonitorBase::Monitor;
      virtual std::string detectorName() const override { return "ECAL"; }
    };
    DECLARE_COMPONENT_WITH_ID( Monitor, "EcalMonitorDD4hep" )
  } // namespace Ecal::DD4hep

  namespace Hcal::DD4hep {
    using MonitorBase = Calo::Monitor<DD4hepBase, LHCb::Detector::Calo::DeCalorimeter, LHCb::MCCaloHitLocation::Hcal,
                                      LHCb::Detector::Calo::DeCalorimeterLocation::Hcal>;
    struct Monitor : public MonitorBase {
      using MonitorBase::Monitor;
      virtual std::string detectorName() const override { return "HCAL"; }
    };
    DECLARE_COMPONENT_WITH_ID( Monitor, "HcalMonitorDD4hep" )
  } // namespace Hcal::DD4hep
} // namespace Gauss::Calo
#endif
