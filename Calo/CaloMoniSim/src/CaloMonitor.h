/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// LHCb
#include "Event/MCCaloHit.h"

// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"

// AIDA
#include "AIDA/IHistogram1D.h"

/** @class MCCaloMonitor MCCaloMonitor.h
 *
 * This class declares all the parameters from MCCaloMonitor.cpp, that
 * obtain MCCaloDigit hits and monitorize them in histograms
 *
 *  @author Carlos Gonzalez
 *  @date   24-11-2003
 *  @date   01-03-2023 (updated by M. Mazurek to adapt to Gaussino)
 */

namespace Gauss::Calo {

  template <class TBase, class TDeCalo>
  using Consumer = Gaudi::Functional::Consumer<void( const LHCb::MCCaloHits&, const TDeCalo& ), TBase>;

  template <class TBase, class TDeCalo, const std::string& CaloHitsLocation, const std::string& DeCaloLocation>
  struct Monitor : public Consumer<TBase, TDeCalo> {
    using KeyValue = class Consumer<TBase, TDeCalo>::KeyValue;

    Monitor( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer<TBase, TDeCalo>(
              name, pSvcLocator,
              { KeyValue{ "MCCaloHits", CaloHitsLocation }, KeyValue{ "DeCalorimeter", DeCaloLocation } } ){};

    StatusCode          initialize() override;
    StatusCode          finalize() override;
    virtual std::string detectorName() const = 0;

    void operator()( const LHCb::MCCaloHits&, const TDeCalo& ) const override;

  private:
    // histograms:
    AIDA::IHistogram1D* m_hName1{ nullptr };
    AIDA::IHistogram1D* m_hName1a{ nullptr };
    AIDA::IHistogram1D* m_hName1b{ nullptr };
    AIDA::IHistogram1D* m_hName1c{ nullptr };
    AIDA::IHistogram1D* m_hName2{ nullptr };
    AIDA::IHistogram1D* m_hName2a{ nullptr };
    AIDA::IHistogram1D* m_hName2b{ nullptr };
    AIDA::IHistogram1D* m_hName2c{ nullptr };
    AIDA::IHistogram1D* m_hName3{ nullptr };
    AIDA::IHistogram1D* m_hName3a{ nullptr };
    AIDA::IHistogram1D* m_hName3b{ nullptr };
    AIDA::IHistogram1D* m_hName3c{ nullptr };
    AIDA::IHistogram1D* m_hName4{ nullptr };
    AIDA::IHistogram1D* m_hName40{ nullptr };
    AIDA::IHistogram1D* m_hName41{ nullptr };
    AIDA::IHistogram1D* m_hName42{ nullptr };
    AIDA::IHistogram1D* m_hName43{ nullptr };
    AIDA::IHistogram1D* m_hName44{ nullptr };
    AIDA::IHistogram1D* m_hName45{ nullptr };

    Gaudi::Property<bool>   m_DivMonitor{ this, "Regions", false };
    Gaudi::Property<double> m_MaxE{ this, "MaximumEnergy", 10 * Gaudi::Units::MeV };
    Gaudi::Property<double> m_MinE{ this, "MinimumEnergy", 0 * Gaudi::Units::MeV };
    Gaudi::Property<double> m_Threshold{ this, "Threshold", 1.5 * Gaudi::Units::MeV };
    Gaudi::Property<int>    m_MaxT{ this, "MaximumTime", 7 };
    Gaudi::Property<int>    m_MinT{ this, "MinimumTime", -1 };

    double             MinT, MaxT;
    int                m_Bin;
    std::string        m_hDir;
    mutable std::mutex m_histo_mutex;

    void bookHistograms();
  };
} // namespace Gauss::Calo

template <class TBase, class TDeCalo, const std::string& CaloHitsLocation, const std::string& DeCaloLocation>
StatusCode Gauss::Calo::Monitor<TBase, TDeCalo, CaloHitsLocation, DeCaloLocation>::initialize() {
  return Consumer<TBase, TDeCalo>::initialize().andThen( [&]() -> StatusCode {
    this->info() << "==> Initialise Monitoring " << detectorName() << endmsg;
    // Histograms
    m_Bin = int( m_MaxT - m_MinT + 1 );
    MinT  = 1. * m_MinT - 0.5;
    MaxT  = 1. * m_MaxT + 0.501;
    bookHistograms();
    return StatusCode::SUCCESS;
  } );
}

template <class TBase, class TDeCalo, const std::string& CaloHitsLocation, const std::string& DeCaloLocation>
void Gauss::Calo::Monitor<TBase, TDeCalo, CaloHitsLocation, DeCaloLocation>::operator()( const LHCb::MCCaloHits& hits,
                                                                                         const TDeCalo& calo ) const {
  this->debug() << "Execute Monitoring " << detectorName() << endmsg;

  int hits_Counter     = 0;
  int hits_CounterBC_1 = 0;
  int hits_CounterBC0  = 0;
  int hits_CounterBC1  = 0;
  int hits_CounterBC2  = 0;
  int hits_CounterBC3  = 0;
  int hits_CounterBC4  = 0;

  std::lock_guard<std::mutex> locker{ m_histo_mutex };
  for ( auto hit : hits ) {
    // NORMAL ENERGY SUBHITS HISTOGRAMS
    int binTime;
    if ( hit->time() >= 7 ) {
      binTime = 7;
    } else {
      binTime = hit->time();
    }
    LHCb::Detector::Calo::CellID ID   = hit->cellID();
    int                          zone = ID.area();

    m_hName1->fill( hit->activeE(), 1. );
    m_hName2->fill( hit->activeE(), hit->activeE() );
    m_hName3->fill( binTime, hit->activeE() );
    if ( m_DivMonitor.value() ) {
      if ( ( 2 == zone && detectorName() != "Hcal" ) || ( 1 == zone && detectorName() == "Hcal" ) ) {
        if ( calo.valid( ID ) ) {
          m_hName1a->fill( hit->activeE(), 1. );
          m_hName2a->fill( hit->activeE(), hit->activeE() );
          m_hName3a->fill( binTime, hit->activeE() );
        }
      }
      if ( ( 1 == zone && detectorName() != "Hcal" ) ) {
        if ( calo.valid( ID ) ) {
          m_hName1b->fill( hit->activeE(), 1. );
          m_hName2b->fill( hit->activeE(), hit->activeE() );
          m_hName3b->fill( binTime, hit->activeE() );
        }
      }
      if ( 0 == zone ) {
        if ( calo.valid( ID ) ) {
          m_hName1c->fill( hit->activeE(), 1. );
          m_hName2c->fill( hit->activeE(), hit->activeE() );
          m_hName3c->fill( binTime, hit->activeE() );
        }
      }
    }
    // BUNCH CROSSING HISTOGRAMS
    if ( hit->activeE() >= m_Threshold.value() ) {
      hits_Counter = hits_Counter + 1;
      switch ( hit->time() ) {
      case -1:
        ++hits_CounterBC_1;
        break;
      case 0:
        ++hits_CounterBC0;
        break;
      case 1:
        ++hits_CounterBC1;
        break;
      case 2:
        ++hits_CounterBC2;
        break;
      case 3:
        ++hits_CounterBC3;
        break;
      case 4:
        ++hits_CounterBC4;
        break;
      }
    }
  } // end for

  m_hName4->fill( hits_Counter, 1. );
  m_hName40->fill( hits_CounterBC_1, 1. );
  m_hName41->fill( hits_CounterBC0, 1. );
  m_hName42->fill( hits_CounterBC1, 1. );
  m_hName43->fill( hits_CounterBC2, 1. );
  m_hName44->fill( hits_CounterBC3, 1. );
  m_hName45->fill( hits_CounterBC4, 1. );
}

template <class TBase, class TDeCalo, const std::string& CaloHitsLocation, const std::string& DeCaloLocation>
StatusCode Gauss::Calo::Monitor<TBase, TDeCalo, CaloHitsLocation, DeCaloLocation>::finalize() {
  this->info() << "Finalize Monitoring " << detectorName() << endmsg;
  return Consumer<TBase, TDeCalo>::finalize();
}

template <class TBase, class TDeCalo, const std::string& CaloHitsLocation, const std::string& DeCaloLocation>
void Gauss::Calo::Monitor<TBase, TDeCalo, CaloHitsLocation, DeCaloLocation>::bookHistograms() {
  std::string tmp_hName1  = "Subhits in the " + detectorName();
  std::string tmp_hName1a = "Subhits in the INNER " + detectorName();
  std::string tmp_hName1b = "Subhits in the MIDDLE " + detectorName();
  std::string tmp_hName1c = "Subhits in the OUTER " + detectorName();
  std::string tmp_hName2  = "Energy Weighted Subhits in the " + detectorName();
  std::string tmp_hName2a = "Energy Weighted Subhits in the INNER " + detectorName();
  std::string tmp_hName2b = "Energy Weighted Subhits in the MIDDLE " + detectorName();
  std::string tmp_hName2c = "Energy Weighted Subhits in the OUTER" + detectorName();
  std::string tmp_hName3  = "Accumulated Energy in the " + detectorName();
  std::string tmp_hName3a = "Accumulated Energy in the INNER " + detectorName();
  std::string tmp_hName3b = "Accumulated Energy in the MIDDLE " + detectorName();
  std::string tmp_hName3c = "Accumulated Energy in the OUTER " + detectorName();
  std::string tmp_hName4  = "Number of Subhits in the " + detectorName();
  std::string tmp_hName40 = "Number of Subhits in the " + detectorName() + " ( BC = -1 )";
  std::string tmp_hName41 = "Number of Subhits in the " + detectorName() + " ( BC = 0  )";
  std::string tmp_hName42 = "Number of Subhits in the " + detectorName() + " ( BC = 1  )";
  std::string tmp_hName43 = "Number of Subhits in the " + detectorName() + " ( BC = 2  )";
  std::string tmp_hName44 = "Number of Subhits in the " + detectorName() + " ( BC = 3  )";
  std::string tmp_hName45 = "Number of Subhits in the " + detectorName() + " ( BC = 4  )";

  m_hName1 = this->book1D( 11, tmp_hName1, m_MinE, m_MaxE, 100 );
  m_hName2 = this->book1D( 12, tmp_hName2, m_MinE, m_MaxE, 100 );
  m_hName3 = this->book1D( 13, tmp_hName3, MinT, MaxT, m_Bin );

  if ( m_DivMonitor.value() ) {
    m_hName1a = this->book1D( 111, tmp_hName1a, m_MinE, m_MaxE, 100 );
    m_hName1b = this->book1D( 112, tmp_hName1b, m_MinE, m_MaxE, 100 );
    m_hName1c = this->book1D( 113, tmp_hName1c, m_MinE, m_MaxE, 100 );
    m_hName2a = this->book1D( 121, tmp_hName2a, m_MinE, m_MaxE, 100 );
    m_hName2b = this->book1D( 122, tmp_hName2b, m_MinE, m_MaxE, 100 );
    m_hName2c = this->book1D( 123, tmp_hName2c, m_MinE, m_MaxE, 100 );
    m_hName3a = this->book1D( 131, tmp_hName3a, MinT, MaxT, m_Bin );
    m_hName3b = this->book1D( 132, tmp_hName3b, MinT, MaxT, m_Bin );
    m_hName3c = this->book1D( 133, tmp_hName3c, MinT, MaxT, m_Bin );
  }
  m_hName4  = this->book1D( 14, tmp_hName4, 0, 1000, 100 );
  m_hName40 = this->book1D( 140, tmp_hName40, 0, 1000, 100 );
  m_hName41 = this->book1D( 141, tmp_hName41, 0, 1000, 100 );
  m_hName42 = this->book1D( 142, tmp_hName42, 0, 1000, 100 );
  m_hName43 = this->book1D( 143, tmp_hName43, 0, 1000, 100 );
  m_hName44 = this->book1D( 144, tmp_hName44, 0, 1000, 100 );
  m_hName45 = this->book1D( 145, tmp_hName45, 0, 1000, 100 );
}
